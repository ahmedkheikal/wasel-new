<?php
use App\Services\WalletService;
use App\Services\PaymentService;
use App\Services\NotificationService;
use App\User;
use App\Models\Reservation;
use Illuminate\Support\Facades\Response;

Route::get('/test', function() {
    \App\MyHelper::incitySelectDate();
});

Route::get('/', 'HomeController@index');

// Route::get('/update-captain-wallets', function()
// {
//     $trips = App\Models\Accounting::where('status', 'settled')
//         ->with('captain', 'trip1', 'trip2')
//         ->get();
//     foreach ($trips as $trip) {

//         $description = ''. -$trip->net_revenue .' ج.م تسوية حساب رحلة '. $trip->trip1->from_city .' إلى '. $trip->trip1->to_city .' يوم '
//             . (new \DateTime($trip->trip1->date))->format('d M Y') .' الساعة '
//             . (new \DateTime($trip->trip1->departue_time))->format('h:i A');
//         if ($trip->trip2 != null)
//
//         WalletService::addToWallet($trip->captain, -$trip->net_revenue, 'own_balance', 'Accounting:' . $trip->id, $description);
//     }
// });
Route::get('/passenger-email', function()
{
    return view('passenger-email');
});
Auth::routes();
Route::get('/register/{referalCode}', 'Auth\RegisterController@showRegistrationFormAffiliate');
Route::get('/invitation-accepted', 'AccountController@invitationAccepted');

Route::group(['prefix' => 'password'], function()
{
    Route::post('/send-reset-code', 'Auth\ResetPasswordController@sendResetCode');
    Route::get('/code', 'Auth\ResetPasswordController@passwordCode');
    Route::post('/code', 'Auth\ResetPasswordController@passwordCodeVerify');
    Route::get('/new', 'Auth\ResetPasswordController@passwordNew');
    Route::post('/new', 'Auth\ResetPasswordController@passwordNewPost');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about');
Route::get('/terms-and-conditions', 'HomeController@termsAndConds');

Route::group(['middleware' => 'CustomAuth'], function()
{
    Route::group(['prefix' => 'phone', 'middleware' => 'PhoneNotVerifiedOnly'], function()
    {
        Route::post('/send-code', 'AuthController@sendVerificationCode');
        Route::get('/verify', 'AuthController@verifyPhone');
        Route::post('/verify', 'AuthController@verifyPhonePost');
        Route::get('/update', 'AuthController@updatePhone');
        Route::get('/code', 'Auth\ResetPasswordController@passwordCode');
    });

    Route::group(['middleware' => 'PhoneVerifiedOnly'], function()
    {
        Route::get('/wallet', 'AccountController@wallet');
        Route::get('/invite-friends', 'AccountController@affiliateShare');
        Route::get('/confirm-economy', 'ReservationController@confirmEconomyReservation');
    });

    Route::get('/reservaion-Successful', 'ReservationController@reservationSuccessful');
    Route::get('/private-reservaion-Successful', 'TripController@privateReservationSuccessful');
    Route::get('/my-trips', 'TripController@myTrips');
    Route::get('/my-incity-trips', 'InCityController@myTrips');
    Route::get('/shipping-orders', 'HomeController@myShippingOrders');
    Route::get('/my-trips/reservation/{id}', 'TripController@singleEcoTrip');
    Route::get('/add-economy-trip', 'TripController@captainAddEconomyRange');
    Route::get('/private-trips', 'TripController@myPrivateTrips');
    Route::get('/private-trips/request/{id}', 'TripController@singlePrivateTrip');
    Route::get('/private-trips/{id}', 'TripController@simglePrivateTrip');
    Route::get('/account-settings', 'AccountController@accountSettings');
    Route::post('/cancel-reservation', 'ReservationController@cancel');
    Route::post('/cancel-private-request', 'TripController@cancelPrivate');
    Route::post('/update-account-info', 'AccountController@updataInfo');
    Route::post('/upload-profile-picture', 'AccountController@updateProfilePic');
    Route::post('/upload-driving-license', 'AccountController@updateDrivingLicense');
    Route::post('/upload-id-card', 'AccountController@updateIdCard');
    Route::post('/update-password', 'AccountController@updatePassword');
    Route::post('/add-new-economy-trip', 'TripController@captainAddNewEcoTrip');
    Route::post('/add-available-travel-range', 'TripController@captainAddAvailableRange')->name('captainAddAvailableRange');
    Route::post('/cancel-economy-trip', 'TripController@cancelEconomyTrip');


    Route::group(['prefix' => 'feedback'], function()
    {
        Route::post('/send-economy', 'ReservationController@sendFeedback');
        Route::post('/send-private', 'TripController@sendFeedbackPrivate');
    });
});

// wasel express
Route::group(['prefix' => 'express'], function()
{
    Route::get('/', 'HomeController@waselExpress');
    Route::get('/terms-and-conditions', 'HomeController@shippingTermsAndConds');
    Route::post('/add', 'HomeController@expressAdd');
    Route::post('/cost', 'HomeController@expressCost');
    Route::get('/success', 'HomeController@expressSuccess');
});


/**
 * InCity Trip Routes
 */
Route::group(['prefix' => 'incity'], function() {
  Route::get('/', 'InCityController@index');
  Route::post('/add', 'InCityController@add');
  Route::get('/success', 'InCityController@success');
});

/**
 * Private Trip Routes
 */
Route::group(['prefix' => 'private-trip', 'namespace' => 'Trips'], function() {
  Route::get('/', 'PrivateTripsController@create');
  /*Route::post('/add', 'PrivateTripsController@store');
  Route::get('/success', 'PrivateTripsController@success');*/
});

/**
 * Economy Trip Routes
 */
Route::group(['prefix' => 'economy-trip', 'namespace' => 'Trips'], function() {
//   Route::get('/', 'EconomyTripsController@create');
  /*Route::post('/add', 'EconomyTripsController@store');
  Route::get('/success', 'EconomyTripsController@success');*/
});

Route::post('/confirm-economy', 'ReservationController@saveReservationData');
Route::post('add-trip', 'TripController@lookupDestinationPost');
Route::get('add-trip', 'TripController@lookupDestination');
Route::post('/add-private-trip', 'TripController@addPrivateTrip');
Route::post('/getAlltos', 'ApiTripController@getAlltos');


// ajax forms
Route::post('/reserve-economy-trip', 'ReservationController@reserveEconomy');
Route::post('/ajax-login', 'AuthController@ajaxLogin');
Route::post('/ajax-register', 'AuthController@ajaxRegister');
Route::get('/become-a-captain', 'AuthController@becomeACaptain');
Route::post('/become-a-captain', 'AuthController@becomeACaptainForm');


Route::get('PaymentService', function()
{
    $payment = new PaymentService([
        "first_name" =>"Ahmed",
        "last_name" => "Heikal",
        "email" => "ahmed.k.heikal@gmail.com",
        "phone" => "01025248181",
        "building" => "10",
        "street" => "El Nokrashy",
        "city" => "Minya",
        "amount_cents" => '250'
    ]);
    $payment->requestOrder();
    $payment_key = $payment->getPaymentKey();
    return view('payment-form', [
        'iframe_url' => "https://accept.paymobsolutions.com/api/acceptance/iframes/7083?payment_token={$payment_key}"
    ]);
});
Route::get('/vl-sms', function()
{
    $users = User::all();
    $i = 0;
    $arraydata = '';
    foreach ($users as $user) {
        if (strlen($user->phone) == 11) {
            NotificationService::sendSms($user->phone, 'اطلب عربية مخصوص حديثة مكيفة من المنيا لأى مركز داخل المحافظة وبين المحافظات بأسعار ثابتة، او اشحن مستنداتك الى القاهرة مع خدمة واصل اكسبريس الجديدة للشحن ب50 جنيه، حمّل الابلكيشن https://goo.gl/P2ekJ6');
        }
    }
    // $reservedFors = Reservation::whereNotNull('setfor_phone')
    //     ->get();
    // foreach ($reservedFors as $reservedFor) {
    //     if (strlen($reservedFor->setfor_phone) == 11) {
    //         $arraydata .= ++$i . ' setfor' . $reservedFor->setfor_phone .'<br>';
    //         // NotificationService::sendSms($reservedFor->setfor_phone, 'اطلب عربية مخصوص حديثة مكيفة من المنيا لأى مركز داخل المحافظة وبين المحافظات بأسعار ثابتة، او اشحن مستنداتك الى القاهرة مع خدمة واصل اكسبريس الجديدة للشحن ب50 جنيه، حمّل الابلكيشن https://goo.gl/P2ekJ6');
    //     }
    // }
    return dd('done');;
});



/**
 * Payment Routes
 */

Route::group(['prefix' => 'payment',
    'namespace' => 'Payment'], function() {

    Route::group(['middleware' => 'auth'], function() {
        // Make Payment with Fawry
        Route::get('fawry/pay/{PaymentData}', 'FawryController@pay')->name('fawry.pay');
    });

    // Fawry Payment callback
    Route::get('fawry/callback', 'FawryController@callback')->name('fawry.callback');
});

/**
 * Send the SMS Message and Email Confirmation of Economy Reservation
 * After the user pay the money by fawry
 */
Route::get('/economy-payment-confirmed', 'Economy\SMSsEmailsController@send')->name('economy-pay.send');

# For test send messages after the pay confirm in economy trip
// Route::get('/callback', 'Payment\FawryController@callback_test');
