<?php

use App\Services\PaymentService;
use App\Services\NotificationService;

Route::group(['namespace' => 'Api'], function() {
    # Auth Routes 
    Route::group(['prefix' => 'auth'], function() {
        Route::post('login', 'UserController@login');
    });
    
    # Routes With the guard api
    Route::group(['middleware' => 'auth:api'], function() {
        # Get the authenticated user data
        Route::get('auth/user', 'UserController@user');
        # The InCity Trips Routes
        Route::group(['prefix' => 'incity', 'middleware' => 'isDriver'], function() {
            
            /*# Get all trips with status "scheduled"
            Route::get('scheduled', 'InCityTripsController@scheduled');
            # Get all trips with status "canceled"
            Route::get('canceled', 'InCityTripsController@canceled');*/

            # Get all trips with status "pending"
            Route::get('pending', 'InCityTripsController@pending');
            # Accept an InCity Trip By Id
            Route::post('accept/{trip_id}', 'InCityTripsController@accept');
            # Get the InCity Trips that the driver accepted
            Route::get('mytrips', 'UserController@mytrips');
        });
    });
});

Route::post('/dest/economy-trips', 'ApiTripController@getEconomyTrips');
Route::group(['prefix' => 'economy'], function()
{
    Route::post('trip-seats', 'ApiTripController@economyTripInfo');
    Route::post('price', 'ApiTripController@calculateEconomyPrice');
    Route::post('/promo', 'ApiTripController@economyPromo');
});

Route::group(['prefix' => 'private'], function()
{
    Route::post('/regions', 'ApiTripController@privateCities');
    Route::post('/price', 'ApiTripController@privatePricing');
    Route::post('/promo', 'ApiTripController@privatePromo');
});

Route::group(['prefix' => 'incity'], function()
{
    Route::post('/getAllTos', 'InCityController@getAllTos');
    Route::post('/price', 'InCityController@price');
    Route::post('promo', 'InCityController@promo');
});

Route::group(['prefix' => 'affiliate'], function()
{
    Route::post('user', 'AffiliateController@user');
});


Route::post('/dest/from-city', 'ApiTripController@fromCityChange');
Route::post('/dest/to-city', 'ApiTripController@toCityChange');
Route::post('/getAlltos', 'ApiTripController@getAlltos');
Route::post('city-info', 'ApiTripController@cityInfo');

// onlly to skip csrf in App\Http\Controllers\AuthController::ajaxLogin()
Route::post('/reserve-economy-trip', 'ReservationController@reserveEconomy');

Route::post('/send/test-notification', 'TripController@testPushNotification');

Route::post('/captain/update-rating', 'ApiTripController@updateCaptainRating');

Route::post('send-push-notification', function()
{
    NotificationService::sendPushNotification(['$registrationIDs'], 'Notification From API', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
});

Route::group(['prefix' => 'express'], function()
{
    Route::post('/cost', 'HomeController@expressCost');
});
