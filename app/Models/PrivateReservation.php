<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Trip;
use App\Models\PrivateTripRequest;

class PrivateReservation extends Model
{
    protected $table = 'private_reservations_2308';

    protected $fillable = [
        'reservation_type',
        'setfor_name',
        'setfor_phone',
    ];

    public function trip()
    {
        return $this->hasOne(Trip::class, 'id', 'trip_id');
    }

    public function request()
    {
        return $this->belongsTo(PrivateTripRequest::class, 'request_id', 'id');
    }
}
