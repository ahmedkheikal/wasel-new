<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Trip;
use App\Models\Feedaback;
use App\User;

class Reservation extends Model
{
    protected $table = 'reservations_5248';

    protected $fillable = [
        'trip_id',
        'user_id',
        'seat_1',
        'seat_2',
        'seat_3',
        'reservation_date' ,
        'reservation_type' ,
        'setfor_name' ,
        'setfor_phone' ,
        'pickup_address' ,
        'arrival_address' ,
        'price',
        'promo_code',
    ];

    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function feedback()
    {
        return $this->hasOne(Feedaback::class, 'reservation_id', 'id');
    }

}
