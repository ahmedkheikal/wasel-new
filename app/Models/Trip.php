<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Reservation;
use App\Models\PrivateReservation;
use App\Models\CancellationRequest;
use Carbon\Carbon;

class Trip extends Model
{
    protected $table = 'available_trips_5394';

    protected $fillable = [
        'driver_id',
        'from_city',
        'to_city',
        'date',
        'departure_time',
        'datetime',
        'pickup_address',
        'arrival_address',
        'seat_1',
        'seat_2',
        'seat_3',
        'trip_status',
    ];

    public function captain()
    {
        return $this->hasOne(User::class, 'id', 'driver_id');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'trip_id', 'id')
                        ->where('reservation_status', 'live');
    }

    public function privateReservation()
    {
        return $this->belongsTo(PrivateReservation::class, 'id', 'trip_id');
    }

    public function tripsInTheSameDay()
    {
        return $this->hasMany('App\Models\Trip', 'date', 'date');
    }

    public function cancellationRequests()
    {
        return $this->hasMany(CancellationRequest::class, 'trip_id', 'id');
    }
}
