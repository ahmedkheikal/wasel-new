<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CancellationRequest extends Model
{
    protected $table = 'cancellation_requests_5354';
    protected $fillable = [
        'trip_id',
    ];
    //
}
