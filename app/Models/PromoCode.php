<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PromoCode extends Model
{
    protected $table = 'promo_codes_5278';
    protected $fillable = [
        'promo_code',
        'type',
        'amount',
        'from_city',
        'to_city',
        'code_status',
        'code_usage',
    ];
    public function Partner()
    {
        return $this->belongsTo(User::class, 'partner', 'id');
    }
}
