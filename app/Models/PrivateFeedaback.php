<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PrivateTripRequest;

class PrivateFeedaback extends Model
{
    protected $table = 'private_feedbacks_2548';

    protected $fillable = [
        'request_id',
        'timing',
        'behaviour',
        'safety',
        'cleanliness',
        'pricing_commitment',
        'pickup_dropoff_commitment',
        'overall_rating',
        'notes',
    ];

    public function request()
    {
        return $this->belongsTo(PrivateTripRequest::class, 'request_id', 'id');
    }
}
