<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Region;
use Illuminate\Support\Facades\Lang;

class City extends Model
{
    protected $table = 'cities_5982';
    //
    public function regions()
    {
        return $this->hasMany(Region::class)->orderBy('order');
    }

    // public function getCityNameAttribute($value)
    // {
    //     return Lang::get('db.' . $value);
    // }

}
