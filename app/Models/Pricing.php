<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\City;

class Pricing extends Model
{
    protected $table = 'pricing_5238';

    public function fromCity()
    {
        return $this->hasOne(City::class, 'city_name', 'from_city');
    }

    public function toCity()
    {
        return $this->hasOne(City::class, 'city_name', 'to_city');
    }
}
