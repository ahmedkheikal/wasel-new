<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Region;

class InCityPromoCode extends Model
{
    protected $table = 'incity_promo_codes_3252';


    public function from()
    {
        return $this->belongsTo(Region::class, 'from_region', 'id');
    }

    public function to()
    {
        return $this->belongsTo(Region::class, 'to_region', 'id');
    }
}
