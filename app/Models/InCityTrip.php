<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Region;

class InCityTrip extends Model
{
    protected $table = 'incity_trips_2548';

    protected $fillable = [
        'from_region',
        'to_region',
        'from_address',
        'to_address',
        'datetime',
        'setfor_name',
        'setfor_phone',
        'price',
        'client_id',
        'driver_id',
        'request_type',
        'trip_type'
    ];

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id', 'id');
    }

    public function captain()
    {
        return $this->belongsTo(User::class, 'driver_id', 'id');
    }

    public function from()
    {
        return $this->hasOne(Region::class, 'id', 'from_region');
    }
    public function to()
    {
        return $this->hasOne(Region::class, 'id', 'to_region');
    }
}
