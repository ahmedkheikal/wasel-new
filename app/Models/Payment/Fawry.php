<?php

namespace App\Models\Payment;

use App\Models\Trip;        # Economy Trip
use App\Models\InCityTrip;  # In City Trip

use Illuminate\Database\Eloquent\Model;

class Fawry extends Model
{

	/**
	 * The Model table name
	 *
	 * @var string
	 */
	protected $table = 'fawry_payment';

    /**
     * The Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
    	'user_id', 'trip_id', 'trip_type', 'status', 'ref_code'
    ];

    /**
     * Get the user for the trip
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function user()
    {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * Get the trip
     *
     * @return \Illiminate\Database\Eloquent\Collection
     */
    public function trip()
    {
    	if ($this->trip_type == 'economy') {
            return Trip::findOrFail($this->trip_id);
        } elseif ($this->trip_type == 'incity') {
            return InCityTrip::findOrFail($this->trip_id);
        }
    }
}
