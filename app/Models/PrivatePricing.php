<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\City;

class PrivatePricing extends Model
{
    //
    protected $table = 'private_pricing_4592';

    public function fromCity()
    {
        return $this->hasOne(City::class, 'id', 'from_city');
    }

    public function toCity()
    {
        return $this->hasOne(City::class, 'id', 'to_city');
    }
}
