<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class WalletTransaction extends Model
{
    protected $table = 'wallet_transactions_5896';
    protected $fillable = [
        'user',
        'amount',
        'balance_channel',
        'subject',
        'description',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user', 'id');
    }
}
