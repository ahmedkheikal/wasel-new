<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PrivatePromoCode extends Model
{
    protected $table = 'private_promo_codes_3564';
    protected $fillable = [
        'promo_code',
        'type',
        'amount',
        'from_city',
        'to_city',
        'code_status',
        'code_usage',
    ];
    public function Partner()
    {
        return $this->belongsTo(User ::class, 'partner', 'id');
    }
}
