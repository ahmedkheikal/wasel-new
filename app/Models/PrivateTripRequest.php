<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Trip;
use App\Models\PrivateFeedaback;
use App\Models\PrivateReservation;

class PrivateTripRequest extends Model
{
    protected $table = 'private_trip_requests_2358';

    protected $fillable = [
        'passenger_id',
        'round_request_id',
        'fullname',
        'phone',
        'email',
        'from_city',
        'from_city_address',
        'to_city',
        'to_city_address',
        'date',
        'departure_time',
        'pickup_address',
        'arrival_address',
        'request_status',
        'request_type',
        'passenger_notes',
        'trip_type',
        'datetime',
        'used_promo',
        'price',
    ];

    public function passenger()
    {
        return $this->belongsTo(User::class, 'passenger_id', 'id');
    }

    public function reservation()
    {
        return $this->hasOne(PrivateReservation::class, 'request_id', 'id');
    }

    public function trip()
    {
        return $this->belongsToMany(Trip::class, 'private_reservations_2308', 'id', 'trip_id', 'request_id', 'id');
    }
    public function feedback()
    {
        return $this->hasOne(PrivateFeedaback::class, 'request_id', 'id');
    }

}
