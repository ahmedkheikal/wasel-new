<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Reservation;

class Feedaback extends Model
{
    protected $table = 'feedbacks_2548';

    protected $fillable = [
        'reservation_id',
        'timing',
        'behaviour',
        'safety',
        'cleanliness',
        'pricing_commitment',
        'pickup_dropoff_commitment',
        'overall_rating',
        'notes',
    ];

    public function reservation()
    {
        return $this->belongsTo(Reservation::class, 'reservation_id', 'id');
    }
}
