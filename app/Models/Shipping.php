<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Shipping extends Model
{
    protected $table = 'shipping_8345';

    protected $fillable = [
        'client_id',
        'driver_id',
        'receiver_name',
        'receiver_phone',
        'datetime',
        'from_city',
        'from_city_address',
        'pickup_address',
        'to_city',
        'to_city_address',
        'arrival_address',
        'order_status',
        'order_type',
        'weight',
        'dimensions',
        'price',
        'other_description',
        'order_description'
    ];

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id', 'id');
    }
    public function captain()
    {
        return $this->belongsTo(User::class, 'driver_id', 'id');
    }
}
