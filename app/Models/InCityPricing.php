<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Region;

class InCityPricing extends Model
{
    protected $table = 'incity_pricing_2548';

    protected $fillable = [
        'from_region',
        'to_region',
        'trip_type',
        'price',
    ];

    public function from()
    {
        return $this->belongsTo(Region::class, 'from_region', 'id');
    }

    public function to()
    {
        return $this->belongsTo(Region::class, 'to_region', 'id');
    }
}
