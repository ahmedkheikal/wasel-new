<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\City;

class Region extends Model
{
    protected $table = 'regions_8562';

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
}
