<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Trip;

class Accounting extends Model
{
    protected $table = 'accounting_9642';

    protected $fillable = [
        'captain_id',
        'trip_1',
        'trip_2',
        'captain_cash',
        'tatal_cash',
        'net_revenue',
        'status',
    ];

    public function captain() {
        return $this->belongsTo(User::class, 'captain_id', 'id');
    }
    public function trip1()
    {
        return $this->hasOne(Trip::class, 'id', 'trip_1');
    }
    public function trip2()
    {
        return $this->hasOne(Trip::class, 'id', 'trip_2');
    }
}
