<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $table = 'password_resets_6590';

    protected $fillable = [
        'account_id',
        'reset_code',
        'reset_status',
    ];

}
