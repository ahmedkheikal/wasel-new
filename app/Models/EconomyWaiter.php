<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\City;

class EconomyWaiter extends Model
{
    protected $table = 'economy_waiters_6585';

    protected $fillable = [
        'user_id',
        'fullname',
        'phone',
        'from_city',
        'to_city',
        'datetime_from',
        'datetime_to',
        'notification_status',
        'user_type',
    ];
}
