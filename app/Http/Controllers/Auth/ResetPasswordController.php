<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Services\Respond;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Services\NotificationService;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
    * Where to redirect users after resetting their password.
    *
    * @var string
    */
    protected $redirectTo = '/home';

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * get user id
     * send reset password code (twiliosms) and email
     *
     * NOTE: Ajax Form
     *
     */
    public function sendResetCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'emailOrPhone' => 'required'
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        if (filter_var( $request->emailOrPhone, FILTER_VALIDATE_EMAIL ))
        $user = User::where('email', $request->emailOrPhone)->first();
        else // suppossed to be a phone
        $user = User::where('phone', $request->emailOrPhone)->first();

        if (!$user)
        return Respond::authenticationError('Authentication Failed');

        $resetCode = rand(99999, 999999);
        PasswordReset::create([
            'account_id' => $user->id,
            'reset_code' => $resetCode,
        ]);

        session([
            'wasel__password_reseter_id' => $user->id
        ]);

        $message = 'كود استعادة كلمة السر لحساب واصل الخاص بك هو '. $resetCode;

        $passwordResets = PasswordReset::where('account_id', $user->id)
                                        ->where('reset_status', 'active')
                                        ->get();
        if (count($passwordResets) < 3)
        NotificationService::sendTwilioSms($user->phone, $message);
        NotificationService::sendMail($user->email, Lang::get('language.resetPassword'), $message);

        return Respond::success('password reset code sent');
    }

    public function passwordCode()
    {
        return view('auth.passwords.code');
    }

    public function passwordCodeVerify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reset_code' => 'required'
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());


        $passwordReset = PasswordReset::where('account_id', session('wasel__password_reseter_id'))
                                        ->where('reset_code', $request->reset_code)
                                        ->where('reset_status', 'active')
                                        ->first();

        if (count((array)$passwordReset)) {
            session([
                'wasel__password_reset_code' => $request->reset_code
            ]);
            session([
                sha1('wasel__password_reset_successful') => sha1('true')
            ]);
            return Respond::success('reset code is valid');
        } else {
            return Respond::authenticationError('code invalid');
        }
    }

    public function passwordNew()
    {
        if (null !== session(sha1('wasel__password_reset_successful')) && session(sha1('wasel__password_reset_successful')) == sha1('true'))
        return view('auth.passwords.reset');
        else
        return redirect('/password/reset');
    }
    public function passwordNewPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $updateUser = User::where('id', session('wasel__password_reseter_id'))
                        ->update([
                            'password' => sha1($request->password)
                        ]);

        if ($updateUser) {
            PasswordReset::where('account_id', session('wasel__password_reseter_id'))
                          ->update([
                                'reset_status' => 'inactive'
                            ]);
            session()->forget('wasel__password_reset_code');
            session()->forget('wasel__password_reseter_id');
            session()->forget(sha1('wasel__password_reset_successful'));

            return Respond::success('');
        }
        else
        return Respond::fatalError();
    }
}
