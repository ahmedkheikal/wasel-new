<?php

namespace App\Http\Controllers;

use Route;
use App\User;
use Validator;
use GuzzleHttp\Client;
use App\Services\Respond;
use App\Services\NotificationService;
use App\Models\PasswordReset;
use App\Models\WalletTransaction;
use Illuminate\Http\Request;
use libphonenumber\PhoneNumberUtil;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    public function ajaxLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'emailphone' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        if (filter_var( $request->emailphone, FILTER_VALIDATE_EMAIL ))
        $authUser = User::where('email', $request->emailphone)->where('password', sha1($request->password))->first();
        else // suppossed to be a phone
        $authUser = User::where('phone', $request->emailphone)->where('password', sha1($request->password))->first();

        if (!$authUser)
        return Respond::authenticationError('Authentication Failed');

        Auth::login($authUser, $request->remember);

        $response = [ 'url' => '/' ];
        if (session()->has('after_login'))
        return Respond::success(
            session('after_login')
        );

        return Respond::success($response);
    }

    public function ajaxRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'nullable|email|max:255|unique:users_5627',
            'phone' => 'required|string|min:4|unique:users_5627', // |regex:/01[0-9]+$/
            'country_code' => 'required',
            'city' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'agree_terms_and_conds' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        if ($request->country_code == '2' || $request->country_code == '20') {
            if (!preg_match('/01[0-9]+$/', $request->phone))
            return Respond::clientError([
                'phone' => 'رقم الهاتف غير صحيح'
            ]);
        }

        // $phoneNumberUtil = PhoneNumberUtil::getInstance();
        // $phoneNumberValid = $phoneNumberUtil->isPossibleNumber($request->phone, $request->country_code);
        // if (!$phoneNumberValid)
        // return Respond::clientError([
        //     'phone' => Lang::get('validation.phoneInvalid')
        // ]);



        $newUser = User::create([
            'username' => $request->name,
            'email' => $request->email,
            'phone_country_code' => $request->country_code,
            'phone' => $request->phone,
            'account_status' => 'live',
            'account_type' => 'passenger',
            'city' => $request->city,
            'password' => sha1($request->password),
        ]);

        if (!$newUser)
        return Respond::authenticationError('Authentication Failed');

        Auth::login($newUser);
        $newUser->referal_code = 'aff' . intval(Auth::user()->id) * 3 . substr(Auth::user()->phone , 7, strlen(Auth::user()->phone) -7  );
        $newUser->save();

        // if (session()->has('after_login'))
        // return Respond::success(
        //     session('after_login')
        // );

        if ($request->referal_code)
        session([
            'referal_code' => $request->referal_code
        ]);
        $response = [ 'url' => '/phone/verify' ];
        return Respond::success($response);
    }

    public static function saveAfterLoginAction($url, $ajax, $method, array $params)
    {
        session( [
            'after_login' => [
                'url' => $url,
                'ajax' => $ajax,
                'method' => $method,
                'params' => $params,
            ]
        ] );
    }

    public function becomeACaptain()
    {
        return view('pages.become-a-captain');
    }

    public function becomeACaptainForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'nullable|email|max:255|unique:users_5627',
            'phone' => 'required|string|max:11|min:11|unique:users_5627|regex:/01[0-9]+$/',
            'city' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
            'agree_terms_and_conds' => 'required',
        ]);


        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $newUser = User::create([
            'username' => $request->name,
            'email' => $request->email,
            'phone_country_code' => '2',
            'phone' => $request->phone,
            'city' => $request->city,
            'account_type' => 'driver',
            'password' => sha1($request->password),
        ]);
        if (!$newUser)
        return Respond::authenticationError('Authentication Failed');

        Auth::login($newUser);

        $newUser->referal_code = 'aff' . intval(Auth::user()->id) * 3 . substr(Auth::user()->phone , 7, strlen(Auth::user()->phone) -7  );
        $newUser->save();

        $response = [ 'url' => '/account-settings' ];

        return Respond::success($response);
    }
    public function verifyPhone()
    {
        return view('pages.verify-phone');
    }

    public function sendVerificationCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $verificationCode = rand(999, 9999);
        $existingCode = PasswordReset::where('account_id')->where('reset_status', 'active')->first();
        if (!$existingCode) {
            $passworReset = PasswordReset::create([
                'account_id' => Auth::user()->id,
                'reset_code' => $verificationCode,
                'reset_status' => 'active',
            ]);
            NotificationService::sendTwilioSms(Auth::user()->phone, 'كود تفعيل حساب واصل الخاص بك هو '. $verificationCode );
        } else {
            NotificationService::sendTwilioSms(Auth::user()->phone, 'كود تفعيل حساب واصل الخاص بك هو '. $existingCode->reset_code );

        }
        $response = [ 'url' => '/' ];
        if (session()->has('after_login'))
        return Respond::success(
            session('after_login')
        );
        return Respond::success();
    }
    public function verifyPhonePost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verification_code' => 'required|exists:password_resets_6590,reset_code',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $verification = PasswordReset::where('reset_code', $request->verification_code)
            ->where('account_id', Auth::user()->id)
            ->where('reset_status', 'active')->first();
        if ($verification) {
            PasswordReset::where('account_id', Auth::user()->id)
                        ->update([
                            'reset_status' => 'inactive'
                        ]);
            Auth::user()->phone_verified  = 1;
            Auth::user()->save();

            if (session()->has('referal_code')) { // only for /register/{refcode}
                $referer = User::whereNotNull('referal_code')->where('referal_code', session('referal_code'))->first();
                if ($referer) {
                    $refererId = $referer->id;
                    WalletTransaction::create([
                        'user' => Auth::user()->id,
                        'amount' => 50,
                        'balance_channel' => 'affiliate',
                        'subject' => 'affiliateReferer:' . $referer->id,
                        'description' => "تم إضافة 50 ج.م إلى محفظتك صالحة للاستخدام في رحلتك القادمة لمدة شهر بعد قبولك دعوة أحد أصدقائك للانضمام إلى واصل",
                    ]);
                    Auth::user()->affiliate_balance += 50;
                    Auth::user()->save();

                    WalletTransaction::create([
                        'user' => $referer->id,
                        'amount' => 50,
                        'balance_channel' => 'affiliate',
                        'subject' => 'affiliateCredit:' . Auth::user()->id,
                        'description' => 'تم إضافة 50 ج.م لمحفظتك صالحة للاستخدام في رحلتك القادمة لمدة شهر لانضمام أحد الأصدقاء إلى واصل عن طريق دعوتك',
                    ]);
                    NotificationService::sendSms(Auth::user()->phone, "تم إضافة 50 ج.م إلى محفظتك صالحة للاستخدام في رحلتك القادمة لمدة شهر بعد قبولك دعوة أحد أصدقائك للانضمام إلى واصل");
                    NotificationService::sendSms($referer->phone, 'تم إضافة 50 ج.م لمحفظتك صالحة للاستخدام في رحلتك القادمة لمدة شهر لانضمام أحد الأصدقاء إلى واصل عن طريق دعوتك');
                    $referer->affiliate_balance += 50;
                    $referer->save();
                } else {
                    $refererId = null;
                }

                session()->forget('referal_code');

                return Respond::success([ 'url'  => '/invitation-accepted' ]);
            }

            if (session()->has('after_login')) {
                $response = session('after_login');
                session()->forget('after_login');
                return Respond::success($response);
            }

            return Respond::success([
                'url' => '/'
            ]);
        }
        return Respond::clientError();
    }
    public function updatePhone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/01[0-9]+$/|unique:users_5627',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        Auth::user()->update([
            'phone' => $request->phone
        ]);

        return Respond::success();
    }
}
