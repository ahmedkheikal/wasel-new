<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Services\Respond;
use App\Services\TripService;
use App\Services\FeedbackService;
use App\Services\NotificationService;
use App\Models\Trip;
use App\Models\Pricing;
use App\Models\Feedaback;
use App\Models\Reservation;
use App\Models\WalletTransaction;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class ReservationController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function reserveEconomy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trip_id' => 'required|exists:available_trips_5394,id',
            'seat_1' => 'required',
            'seat_2' => 'required',
            'seat_3' => 'required',
            'setfor_phone' => 'nullable|regex:/01[0-9]+$/',
        ]);

        if (!Auth::check()) {
            session( [
                'after_login' => [
                    'url' => '/reserve-economy-trip',
                    'ajax' => true,
                    'method' => 'POST',
                    'params' => [
                        'trip_id' => $request->trip_id,
                        'seat_1' => $request->seat_1,
                        'seat_2' => $request->seat_2,
                        'seat_3' => $request->seat_3,
                        'use_50EGP_credit' => $request->use_50EGP_credit,
                        'promo_code' => $request->promo_code,
                    ],
                ]
            ] );
            return Respond::authenticationError();
        }
        if (session()->has('after_login'))
        session()->forget('after_login');

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $reservation = TripService::makeEcoReservaion(
            $request->trip_id,
            $request->seat_1,
            $request->seat_2,
            $request->seat_3,
            $request->promo_code,
            $request->reservation_type,
            $request->setfor_name,
            $request->setfor_phone
        );

        if (!$reservation)
        return Respond::fatalError('couldn\'t add reservaion');

        if ($request->use_50EGP_credit  == '1' &&
            (Auth::user()->affiliate_balance >= 25 || Auth::user()->own_balance >= 25)
        ) {
            $balance_channel = Auth::user()->affiliate_balance >= 25 ? 'affiliate' : 'own';
            WalletTransaction::create([
                'user' => Auth::user()->id,
                'amount' => -25,
                'balance_channel' => $balance_channel,
                'description' => 'استخدام 25 ج.م من رصيد المحفظة في رحلة اقتصادية',
                'subject' => 'creditUseEconomyTrip:' . $request->id,
            ]);
            $request->price -= 25;
            if     (Auth::user()->affiliate_balance >= 25) Auth::user()->affiliate_balance -= 25;
            elseif (Auth::user()->own_balance >= 25) Auth::user()->own_balance -= 25;
            Auth::user()->save();
            $reservation->update([
                'price' => $reservation->price - 25
            ]);
        }


        TripService::credtiPartnerForEconomy($reservation, $request->promo_code);
        // session([
        //     'currentReservation' => [
        //         'trip_id' => $request->trip_id,
        //         'reservation_id' => $reservation->id,
        //     ]
        // ]);

        $trip = Trip::where('id', $request->trip_id)->with('captain')->first();

        $delete_sameDay_trips = Trip::where('driver_id', $trip->driver_id)
                                    ->where('date', $trip->date)
                                    ->where('from_city', $trip->from_city)
                                    ->where('to_city', $trip->to_city)
                                    ->where('seat_1', '0')
                                    ->where('seat_2', '0')
                                    ->where('seat_3', '0')
                                    ->delete();

        if ($reservation->setfor_name != null && $reservation->setfor_phone != null) {
            $response = app('App\Http\Controllers\Payment\FawryController')->pay([
                        'user_id'   => Auth::user()->id,
                        'mobile'    => $reservation->setfor_phone ? $reservation->setfor_phone : Auth::user()->phone, // if reservation is for others message is sent to person who will take the seat not the person who reserved it
                        'amount'    => $reservation->price,
                        'org_code'  => 68,
                        'trip_id'   => $reservation->trip_id,
                        'trip_type' => 'economy'
                    ]);
        } else {
            $response = app('App\Http\Controllers\Payment\FawryController')->pay([
                    'user_id'   => Auth::user()->id,
                    'mobile'    => Auth::user()->phone,
                    'amount'    => $reservation->price,
                    'org_code'  => 68,
                    'trip_id'   => $reservation->trip_id,
                    'trip_type' => 'economy'
                ]);
        }

        if ($response->status() == 200) {
            return Respond::success([
                'trip' => $trip,
                'url' => '/reservaion-Successful'
            ]);
        } else {
            return response(['message' => $response], 500);
        }
    }

    public function reservationSuccessful()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            if (basename($_SERVER['HTTP_REFERER']) != 'confirm-economy' && basename($_SERVER['HTTP_REFERER']) != 'login' && basename($_SERVER['HTTP_REFERER']) != 'verify')
            return Respond::clientError('referer error');
        }

        return view('pages.reservation-successful');
    }

    public function cancel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reservation_id' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $reservation = Reservation::find($request->reservation_id);
        $trip = Trip::where('id', $reservation->trip_id)->with('captain')->first();

        $updateReservation = Reservation::where('id', $request->reservation_id )
                    ->update([
                        'reservation_status' => 'canceled',
                    ]);

        if ($reservation->seat_1 == '1')
        $trip->seat_1 = '0';

        if ($reservation->seat_2 == '1')
        $trip->seat_2 = '0';

        if ($reservation->seat_3 == '1')
        $trip->seat_3 = '0';

        $trip->save();

        $captainMessage =  'قام العميل '. Auth::user()->username .' بإلغاء حجزه في رحلة '. (new \DateTime($trip->datetime))->format('d M')
                        .' الساعة '. (new \DateTime($trip->datetime))->format('h:i A')
                        .' من '. Lang::get("db.{$trip->from_city}")
                        .' إلى ' . Lang::get("db.{$trip->to_city}") .'';
        NotificationService::sendSms($trip->captain->phone, $captainMessage);

        $clientMessage = 'تم إلغاء الحجز  في رحلة '. $trip->captain->username
                        .' يوم '. (new \DateTime($trip->datetime))->format('d M')
                        .' الساعة '. (new \DateTime($trip->datetime))->format('d M')
                        .' من '. $trip->from_city
                        .' إلى '. $trip->to_city;
        NotificationService::sendSms(Auth::user()->phone, $clientMessage);

        return Respond::success(Lang::get('language.reservationCanceledSuccessfully'));
    }

    public function confirmEconomyReservation()
    {
        $data['trip'] = Trip::find(
            session('tripToReserve')['trip_id']
        );
        $data['currentReservation'] = session('tripToReserve');

        if (!$data['trip'])
        NotificationService::sendMail("heikal@waselegypt.com", '$data[\'trip\']', json_encode(session('tripToReserve')));
        $price = Pricing::where('from_city', $data['trip']->from_city)->where('to_city', $data['trip']->to_city)->first();

        $price->price_per_ticket = intval($price->price_per_ticket);
        if (!$price)
        return false;

        $totalPrice = 0;
        // setting total price
        if ($data['currentReservation']['seat_1'] == '1')
        $totalPrice += $price->price_per_ticket;
        if ($data['currentReservation']['seat_2'] == '1')
        $totalPrice += $price->price_per_ticket;
        if ($data['currentReservation']['seat_3'] == '1')
        $totalPrice += $price->price_per_ticket;

        $data['currentReservation']['promo_code'] = isset($data['currentReservation']['promo_code']) ? $data['currentReservation']['promo_code'] : '';
        $totalPrice = TripService::applyEcoPromo( $totalPrice, $data['currentReservation']['promo_code'] );

        if ($data['currentReservation']['use_50EGP_credit'] == '1')
        $totalPrice -= 25;

        $data['currentReservation']['price'] = $totalPrice;

        if (isset(session('tripToReserve')['reservation_type'])) {
            $data['currentReservation']['reservation_type'] = session('tripToReserve')['reservation_type'];
            $data['currentReservation']['setfor_name'] = session('tripToReserve')['setfor_name'];
            $data['currentReservation']['setfor_phone'] = session('tripToReserve')['setfor_phone'];
        }

        return view('pages.confirm-economy', $data);
    }
    public function saveReservationData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'seat_1' => 'required|string',
            'seat_2' => 'required|string',
            'seat_3' => 'required|string',
            'trip_id' => 'required|string',
        ]);

        session([
            'tripToReserve' => $request->all()
        ]);

        if (!Auth::check()) {
            session( [
                'after_login' => [
                    'url' => '/confirm-economy',
                    'ajax' => false,
                    'method' => 'POST',
                    'params' => [
                        'trip_id' => $request->trip_id,
                        'seat_1' => $request->seat_1,
                        'seat_2' => $request->seat_2,
                        'seat_3' => $request->seat_3,
                        'use_50EGP_credit' => $request->use_50EGP_credit,
                        'promo_code' => $request->promo_code,
                    ],
                ]
            ] );
            return Respond::authenticationError();
        } elseif (Auth::user()->phone_verified == '0') {
            session( [
                'after_login' => [
                    'url' => '/confirm-economy',
                    'ajax' => false,
                    'method' => 'POST',
                    'params' => [
                        'trip_id' => $request->trip_id,
                        'seat_1' => $request->seat_1,
                        'seat_2' => $request->seat_2,
                        'seat_3' => $request->seat_3,
                        'use_50EGP_credit' => $request->use_50EGP_credit,
                        'promo_code' => $request->promo_code,
                    ],
                ],
            ]);
            return Respond::emailPhoneNotVerified();
        }

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        return Respond::success([
            'url' => '/confirm-economy',
        ]);
    }

    public function sendFeedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reservation_id' => 'required|exists:reservations_5248,id',
            'timing' => 'required',
            'behaviour' => 'required',
            'safety' => 'required',
            'cleanliness' => 'required',
            'pricing_commitment' => 'required',
            'pickup_dropoff_commitment' => 'required',
            'overall_rating' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $feedback = Feedaback::create([
            'reservation_id' => $request->reservation_id,
            'timing' => $request->timing,
            'behaviour' => $request->behaviour,
            'safety' => $request->safety,
            'cleanliness' => $request->cleanliness,
            'pricing_commitment' => $request->pricing_commitment,
            'pickup_dropoff_commitment' => $request->pickup_dropoff_commitment,
            'overall_rating' => $request->overall_rating,
            'notes' => $request->notes,
        ]);

        if ($feedback) {
            $reservation = Reservation::where('id', $request->reservation_id)->with('trip.captain')->first();
            FeedbackService::updateCaptainRating($reservation->trip->captain);
            return Respond::success($feedback);
        }

        return Respond::fatalError();
    }
}
