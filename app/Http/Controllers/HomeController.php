<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Shipping;
use App\Models\PrivatePricing;
use App\Services\Respond;
use App\Services\ShippingService;
use App\Services\NotificationService;
use Validator;
use Carbon\Carbon;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // TODO: remove currentreservation session variable data
        // $this->middleware('auth');
    }

    /**
     * Homepage
     * @return View  pages.home with existing cities
     */
    public function index()
    {
        $privatePricing = PrivatePricing::with('fromCity')->get();
        $newCities = [];
        foreach ($privatePricing as $pricing)
            $newCities[] = $pricing->fromCity->city_name;

        $cities = City::whereIn('city_name', $newCities)->orderBy('order')->get();

        if (Auth::user() && Auth::user()->account_type == 'driver')
        return view('pages.driver-pages.add-economy-range', [
            'cities' => $cities,
        ]);

        $data['cities'] = $cities;
        $data['newCities'] = \array_unique($newCities);
        return view('pages.home', $data);
    }

    public function about()
    {
        return view('pages.about');
    }

    public function termsAndConds()
    {
        return view('pages.terms-and-conds');
    }

    public function waselExpress()
    {
        return view('pages.wasel-express');
    }

    /**
     * Add Shipping order (wasel express)
     * @param  Request $request     array keys given in validator rules
     * @return JSON                 Success status and order details
     */
    public function expressAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'datetime'          => 'required|date|date_format:Y-m-d',
            'receiver_name'     => 'required',
            'receiver_phone'    => 'required|regex:/01[0-9]+$/|min:11|max:11',
            'from_city'         => 'required',
            'from_city_address' => 'required',
            'pickup_address'    => 'required',
            'to_city'           => 'required',
            'to_city_address'   => 'required',
            'arrival_address'   => 'required',
            'order_type'        => 'required|in:box,document,gadget,other',
            'weight'            => 'required',
            // 'other_description' => 'string|nullable',
            'order_description' => 'required',
        ], [
            'regex' => 'رقم الهاتف غير صحيح',
            'min' => 'رقم الهاتف غير صحيح',
            'max' => 'رقم الهاتف غير صحيح',
            'date_format' => 'برجاء اختيار التاريخ',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        if (!Auth::check()) {
            session( [
                'after_login' => [
                    'url' => '/express/add',
                    'ajax' => true,
                    'method' => 'POST',
                    'params' => [
                        // 'datetime' => $request->datetime,
                        'receiver_name' => $request->receiver_name,
                        'receiver_phone' => $request->receiver_phone,
                        'from_city' => $request->from_city,
                        'from_city_address' => $request->from_city_address,
                        'pickup_address' => $request->pickup_address,
                        'to_city' => $request->to_city,
                        'to_city_address' => $request->to_city_address,
                        'arrival_address' => $request->arrival_address,
                        'order_type' => $request->order_type,
                        'weight' => $request->weight,
                        'price' => $request->price,
                        // 'other_description' => $request->other_description,
                        'order_description' => $request->order_description,
                    ],
                    ],
            ] );
                return Respond::authenticationError();
        }
        if (session()->has('after_login'))
        session()->forget('after_login');


        if ($request->order_type == 'box' && (
            $request->length == '' || $request->length == null ||
            $request->width == '' || $request->width == null ||
            $request->height == '' || $request->height == null
        ))
            return Respond::clientError([
                'length' => ['required'],
                'width'  => ['required'],
                'height' => ['required'],
            ]);

        $shipping = Shipping::create([
            'client_id' => Auth::user()->id,
            'datetime' => Carbon::now()->format('Y-m-d H:i:s'),
            'receiver_name' => $request->receiver_name,
            'receiver_phone' => $request->receiver_phone,
            'from_city' => $request->from_city,
            'from_city_address' => $request->from_city_address,
            'pickup_address' => $request->pickup_address,
            'to_city' => $request->to_city,
            'to_city_address' => $request->to_city_address,
            'arrival_address' => $request->arrival_address,
            'order_type' => $request->order_type,
            'weight' => $request->weight,
            'dimensions' => $request->length . ',' . $request->width . ',' . $request->height,
            // 'other_description' => $request->other_description,
            'order_description' => $request->order_description,
            'price' => ShippingService::calculatePrice($request->from_city, $request->to_city, $request->weight, $request->order_type, [
                'length' => $request->length,
                'width' => $request->width,
                'height' => $request->height,
            ]),
        ]);

        $message = 'Dear Admin, <br>
            A new Shipment request was sent via WASEL express service, Please
            <a href="https://www.waselegypt.com/private_52368545/dashboard/shipment?id='. $shipping->id .'">
                login
            </a>
            and act accordingly to fulfill this order <br> <br>
            Regards, <br>
                &nbsp; &nbsp; WASEL Notification System
        ';
        NotificationService::sendMail('elrawy@waselegypt.com', 'New Shipping order', $message);
        // NotificationService::notifyAdmins('New WASEL Express Request', $message);
        if ($shipping)
        return Respond::success([
            'url' => '/express/success',
        ]);

    }
    public function expressCost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from' => 'required',
            'to' => 'required',
            'weight' => 'required',
            'type' => 'required',
            'length' => 'nullable',
            'width' => 'nullable',
            'height' => 'nullable',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());
        if (
            $request->type == 'box' && (
                $request->length == null ||
                $request->width  == null ||
                $request->height == null
            )) {
            return Respond::clientError([
                'length' => 'required',
                'width'  => 'required',
                'height' => 'required',
            ]);
        }
        if ($request->type == 'box')
        $dimentions = [
            'length' => $request->length,
            'width'  => $request->width,
            'height' => $request->height,
        ];
        else
        $dimentions = [];
        return Respond::success([
            'price' => ShippingService::calculatePrice($request->from, $request->to, $request->weight, $request->type, $dimentions)
        ]);
    }

    public function expressSuccess()
    {
        return view('pages.express-success');
    }

    public function shippingTermsAndConds($value='')
    {
        return view('pages.shipping-terms-and-conds');
    }

    public function myShippingOrders()
    {
        $data['orders'] = Shipping::where('client_id', Auth::user()->id)
            ->with('captain')
            ->get();
        // return $data['orders'];
        return view('pages.user-pages.shipping.my-orders', $data);
    }
}
