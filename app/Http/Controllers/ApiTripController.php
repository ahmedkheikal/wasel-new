<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Trip;
use App\Models\City;
use App\Models\Region;
use App\Models\Pricing;
use App\Models\PrivatePricing;
use App\Models\PrivatePromoCode;
use App\Models\PromoCode;
use App\Services\Respond;
use App\Services\TripService;
use App\Services\FeedbackService;
use App\User;
use Illuminate\Support\Facades\Lang;

class ApiTripController extends Controller
{
    /**
     * gets from city as a string and returns all private and economy trips ging from that cuty
     *
     * @param String $from_city
     *
     * @return JSON $response
     *         default $to_city and economy trips (if any)
     *         regions inside that city and regions inside the default $to_city
     */
    public function fromCityChange(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required'
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $pricings = Pricing::where('from_city', $request->from_city)
                    ->with('fromCity.regions', 'toCity.regions')
                    ->get();

        $economyTrips = TripService::getEconomyTrips($request->from_city, $pricings->first()->to_city, 'live')
                            ->with('tripsInTheSameDay')
                            ->get();

        return Respond::success([
            'prices' => $pricings,
            'available_economy' => $economyTrips,

        ]);

    }


    /**
     *
     * @param String $from_city
     * @param String $to_city
     *
     * @return Collection $trips
     */
    public function getEconomyTrips(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required|exists:cities_5982,city_name',
            'to_city' => 'required|exists:cities_5982,city_name',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $trips = TripService::getEconomyTrips($request->from_city, $request->to_city, 'live')
                            ->orderBy('datetime')
                            ->get();

        $temp_array = [];
        foreach ($trips as $trip ) {
            $sameDay_trips = Trip::select('departure_time')
                    ->where('from_city', $request->from_city)
                    ->where('to_city', $request->to_city)
                    ->where('date', $trip->date)
                    ->where('datetime', '>', Carbon::now())
                    ->where('trip_status', 'live')
                    ->where(function ($query) {
                        $query->where('seat_1', '=', '0')
                            ->orWhere('seat_2', '=', '0')
                            ->orWhere('seat_3', '=', '0');
                    })
                    ->where(function ($query) use ($trip){
                        if ($trip->date == Carbon::now())
                        $query->where('departure_time', '>' , Carbon::now());
                        else
                        $query->whereRaw('1');
                    })
                    ->orderBy('departure_time')
                    ->get();
                    $trips_array = [];
            foreach ($sameDay_trips  as $formattedDateTrip ) {
                $formattedDateTrip->formatted_departure_time = (new \DateTime($formattedDateTrip->departure_time))->format('h:i A');
                $trips_array[] = $formattedDateTrip;
            }
            $temp_array[$trip->date] = $trips_array;
        }
        return Respond::success($temp_array);

    }

    public function economyTripInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required|exists:cities_5982,city_name',
            'to_city' => 'required|exists:cities_5982,city_name',
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|date_format:H:i:s',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors()->toArray());

        $seats = TripService::getEconomyTrips($request->from_city, $request->to_city, 'live')
                            ->select('id', 'seat_1', 'seat_2', 'seat_3')
                            ->where('datetime', $request->date .' '. $request->time)
                            ->first();
        return Respond::success($seats);
    }

    public function calculateEconomyPrice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required|exists:cities_5982,city_name',
            'to_city' => 'required|exists:cities_5982,city_name',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $price = Pricing::where('from_city', $request->from_city)->where('to_city', $request->to_city)->first();
        if (!$price)
        return Respond::clientError();

        return Respond::success([
            'price' => intval($price->price_per_ticket)
        ]);
    }

    public function privateCities(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city_name' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $city_id = City::where('city_name', $request->city_name)->select('id')->first()->id;
        return Respond::success(
            Region::where('city_id', $city_id)->orderBy('order')->get()
        );
    }

    public function privatePricing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required',
            'to_city' => 'required',
            'from_region' => 'required',
            'to_region' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $from_city = City::where('city_name', $request->from_city)->first();
        $to_city = City::where('city_name', $request->to_city)->first();

        $base_price = PrivatePricing::where('from_city', $from_city->id)
                                    ->where('to_city', $to_city->id)
                                    ->where('trip_type', $request->going_and_comingback)
                                    ->first();
        $price_going = PrivatePricing::where('from_city', $from_city->id)
                                    ->where('to_city', $to_city->id)
                                    ->where('trip_type', 'going')
                                    ->first();
        $price_going_and_comingback = PrivatePricing::where('from_city', $from_city->id)
                                    ->where('to_city', $to_city->id)
                                    ->where('trip_type', 'going_and_comingback')
                                    ->first();
        $price_going_and_comingback_otherday = PrivatePricing::where('from_city', $from_city->id)
                                    ->where('to_city', $to_city->id)
                                    ->where('trip_type', 'going_and_comingback_otherday')
                                    ->first();

        $additional_amount_from = Region::where('region', $request->from_region)->first();
        $additional_amount_to = Region::where('region', $request->to_region)->first();

        if (! ($base_price && $additional_amount_from && $additional_amount_to)   )
        return Respond::clientError(Lang::get('language.pricingRuleNotFound'));

        if ($base_price->relation == 'sn') {
            $additional_amount_from = $additional_amount_from->north_amount;
            $additional_amount_to = $additional_amount_to->south_amount;
        } else {
            $additional_amount_from = $additional_amount_from->south_amount;
            $additional_amount_to = $additional_amount_to->north_amount;
        }
        if ($request->trip_type == 'going_and_comingback_otherday') {
            $additional_amount_from *= 2;
            $additional_amount_to *= 2;
        }

        $response = [
            'base_price' => intval($base_price->price),
            'price_going' => intval($price_going->price),
            'price_going_and_comingback' => intval($price_going_and_comingback->price),
            'price_going_and_comingback_otherday' => intval($price_going_and_comingback_otherday->price),
            'additional_amount_from' => intval($additional_amount_from),
            'additional_amount_to' => intval($additional_amount_to),
        ];

        return Respond::success($response);
    }

    /**
     * gets all available destinations regarding private pricing from a given city
     *
     */
    public function getAlltos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $cities = PrivatePricing::
                whereHas('fromCity', function ($query) use ($request) {
                    $query->where('city_name', $request->from_city);
                })
                ->select('to_city')
                ->with('toCity')
                ->get();

        $responseArray = [];
        foreach ($cities as $key => $city) {
            $city->toCity->selected = session('to_city') == $city->toCity->city_name ? 1 : 0;
            $city->toCity->city_name_ar = Lang::get("db.{$city->toCity->city_name}");
            $responseArray[] = $city->toCity;
        }

        // sorting array using flip sort
        $exitLoop = false;
        while (!$exitLoop) {
            $exitLoop = true;
            for ($i=0; $i < count($responseArray) - 1; $i++) {
                if ($responseArray[$i]->order > $responseArray{$i + 1}->order) {
                    $elementOne = $responseArray[$i];
                    $elementTwo = $responseArray[$i + 1];

                    $responseArray[$i] = $elementTwo;
                    $responseArray[$i + 1] = $elementOne;
                    $exitLoop = false;
                }
            }
        }
        return Respond::success($responseArray);
    }

    public function privatePromo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required',
            'to_city' => 'required',
            'promo_code' => 'required|exists:private_promo_codes_3564',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $promo_code = PrivatePromoCode::where('promo_code', $request->promo_code)->first();

        if ($promo_code->code_status !== 'live')
        return Respond::clientError('Promo Code is inactive');

        if ($promo_code->all_destinations == '0') {
            if ($request->from_city !== $promo_code->from_city || $request->to_city !== $promo_code->to_city)
            return Respond::clientError('Promo Code is inactive for this destination');
        }

        return Respond::success($promo_code);
    }

    public function economyPromo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required',
            'to_city' => 'required',
            'promo_code' => 'required|exists:promo_codes_5278',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $promo_code = PromoCode::where('promo_code', $request->promo_code)->first();

        if ($promo_code->code_status !== 'live')
        return Respond::clientError('Promo Code is inactive');

        // if ($request->from_city !== $promo_code->from_city || $request->to_city !== $promo_code->to_city)
        // return Respond::clientError('Promo Code is inactive');

        return Respond::success($promo_code);
    }

    public function cityInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city_name' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $city = City::where('city_name', $request->city_name)->first();
        return Respond::success($city);
    }

    public function updateCaptainRating(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'captain_id' => 'required|exists:users_5627,id',
        ]);

        if ($request->secret_token !== '5d545c7aef122d396b316165eea2e107871181f0')
        return Respond::authenticationError();

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        if (FeedbackService::updateCaptainRating(User::find($request->captain_id)))
        return Respond::success();
        return Respond::fatalError();
    }
}
