<?php

namespace App\Http\Controllers\Trips;

use App\Models\City;
use App\Models\PrivatePricing;
use App\Models\TripService;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrivateTripsController extends Controller
{
	/**
	* Display The Add Private Trip Page
	*
	* @return \Illuminate\Http\Response
	*/
	public function create()
	{
		$fromCityId     = City::where('city_name', 'Cairo')->first()->id;
        $privatePricing = PrivatePricing::with('fromCity')->get();

        $newCities = [];

        foreach ($privatePricing as $pricing)
            $newCities[] = $pricing->fromCity->city_name;

        $cities = City::whereIn('city_name', $newCities)->orderBy('order')->get();

        $data['cities']     = $cities;

        $data['toCities']   = PrivatePricing::where('from_city', $fromCityId)->with('toCity')->get();
        $data['from_city']  = 'Cairo';
        $data['to_city']    = 'Minya';

        $tempArray = [];
        foreach ($data['toCities'] as $toCity) {
            $tempArray[] = $toCity->toCity;
        }

        $data['toCities'] = array_unique($tempArray);

		return view('pages.private-trip.create', $data);
	}
}
