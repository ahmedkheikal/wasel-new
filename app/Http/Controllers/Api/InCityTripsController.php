<?php

namespace App\Http\Controllers\Api;

use App\Models\InCityTrip as Trip;
use App\Http\Resources\InCityTrip as InCityTripResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InCityTripsController extends Controller
{
    /**
     * Get all the pendign incity trips
     * 
     * @return \Illuminate\Http\Response
     */
    public function pending()
    {
    	$trips = Trip::where('trip_status', 'pending')->paginate(10);
    	
    	return InCityTripResource::collection($trips);
    }

    /**
     * Get all the scheduled incity trips
     * 
     * @return \Illuminate\Http\Response
     */
    public function scheduled()
    {
    	$trips = Trip::where('trip_status', 'scheduled')->paginate(10);
    	
    	return InCityTripResource::collection($trips);
    }

    /**
     * Get all the canceled incity trips
     * 
     * @return \Illuminate\Http\Response
     */
    public function canceled()
    {
    	$trips = Trip::where('trip_status', 'canceled')->paginate(10);
    	
    	return InCityTripResource::collection($trips);
    }

    /**
     * Accept an InCity Trip By Driver
     * 
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function accept(Request $request, $id)
    {
    	$trip = Trip::findOrFail($id);
    	
    	$trip->trip_status = 'scheduled';
    	$trip->driver_id = auth()->user()->id;
    	$trip->save();

    	return response()->json([
    		'success' => 'Accepted'
    	], 200);
    }
}
