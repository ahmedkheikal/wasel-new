<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\InCityTrip as Trip;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\InCityTrip as InCityTripResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	/**
	 * @var int
	 */
	public $successStatus = 200;

    /**
     * Log the user in with the "api"
     * 
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
    	if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
    		
            $user = Auth::user();
    		$token = $user->createToken('WaselUser')->accessToken;
    		
            return response()->json([
    			'success'   => 'Logged in',
                'token'     => $token
    		], $this->successStatus);
    	}

    	return response()->json([
    		'error' => 'Unauthorised'
    	], 401);
    }

    /**
     * Get the current user data
     * 
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
    	$user = Auth::user();
        
    	return new UserResource($user);
    }

    /**
     * Get the trips that the driver was accepted
     * 
     * @return \Illuminate\Http\Response
     */
    public function mytrips()
    {
        $trips = Trip::where('driver_id', Auth::user()->id)
                     ->where('trip_status', 'scheduled')
                     ->paginate(10);
        
        return InCityTripResource::collection($trips);
    }
}
