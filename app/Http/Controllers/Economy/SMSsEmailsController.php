<?php

namespace App\Http\Controllers\Economy;

use App\Services\Respond;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Lang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SMSsEmailsController extends Controller
{
	/**
	 * Send The Emails and SMSs to Passenger and Captain after the Fawry Payment Done
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function send($reservation, $trip, $user)
    {
    	$seats_to_reserve_email = $reservation->seat_1 == '1' ? '
        <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box;">
            <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                <h4 style="margin: 0; padding: 5px 10px; font-size: 9px;">
                    '. Lang::get('language.frontSeat') .'
                </h4>
            </div>
        </div>
        ' : '';
        $seats_to_reserve_email .= $reservation->seat_2 == '1' ? '
        <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box;">
            <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                <h4 style="margin: 0; padding: 5px 10px; font-size: 9px;">
                    '. Lang::get('language.backSeatLeft') .'
                </h4>
            </div>
        </div>
        ' : '';
        $seats_to_reserve_email .= $reservation->seat_3 == '1' ? '
        <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box;">
            <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                <h4 style="margin: 0; padding: 5px 10px; font-size: 9px;">
                    '. Lang::get('language.backSeatRight') .'
                </h4>
            </div>
        </div>
        ' : '' ;

        $seats_to_reserve_sms  = $reservation->seat_1 == '1' ? 'المقعد الأمامي' : '';
        $seats_to_reserve_sms .= $reservation->seat_2 == '1' ? 'المقعد الخلفي الأيمن' : '';
        $seats_to_reserve_sms .= $reservation->seat_3 == '1' ? 'المقعد الخلفي الأيسر' : '' ;

        if ($reservation->reservation_type == 'self') {
            $clientSms = 'تم الحجز بنجاح فى رحلة '. Lang::get("db.{$trip->from_city}") .' - '. Lang::get("db.{$trip->to_city}") .'، '. (new \DateTime($trip->datetime))->format('d M') .' الساعه '. (new \DateTime($trip->datetime))->format('h:i A') .' '
                . $seats_to_reserve_sms .'،  التكلفة:'
                . $reservation->price .' ج.م ، هاتف الكابتن '
                . $trip->captain->username .': '. $trip->captain->phone . "\n"
                . 'نوع السيارة: '. $trip->captain->car_model .', اللون: '. $trip->captain->car_color .', رثم اللوحة: '. $trip->captain->chassis_no . "\n"
                . ' برجاء الاتصال بالكابتن لتأكيد الحجز';

            $captainRating = $trip->captain->rating ? $trip->captain->rating : 5;

            $clientEmail = '
                <div style="font-family: sans-serif; color: #222; max-width: 500px; width: 100%; margin:auto; padding: 10px">
                    <h2 style="text-align: center">Ticket details</h2>
                    <div style="margin-bottom: 70px; min-height: 180px">
                        <div style="width: 50%; float: left; ">
                            <div style="text-align:center">
                                <img style="width: 50%; display: block; margin: auto" src="'. $trip->captain->profile_picture .'" alt="'. $trip->captain->username .'">
                                <h3>'. $trip->captain->username .'</h3>
                                <small>Car: '.  $trip->captain->car_color  .' ' . $trip->captain->car_model .'</small><br>
                                <small>Plate No.. '. $trip->captain->chassis_no .'</small><br>
                                <small>Phone: '. $trip->captain->phone .'</small> <br>
                                <small>
                                    Rating: '. $captainRating .'
                                    <img style="height: 10px;" src="https://www.waselegypt.com/images/star-rating.png" alt="star">
                                </small>
                            </div>

                        </div>
                        <div style="width: 50%; float: right ">
                            <h4 style="margin-top: 0">Details</h4>
                            <div class="ticket-info">
                                <p>From: '. $trip->from_city .'</p>
                                <p>To: '. $trip->to_city. ' </p>
                                <p>Date: '. (new \DateTime($trip->date))->format('d ') . Lang::get('language.' . (new \DateTime($trip->date))->format('M')) . (new \DateTime($trip->date))->format(' Y') .'</p>
                                <p>Time: '. (new \DateTime($trip->departure_time))->format('h:i ') . Lang::get('language.' . (new \DateTime($trip->departure_time))->format('A')) .'</p>
                                <p>Seats to reserve: '. $seats_to_reserve_email .'
                                </p>
                                <h3>Ticket total '. $reservation->price .'</h3>
                            </div>
                        </div>

                    </div>
                    <br>
                    <div style="width: 100%">
                        <p>  for more information, please contact <a href="mailto:support@waselegypt.com" style="color: white">support@waselegypt.com</a> </p>

                    </div>
                </div>
            ';

            $clientEmail = '
            <div style="font-family: sans-serif; color: white; max-width: 500px; width: 100%; margin:auto; padding: 0; box-sizing: border-box;">
                <div style="margin-top: 20px;margin-bottom: 50px; min-height: 180px">
                    <div style="width: 50%; float: left; ">
                        <div style="text-align:center; border-right: 4px solid white;">
                            <img style="width: 50%; display: block; margin: auto; border-radius: 50%; object-fit: cover;" src="'. $trip->captain->profile_picture .'" alt="'. $trip->captain->username .'">
                            <h3 style="margin: 5px 0">'. $trip->captain->username .'</h3>
                            <p style="margin-top: 0">
                                '. $captainRating .'
                                <img style="height: 15px; max-height: 15px;" src="https://www.waselegypt.com/images/star-rating.png" alt="star">
                            </p>
                        </div>

                    </div>
                    <div style="width: 50%; float: right; text-align: left; padding-left: 30px; box-sizing: border-box; font-size: 19px">
                        <p style="margin: 0; ">'. $trip->captain->phone .'</p>
                        <p style="margin: 0;">'. $trip->captain->car_model .'</p>
                        <p style="margin: 0;">'. $trip->captain->car_color .'</p>
                        <p style="margin: 0;">'. $trip->captain->chassis_no .'</p>

                    </div>

                </div>
                <div style="min-height: 25vh; text-align: right; font-weight: bolder">

                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                من:
                                 '. Lang::get("db." . $trip->from_city) .'
                            </h4>
                        </div>
                    </div>
                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                إلى:
                                '. Lang::get("db." . $trip->to_city) .'
                            </h4>
                        </div>
                    </div>

                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                التاريخ:
                                '. (new \DateTime($trip->date))->format('d ') . Lang::get('language.' . (new \DateTime($trip->date))->format('M')) . (new \DateTime($trip->date))->format(' Y') .'
                            </h4>
                        </div>
                    </div>
                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                الوقت:
                                '. (new \DateTime($trip->departure_time))->format('h:i ') . Lang::get('language.' . (new \DateTime($trip->departure_time))->format('A')) .'
                            </h4>
                        </div>
                    </div>

                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                المقاعد المحجوزة
                            </h4>
                        </div>
                    </div>

                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;  height: 54px">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                        </div>
                    </div>

                    '. $seats_to_reserve_email .'
                    <div style="width: 100%">
                    </div>

                    <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box; color: white; margin-top: 20px; margin-bottom: -50px" dir="rtl">
                        السعر:
                        <div dir="rtl" style="background: #38c172; border-radius: 20px; color: white; font-weight: bold; text-align: center;">
                            <h4 style="margin: 0; padding: 5px 10px; font-size: 15px;">
                                '. $reservation->price .' ج.م
                            </h4>
                        </div>
                    </div>

                </div>
            </div>
            ';

            $smsPassenger = NotificationService::sendSms($user->phone, $clientSms);
            $emailPassenger = NotificationService::sendTicketMail($user->email, Lang::get('titles.ticketInfo'), $clientEmail);

            $captainSms = 'قام العميل '
                . $user->username .': '. $user->phone .' بالحجز معك في رحلة '
                . Lang::get("db.{$trip->from_city}") .' - '. Lang::get("db.{$trip->to_city}") .' يوم '
                . (new \DateTime($trip->date))->format('d M') .' الساعة '. (new \DateTime($trip->departure_time))->format('h:i ') . Lang::get('language.' . (new \DateTime($trip->departure_time))->format('A')) .'، المقاعد المحجوزة '
                . $seats_to_reserve_sms .' التكلفة الكلية: '. $reservation->price .' ج.م' . "\n"
                . ' برجاء الاتصال بالعميل لتأكيد الحجز';
            $captainEmail = '
                <div style="font-family: sans-serif; color: #222; max-width: 500px; width: 100%; margin:auto;  padding: 10px">
                    <h2 style="text-align: center">'. Lang::get('titles.reservationDetails') .'</h2>
                    <div style="margin-bottom: 70px; min-height: 180px">
                        <div style="width: 50%; float: left; ">
                            <div style="text-align:center">
                                <img style="width: 50%; display: block; margin: auto" src="'. $user->profile_picture .'" alt="'. $user->username .'">
                                <h3>'. $user->username .'</h3>
                                <small>Email: '. $user->email .'</small><br>
                                <small>Phone: '. $user->phone .'</small><br>
                                <small>Job:  '. $user->job_title .'</small><br>
                            </div>

                        </div>
                        <div style="width: 50%; float: right ">
                            <h4 style="margin-top: 0">Details</h4>
                            <div class="ticket-info">
                                <p>From: '. $trip->from_city .'</p>
                                <p>To: '. $trip->to_city. ' </p>
                                <p>Date: '. ( new \DateTime($trip->date ) )->format('d M Y') .'</p>
                                <p>Time: '. ( new \DateTime($trip->departure_time ) )->format('h:i A') .'</p>
                                <p>Seats to reserve: '. $seats_to_reserve_email .'
                                </p>
                                <h3>Ticket total'. $reservation->price .'</h3>
                            </div>
                        </div>

                    </div>

                    <br>
                    <div style="width: 100%">
                    </div>
                </div>
            ';
            $emailCaptain = NotificationService::sendMail($trip->captain->email, Lang::get('titles.reservationSuccessful'), $captainEmail);
            $smsCaptain = NotificationService::sendSms($trip->captain->phone, $captainSms);
        } elseif ($reservation->reservation_type == 'other') {
            $clientSms = 'تم الحجز بنجاح فى رحلة '. Lang::get("db.{$trip->from_city}") .' - '. Lang::get("db.{$trip->to_city}") .'، '
                . (new \DateTime($trip->datetime))->format('d M') .' الساعه '. (new \DateTime($trip->datetime))->format('h:i A') .' '
                . $seats_to_reserve_sms .'،  التكلفة:'
                . $reservation->price .' ج.م ، هاتف الكابتن '
                . $trip->captain->username .': '. $trip->captain->phone
                . 'الحجز باسم '. $reservation->setfor_name .': '. $reservation->setfor_phone .'برجاء الاتصال بالكابتن لتأكيد الحجز';
            $setforSms = 'قام '. $user->username .' بالحجز لك على موقع waselegypt.com في رحلة '. Lang::get("db.{$trip->from_city}") .' - '. Lang::get("db.{$trip->to_city}")
                .' يوم '. (new \DateTime($trip->datetime))->format('d M') .' الساعة '. (new \DateTime($trip->datetime))->format('h:i A')
                .'، المقاعد المحجوزة '. $seats_to_reserve_sms .' الكابتن '. $trip->captain->username .': '. $trip->captain->phone .'، التكلفة الكلية '
                . $reservation->price .' ج.م' ."\n"
                . 'نوع السيارة: '. $trip->captain->car_model .', اللون: '. $trip->captain->car_color .', رثم اللوحة: '. $trip->captain->chassis_no;

            $captainRating = $trip->captain->rating ? $trip->captain->rating : 5;
            $clientEmail = '
                <div style="font-family: sans-serif; color: #222; max-width: 500px; width: 100%; margin:auto; padding: 10px">
                    <h2 style="text-align: center">Ticket details</h2>
                    <div style="margin-bottom: 70px; min-height: 180px">
                        <div style="width: 50%; float: left; ">
                            <div style="text-align:center">
                                <img style="width: 50%; display: block; margin: auto" src="'. $trip->captain->profile_picture .'" alt="'. $trip->captain->username .'">
                                <h3>'. $trip->captain->username .'</h3>
                                <small>Car: '.  $trip->captain->car_color  .' ' . $trip->captain->car_model .'</small><br>
                                <small>Plate No.. '. $trip->captain->chassis_no .'</small><br>
                                <small>Phone: '. $trip->captain->phone .'</small>
                                <small>
                                    Rating: '. $captainRating .'
                                    <img style="height: 10px;" src="https://www.waselegypt.com/images/star-rating.png" alt="star">
                                </small>
                            </div>

                        </div>
                        <div style="width: 50%; float: right ">
                            <h4 style="margin-top: 0">Details</h4>
                            <div class="ticket-info">
                                <p>From: '. $trip->from_city .'</p>
                                <p>To: '. $trip->to_city. ' </p>
                                <p>Date: '. (new \DateTime($trip->date))->format('d ') . Lang::get('language.' . (new \DateTime($trip->date))->format('M')) . (new \DateTime($trip->date))->format(' Y') .'</p>
                                <p>Time: '. (new \DateTime($trip->departure_time))->format('h:i ') . Lang::get('language.' . (new \DateTime($trip->departure_time))->format('A')) .'</p>
                                <p>Seats to reserve: '. $seats_to_reserve_email .'
                                </p>
                                <h3>Ticket total '. $reservation->price .'</h3>
                            </div>
                        </div>

                    </div>
                    <br>
                    <div style="width: 100%">
                        <p>  for more information, please contact <a href="mailto:support@waselegypt.com" style="color: white">support@waselegypt.com</a> </p>

                    </div>
                </div>
            ';

            $clientEmail = '
            <div style="font-family: sans-serif; color: white; max-width: 500px; width: 100%; margin:auto; padding: 0; box-sizing: border-box;">
                <div style="margin-top: 20px;margin-bottom: 50px; min-height: 180px">
                    <div style="width: 50%; float: left; ">
                        <div style="text-align:center; border-right: 4px solid white;">
                            <img style="width: 50%; display: block; margin: auto; border-radius: 50%; object-fit: cover;" src="'. $trip->captain->profile_picture .'" alt="'. $trip->captain->username .'">
                            <h3 style="margin: 5px 0">'. $trip->captain->username .'</h3>
                            <p style="margin-top: 0">
                                '. $captainRating .'
                                <img style="height: 15px; max-height: 15px;" src="https://www.waselegypt.com/images/star-rating.png" alt="star">
                            </p>
                        </div>

                    </div>
                    <div style="width: 50%; float: right; text-align: left; padding-left: 30px; box-sizing: border-box; font-size: 19px">
                        <p style="margin: 0; ">'. $trip->captain->phone .'</p>
                        <p style="margin: 0;">'. $trip->captain->car_model .'</p>
                        <p style="margin: 0;">'. $trip->captain->car_color .'</p>
                        <p style="margin: 0;">'. $trip->captain->chassis_no .'</p>

                    </div>

                </div>
                <div style="min-height: 25vh; text-align: right; font-weight: bolder">

                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                من:
                                 '. Lang::get("db." . $trip->from_city) .'
                            </h4>
                        </div>
                    </div>
                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                إلى:
                                '. Lang::get("db." . $trip->to_city) .'
                            </h4>
                        </div>
                    </div>

                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                التاريخ:
                                <span dir="ltr">
                                    '. (new \DateTime($trip->date))->format('d ') . Lang::get('language.' . (new \DateTime($trip->date))->format('M')) . (new \DateTime($trip->date))->format(' Y') .'
                                </span>
                            </h4>
                        </div>
                    </div>
                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                الوقت:
                                <span dir="ltr">
                                    '. (new \DateTime($trip->departure_time))->format('h:i ') . Lang::get('language.' . (new \DateTime($trip->departure_time))->format('A')) .'
                                </span>
                            </h4>
                        </div>
                    </div>

                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                            <h4 style="margin: 0; padding: 5px 10px">
                                المقاعد المحجوزة:
                            </h4>
                        </div>
                    </div>

                    <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;  height: 54px">
                        <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                        </div>
                    </div>

                    '. $seats_to_reserve_email .'
                    <div style="width: 100%">
                    </div>

                    <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box; color: white; margin-top: 20px; margin-bottom: -50px">
                        السعر:
                        <div dir="rtl" style="background: #38c172; border-radius: 20px; color: white; font-weight: bold; text-align: center;">
                            <h4 style="margin: 0; padding: 5px 10px; font-size: 15px;">
                                '. $reservation->price .' ج.م
                            </h4>
                        </div>
                    </div>

                </div>
            </div>
            ';

            $smsPassenger = NotificationService::sendSms($user->phone, $clientSms);
            $emailPassenger = NotificationService::sendTicketMail($user->email, Lang::get('titles.ticketInfo'), $clientEmail);
            $smsSetfor = NotificationService::sendSms($request->setfor_phone, $setforSms);

            $captainSms = 'قام العميل '. $user->username .' بالحجز معك في رحلة '. Lang::get("db.{$trip->from_city}") .' - '. Lang::get("db.{$trip->to_city}")
                .' يوم '. (new \DateTime($trip->date))->format('d M') .' الساعة '. (new \DateTime($trip->departure_time))->format('h:i ') . Lang::get('language.' . (new \DateTime($trip->departure_time))->format('A'))
                .'، باسم '. $reservation->setfor_name .': '. $reservation->setfor_phone
                .'، المقاعد المحجوزة '. $seats_to_reserve_sms .' التكلفة الكلية '. $reservation->price .' ج.م';
            $smsCaptain = NotificationService::sendSms($trip->captain->phone, $captainSms);


            $captainEmail = '
                <div style="font-family: sans-serif; color: #222; max-width: 500px; width: 100%; margin:auto;  padding: 10px">
                    <h2 style="text-align: center">'. Lang::get('titles.reservationDetails') .'</h2>
                    <div style="margin-bottom: 70px; min-height: 180px">
                        <div style="width: 50%; float: left; ">
                            <div style="text-align:center">
                                <img style="width: 50%; display: block; margin: auto" src="'. $user->profile_picture .'" alt="'. $user->username .'">
                                <h3>'. $user->username .'</h3>
                                <small>Email: '. $user->email .'</small><br>
                                <small>Phone: '. $user->phone .'</small><br>
                                <small>Job:  '. $user->job_title .'</small><br>
                            </div>

                        </div>
                        <div style="width: 50%; float: right ">
                            <h4 style="margin-top: 0">Details</h4>
                            <div class="ticket-info">
                                <p>From: '. $trip->from_city .'</p>
                                <p>To: '. $trip->to_city. ' </p>
                                <p>Date: '. ( new \DateTime($trip->date ) )->format('d M Y') .'</p>
                                <p>Time: '. ( new \DateTime($trip->departure_time ) )->format('h:i A') .'</p>
                                <p>Seats to reserve: '. $seats_to_reserve_email .'
                                </p>
                                <h3>Ticket total'. $reservation->price .'</h3>
                            </div>
                        </div>

                    </div>
                    <br>
                    <div style="width: 100%">
                    </div>
                </div>
            ';
            $emailCaptain = NotificationService::sendMail($trip->captain->email, Lang::get('titles.reservationSuccessful'), $captainEmail);
        }


        if (!$emailPassenger || !$smsPassenger || !$emailCaptain || !$smsCaptain)
        return Respond::successNoNotification([
            'url' => '/reservaion-Successful',
        ]);
    }
}
