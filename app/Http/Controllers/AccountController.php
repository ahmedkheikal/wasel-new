<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Respond;
use App\Services\FileService;
use Validator;
use App\User;
use App\Models\PromoCode;
use App\Models\WalletTransaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class AccountController extends Controller
{

    public function accountSettings()
    {
        return view('pages.user-pages.account-settings');
    }

    public function updataInfo(Request $request)
    {
        if (Auth::user()->account_type == 'passenger') {
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'phone' => 'required|min:11|max:11',
                'email' => 'required|email',
                'city' => 'required',
                'job_title' => 'required',
                'gender' => 'required',
            ]);

            if ($validator->fails())
            return Respond::clientError($validator->errors());

            $updateUser = User::where('id', Auth::user()->id)->update([
                'username' => $request->username,
                'phone' => $request->phone,
                'email' => $request->email,
                'city' => $request->city,
                'job_title' => $request->job_title,
                'gender' => $request->gender,
            ]);

            if (!$updateUser)
            return Respond::fatalError('Couldn\'t update user');

            return Respond::success('Account info updated successfully');
        } elseif (Auth::user()->account_type == 'driver') {
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'phone' => 'required|min:11|max:11',
                'email' => 'required|email',
                'city' => 'required',
                'job_title' => 'required',
                'gender' => 'required',
                'car_model' => 'required',
                'car_color' => 'required',
                'chassis_no' => 'required',
            ]);

            if ($validator->fails())
            return Respond::clientError($validator->errors());

            $updateUser = User::where('id', Auth::user()->id)->update([
                'username' => $request->username,
                'phone' => $request->phone,
                'email' => $request->email,
                'city' => $request->city,
                'job_title' => $request->job_title,
                'gender' => $request->gender,
                'car_model' => $request->car_model,
                'car_color' => $request->car_color,
                'chassis_no' => $request->chassis_no,
            ]);

            if (!$updateUser)
            return Respond::fatalError('Couldn\'t update user');

            return Respond::success('Account info updated successfully');
        }
    }

    public function updateProfilePic(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'profile_picture' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());


        $file = FileService::uploadFilePublicly('profile-pictures', $request->file('profile_picture'));
        User::where('id', Auth::user()->id)->update([
            'profile_picture' => '/public/' . $file
        ]);

        return Respond::success(
            Lang::get('language.profilePicUpdatedSuccessfully')
        );
    }
    public function updateDrivingLicense(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driving_license' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());


        $file = FileService::uploadFilePublicly('driving-licenses', $request->file('driving_license'));
        User::where('id', Auth::user()->id)->update([
            'driving_license' => '/public/' . $file
        ]);

        return Respond::success(
            Lang::get('language.drivingLicenseUpdatedSuccessfully')
        );
    }
    public function updateIdCard(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_card' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());


        $file = FileService::uploadFilePublicly('id-cards', $request->file('id_card'));
        User::where('id', Auth::user()->id)->update([
            'id_card' => '/public/' . $file
        ]);

        return Respond::success(
            Lang::get('language.idCardUpdatedSuccessfully')
        );
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|confirmed|min:8',
            'new_password_confirmation' => 'required|min:8',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        if (sha1($request->old_password) != Auth::user()->password)
        return Respond::authenticationError(Lang::get('language.oldPasswordIncorrect'));

        $updatePassword = User::where('id', Auth::user()->id)->update([
            'password' => sha1($request->new_password),
        ]);

        if ($updatePassword)
        return Respond::success(Lang::get('language.passwordUpdatedSuccessfully'));

    }
    public function affiliateShare()
    {
        $user = User::find(Auth::user()->id);
        if (Auth::user()->referal_code == null || Auth::user()->referal_code == '') {
            $user->referal_code = 'aff' . intval(Auth::user()->id) * 3 . substr(Auth::user()->phone , 7, strlen(Auth::user()->phone) -7  );
            $user->save();
        }
        $data['invitationText'] =
        'احصل على ٥٠ جنيه خصم على رحلتك الأولى تضاف إلى محفظتك عند تسجيلك فى خدمة "واصل" للسفر بين المحافظات من خلال '
        . "\r\n"
        . env('APP_URL', 'https://www.waselegypt.com/') . 'register/' .Auth::user()->referal_code . "\r\n" .
        'أو حمل التطبيق من خلال https://goo.gl/P2ekJ6';
        $data['htmlInvitationText'] =
        'احصل على ٥٠ جنيه خصم على رحلتك الأولى تضاف إلى محفظتك عند تسجيلك فى خدمة "واصل" للسفر بين المحافظات من خلال
        '. env('APP_URL', 'https://www.waselegypt.com/') . 'register/' .Auth::user()->referal_code . "\r\n" .
        'أو حمل التطبيق من خلال https://goo.gl/P2ekJ6';

        $data['user'] = User::where('referal_code', Auth::user()->referal_code)->first();
        $data['invitationURL'] = env('APP_URL', 'https://www.waselegypt.com/') . 'register/' .Auth::user()->referal_code;


        return view('pages.affiliate-share', $data);
    }

    public function wallet()
    {
        $data['paidTransactions'] = WalletTransaction::where('user', Auth::user()->id)
                ->with('user')
                ->orderBy('id', 'desc')
                ->paginate(10);
        if (Auth::user()->account_type == 'partner') {
            $data['partnerPromoCode'] = PromoCode::where('partner', Auth::user()->id)
                ->where('code_status', 'live')
                ->first();

        }
        return view('pages.wallet', $data);
    }

    public function invitationAccepted()
    {
        return view('pages.invitation-accepted');
    }
}
