<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use App\Models\City;
use App\Models\Trip;
use App\Models\PromoCode;
use App\Models\Reservation;
use App\Models\EconomyWaiter;
use App\Models\PrivatePricing;
use App\Models\PrivatePromoCode;
use App\Models\PrivateFeedaback;
use App\Models\PrivateTripRequest;
use App\Models\WalletTransaction;
use App\Models\PrivateReservation;
use App\Models\CancellationRequest;
use App\Services\TripService;
use App\Services\Respond;
use App\Services\FeedbackService;
use App\Services\NotificationService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class TripController extends Controller
{
    public function testPushNotification(Request $request)
    {
        // code...
        $notificationData = [

        ];
        return NotificationService::sendPushNotification('fLfIc_R4Ehw:APA91bHH3VSNGGrz2gIe7qzBHweBIKgE5cldouamQ--7hVl9iNnRb8inYerL4Is3UVYKvMWDiwukPGQoojhSiMdhgwdHuSrgSeNueAeylPU1Alv3cggNk5faFn0ewesv3gvk4HMhztkSMJo55HmaOI8_ERQLnNCfaQ', $notificationData, 'driver');
    }
    //
    public function lookupDestinationPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required|different:to_city',
            'to_city' => 'required|different:from_city',
        ]);

        if (!$request->isMethod('POST'))
        return Response::json([
            'code' => '422',
            'response' => 'method not allowed',
        ]);

        if ($validator->fails())
        return redirect('/')->withErrors($validator);
        // return response()->json();


        $data['cities'] = City::all();

        session( ['from_city' => $request->from_city] );
        session( ['to_city' => $request->to_city] );

        return redirect('/add-trip');
    }

    public function lookupDestination()
    {
        $fromCityId     = City::where('city_name', 'Cairo')->first()->id;
        $privatePricing = PrivatePricing::with('fromCity')->get();
        
        $newCities = [];
        
        foreach ($privatePricing as $pricing)
            $newCities[] = $pricing->fromCity->city_name;

        $cities = City::whereIn('city_name', $newCities)->orderBy('order')->get();

        $data['cities']     = $cities;

        $data['toCities']   = PrivatePricing::where('from_city', $fromCityId)->with('toCity')->get();
        $data['from_city']  = 'Cairo';
        $data['to_city']    = 'Minya';

        $tempArray = [];
        foreach ($data['toCities'] as $toCity) {
            $tempArray[] = $toCity->toCity;
        }
        $data['toCities'] = array_unique($tempArray);

        $data['ecoTrips'] = TripService::getEconomyTrips('Cairo', 'Minya', 'live')->get();

        return view('pages.destination-lookup', $data);
    }

    public function myTrips()
    {
        if (Auth::user()->account_type == 'passenger') {
            $reservations = Reservation::where('user_id', Auth::user()->id)
            ->where('reservation_status', 'live')
            ->with('trip.captain', 'feedback')
            ->whereHas('trip', function ($query) {
                $query->where('datetime', '>', Carbon::now())
                ->where('trip_status', 'live');
            })
            ->paginate(15);
            $prevReservations = Reservation::where('user_id', Auth::user()->id)
            // ->where('reservation_status', 'live')
            ->with('trip.captain', 'trip.reservations.user')
            ->whereHas('trip', function ($query) {
                $query->where('datetime', '<', Carbon::now())
                ->where('trip_status', 'live');
            })
            ->paginate(15);
            return view('pages.user-pages.my-trips', [
            'reservations' => $reservations,
            'prevReservations' => $prevReservations,
            ]);
        } elseif (Auth::user()->account_type == 'driver') {

            $prevTrips = Trip::where('driver_id', Auth::user()->id)
            ->where('datetime', '<', Carbon::now())
            ->where('trip_status', '<>', 'private')
            ->with('cancellationRequests')
            ->with('reservations.user')
            ->whereHas('reservations', function ($query) {
                $query->where('reservation_status', 'live');
            })
            ->paginate(15);

            // $pendingTrips = Trip::where('driver_id', Auth::user()->id)
            //                     ->where('trip_status', 'pending')
            //                     ->where('datetime', '<', Carbon::now())
            //                     ->with('reservations.user')
            //                     ->get();

            $upcomingTrips = Trip::where('driver_id', Auth::user()->id)
            ->where(function ($query) {
                $query->where('trip_status', 'live')
                ->orWhere('trip_status', 'pending');
            })
            ->where('datetime', '>', Carbon::now())
            ->with('reservations.user')
            ->get();
            $cities = City::get();
            return view('pages.driver-pages.my-trips', [
            'prevTrips' => $prevTrips,
            // 'pendingTrips' => $pendingTrips,
            'upcomingTrips' => $upcomingTrips,
            'cities' => $cities,
            ]);
        } elseif (Auth::user()->account_type == 'partner') {
            $data['reservations'] = Reservation::where('user_id', Auth::user()->id)
                        ->where('reservation_status', 'live')
                        ->with('trip.captain')
                        // ->whereHas('trip', function ($query) {
                        //     $query->where('datetime', '>', Carbon::now())
                        //     ->where('trip_status', 'live');
                        // })
                        ->paginate(15);

            // start refered trips
            $ngoPromo = PromoCode::where('partner', Auth::user()->id)->first();
            if ($ngoPromo)
            $data['referedEcoReservations'] = Reservation::where('promo_code', $ngoPromo->promo_code)
                    ->with('trip.captain', 'user')
                    ->paginate(15);
            else
            $data['referedEcoReservations'] = null;

            $ngoPrivatePromo = PrivatePromoCode::where('partner', Auth::user()->id)->first();
            if ($ngoPrivatePromo)
            $data['referedPrivateRequests'] = PrivateTripRequest::where('used_promo', $ngoPrivatePromo->id)
                ->with('passenger', 'reservation.trip')
                ->paginate(15);
            else
            $data['referedPrivateRequests'] = null;
            // end refered trips
            $data['prevReservations'] = Reservation::where('user_id', Auth::user()->id)
                        ->with('trip.captain', 'trip.reservations.user')
                        ->whereHas('trip', function ($query) {
                            $query->where('datetime', '<', Carbon::now())
                            ->where('trip_status', 'live');
                        })
                        ->paginate(15);
            return view('pages.partner-pages.my-trips', $data);
        }
    }

    public function singleEcoTrip($id)
    {
        $reservation = Reservation::where('id', $id)
            ->with('trip.captain')
            ->first();
        if (Auth::user()->account_type == 'passenger') {

            $userOwn = false;
            if ($reservation && $reservation->user_id == Auth::user()->id)
            $userOwn = true;

            if ($userOwn && $reservation) {
                return view('pages.user-pages.single-economy-trip')->with('reservation', $reservation);
            } else {
                return "Trip, User mismatch";
            }
        } else {
            return "Not Supported Yet";
        }
    }

    public function myPrivateTrips()
    {
        if (Auth::user()->account_type == 'passenger') {
            $upcomingTrips = PrivateTripRequest::where('passenger_id', Auth::user()->id)
            ->where('datetime', '>', Carbon::now())
            ->get();
            // $upcomingTrips = PrivateTripRequest::where('passenger_id', Auth::user()->id)
            // ->where('datetime', '>', Carbon::now())
            // ->where('datetime', '>', Carbon::now())
            // ->get();
            $previousTrips = PrivateTripRequest::where('passenger_id', Auth::user()->id)
                    ->where('datetime', '<', Carbon::now())
                    ->where('request_status', 'scheduled')
                    ->where('request_status', '<>', 'pending')
                    ->with('reservation.trip.captain', 'feedback')
                    ->paginate(15);

            // $data['pendingTrips'] = $pendingTrips;
            $data['upcomingTrips'] = $upcomingTrips;
            $data['previousTrips'] = $previousTrips;
            return view('pages.user-pages.private-trips', $data);
        } elseif (Auth::user()->account_type == 'driver') {
            $prevPrivateTrips = Trip::where('trip_status', 'private')
            ->where('datetime', '<', Carbon::now())
            ->where('driver_id', Auth::user()->id)
            ->with('privateReservation.request.passenger')
            ->whereHas('privateReservation.request', function ($query) {
                $query ->where('request_status', 'scheduled');
            })
            ->paginate(15);

            $upcomingPrivateTrips = Trip::where('trip_status', 'private')
            ->where('datetime', '>', Carbon::now())
            ->where('driver_id', Auth::user()->id)
            ->with('privateReservation.request')
            ->whereHas('privateReservation.request', function ($query) {
                $query ->where('request_status', 'scheduled');
            })
            ->get();


            return view('pages.driver-pages.private-trips', [
                'prevPrivateTrips'     => $prevPrivateTrips,
                'upcomingPrivateTrips' => $upcomingPrivateTrips,
            ]);
        } elseif (Auth::user()->account_type == 'partner') {
            $data['upcomingTrips'] = PrivateTripRequest::where('passenger_id', Auth::user()->id)
                    ->where('datetime', '>', Carbon::now())
                    ->get();

            $data['previousTrips'] = PrivateTripRequest::where('passenger_id', Auth::user()->id)
                    ->where('datetime', '<', Carbon::now())
                    ->where('request_status', 'scheduled')
                    ->where('request_status', '<>', 'pending')
                    ->with('reservation.trip.captain')
                    ->paginate(15);
            return view('pages.partner-pages.private-trips', $data);
        }


    }

    public function singlePrivateTrip($id)
    {
        if (Auth::user()->account_type == 'passenger') {
            $data['request'] = $request = PrivateTripRequest::where('id', $id)
                ->with('reservation.trip.captain')
                ->first();
            if (Auth::user()->id != $data['request']->passenger_id)
            return "Request, client mismatch";

            return view('pages.user-pages.single-private-trip', $data);
        } else {
            return "Not Supported Yet";
        }
    }

    public function cancelPrivate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'request_id' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $privateRequest = PrivateTripRequest::where('id', $request->request_id)
        ->with('reservation.trip.captain')
        ->first();
        if ($privateRequest->passenger_id != Auth::user()->id)
        return Respond::clientError('Trip Does not belong to you');

        $update = $privateRequest->update([
            'request_status' => 'canceled',
        ]);

        if ($privateRequest->request_status == 'scheduled') {
            $captainMessage = 'قام العميل '
                            . Auth::user()->name .': '. Auth::user()->phone
                            .' بإلغاء طلب الرحلة الخاصة من ' . $privateRequest->from_city
                            .' ألى '. $privateRequest->to_city
                            .' يوم '. (new \DateTime($privateRequest->datetime))->format('d M')
                            .' الساعة '. (new \DateTime($privateRequest->datetime))->format('h:i A') .'';
            NotificationService::sendSms($privateRequest->reservation->trip->captain->phone, $captainMessage);

            $clientSms = 'تم إلغاء طلب الرحلة الخاصة من '
                        . $privateRequest->from_city
                        .' ألى '. $privateRequest->to_city
                        .' يوم '. (new \DateTime($privateRequest->datetime))->format('d M')
                        .' الساعة '. (new \DateTime($privateRequest->datetime))->format('h:i A')
                        .'. برجاء التأكد من أن الكابتن ' . $privateRequest->reservation->trip->captain->username .': '. $privateRequest->reservation->trip->captain->phone .' لن يكون موجودا في العنوان المحدد عند قدوم موعد الرحلة';
            NotificationService::sendSms(Auth::user()->phone, $clientSms);
        }


        if ($update)
        return Respond::success(Lang::get('language.success'));

        return Respond::fatalError('Internal Server Error');
    }

    public function addPrivateTrip(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_city' => 'required',
            'to_city' => 'required',
            'from_city_region' => 'required',
            'to_city_region' => 'required',
            'from_city_address' => 'required',
            'to_city_address' => 'required',
            'date' => 'required',
            'time' => 'required',
            'trip_type' => 'required',
            'price' => 'required',
            'setfor_phone' => 'nullable|regex:/01[0-9]+$/',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());
        $requestTime = date_create_from_format('h:i A', $request->time)->format('H:i:s');
        $currentDayTimestamp = (new \DateTime( Carbon::now()->format('Y-m-d') ))->getTimestamp();
        $requestTimestamp = (new \DateTime($request->date . ' ' . $requestTime))->getTimestamp();
        if ($requestTimestamp < $currentDayTimestamp)
        return Respond::clientError([
            'justdate' => Lang::get('language.dateShouldBeInTheFuture')
        ]);
        if ($request->trip_type == 'going_and_comingback_otherday') {
            if ($request->return_date == null || $request->return_time == null)
            return Respond::clientError([
                'return_date' => Lang::get('validation.required'),
                'return_time' => Lang::get('validation.required'),
            ]);
        }


        if (!Auth::check()) {
            session([
                'after_login' => [
                    'url' => '/add-private-trip',
                    'ajax' => true,
                    'method' => 'POST',
                    'params' => [
                        'from_city' => $request->from_city,
                        'to_city' => $request->to_city,
                        'from_city_region' => $request->from_city_region,
                        'to_city_region' => $request->to_city_region,
                        'from_city_address' => $request->from_city_address,
                        'to_city_address' => $request->to_city_address,
                        'date' => $request->date,
                        'time' => $request->time,
                        'trip_type' => $request->trip_type,
                        'price' => $request->price,
                        'use_50EGP_credit' => $request->use_50EGP_credit,
                        'private_promo' => $request->private_promo,
                    ],
                ]
            ]);
            return Respond::authenticationError();
        }
        if (Auth::user()->phone_verified == '0') {
            session([
                'after_login' => [
                    'url' => '/add-private-trip',
                    'ajax' => true,
                    'method' => 'POST',
                    'params' => [
                        'from_city' => $request->from_city,
                        'to_city' => $request->to_city,
                        'from_city_region' => $request->from_city_region,
                        'to_city_region' => $request->to_city_region,
                        'from_city_address' => $request->from_city_address,
                        'to_city_address' => $request->to_city_address,
                        'date' => $request->date,
                        'time' => $request->time,
                        'trip_type' => $request->trip_type,
                        'price' => $request->price,
                        'use_50EGP_credit' => $request->use_50EGP_credit,
                        'private_promo' => $request->private_promo,
                    ],
                ]
            ]);
            return Respond::emailPhoneNotVerified();
        }


        if (session()->has('after_login'))
        session()->forget('after_login');

        if (!$request->passenger_notes)
        $request->passenger_notes = '';


        $promo = PrivatePromoCode::select('id')
        ->where('promo_code', $request->private_promo)->where('code_status', 'live')
        ->first();

        if (!$promo)
        $promo = 89; // predefined empty promo code in our database
        else {
            if ($promo->type == 'flat_rate')
            $request->price -= $promo->amount;
            elseif ($promo->type == 'percent')
            $request->price -= $request->price * ( $promo->amount / 100 );

            if ($promo->code_usage == 'one use')
            $promo->code_status = 'used' && $promo->save();
            $promo = $promo->id;
        }

        if ($request->trip_type == 'going_and_comingback_otherday') {
            $round_request_price = $request->price - $request->price_going;
            $request->price = $request->price_going;
        }
        if ($request->use_50EGP_credit == "1") {
            if (Auth::user()->affiliate_balance < 50) {
                $request->price += 50;
            } else {
                WalletTransaction::create([
                    'user' => Auth::user()->id,
                    'amount' => -50,
                    'balance_channel' => 'affiliate',
                    'description' => 'استخدام 50 ج.م من رصيد المحفظة في رحلة خاصة',
                    'subject' => 'creditUsePrivateTrip:' . $request->id,
                ]);
                // $request->price -= 50;
                Auth::user()->affiliate_balance -= 50;
                Auth::user()->save();
            }
        }

        if ($request->request_type == 'self' || $request->request_type == null) {
            $request->request_type = 'self';
            $name = Auth::user()->username;
            $phone = Auth::user()->phone;
        } else {
            if (
                $request->setfor_name == '' ||
                $request->setfor_name == null
            ) {
                return Respond::clientError([
                    'setfor_name' => Lang::get('validation.setforNameIsRequired')
                ]);
            }
            if (
                $request->setfor_phone == '' ||
                $request->setfor_phone == null
            ) {
                return Respond::clientError([
                    'setfor_phone' => Lang::get('validation.setforPhoneIsRequired')
                ]);
            }
            $name = $request->setfor_name;
            $phone = str_replace(' ', '', $request->setfor_phone);
        }

        $privateRequest = PrivateTripRequest::create([
            'passenger_id' => Auth::user()->id,
            // 'round_request_id',
            'fullname' => $name,
            'phone' => $phone,
            'email' => Auth::user()->email,
            'from_city' => $request->from_city,
            'from_city_address' => $request->from_city_region,
            'to_city' => $request->to_city,
            'to_city_address'  => $request->to_city_region,
            'date' => $request->date,
            'departure_time' => $requestTime,
            'pickup_address' => $request->from_city_address,
            'arrival_address' => $request->to_city_address,
            'passenger_notes' => $request->passenger_notes,
            'trip_type' => $request->trip_type,
            'datetime' => $request->date . ' ' . $requestTime,
            'used_promo' => $promo,
            'price' => $request->price,
            'passenger_notes' => $request->passenger_notes,
            'request_type' => $request->request_type,
        ]);

        if ($request->trip_type == 'going_and_comingback_otherday') {
            $returnTime = date_create_from_format('h:i A', $request->return_time)->format('H:i:s');
            $returnRequest = PrivateTripRequest::create([
                'passenger_id' => Auth::user()->id,
                // 'round_request_id',
                'fullname' => $name,
                'phone' => $phone,
                'email' => Auth::user()->email,
                'from_city' => $request->to_city,
                'from_city_address' => $request->to_city_region,
                'to_city' => $request->from_city,
                'to_city_address'  => $request->from_city_region,
                'date' => $request->return_date,
                'departure_time' => $returnTime,
                'pickup_address' => $request->from_city_address,
                'arrival_address' => $request->to_city_address,
                'passenger_notes' => $request->passenger_notes,
                'trip_type' => 'going',
                'datetime' => $request->return_date . ' ' . $returnTime,
                'used_promo' => 89,
                'price' => $round_request_price,
                'passenger_notes' => $request->passenger_notes,
                'request_type' => $request->request_type,
            ]);
            $privateRequest->round_request_id = $returnRequest->id;
            $privateRequest->save();
        }

        if (!$privateRequest)
        return Respond::fatalError();

        TripService::credtiPartnerForPrivate($privateRequest, $promo);

        $message = '
            <p>
                Dear Admin, <br><br>
                A New private trip request was added to <a href="'. env('APP_URL', 'http://www.waselegypt.com/') .'">'. env('APP_URL', 'http://www.waselegypt.com/') .'</a> please <a href="http://waselegypt.com/private_52368545/dashboard/admin-auth">log in</a> to the admin panel to review it  <br><br>
                Regards, <br>
                &nbsp; &nbsp;WASEL notification system
            </p>
        ';
        NotificationService::notifyAdmins('New PRIVATE trip request', $message);

        if ($privateRequest->request_type == 'other')
        NotificationService::sendSms($phone, 'قام '. Auth::user()->username .' بإرسال طلب رحلة خاصة باسمك علي موقع waselegypt.com من '. Lang::get("db.{$request->from_city}") .' إلى '. Lang::get("db.{$request->to_city}") .' يوم '. (new \DateTime($request->date))->format('d M') .' الساعة '
            . (new \DateTime($privateRequest->datetime))->format('h:i A') .' التكلفة المقدرة للرحلة '. $request->price .' ج.م');

        // Lang::get('language.privateTripSentSuccessfullly')
        return Respond::success([
            'url' => '/private-reservaion-Successful'
        ]);
    }

    public function privateReservationSuccessful()
    {
        return view('pages.private-reservation-successful');
    }

    public function captainAddNewEcoTrip(Request $request)
    {
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'from_city' => 'required',
            'to_city' => 'required|different:from_city',
            'date' => 'required|after_or_equal:today|date|date_format:Y-m-d',
            'range_start' => 'required|date_format:g:i A',
            'range_end' => 'required|date_format:g:i A',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());


        $request->date = date_create_from_format('Y-m-d', $request->date)->format('Y-m-d');


        $range_start = strtotime($request->range_start);
        $range_end = strtotime($request->range_end);
        $hours = round(abs($range_end - $range_start) / 3600);

        for ($i=0; $i <= $hours; $i++) {
            $createTrip = Trip::create([
                'driver_id' => Auth::user()->id,
                'from_city' => $request->from_city,
                'to_city' => $request->to_city,
                'date' => $request->date,
                'departure_time' => date('H:i:s', (int)$range_start),
                'datetime' => $request->date .' ' . date('H:i:s', (int)$range_start),
                'pickup_address' => $request->pickup_address,
                'arrival_address' => $request->arrival_address,
                'seat_1' => '0',
                'seat_2' => '0',
                'seat_3' => '0',
                'trip_status' => 'pending',
            ]);
            $range_start += 3600;
        }

        $message = '
        <p>
            Dear Admin, <br><br>
            A New trip was added to <a href="http://waselegypt.com">'. env('APP_URL', 'waselegypt.com') .'</a> please <a href="http://waselegypt.com/private_52368545/dashboard/admin-auth">log in</a> to the admin panel to review it  <br><br>
            Regards, <br>
            &nbsp; &nbsp;WASEL notification system
        </p>
        ';
        NotificationService::notifyAdmins('New trip request', $message);

        if ($createTrip)
        return Respond::success(Lang::get('language.success'));
    }

    public function captainAddAvailableRange(Request $request)
    {
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'from_city' => 'required',
            'to_city' => 'required|different:from_city',
            'date' => 'required|after_or_equal:today|date|date_format:Y-m-d',
            'range_start' => 'required|date_format:g:i A',
            'range_end' => 'required|date_format:g:i A',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $request->date = date_create_from_format('Y-m-d', $request->date)->format('Y-m-d');
        $range_start = strtotime($request->range_start);
        $range_end = strtotime($request->range_end);

        $createRange = EconomyWaiter::create([
            'user_id' => Auth::user()->id,
            'fullname' => Auth::user()->username,
            'phone' => Auth::user()->phone,
            'from_city' => $request->from_city,
            'to_city' => $request->to_city,
            'datetime_from' => $request->date . ' ' . date('H:i:s', $range_start),
            'datetime_to' =>  $request->date . ' ' . date('H:i:s', $range_end),
            'notification_status' => 'not sent',
            'user_type' => 'driver',
        ]);

        $message = '
        <p>
            Dear Admin, <br><br>
            A captain announced that he\'s available to take an economy trip on '. $request->date .'. from '. date('h:i A', $range_start) .' to '. date('h:i A', $range_end) .' <br>
            Kindly review this announcment for trip matching
            Regards, <br>
            &nbsp; &nbsp;WASEL notification system
        </p>
        ';
        NotificationService::notifyAdmins('New trip request', $message);

        if ($createRange)
        return Respond::success(Lang::get('language.success'));
    }

    public function cancelEconomyTrip(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trip_id' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $trip = Trip::where('id', $request->trip_id)
                    ->with('reservations')
                    ->first();
        if (Auth::user()->id != $trip->driver_id)
        return Respond::authenticationError();

        if ($trip->reservations->count()) {
            CancellationRequest::create([
                'trip_id' => $trip->id,
            ]);
            return Respond::success(Lang::get('language.cancellationRequestSent'));
        } else {
            $trip->trip_status = 'canceled';
            $trip->save();
            return Respond::success(Lang::get('language.tripCanceledSuccessfully'));
        }
    }

    public function sendFeedbackPrivate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'request_id' => 'required|exists:private_trip_requests_2358,id',
            'timing' => 'required',
            'behaviour' => 'required',
            'safety' => 'required',
            'cleanliness' => 'required',
            'pricing_commitment' => 'required',
            'pickup_dropoff_commitment' => 'required',
            'overall_rating' => 'required',
        ]);


        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $feedback = PrivateFeedaback::create([
            'request_id' => $request->request_id,
            'timing' => $request->timing,
            'behaviour' => $request->behaviour,
            'safety' => $request->safety,
            'cleanliness' => $request->cleanliness,
            'pricing_commitment' => $request->pricing_commitment,
            'pickup_dropoff_commitment' => $request->pickup_dropoff_commitment,
            'overall_rating' => $request->overall_rating,
            'notes' => $request->notes,
        ]);

        if ($feedback) {
            $request = PrivateTripRequest::where('id', $request->request_id)->with('reservation.trip.captain')->first();
            FeedbackService::updateCaptainRating($request->reservation->trip->captain);
            return Respond::success($feedback);
        }

        return Respond::fatalError();

    }

    public function captainAddEconomyRange()
    {
        $cities = City::orderBy('order')->get();
        $months = [
            'يناير',
            'فبراير',
            'مارس',
            'أبريل',
            'مايو',
            'يونيو',
            'يوليو',
            'أغسطس',
            'سبتمبر',
            'أكتوبر',
            'نوفمبر',
            'ديسمبر',
        ];
        return view('pages.driver-pages.add-economy-range', [
            'cities' => $cities,
            'months' => $months,
        ]);
    }
}
