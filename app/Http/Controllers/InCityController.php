<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\InCityTrip;
use App\Models\InCityPricing;
use App\Models\PrivatePricing;
use App\Models\InCityPromoCode;
use App\Models\City;
use App\Services\Respond;
use App\Services\NotificationService;
use Validator;
use Carbon\Carbon;

class InCityController extends Controller
{
    public function index()
    {
        $fromCityId = City::where('city_name', 'Minya')->first()->id;
        $privatePricing = PrivatePricing::with('fromCity')->get();
        $newCities = [];
        foreach ($privatePricing as $pricing)
        $newCities[] = $pricing->fromCity->city_name;

        $cities = City::whereIn('city_name', $newCities)->orderBy('order')->get();

        $data['cities'] = $cities;

        $data['toCities'] = PrivatePricing::where('from_city', $fromCityId)->with('toCity')->get();
        $data['from_city'] = 'Minya';
        $data['to_city'] = 'Minya';

        $tempArray = [];
        foreach ($data['toCities'] as $toCity)
        $tempArray[] = $toCity->toCity;

        $data['toCities'] = array_unique($tempArray);


        return view('pages.incity-lookup', $data);
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_region'   => 'required',
            'to_region'     => 'required',
            'from_address'  => 'required',
            'to_address'    => 'required',
            'setfor_name'   => 'nullable',
            'setfor_phone'  => 'nullable',
            'date'          => 'required',
            'return_date'   => 'nullable',
            'time'          => 'required',
            'return_time'   => 'nullable',
            'trip_type'     => 'required',
            'price'         => 'required',
            'promo_code'    => 'nullable',
            'use_10EGP_credit'  => 'nullable',
            'passenger_notes'   => 'nullable',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        // handling price (promo code and wallet discount)
        $price = $this->calculatePrice($request->from_region, $request->to_region, $request->trip_type);
        if ($request->promo_code) $price->price = $this->applyPromo($request->promo_code, $price->price);
        if ($request->use_10EGP_credit == '1') $price->price -= 10;

        $time = date_create_from_format('h:i A', $request->time);

        if (!Auth::check()) {
            session([
                'after_login' => [
                    'url' => '/incity/add',
                    'ajax' => true,
                    'method' => 'POST',
                    'params' => [
                        'from_region'     => $request->from_region,
                        'to_region'       => $request->to_region,
                        'from_address'    => $request->from_address,
                        'to_address'      => $request->to_address,
                        'setfor_name'     => $request->setfor_name,
                        'setfor_phone'    => $request->setfor_phone,
                        'date'            => $request->date,
                        'return_date'     => $request->return_date,
                        'time'            => $request->time,
                        'return_time'     => $request->return_time,
                        'trip_type'       => $request->trip_type,
                        'price'           => $request->price,
                        'promo_code'      => $request->promo_code,
                        'use_10EGP_credit'=> $request->use_10EGP_credit,
                        'passenger_notes' => $request->passenger_notes
                    ],
                ]
            ]);
            return Respond::authenticationError();
        }
        if (Auth::user()->phone_verified == '0') {
          session([
              'after_login' => [
                  'url' => '/incity/add',
                  'ajax' => true,
                  'method' => 'POST',
                  'params' => [
                      'from_region'     => $request->from_region,
                      'to_region'       => $request->to_region,
                      'from_address'    => $request->from_address,
                      'to_address'      => $request->to_address,
                      'setfor_name'     => $request->setfor_name,
                      'setfor_phone'    => $request->setfor_phone,
                      'date'            => $request->date,
                      'return_date'     => $request->return_date,
                      'time'            => $request->time,
                      'return_time'     => $request->return_time,
                      'trip_type'       => $request->trip_type,
                      'price'           => $request->price,
                      'promo_code'      => $request->promo_code,
                      'use_10EGP_credit'=> $request->use_10EGP_credit,
                      'passenger_notes' => $request->passenger_notes
                  ],
                ]
            ]);
            return Respond::emailPhoneNotVerified();
        }

        if (session()->has('after_login'))
          session()->forget('after_login');

        $trip = InCityTrip::create([
            'from_region'   => $request->from_region,
            'to_region'     => $request->to_region,
            'datetime'      => $request->date .' ' . $time->format('H:i:s'),
            'from_address'  => $request->from_address,
            'to_address'    => $request->to_address,
            'client_id'     => Auth::user()->id,
            'driver_id'     => null,
            'setfor_name'     => $request->setfor_name,
            'setfor_phone'    => $request->setfor_phone,
            'trip_type'     => $request->trip_type,
            'request_type'     => $request->request_type ?? 'self',
            'price'         => $price ? $price->price : 0,
        ]);

        /*$response = app('App\Http\Controllers\Payment\FawryController')->pay([
                        'user_id'   => Auth::user()->id,
                        'mobile'    => Auth::user()->phone,
                        'amount'    => $trip->price,
                        'org_code'  => 68,
                        'trip_id'   => $trip->id,
                        'trip_type' => 'incity'
                    ]);

        if ($response->status() == 200) {
            return Respond::success([
                'trip' => $trip,
                'url' => '/incity/success'
            ]);
        } else {
            return response(['message' => 'Trip Added But Payment Faild'], 500);
        }*/

        NotificationService::notifyAdmins('new incity trip', 'a new inCity trip was added, act accordingly');
        return Respond::success([
            'trip' => $trip,
            'url' => '/incity/success'
        ]);
    }

    /**
     * calculate price of a certain route
     * @param  Request $request
     * @return JSON             price amount and info
     */
    public function price(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_region' => 'required',
            'to_region' => 'required',
            'going_and_comingback' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $price = InCityPricing::where('from_region', $request->from_region)
            ->where('to_region', $request->to_region)
            ->where('trip_type', $request->going_and_comingback)
            ->first();
        return Respond::success([
            'price' => $price ? $price->price : 0
        ]);
    }

    /**
     * Apply Promo
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function promo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            /*'from_region' => 'required',
            'to_region' => 'required',*/
            'promo_code' => 'required|exists:incity_promo_codes_3252',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $promo = InCityPromoCode::where('promo_code', $request->promo_code)
                    ->where('code_status', 'live')
                    ->first();
        if ($promo) {
            if ($promo->code_usage == 'one use') {
                $promo->code_status = 'used';
                $promo->save();
            }
            return Respond::success($promo);
        }
        return Respond::clientError('Promo Code Used!');
    }

    /**
     * get all available regions from a certain region
     * @param  Request $request
     * @return JSON             [description]
     */
    public function getAllTos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_region' => 'required',
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $prices = InCityPricing::where('from_region', $request->from_region)
            ->with('to')
            ->get();
        $tempArray = [];
        foreach ($prices as $price)
            $tempArray[] = $price->to;
        $prices = $tempArray;

        return Respond::success($prices);
    }

    public function success()
    {
        return view('pages.incity-success');
    }
    public function calculatePrice($fromRegion, $toRegion, $tripType)
    {
        return InCityPricing::
              where('from_region', $fromRegion)
            ->where('to_region',   $toRegion)
            ->where('trip_type',   $tripType)
            ->first();
    }

    public function myTrips()
    {
        $data['trips'] = InCityTrip::where('client_id', Auth::user()->id)
            ->where('datetime', '>', Carbon::now())
            ->with('captain', 'client')
            ->paginate(15);
        $data['prevTrips'] = InCityTrip::where('client_id', Auth::user()->id)
            ->where('datetime', '<', Carbon::now())
            ->with('captain', 'client')
            ->paginate(15);
        return view('pages.user-pages.my-incity-trips', $data);
    }

    /**
     * Apply InCity Promo Code
     * @param  $promo
     * @param  $price    Initial Price
     * @return Int       Price After Applying Promo Code
     */
    public function applyPromo($promo, $price)
    {
        $promo = InCityPromoCode::where('promo_code', $promo)->first();

        if ($promo->type == 'flat_rate')
        $price -= $promo->amount;
        elseif ($promo->type == 'percent')
        $price -= $price * ( $promo->amount / 100 );

        if ($promo->code_usage == 'one use')
        $promo->code_status = 'used' && $promo->save();
        $promo = $promo->id;

        return $price;

    }
}
