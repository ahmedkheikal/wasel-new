<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Services\Respond;
use Validator;

class AffiliateController extends Controller
{
    /**
     * gets referal code and returns user instance
     *
     * @param $request->referal_code
     *
     * @return User $user
     *
     */
    public function user(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'referal_code' => 'required|string|exists:users_5627,referal_code'
        ]);

        if ($validator->fails())
        return Respond::clientError($validator->errors());

        $user = User::where('referal_code', $request->referal_code)->first();
        if (!$user)
        return Respond::clientError([
            'referal_code' => 'user with this referal code not found'
        ]);

        return Respond::success($user);
    }
}
