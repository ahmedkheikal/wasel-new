<?php

namespace App\Http\Controllers\Payment;

use App\Models\Reservation;
use App\Models\Payment\Fawry;
use GuzzleHttp\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FawryController extends Controller
{
    /**
     * Make a Payment With Fawry
     *
     * @param  array $PaymentData
     * @return \Illuminate\Http\Response
     */
    public function pay(array $PaymentData)
    {
        $user_id    = $PaymentData['user_id'];      # The User How Send a Trip Reservation Request
        $mobile     = $PaymentData['mobile'];       # The User Mobile Number
        $amount     = $PaymentData['amount'];       # The Price of the Trip
        $org_code   = $PaymentData['org_code'];     # The Organization Code
        $trip_id    = $PaymentData['trip_id'];      # The Trip ID
        $trip_type  = $PaymentData['trip_type'];    # The Trip Type [private, economy, incity, express]

        $payment_request_url    = "https://bus.tazcara.com/api/make-payment-request";
        $securityCode           = env('fawry_pay_securityCode', '17b5b69eff5777b0661dc05fdfa1a617');
        $ref_code               = sha1(time() . mt_rand(1, 1000000000));
        $signature              = hash('sha256', $mobile.$amount.$org_code.$securityCode.$ref_code);

        $client = new Client();
        $response = $client->request('GET', $payment_request_url, [
            'query' => [
                'mobile'    => $mobile,
                'amount'    => $amount,
                'org_code'  => $org_code,
                'signature' => $signature,
                'ref_code'  => $ref_code
            ]
        ]);

        $data               = json_decode($response->getBody(), true);
        $responseStatus     = $data['status'];
        $responseMessage    = $data['message'];

        if ($responseStatus == 200) {
            Fawry::create([
                'user_id'   => $user_id,
                'trip_id'   => $trip_id,
                'trip_type' => $trip_type,
                'status'    => 'pending',
                'ref_code'  => $ref_code
            ]);

            return response()->json([
                'message' => 'payment pending'
            ], 200);
        }

        return response()->json([
            // 'message' => 'payment faild: unexpected error occurred!'
            'message' => $responseMessage
        ], 500);
    }

    /**
     * The Fawry Payment Callback
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function callback(Request $request)
    {
        $org_code = 68;
    	$status       = $request->status;
    	$ref_code     = $request->ref_code;
        $securityCode = env('fawry_pay_securityCode', '17b5b69eff5777b0661dc05fdfa1a617');
        $signature    = hash('sha256', $org_code.$securityCode.$ref_code.'PAID');
    	$payment 	  = Fawry::where('status', 'pending')
    						->where('ref_code', $ref_code)
    						->first();

    	if ($signature == $request->signature && $payment) {
            $payment->update([
                'status' => $status
            ]);

            if ($payment->trip_type == 'economy') {
                $user           = $payment->user;
                $trip           = $payment->trip();
                $reservation    = Reservation::where('trip_id', $payment->trip_id)
                                            ->where('user_id', $user->id)
                                            ->first();

                $response = app('App\Http\Controllers\Economy\SMSsEmailsController')->send($reservation, $trip, $user);
            }

    		return response()->json([
    			'message' => 'payment done',
    			'company' => 'waselegypt.com'
    		], 200);
    	}

    	return response()->json([
    		'message' => 'payment faild',
    		'company' => 'waselegypt.com'
    	], 500);
    }

    public function callback_test(Request $request)
    {
        $payment    = Fawry::findOrFail(2);

        if ($payment) {
            /*$payment->status = $status;
            $payment->save();*/

            if ($payment->trip_type == 'economy') {
                $user           = $payment->user;
                $trip           = $payment->trip();
                $reservation    = Reservation::where('trip_id', $payment->trip_id)
                                            ->where('user_id', $user->id)
                                            ->first();
                // dd($user, $trip, $reservation);
                $response = app('App\Http\Controllers\Economy\SMSsEmailsController')->send($reservation, $trip, $user);
                dd($response);
            }

            return response()->json([
                'message' => 'payment done',
                'company' => 'waselegypt.com'
            ], 200);
        }

        return response()->json([
            'message' => 'payment faild',
            'company' => 'waselegypt.com'
        ], 500);
    }
}
