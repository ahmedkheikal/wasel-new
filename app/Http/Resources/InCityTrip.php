<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InCityTrip extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'from_region'       => $this->from_region,
            'to_region'         => $this->to_region,
            'from_address'      => $this->from_address,
            'to_address'        => $this->to_address,
            'datetime'          => $this->datetime,
            'client_id'         => $this->client_id,
            'setfor_name'       => $this->setfor_name,
            'setfor_phone'      => $this->setfor_phone,
            'driver_id'         => $this->driver_id,
            'price'             => $this->price,
            'trip_status'       => $this->trip_status,
            'trip_type'         => $this->trip_type,
            'passenger_notes'   => $this->passenger_notes,
            'created_at'        => $this->created_at,
            'updated_at'        => $this->updated_at
        ];
    }
}
