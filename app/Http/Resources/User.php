<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'username'              => $this->username,
            'email'                 => $this->email,
            'phone_country_code'    => $this->phone_country_code,
            'phone'                 => $this->phone,
            'gender'                => $this->gender,
            'city'                  => $this->city,
            'job_title'             => $this->job_title,
            'account_type'          => $this->account_type,
            'account_status'        => $this->account_status,
            'car_model'             => $this->car_model,
            'car_color'             => $this->car_color,
            'chassis_no'            => $this->chassis_no,
            'profile_picture'       => $this->profile_picture,
            'driving_license'       => $this->driving_license,
            'id_card'               => $this->id_card,
            'user_notes'            => $this->user_notes,
            'created_at'            => $this->created_at,
            'updated_at'            => $this->updated_at,
            'record_status'         => $this->record_status,
            'phone_verified'        => $this->phone_verified,
            'referal_code'          => $this->referal_code,
            'affiliate_of'          => $this->affiliate_of,
            'affiliate_balance'     => $this->affiliate_balance,
            'own_balance'           => $this->own_balance,
            'hold_balance'          => $this->hold_balance,
            'rating'                => $this->rating,
            'starred'               => $this->starred
        ];
    }
}
