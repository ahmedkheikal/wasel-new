<?php

namespace App\Http\Middleware;

use Closure;

class IsDriver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->account_type == 'driver') {
            return $next($request);
        }

        return response()->json([
            'error' => 'Not Allowed Access Page'
        ]);
    }
}
