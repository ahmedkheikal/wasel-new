<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use App\Services\NotificationService;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        return $next($request);

        if ($request->url())
        session([
            'after_login' => [
                'url' => $request->url(),
                'ajax' => false,
                'method' => 'GET',
            ]
        ]);
        return redirect('/login');
    }
}
