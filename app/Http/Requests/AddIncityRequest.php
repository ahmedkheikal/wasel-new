<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddIncityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'from_region'       => 'required',
          'to_region'         => 'required',
          'from_address'      => 'required',
          'to_address'        => 'required',
          'request_type'      => 'required',
          'setfor_name'       => 'nullable',
          'setfor_phone'      => 'nullable',
          'date'              => 'required',
          'return_date'       => 'nullable',
          'time'              => 'required',
          'return_time'       => 'nullable',
          'trip_type'         => 'required',
          'price'             => 'required',
          'promo_code'        => 'nullable',
          'use_50EGP_credit'  => 'nullable',
          'passenger_notes'   => 'nullable',
        ];
    }


    /**
     * Get the messages error from the validation
     *
     * @return array
     */
     public function messages()
     {
         return [
           
         ];
     }
}
