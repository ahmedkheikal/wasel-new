<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\SendEmails',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // NOTE: ERROR: There are no commands defined in the "fawrypay" namespace.
        $schedule->command('fawrypay:cancelPendignFawryPaymentTripsAfterTwoHours')
                 ->everyMinute();
        $schedule->command('trip:cancelUnreservedEconomyFuture')
                ->everyThirtyMinutes();
        $schedule->command('notify:captainAndClientEconomy')
                ->hourly();
        $schedule->command('notify:captainAndClientPrivate')
                ->hourly();
        $schedule->command('send:feedbackRequestPrivate')
                ->hourly();
        $schedule->command('send:feedbackRequestEconomy')
                ->hourly();
        $schedule->command('wallet:activateHolded')
                ->hourly();
        /**
         * Database daily backup using shell script
         * @var Schedule
         */
        $schedule->exec('/home/k64gh2x6q0b6/databaseBackup.sh')
                ->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
