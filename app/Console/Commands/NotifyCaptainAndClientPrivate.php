<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\PrivateTripRequest;
use App\Services\NotificationService;
use Carbon\Carbon;

class NotifyCaptainAndClientPrivate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:captainAndClientPrivate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $privateTrips = PrivateTripRequest::whereRaw("EXTRACT(hour from datetime) = EXTRACT(hour from NOW() + INTERVAL 13 HOUR)")
                ->whereRaw("DATE(datetime) = '". Carbon::now()->format('Y-m-d') ."'")
                // whereRaw('datetime = NOW() + INTERVAL 13 HOUR')
                ->with('passenger', 'reservation.trip.captain')
                ->get();
        foreach ($privateTrips as $request) {
            $captainSms = 'لا تنسى رحلتك الخاصة اليوم من '. $request->from_city . ', ' . $request->from_city_addres . ': ' . $request->pickup_address ."\n\r"
                . ' إلى '. $request->to_city . ', ' . $request->to_city_addres . ': ' . $request->arrival_address ."\n\r"
                . ' الساعة '. (new \DateTime($request->datetime))->format("h:i A") . "\n\r"
                . ' برجاء التواصل مع العميل '. $request->fullname .': '. $request->phone ;
            $captainEmail = 'لا تنسى رحلتك الخاصة اليوم من '. $request->from_city . ', ' . $request->from_city_addres . ': ' . $request->pickup_address ."\n\r"
                . ' إلى '. $request->to_city . ', ' . $request->to_city_addres . ': ' . $request->arrival_address ."<br>"
                . ' الساعة '. (new \DateTime($request->datetime))->format("h:i A") . "<br>"
                . ' برجاء التواصل مع العميل '. $request->fullname .': '. $request->phone ;
            NotificationService::sendSms($request->reservation->trip->captain->phone, $captainSms);
            NotificationService::sendMail($request->reservation->trip->captain->email, 'Private Trip Remider', $captainEmail);

            $clientSms = 'لقد قمت بحجز رحلة خاصة اليوم '. $request->from_city . ', ' . $request->from_city_addres . ': ' . $request->pickup_address ."\n\r"
                    . ' إلى '. $request->to_city . ', ' . $request->to_city_addres . ': ' . $request->arrival_address ."\n\r"
                    . ' الساعة '. (new \DateTime($request->datetime))->format("h:i A") .' برحاء التواصل مع الكابتن '. $request->reservation->trip->captain->username .': '. $request->reservation->trip->captain->phone;
            $clientEmail = 'لقد قمت بحجز رحلة خاصة اليوم '. $request->from_city . ', ' . $request->from_city_addres . ': ' . $request->pickup_address ."\n\r"
                    . ' إلى '. $request->to_city . ', ' . $request->to_city_addres . ': ' . $request->arrival_address ."\n\r"
                    . ' الساعة '. (new \DateTime($request->datetime))->format("h:i A") .' برحاء التواصل مع الكابتن '. $request->reservation->trip->captain->username .': '. $request->reservation->trip->captain->phone;
            NotificationService::sendSms($request->passenger->phone, $clientSms);
            NotificationService::sendMail($request->email, 'Private Trip Remider', $clientEmail);

        }
    }
}
