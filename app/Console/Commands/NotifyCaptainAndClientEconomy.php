<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Trip;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Lang;
use Carbon\Carbon;

class NotifyCaptainAndClientEconomy extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'notify:captainAndClientEconomy';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Notify Captain and client about before reserved economy trip with one hour ';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        // email and sms
        // check if there is an upcoming economy trip
        // at least one hour far from current time
        // then notify captain and customers to contact the captain
        $EcoTripsOneHourFromNow = Trip::whereRaw("EXTRACT(hour from datetime) = EXTRACT(hour from NOW() + INTERVAL 13 HOUR)")
                ->whereRaw("DATE(datetime) = '". Carbon::now()->format('Y-m-d') ."'")
                ->where('trip_status', 'live')
                ->with('captain', 'reservations.user')
                ->whereHas('reservations', function ($query) {
                    $query->where('reservation_status', 'live');
                })
                ->get();
        foreach ($EcoTripsOneHourFromNow as $trip) {
            $captainEmail = '
                لا تنسى رحلتك الاقتصادية من '. $trip->from_city .' إلى '. $trip->to_city .' اليوم، الساعة '. (new \DateTime($trip->departure_time))->format('h:i A') .' <br>
                برجاء التواصل مع العملاء المسجلين حجزهم بالرحلة
                ';
            // NotificationService::sendMail('ahmed.k.heikal@gmail.com', "trip reminder", json_encode($trip));

            NotificationService::sendMail($trip->captain->email, 'رحلة اقتصادية خلال ساعات', $captainEmail);
            $captainSms = 'لا تنسى رحلتك الاقتصادية من '. Lang::get("db." . $trip->from_city) .' إلى '. Lang::get("db." . $trip->to_city) .' اليوم، الساعة '. (new \DateTime($trip->departure_time))->format('h:i A') .' برجاء التواصل مع العملاء المسجلين حجزهم بالرحلة';
            NotificationService::sendSms($trip->captain->phone, $captainSms);

            foreach ($trip->reservations as $reservation) {
                $seats = $reservation->seat_1 == '1' ? 'الكرسي الأمامي' : '';
                $seats .= $reservation->seat_2 == '1' ? 'الكرسي الخلفي الأيمن' : '';
                $seats .= $reservation->seat_3 == '1' ? 'الكرسي الخلفي الأيسر' : '';
                $passengerEmail = '
                    لقد قمت بالحجز في رحلة '. Lang::get("db." . $trip->from_city) .' - '. Lang::get("db." . $trip->to_city) .' اليوم، الساعة '. (new \DateTime($trip->datetime))->format('h:i A') .' <br>
                    برجاء التواصل مع الكابتن '. $trip->captain->username .':
                    <a href="tel:'. $trip->captain->phone .'">
                        '. $trip->captain->phone .'
                    </a>
                ';
                $passengerSms = 'لقد قمت بالحجز في رحلة '. Lang::get("db." . $trip->from_city) .' - '. Lang::get("db." . $trip->to_city) .' اليوم، الساعة '. (new \DateTime($trip->datetime))->format('h:i A') .' برجاء التواصل مع الكابتن '. $trip->captain->username .': '. $trip->captain->phone;
                NotificationService::sendMail($reservation->user->email, 'رحلة اقتصادية خلال ساعات', $passengerEmail);
                NotificationService::sendSms($reservation->user->phone, $passengerSms);
            }
        }
    }
}
