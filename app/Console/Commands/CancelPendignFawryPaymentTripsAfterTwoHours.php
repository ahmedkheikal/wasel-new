<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Payment\Fawry;
use App\Models\Reservation;

use Illuminate\Console\Command;

class CancelPendignFawryPaymentTripsAfterTwoHours extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fawrypay:cancelPendignFawryPaymentTripsAfterTwoHours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel pending Fawry Payment Trips After Two Hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current = Carbon::now();

        $payment = Fawry::where('status', 'pending')->get();

        foreach ($payment as $pay) {

            $created_at_days    = $pay->created_at->day;
            // The Hourse between current time and created_at time
            $hours  = intval($pay->created_at->diffInHours($current));

            if ( ($current->day - $created_at_days) > 0) {
                $reservation = Reservation::where('trip_id', $pay->trip_id)
                                          ->where('user_id', $pay->user_id)->first();
                if ($reservation) {
                    $reservation->reservation_status = 'canceled';
                    $reservation->save();

                    $trip = $pay->trip();

                    $seat = '';
                    if ($reservation->seat_1 == 1)
                    $trip->seat_1 = 0;
                    if ($reservation->seat_2 == 1)
                    $trip->seat_2 = 0;
                    if ($reservation->seat_3 == 1)
                    $trip->seat_3 = 0;

                    $trip->save();

                    $pay->update([
                        'status' => 'UNPAID'
                    ]);
                }
            }

            if ($hours >= 2) {
                $reservation = Reservation::where('trip_id', $pay->trip_id)
                                          ->where('user_id', $pay->user_id)->first();

              if ($reservation) {

                  $reservation->reservation_status = 'canceled';
                  $reservation->save();

                  $seat = '';
                  if ($reservation->seat_1 == 1)
                  $seat = 'seat_1';
                  if ($reservation->seat_2 == 1)
                  $seat = 'seat_2';
                  if ($reservation->seat_3 == 1)
                  $seat = 'seat_3';

                  $trip = $pay->trip();
                  $trip->$seat = 0;
                  $trip->save();

                  $pay->update([
                      'status' => 'UNPAID'
                  ]);
              }

            }
        }
    }
}
