<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Models\WalletTransaction;

class WalletValidate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallet:validate {userId : user id to check}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sums up wallet transactions for user and validates his balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::find($this->argument('userId'));
        $walletTransactions = WalletTransaction::where('user', $user->id)
                ->get();
        $ownBalance = 0;
        $affiliateBalance = 0;
        $holdBalance = 0;
        foreach ($walletTransactions as $transaction) {
            if ($transaction->balance_channel == 'own')
            $ownBalance += $transaction->amount;

            if ($transaction->balance_channel == 'affiliate')
            $affiliateBalance += $transaction->amount;

            if ($transaction->balance_channel == 'hold')
            $holdBalance += $transaction->amount;

        }
        if ($user->own_balance != $ownBalance) {
            echo "Own balance of user = " . $user->own_balance . "\n" .
            "while transaction total evaluates to: " . $ownBalance ."\n";
            $user->own_balance = $ownBalance;
        } elseif ($user->affiliate_balance != $affiliateBalance) {
            echo "Affiliate balance of user = " . $user->affiliate_balance . "\n" .
            "while transaction total evaluates to: " . $affiliateBalance ."\n";
            $user->affiliate_balance = $affiliateBalance;
        } elseif ($user->hold_balance != $holdBalance) {
            echo "Hold balance of user = " . $user->hold_balance . "\n" .
            "while transaction total evaluates to: " . $holdBalance ."\n";
            $user->hold_balance = $holdBalance;
        } else {
            echo "Everything is okay \n";
        }
        $user->save();

    }
}
