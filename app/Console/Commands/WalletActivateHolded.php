<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Reservation;
use App\Models\WalletTransaction;
use App\Models\PrivateTripRequest;
use App\User;
use Carbon\Carbon;

class WalletActivateHolded extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallet:activateHolded';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Holded funds to own or affiliate account when the desired condition comes true';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     * Conditions:
     *      1. affiliateCredit
     *      2. partnerEcoPromoReward
     *      3. partnerPriPromoReward
     *      4. partnerPriReward
     *      5. partnerEcoReward
     *
     * @return mixed
     */
    public function handle()
    {
        $holdedTransactions = WalletTransaction::where('balance_channel', 'hold')->get();

        foreach ($holdedTransactions as $transaction) {
            $transaction->subject = explode(':', $transaction->subject);
            if ($transaction->subject[0] == 'affiliateCredit') {
                // code...
            } elseif ($transaction->subject[0] == 'partnerEcoPromoReward' || $transaction->subject[0] == 'partnerEcoReward') {
                $reservation = Reservation::where('id', $transaction->subject[1])
                        ->with('trip')
                        ->first();
                if (count($reservation)) {
                    if ($reservation->trip->datetime < Carbon::now()) {
                        if ($reservation->trip->trip_status == 'live' && $reservation->reservation_status == 'live') {
                            $user = User::find($transaction->user);
                            $transaction->balance_channel = 'own';
                            $transaction->subject = implode(':', $transaction->subject);
                            $transaction->save();

                            $user->hold_balance -= $transaction->amount;
                            $user->own_balance += $transaction->amount;
                            $user->save();
                        } else {
                            $user = User::find($transaction->user);
                            $user->hold_balance -= $transaction->amount;
                            $user->save();

                            $transaction->delete();
                        }
                    }
                } else {
                    $user = User::find($transaction->user);
                    $user->hold_balance -= $transaction->amount;
                    $user->save();

                    $transaction->delete();
                }
            } elseif ($transaction->subject[0] == 'partnerPriReward' || $transaction->subject[0] == 'partnerPriPromoReward') {
                $request = PrivateTripRequest::where('id', $transaction->subject[1])
                                             ->first();
                 if (count($request)) {
                     if ($request->datetime < Carbon::now()) {
                         if ($request->request_status == 'scheduled') {
                             $user = User::find($transaction->user);
                             $transaction->balance_channel = 'own';
                             $transaction->subject = implode(':', $transaction->subject);
                             $transaction->save();

                             $user->hold_balance -= $transaction->amount;
                             $user->own_balance += $transaction->amount;
                             $user->save();
                             echo "activated transaction" . $transaction->id ."\n";
                         } else {
                             $user = User::find($transaction->user);
                             $user->hold_balance -= $transaction->amount;
                             $user->save();

                             $transaction->delete();
                             echo "deleted transaction" . $transaction->id . "\n";
                         }
                     }
                 } else {
                     $user = User::find($transaction->user);
                     $user->hold_balance -= $transaction->amount;
                     $user->save();

                     $transaction->delete();
                     echo "deleted transaction" . $transaction->id . "\n";
                 }
            }
        }
    }
}
