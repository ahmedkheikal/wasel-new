<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\PrivateTripRequest;
use App\Services\NotificationService;
use Carbon\Carbon;

class SendFeedbackRequestPrivate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:feedbackRequestPrivate';
    /**

     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remiding customers to rate their economy trips after they are Successful';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $requestssFromFiveHours = PrivateTripRequest::where('request_status', 'scheduled')
                ->with('reservation.trip.captain', 'passenger')
                // ->where('date', date('Y-m-d'))
                ->whereRaw("DATE(datetime) = '". Carbon::now()->format('Y-m-d') ."'")
                ->whereRaw('extract(hour from datetime) = extract(hour from NOW() + INTERVAL 4 HOUR)')
                ->get();
        foreach ($requestssFromFiveHours as $reservation) {
            $sms = 'ساعدنا على تطوير الخدمة بتقييم رحلتك السابقة مع الكابتن '. $reservation->reservation->trip->captain->username . ' من خلال ' . "\n\r".
                'https://www.waselegypt.com/private-trips/request/' . $reservation->id;
            NotificationService::sendSms($reservation->passenger->phone, $sms);
        }

    }
}
