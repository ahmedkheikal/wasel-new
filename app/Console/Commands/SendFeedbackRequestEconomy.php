<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Reservation;
use App\Services\NotificationService;
use Carbon\Carbon;

class SendFeedbackRequestEconomy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:feedbackRequestEconomy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $reservationsFromFiveHours = Reservation::where('reservation_status', 'live')
                ->with('trip.captain', 'user')
                ->whereHas('trip', function ($query) {
                    $query
                    // ->whereRaw('datetime = NOW() + INTERVAL 4 HOUR')
                    ->whereRaw("DATE(datetime) = '". Carbon::now()->format('Y-m-d') ."'")
                    ->whereRaw('extract(hour from datetime) = extract(hour from NOW() + INTERVAL 4 HOUR)');
                })
                ->get();
        foreach ($reservationsFromFiveHours as $reservation) {
            $sms = 'ساعدنا على تطوير الخدمة بتقييم رحلتك السابقة مع الكابتن '. $reservation->trip->captain->username . ' من خلال ' . "\n\r".
                'https://www.waselegypt.com/my-trips/reservation/' . $reservation->id;
            NotificationService::sendSms($reservation->user->phone, $sms);
        }

    }
}
