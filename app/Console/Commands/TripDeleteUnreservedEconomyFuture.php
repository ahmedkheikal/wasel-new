<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Trip;
use App\Services\NotificationService;
use Carbon\Carbon;

class TripDeleteUnreservedEconomyFuture extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trip:cancelUnreservedEconomyFuture';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel Empty economy Trips One Hour Far from now ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $update = Trip::whereRaw('`datetime` <= NOW() + INTERVAL 10 HOUR') // considering server timezone issue
            ->where('datetime', '>', Carbon::now())
            ->where('seat_1', '0')
            ->where('seat_2', '0')
            ->where('seat_3', '0')
            ->where('trip_status', 'live')
            ->update([
                'trip_status' => 'canceled'
            ]);
    }
}
