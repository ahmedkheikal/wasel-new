<?php

namespace App\Services;

use App\Models\Trip;
use App\Models\Pricing;
use App\Models\PromoCode;
use App\Models\Reservation;
use App\Models\PrivatePromoCode;
use App\Models\WalletTransaction;
use App\Models\PrivateTripRequest;
use App\Services\Respond;
use App\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class TripService
{
    public static function getEconomyTrips($from, $to, $status = 'live')
    {
        $trips = Trip::where('from_city',  $from)
              ->where('to_city', $to)
              ->where('datetime', '>', Carbon::now())
              ->where(function ($query) {
                  $query->where('seat_1', '=', '0')
                      ->orWhere('seat_2', '=', '0')
                      ->orWhere('seat_3', '=', '0');
              })
              ->where('trip_status', $status)
              ->with('captain', 'reservations');
              // ->get();

        return $trips;
    }

    /**
     *
     * get available economy trips for reservations
     *
     * @param String $from
     * @param String $to
     *
     * @return Collection $trips
     *
     */
    public static function getAvailableEconomyTrips($from, $to)
    {
        $trips = Trip::where('from_city',  $from)
              ->where('to_city', $to)
              ->where('datetime', '>', Carbon::now())
              ->where('trip_status', 'live')
              ->where(function ($query) {
                  $query->where('seat_1', '0')
                        ->orWhere('seat_2', '0')
                        ->orWhere('seat_3', '0');
              })
              ->with('captain')
              ->get();

        return $trips;
    }

    /**
     *
     * recieves promo code and price and returns price after applying promo code
     * @param String $promo_code
     * @param int $price
     *
     * @return int $price
     *
     */
    public static function applyEcoPromo(int $price, string $promo_code)
    {
        $promo = PromoCode::where('promo_code', $promo_code)->where('code_status', 'live')->first();
        if ($promo) {
            if ($promo->type == 'flat_rate')
            $price -= intval($promo->amount);
            elseif ($promo->type == 'percent')
            $price -= $price * ( intval($promo->amount) / 100 );

            if ($promo->code_usage == 'one use') {
                $promo->code_status = 'used' ;
                $promo->save();
            }
        } else {
            return $price;
        }

        return $price;
    }
    /**
     * @param $trip_id
     * @param $seat_1
     * @param $seat_2
     * @param $seat_3
     *
     * make reservaion and update trip seats
     *
     * @return query result
     *      true, false
     */
    public static function makeEcoReservaion($trip_id, $seat_1, $seat_2, $seat_3, $promo, $reservation_type, $setfor_name, $setfor_phone)
    {
        $trip = Trip::find($trip_id);
        $price = Pricing::where('from_city', $trip->from_city)->where('to_city', $trip->to_city)->first();

        if (!$price)
        return false;
        $price->price_per_ticket = intval($price->price_per_ticket);

        // checking if attempted seats are not already reserved
        if ($seat_1 == '1' && $trip->seat_1 == '1')
        return false;
        if ($seat_2 == '1' && $trip->seat_2 == '1')
        return false;
        if ($seat_3 == '1' && $trip->seat_3 == '1')
        return false;

        $totalPrice = 0;
        // setting total price
        if ($seat_1 == '1')
        $totalPrice += $price->price_per_ticket;
        if ($seat_2 == '1')
        $totalPrice += $price->price_per_ticket;
        if ($seat_3 == '1')
        $totalPrice += $price->price_per_ticket;

        $promo = null !== $promo ? $promo : '';
        $totalPrice = self::applyEcoPromo($totalPrice, $promo);

        if ($reservation_type == 'self' || $reservation_type == null) {
            $reservation_type = 'self';
            $setfor_name = null;
            $setfor_phone = null;
        } else {
            $setfor_phone = str_replace(' ', '', $setfor_phone);
        }

        $reservation = Reservation::create([
            'trip_id' => $trip_id,
            'user_id' => Auth::user()->id,
            'seat_1' => $seat_1,
            'seat_2' => $seat_2,
            'seat_3' => $seat_3,
            'reservation_date' => Carbon::now(),
            'pickup_address' => $trip->pickup_address,
            'arrival_address' => $trip->arrival_address,
            'promo_code' => $promo,
            'price' => $totalPrice,
            'reservation_type' => $reservation_type,
            'setfor_name' => $setfor_name,
            'setfor_phone' => $setfor_phone,
        ]);

        if (!$reservation)
        return false;

        if ($seat_1 == '1' && $trip->seat_1 == '0')
        Trip::where('id', $trip_id)->update(['seat_1' => '1']);

        if ($seat_2 == '1' && $trip->seat_2 == '0')
        Trip::where('id', $trip_id)->update(['seat_2' => '1']);

        if ($seat_3 == '1' && $trip->seat_3 == '0')
        Trip::where('id', $trip_id)->update(['seat_3' => '1']);

        return $reservation;
    }

    public static function egyptCurrentTimeStamp()
    {
        $tz = 'Africa/Cairo';
        $timestamp = time();
        $dt = new \DateTime("now", new \DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        return $dt->format('Y-m-d H:i:s');
    }

    public static function getEcoPrice($id)
    {
        $reservations = Reservation::where('trip_id', $id)->where('reservation_status', 'live')->get();
        $price = 0;
        foreach ($reservations as $reservation) {
            $price += intval($reservation->price);
        }
        return $price;
    }

    public function getToCities(string $from)
    {
        $cities = PrivatePricing::
        whereHas('fromCity', function ($query) use ($request) {
            $query->where('city_name', $from);
        })
        ->select('to_city')
        ->with('toCity')
        ->get();

        $responseArray = [];
        foreach ($cities as $key => $city) {
            $city->toCity->selected = session('to_city') == $city->toCity->city_name ? 1 : 0;
            $city->toCity->city_name_ar = Lang::get("db.{$city->toCity->city_name}");
            $responseArray[] = $city->toCity;
        }

        // sorting array using flip sort
        $exitLoop = false;
        while (!$exitLoop) {
            $exitLoop = true;
            for ($i=0; $i < count($responseArray) - 1; $i++) {
                if ($responseArray[$i]->order > $responseArray{$i + 1}->order) {
                    $elementOne = $responseArray[$i];
                    $elementTwo = $responseArray[$i + 1];

                    $responseArray[$i] = $elementTwo;
                    $responseArray[$i + 1] = $elementOne;
                    $exitLoop = false;
                }
            }
        }
        return $responseArray;
    }

    public static function credtiPartnerForPrivate(PrivateTripRequest $request, $promo_id)
    {
        if (Auth::user()->account_type == 'partner') {
            $commition = round($request->price * 5/100);
            WalletTransaction::create([
                'user' => Auth::user()->id,
                'amount' => $commition,
                'balance_channel' => 'hold',
                'description' => $commition .' ج.م عمولة طلب رحلة خاصة من '. Lang::get('db.' . $request->from_city) .' إلى '. Lang::get('db.' . $request->to_city)
                    .' يوم '. (new \DateTime($request->datetime))->format('d ') . Lang::get('language.' . (new \DateTime($request->datetime))->format('M')) ,
                'subject' => 'partnerPriReward:' . $request->id,
            ]);
            $newBalance = intval(Auth::user()->hold_balance) + $commition;
            $UpdateBalance = User::where('id', Auth::user()->id)
                ->update([
                    'hold_balance' => $newBalance
                ]);
            return $UpdateBalance;
        } elseif (Auth::user()->account_type == 'passenger') {
            $promo_code = PrivatePromoCode::where('id', $promo_id)
                    ->where('code_status', 'live')
                    ->whereNotNull('partner')
                    ->with('Partner')
                    ->first();
            if ($promo_code)
            if ($promo_code->partner !== null &&  $promo_code->partner !== '') {
                $commition = round($request->price * 5/100);
                WalletTransaction::create([
                    'user' => $promo_code->Partner->id,
                    'amount' => $commition,
                    'balance_channel' => 'hold',
                    'description' => $commition .' ج.م عن طلب أحد العملاء لرحلة خاصة باستخدام البرومو كود الخاص بك',
                    'subject' => 'partnerPriPromoReward:' . $request->id,
                ]);
                $newBalance = $promo_code->Partner->hold_balance + $commition;
                $UpdateBalance = User::where('id', $promo_code->Partner->id)
                ->update([
                    'hold_balance' => $newBalance
                ]);
                return $UpdateBalance;
            }
        }
    }

    public static function credtiPartnerForEconomy(Reservation $reservation, $promo_code)
    {
        $reservation = Reservation::where('id', $reservation->id)
                ->with('trip')
                ->first();

        if (Auth::user()->account_type == 'partner') {
            $commition = round($reservation->price * 5/100);
            WalletTransaction::create([
                'user' => Auth::user()->id,
                'amount' => $commition,
                'balance_channel' => 'own',
                'description' => $commition .' ج.م عمولة طلب رحلة اقتصادية من '. Lang::get('db.' . $reservation->trip->from_city) .' إلى '. Lang::get('db.' . $reservation->trip->to_city)
                    .' يوم '. (new \DateTime($reservation->trip->datetime))->format('d ') . Lang::get('language.' . (new \DateTime($reservation->trip->datetime))->format('M')) ,
                'subject' => 'partnerEcoReward:' . $reservation->id,
            ]);
            $newBalance = intval(Auth::user()->hold_balance) + $commition;
            $UpdateBalance = User::where('id', Auth::user()->id)
                ->update([
                    'hold_balance' => $newBalance
                ]);
            return $UpdateBalance;
        } elseif (Auth::user()->account_type == 'passenger') {
            $commition = round($reservation->price * 5/100);
            $promo_code = PromoCode::where('promo_code', $promo_code)
                    ->where('code_status', 'live')
                    ->whereNotNull('partner')
                    ->with('Partner')
                    ->first();
            if ($promo_code)
            if ($promo_code->partner !== null &&  $promo_code->partner !== '') {
                WalletTransaction::create([
                    'user' => $promo_code->partner,
                    'amount' => $commition,
                    'balance_channel' => 'hold',
                    'description' =>  $commition .' ج.م عن حجز أحد العملاء لرحلة اقتصادية باستخدام البرومو كود الخاص بك',
                    'subject' => 'partnerEcoPromoReward:' . $reservation->id,
                ]);
                $newBalance = $promo_code->Partner->hold_balance + $commition;
                $UpdateBalance = User::where('id', $promo_code->Partner->id)
                    ->update([
                        'hold_balance' => $newBalance
                    ]);
            }
        }
    }
}
