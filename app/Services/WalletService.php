<?php

namespace App\Services;

use App\User;
use App\Models\WalletTransaction;

class WalletService
{

    public static function addToWallet(User $user, float $amount, string $balance_channel, string $subject, string $description)
    {
        $new_transaction = WalletTransaction::create([
            'user' => $user->id,
            'amount' => $amount,
            'balance_channel' => str_replace('_balance', '', $balance_channel),
            'subject' => $subject,
            'description' => $description,
        ]);

        $newBalance = $user[$balance_channel] + $amount;

        if ($new_transaction)
        $user[$balance_channel] = $newBalance;
        else
        return false;


        if ($new_transaction && $user->save())
        return true;

        return false;
    }

}
