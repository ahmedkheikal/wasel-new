<?php

namespace App\Services;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Lang;

class Respond
{
    public static function success($message = 'success')
    {
        return Response::json([
            'code' => '200',
            'response' => $message
        ]);
    }
    public static function successNoNotification($message = 'تمت العملية بنجاح مع العلم أنه قد يتعذر إرسال رسالة التأكيد')
    {
        return Response::json([
            'code' => '200',
            'response' => $message
        ]);
    }
    public static function clientError($message = 'Unprocessable Entity')
    {
        return Response::json([
            'code' => '422',
            'response' => $message
        ]);
    }
    public static function authenticationError($message = 'You are not authenticated')
    {
        return Response::json([
            'code' => '401',
            'response' => $message
        ]);
    }
    public static function emailPhoneNotVerified($message = 'Email / Phone not verified')
    {
        return Response::json([
            'code' => '403',
            'response' => $message
        ]);
    }
    public static function fatalError($message = 'Server Error')
    {
        return Response::json([
            'code' => '500',
            'response' => $message
        ]);
    }
}
