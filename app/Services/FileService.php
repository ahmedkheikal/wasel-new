<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;

class FileService
{
    public static function uploadFilePublicly($pathInPublic, UploadedFile $file)
    {
        if (Auth::check()) {
            // code...
            $firstPart = Auth::user()->id;
        } else {
            $firstPart = str_random(5);
        }
        $folderPath = public_path($pathInPublic); // making folder path
        if (!is_dir($folderPath))  // check if dir exist
        mkdir($folderPath, 0777); // create directory if it does not exist

        $content = file_get_contents($file->getRealPath()); // getting file contents
        // generating filename randomly
        $filename = $firstPart . '__id__' . str_random(6) . '.' . $file->getClientOriginalExtension();
        $finalPath = "$folderPath/$filename";
        file_put_contents($finalPath, $content); // putting file contents

        // returning path after the public folder
        return "$pathInPublic/$filename";
    }
}
