<?php

namespace App\Services;

use App\Models\City;
use App\Models\Region;
use App\Models\PrivatePricing;
use App\Services\Respond;
use Illuminate\Support\Lang;

class PrivateTripService
{
    /**
    * gets destination and returns base price for private trip (going, goingAndComingback)
    *
    * @param $from_city
    * @param $to_city
    *
    * @return Collection $price
    *
    */
    public static function getTotalPrice($from_city, $to_city, $from_region, $to_region, $trip_type)
    {
        $from_city = City::where('city_name', $from_city)->first();
        $to_city = City::where('city_name', $to_city)->first();

        $base_price = PrivatePricing::where('from_city', $from_city->id)
                                    ->where('to_city', $to_city->id)
                                    ->where('trip_type', $trip_type)
                                    ->first();

        $additional_amount_from = Region::where('region', $from_region)->first();
        $additional_amount_to = Region::where('region', $to_region)->first();

        if (! ($base_price && $additional_amount_from && $additional_amount_to)   )
        return Respond::clientError(Lang::get('language.pricingRuleNotFound'));

        if ($base_price->relation == 'sn') {
            $additional_amount_from = $additional_amount_from->north_amount;
            $additional_amount_to = $additional_amount_to->south_amount;
        } else {
            $additional_amount_from = $additional_amount_from->south_amount;
            $additional_amount_to = $additional_amount_to->north_amount;
        }

        return $response = [
            'base_price' => intval($base_price->price),
            'additional_amount_from' => intval($additional_amount_from),
            'additional_amount_to' => intval($additional_amount_to),
        ];
    }

}
