<?php

namespace App\Services;

use App\User;
use App\Models\Trip;
use App\Models\PrivateTripRequest;

class FeedbackService
{
    public static function getCaptainRatingFromEconomy(User $user)
    {
        $captainTrips = Trip::where('driver_id', $user->id)
            ->where('trip_status', 'live')
            ->with('reservations.feedback')->get();

        $totalRating = 0;
        $countRating = 0;
        foreach ($captainTrips as $trip) {
            foreach ($trip->reservations as $reservation) {
                if ($reservation->feedback) {
                    $totalRating += $reservation->feedback->overall_rating;
                    $countRating++;
                }
            }
        }

        if ($countRating > 1)
        return $totalRating / $countRating;
        elseif ($countRating == 1)
        return $totalRating;
        return 0;
    }
    public static function getCaptainRatingFromPrivate(User $user)
    {
        $captainTrips = Trip::where('driver_id', $user->id)
                            ->where('trip_status', 'private')
                            ->with('privateReservation.request.feedback')->get();

        $totalRating = 0;
        $countRating = 0;
        foreach ($captainTrips as $trip) {
            if ($trip->privateReservation && $trip->privateReservation->request->feedback) {
                $totalRating += $trip->privateReservation->request->feedback->overall_rating;
                $countRating++;
            }
        }
        if ($countRating > 1)
        return $totalRating / $countRating;
        elseif ($countRating == 1)
        return $totalRating ;
        return 0;
    }

    public static function updateCaptainRating(User $user)
    {
        $ratingPrivate = self::getCaptainRatingFromPrivate($user);
        $ratingEconomy = self::getCaptainRatingFromEconomy($user);
        if (!$ratingPrivate)
        $rating = $ratingEconomy;
        elseif (!$ratingEconomy)
        $rating = $ratingPrivate;
        else
        $rating = ($ratingPrivate + $ratingEconomy) / 2;
        return User::where('id', $user->id)->update([
            'rating' => $rating
        ]);
    }
}
