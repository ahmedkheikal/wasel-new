<?php
namespace App\Services;

use GuzzleHttp\Client;
use Guzzle\Http\Exception\ServerErrorResponseException;
use Psy\Exception\Exception;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use Log;

class PaymentService
{
    protected $first_name;
    protected $last_name;
    protected $email;
    protected $phone;
    protected $building;
    protected $street;
    protected $city;

    protected $baseUrl = 'https://accept.paymobsolutions.com';
    protected $auth_token;
    protected $merchant_id;
    protected $order_id;
    protected $payment_key;
    protected $amount_cents;

    function __construct($args)
    {
        $this->first_name = $args['first_name'];
        $this->last_name = $args['last_name'];
        $this->email = $args['email'];
        $this->phone = $args['phone'];
        $this->building = $args['building'];
        $this->street = $args['street'];
        $this->city = $args['city'];
        $this->amount_cents = $args['amount_cents'];
    }

    public function authenticate()
    {
        try {
            $client = new Client();
            $response = $client->post($this->baseUrl . '/api/auth/tokens', [
                'headers' => [
                    'content-type' => 'application/json'
                ],
                'json' => [
                    'api_key' => env('accept_api_key')
                ],
            ]);
            $data = json_decode($response->getBody());
            $this->auth_token = $data->token;
            $this->merchant_id = $data->profile->id;
            return true;
        } catch (ServerException $e) {
            Log::error($e);
            return false;
        } catch (ServerErrorResponseException $e) {
            Log::error($e);
            return false;
        } catch (ClientException $e) {
            Log::error($e);
            return false;
        } catch (Exception $e) {
            Log::error($e);
            return false;
        }

    }
    public function requestOrder()
    {
        try {
            if ($this->authenticate()) {
                $client = new Client();
                $response = $client->post($this->baseUrl . '/api/ecommerce/orders', [
                    'headers' => [
                        'content-type' => 'application/json'
                    ],
                    'json' => [
                        "auth_token" => $this->auth_token, // auth token obtained from step1
                        "delivery_needed" => "false",
                        "merchant_id" => $this->merchant_id,      // merchant_id obtained from step 1
                        "amount_cents" => $this->amount_cents,
                        "currency" => "EGP",
                        "merchant_order_id" => str_random(4) ,  // unique alpha-numerice value, example=> E6RR3
                        "items" => [],
                        "shipping_data" => [      // Mandatory if the delivery is needed
                            "building" => $this->building,
                            "street" => $this->street,
                            "city" => $this->city,
                            "first_name" => $this->first_name,
                            "last_name" => $this->last_name,
                            "phone_number" => $this->phone,
                            "email" => $this->email,
                            "country" => "EG",
                            "apartment" => "EG",
                            "floor" => "EG",
                        ]
                    ],
                ]);
                $order = json_decode($response->getBody());
                $this->order_id = $order->id;
                $this->requestPaymentKey();

                return json_decode($response->getBody());
                return true;
            } else {
                return false;
            }

        } catch (ServerException $e) {
            Log::error($e);
            return false;
        } catch (ServerErrorResponseException $e) {
            Log::error($e);
            return false;
        } catch (ClientException $e) {
            echo $e->getMessage();
            Log::error($e);
            return false;
        } catch (Exception $e) {
            Log::error($e);
            return false;
        }
    }

    public function requestPaymentKey()
    {
        try {
            $client = new Client();
            $response = $client->post($this->baseUrl . '/api/acceptance/payment_keys', [
                'headers' => [
                    'content-type' => 'application/json'
                ],
                'json' => [
                    "auth_token" => $this->auth_token, // auth token obtained from step1
                    "amount_cents" => "100",
                    "expiration" => 36000,
                    "order_id" => $this->order_id,    // id obtained in step 2
                    "billing_data" => [
                        "building" => $this->building,
                        "street" => $this->street,
                        "city" => $this->city,
                        "first_name" => $this->first_name,
                        "last_name" => $this->last_name,
                        "phone_number" => $this->phone,
                        "email" => $this->email,
                        "country" => "EG",
                        "apartment" => "EG",
                        "floor" => "EG",
                    ],
                    "currency" => "EGP",
                    "integration_id" => 4229,  // card integration_id will be provided upon signing up,
                    "lock_order_when_paid" => "false" // optional field (*)
                ],
            ]);
            $paymentKey = json_decode($response->getBody());
            $this->payment_key = $paymentKey->token;

            return json_decode($response->getBody());
            return true;
        } catch (ServerException $e) {
            Log::error($e);
            return false;
        } catch (ServerErrorResponseException $e) {
            Log::error($e);
            return false;
        } catch (ClientException $e) {
            echo $e->getMessage();
            Log::error($e);
            return false;
        } catch (Exception $e) {
            Log::error($e);
            return false;
        }
    }
    public function getPaymentKey()
    {
        return $this->payment_key;
    }
}
