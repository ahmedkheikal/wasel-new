<?php

namespace App\Services;

/**
 *
 */
class ShippingService
{
    /**
     * [calculatePrice description]
     * @param  string $from       Order Pickup City FUTURE SUPPORT
     * @param  string $to         Order Dropoff city FUTURE SUPPORT
     * @param  string $weight     Actual weight
     * @param  string $type       shipment type [document, gadget, box, other] (specifies calculation parameters)
     * @param  array  $dimentions [length, width, height]
     * @return int                shipping cost
     */
    public static function calculatePrice(string $from, string $to, string $weight, string $type, array $dimentions = [])
    {
        list($weightInt, $unit) =  explode(' ', $weight);

        $base_price = ''; $additional_cost = '';
        if (strtolower($unit) == 'kg') {
            $base_price = 50;
        } elseif (strtolower($unit) == 'g') {
            $weightInt /= 1000;
            $base_price = 50;
        }
        // checking if shipment dimentions are provided
        if (count($dimentions)) {
            $lengthInt = explode(' ', $dimentions['length'])[0]; // , $lengthUnit
            $widthInt = explode(' ', $dimentions['width'])[0]; // , $widthUnit
            $heightInt = explode(' ', $dimentions['height'])[0]; // , $heightUnit
        } else {
            $lengthInt = 0; // $lengthUnit = 'cm';
            $widthInt = 0; // $widthUnit = 'cm';
            $heightInt = 0; // $heightUnit = 'cm';
        }
        try {
            $volWeight = ($lengthInt * $widthInt * $heightInt) / 5000;
        } catch (\Exception $e) {
            $volWeight = 0;
        }


        // if (strtolower($lengthUnit) == 'cm') {
        // } elseif (strtolower($lengthUnit) == 'm') {
        //     $volWeight = ($lengthInt * $widthInt * $heightInt) * 200;
        // }
        if ($volWeight > $weightInt) {
            $additional_cost = ($volWeight * 15) - 15;
            return $base_price + $additional_cost;
        } else {
            $additional_cost = ($weightInt * 15) - 15;
            return $base_price + $additional_cost;
        }
    }
}
