<?php

namespace App\Services;

use GuzzleHttp\Client;
use Guzzle\Http\Exception\ServerErrorResponseException;
use Psy\Exception\Exception;
use App\Models\Feature;
use Httpful\Request;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use Twilio\Rest\Client as TwilioClient;
use Log;
use Twilio\Exceptions\RestException;
use App\User;
use SoapClient;

class NotificationService
{
    public static function sendSms($phone, $message) // smsmisr
    {
        $sendSmsFeature = Feature::where('feature_name', 'sendSMS')->first();
        if ($sendSmsFeature->feature_status == 'enabled')
        self::VLsms($phone, $message);
    }

    public static function smsMisrSms($phone, $message)
    {
        $sendSmsFeature = Feature::where('feature_name', 'sendSMS')->first();

        $user = User::where('phone', $phone)->first();
        if ($user) {
            if ($user->phone_country_code == '20' || $user->phone_country_code == '2' || $user->phone_country_code == 'EG') {
                if (strlen($phone) == 11) {
                    if (!preg_match('/01[0-9]+$/', $phone))
                    return false;
                    $phone = '2' . $phone;
                }
                elseif (strlen($phone) == 10) {
                    if (!preg_match('/1[0-9]+$/', $phone))
                    return false;
                    $phone = '20' . $phone;
                }
            }
        } elseif (strlen($phone) == 11) {
            if (!preg_match('/01[0-9]+$/', $phone))
            return false;
            $phone = '2' . $phone;
        } elseif (strlen($phone) == 10) {
            if (!preg_match('/1[0-9]+$/', $phone))
            return false;
            $phone = '20' . $phone;
        }

        // if ($user->phone_verified == '0')
        // self::validatePhone($phone);

        if ($sendSmsFeature->feature_status == 'enabled') {
            $client = new Client();

            $message = urlencode($message);
            $url = "https://smsmisr.com/api/webapi/?username=6MXWS55F&password=CRKGS8&language=2&sender=Wasel&mobile={$phone}&message={$message}&DelayUntil=";

            try {
                $res = $client->request(
                    'POST',
                    $url,
                    [
                        'allow_redirects' => false
                    ]
                );
                // echo $res->getBody();
                Log::error($res->getBody());

            } catch (Exception $e) {
                Log::error($e);
                return false;
            }
            return true;
        } else {
            return true;
        }
    }

    public static function objectToArray($d)
    {
        if (is_object($d)) {
            $d = get_object_vars($d);
        }
        if (is_array($d)) {
            return $d;
            return array_map(__FUNCTION__, $d);
        }
        else {
            return $d;
        }
    }

    public static function VLsms($phone, $message)
    {
        if (strlen($phone) == 11) {
            if (!preg_match('/01[0-9]+$/', $phone))
            return false;
            $phone = '2' . $phone;
        } elseif (strlen($phone) == 10) {
            if (!preg_match('/1[0-9]+$/', $phone))
            return false;
            $phone = '20' . $phone;
        }

        $client = new SoapClient("https://smsvas.vlserv.com/KannelSending/service.asmx?wsdl", ['trace' => 1]);
        // $userName = "WaselEgypt";
        $userName = "WASEL";
        // $Password = "UgGsbAyoKW";
        $Password = "gJfp9PZ799";
        $SMSText = $message;
        $SMSLang = "a";
        $SMSSender = "WASEL";
        $SMSReceiver = $phone;
        $result = $client->SendSMS(
            array(
                "UserName" => $userName,
                "Password" => $Password,
                "SMSText" => $SMSText,
                "SMSLang" => $SMSLang,
                "SMSSender" => $SMSSender,
                "SMSReceiver" => $SMSReceiver
            )
        );
        $response_arr = self::objectToArray($result);
        if ($response_arr['SendSMSResult'] == 0) {
            return 0;
        } else {
            $emailText = '
                Message: '. $message .' <br>
                Phone: '. $phone .' <br>
                <pre>
                    '. json_encode($response_arr) .'
                </pre>
            ';
            self::sendMail('heikal@waselegypt.com', 'VLsmsError', $emailText);
            return $response_arr;
        }
    }


    public static function sendTwilioSms($phone, $message)
    {
        $user = User::where('phone', $phone)->first();
        if ($user->phone_country_code == '20' || $user->phone_country_code == '2' || $user->phone_country_code == 'EG') {
            if (strlen($phone) == 11) {
                if (!preg_match('/01[0-9]+$/', $phone))
                return false;
                $phone = '+2' . $phone;
            } elseif (strlen($phone) == 10) {
                if (!preg_match('/1[0-9]+$/', $phone))
                return false;
                $phone = '+20' . $phone;
            }
        }

        if ($user->phone_verified == '0')
        self::validatePhone($phone);

        // Your Account SID and Auth Token from twilio.com/console
        $sid = env('twilio_sid');
        $token = env('twilio_auth_token');
        $client = new TwilioClient($sid, $token);

        // Use the client to do fun stuff like send text messages!
        try {
            $client->messages->create(
                $phone, // to
                [
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+18445152242',
                    // 'from' => 'WASEL',
                    // the body of the text message you'd like to send
                    'body' => $message,
                ]
            );
        } catch (RestException $e) {
            Log::error($e);
        }

    }

    public static function ticketMailTemplate($emailContent)
    {
        return '
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang="en">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <title></title>
            <style type="text/css">
                /* cairo-200 - latin */
                @font-face {
                  font-family: "Cairo";
                  font-style: normal;
                  font-weight: 200;
                  src: local("Cairo ExtraLight"), local("Cairo-ExtraLight"),
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-200.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-200.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                }
                /* cairo-300 - latin */
                @font-face {
                  font-family: "Cairo";
                  font-style: normal;
                  font-weight: 300;
                  src: local("Cairo Light"), local("Cairo-Light"),
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-300.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-300.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                }
                /* cairo-600 - latin */
                @font-face {
                  font-family: "Cairo";
                  font-style: normal;
                  font-weight: 600;
                  src: local("Cairo SemiBold"), local("Cairo-SemiBold"),
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-600.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-600.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                }
                /* cairo-regular - latin */
                @font-face {
                  font-family: "Cairo";
                  font-style: normal;
                  font-weight: 400;
                  src: local("Cairo"), local("Cairo-Regular"),
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-regular.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-regular.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                }
                /* cairo-700 - latin */
                @font-face {
                  font-family: "Cairo";
                  font-style: normal;
                  font-weight: 700;
                  src: local("Cairo Bold"), local("Cairo-Bold"),
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-700.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-700.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                }
                /* cairo-900 - latin */
                @font-face {
                  font-family: "Cairo";
                  font-style: normal;
                  font-weight: 900;
                  src: local("Cairo Black"), local("Cairo-Black"),
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-900.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                       url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-900.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                }

                    @media screen {
                        * , body {
                            font-family: "Cairo"
                            font-style: normal;
                        }
                    }
                    * , body {
                        font-family: "Cairo"
                        font-style: normal;
                    }
            </style>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo">

        </head>
        <body style="margin:0; padding:0; background-color: #F2F2F2;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo">

                <style type="text/css">
                    /* cairo-200 - latin */
                    @font-face {
                      font-family: "Cairo";
                      font-style: normal;
                      font-weight: 200;
                      src: local("Cairo ExtraLight"), local("Cairo-ExtraLight"),
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-200.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-200.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                    }
                    /* cairo-300 - latin */
                    @font-face {
                      font-family: "Cairo";
                      font-style: normal;
                      font-weight: 300;
                      src: local("Cairo Light"), local("Cairo-Light"),
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-300.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-300.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                    }
                    /* cairo-600 - latin */
                    @font-face {
                      font-family: "Cairo";
                      font-style: normal;
                      font-weight: 600;
                      src: local("Cairo SemiBold"), local("Cairo-SemiBold"),
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-600.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-600.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                    }
                    /* cairo-regular - latin */
                    @font-face {
                      font-family: "Cairo";
                      font-style: normal;
                      font-weight: 400;
                      src: local("Cairo"), local("Cairo-Regular"),
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-regular.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-regular.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                    }
                    /* cairo-700 - latin */
                    @font-face {
                      font-family: "Cairo";
                      font-style: normal;
                      font-weight: 700;
                      src: local("Cairo Bold"), local("Cairo-Bold"),
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-700.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-700.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                    }
                    /* cairo-900 - latin */
                    @font-face {
                      font-family: "Cairo";
                      font-style: normal;
                      font-weight: 900;
                      src: local("Cairo Black"), local("Cairo-Black"),
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-900.woff2") format("woff2"), /* Chrome 26+, Opera 23+, Firefox 39+ */
                           url("https://www.waselegypt.com/fonts/cairo/cairo-v4-latin-900.woff") format("woff"); /* Chrome 6+, Firefox 3.6+, IE 9+, Safari 5.1+ */
                    }

                    * , body {
                        font-family: "Cairo"
                        font-style: normal;
                    }
                </style>

                <tr>
                    <td align="center" valign="top">
                        <div style="border-radius: 5px; max-width: 500px; background: #fba829; margin: 0; box-shadow: 0 3px 1px rgba(0,0,0,0.13);">
                            <img src="http://betalaravel.waselegypt.com/images/logo.png" style="min-height: 40px; max-height: 40px; text-align: center; margin-top: 20px; object-fit: cover;">
                            <div style="border-radius: 5px; margin: 2%; padding: 0; width: 90%; background: transparent; padding-top: 20px">


                                    '.
                                        $emailContent
                                    .'


                            </div>

                            <small style="font-size: 10px"><br></small>
                            <img src="https://www.waselegypt.com/images/email-background.png" style="width: 100vw; max-width: 500px; ">
                            <footer style="text-align: right; color: white; text-align: center; background: #026dc0; font-weight: bold; margin-bottom: -60px;">
                                <p style="padding-top: 0; margin: 0; padding-top: 5px">
                                    <a style="color: white; text-decoration: none;" href="mailto:support@waselegypt.com">
                                        support@waselegypt.com
                                    </a>
                                </p>
                                <p style="margin-top: 0; padding-bottom: 10px">
                                    <a style="color: white; text-decoration: none;"  href="tel:+201000633110">
                                        +2 01000 633 110
                                    </a>
                                </p>
                            </footer>
                            <small style="font-size: 10px"><br><br></small>

                        </div>
                    </td>
                </tr>
            </table>

        </body>
        </html>

        ';
        // code...
    }
    public static function emailTemplate($emailContent)
    {
        return '
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang="en">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <title></title>

            <style type="text/css">
            </style>
        </head>
        <body style="margin:0; padding:0; background-color:#F2F2F2;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2">
                <tr>
                    <td align="center" valign="top">
                        <div style="border-radius: 5px; max-width: 500px; background: white; margin: 10px; width: 100%; box-shadow: 0 3px 1px rgba(0,0,0,0.13);">
                            <img src="http://betalaravel.waselegypt.com/images/logo-orange.png" style="min-height: 50px; max-height: 50px; text-align: center; margin-top: 20px; object-fit: cover;">
                            <div style="border-radius: 5px; margin: 2%; padding: 10px; width: 90%; background:#fafafa; box-shadow: inset 0 3px 1px rgba(0,0,0,0.13);">


                                '. $emailContent .'


                            </div>

                            <small style="font-size: 10px"><br></small>
                            <footer style="text-align: center:">
                                <p>
                                    <a style="text-decoration: none; color: #fba829" href="mailto:support@waselegypt.com">
                                        support@waselegypt.com
                                    </a>
                                </p>
                                <p>
                                    <a style="text-decoration: none; color: #fba829"  href="tel:+201000633110">
                                        +2 01000 633 110
                                    </a>
                                </p>
                            </footer>
                            <small style="font-size: 10px"><br><br></small>

                        </div>
                    </td>
                </tr>
            </table>

        </body>
        </html>

        ';
    }

    public static function sendMail($to_email, $subject, $message)
    {
        $client = new Client();
        $url = 'https://mail.zoho.com/api/accounts/5962878000000008002/messages';
        try {

            if ($to_email !== '' && $to_email !== null)
            $response = $client->post($url, [
                'headers' => [
                    'Authorization' => 'Zoho-authtoken 6a02c0231b19f1581cca0a3e18deabd7',
                    'content-type' => 'application/json'
                ],
                'json' => [
                    'fromAddress' => 'Wasel <no-reply@waselegypt.com>',
                    'toAddress' => $to_email,
                    'subject' => $subject,
                    'content' => self::emailTemplate($message),
                ],
            ]);
            else
            return false;

            return true;
        } catch (ServerException $e) {
            Log::error($e);
            return false;
        } catch (ServerErrorResponseException $e) {
            Log::error($e);
            return false;
        } catch (ClientException $e) {
            Log::error($e);
            return false;
        } catch (Exception $e) {
            Log::error($e);
            return false;
        }
    }


    public static function sendTicketMail($to_email, $subject, $message)
    {
        $client = new Client();
        $url = 'https://mail.zoho.com/api/accounts/5962878000000008002/messages';
        try {

            if ($to_email !== '' && $to_email !== null)
            $response = $client->post($url, [
                'headers' => [
                    'Authorization' => 'Zoho-authtoken 6a02c0231b19f1581cca0a3e18deabd7',
                    'content-type' => 'application/json'
                ],
                'json' => [
                    'fromAddress' => 'Wasel <no-reply@waselegypt.com>',
                    'toAddress' => $to_email,
                    'subject' => $subject,
                    'content' => self::ticketMailTemplate($message),
                ],
            ]);
            else
            return false;

            return true;
        } catch (ServerException $e) {
            Log::error($e);
            return false;
        } catch (ServerErrorResponseException $e) {
            Log::error($e);
            return false;
        } catch (ClientException $e) {
            Log::error($e);
            return false;
        } catch (Exception $e) {
            Log::error($e);
            return false;
        }
    }



    public static function notifyAdmins($subject, $message)
    {
        self::sendMail('hossam.abotaleb@waselegypt.com', $subject, $message);
        self::sendMail('support@waselegypt.com', $subject, $message);
    }


    public static function sendNotification($user, array $data, string $user_type)
    {
        // sending the push notification using the push notification array
        self::sendPushNotification($user['device_token'], $data, $user_type);
    }

    /**
     * function to send push notification to any user in our system
     * @param string $device_token
     * @param array $data
     * @param string $user_type
     * @return bool
     */
    public static function sendPushNotification(array $registrationIDs, $title, $body)
    {

        try {
            $fcmMsg = [
                'body' => $body,
                'title' => $title,
                'sound' => "default",
                'color' => "#203E78"
            ];
            $client = new Client();
            $url = 'https://fcm.googleapis.com/fcm/send';

            echo json_encode(
                [
                    // 'registration_ids' => $registrationIDs,
                    'to' => 'com.wasel',
                    'data' => ['id' => 10],
                    'priority' => 'high',
                    'notification' => $fcmMsg
                ]
            );
            // sample $registrationIDs
            $response = $client->post($url, [
                'headers' => [
                    'Authorization' => 'key=AIzaSyBJTqpsgb0N37xpM5hA4QBWUjtN5lJ0KTE',
                    'content-type' => 'application/json'
                ],
                'json' => [
                    // 'registration_ids' => $registrationIDs,
                    'to' => 'com.wasel',
                    'data' => ['id' => 10],
                    'priority' => 'high',
                    'notification' => $fcmMsgZ
                ],
            ]);

            return $response->getBody();

            // echo $response->getBody();
            return true;
        } catch (Exception $e) {
            return "Exception :   ". $e->getMessage();
            return false;
        }
    }
    public static function validatePhone($phone)
    {
        return true;
    }
}
