<?php

namespace App\Providers;

use Schema;
use App;
use Illuminate\Support\ServiceProvider;
use View;
// use Illuminate\Support\Facades\View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        App::setlocale('ar');

        View::composer('pages.driver-pages.my-trips', function($view)
        {
            function fmt_from_datetime($datetime, $fmt) {
                return date_format(date_create($datetime), $fmt);
            }

            $current_month = fmt_from_datetime(date('Y/m/d H:i:s'), 'M');
            $dict2 = [
                'Jan' => 'يناير',
                'Feb' => 'فبراير',
                'Mar' => 'مارس',
                'Apr' => 'أبريل',
                'May' => 'مايو',
                'Jun' => 'يونيو',
                'Jul' => 'يوليو',
                'Aug' => 'أغسطس',
                'Sep' => 'سبتمبر',
                'Oct' => 'أكتوبر',
                'Nov' => 'نوفمبر',
                'Dec' => 'ديسمبر',
            ];

            $months = [
                'يناير',
                'فبراير',
                'مارس',
                'أبريل',
                'مايو',
                'يونيو',
                'يوليو',
                'أغسطس',
                'سبتمبر',
                'أكتوبر',
                'نوفمبر',
                'ديسمبر',
            ];
            $loop_start = array_search($dict2[$current_month], $months);
            $data['months'] = [];
            for ($i = $loop_start; $i < count($months); $i++) {
                $data['months'][$i + 1] = $months[$i];
            }

            $view->with('months', $months);
        });
        // \DB::select('SET time_zone = EG;');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
