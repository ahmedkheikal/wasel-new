@extends('layouts.page')

@section('styles')
    @include('pages.private-trip.styles')
@endsection

@section('content')
    {{-- Start Navabar --}}
    @include('components.header')
    {{-- End Navabar --}}

    {{-- Start .container --}}
    <div class="container" style="margin-top: 80px">

        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>

        <div class="row" dir="rtl">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <div class="card-body">

                        <div class="text-center">
                            <div class="col-12">
                                <p style="color: #555">خدمة واصل للرحلات الالقتصادية</p>
                            </div>
                            <div class="mb-4 col-md-12 text-right fromto" style="background: #fbfbfb;">
                                <div class="row ">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-8">
                                        {{-- Start Choose Gov Form --}}
                                        @include('pages.private-trip.gov-form')
                                        {{-- End Choose Gov Form --}}
                                    </div>
                                </div>
                            </div>{{-- .fromto --}}
                        </div>{{-- .text-center --}}

                        {{-- Start Economy Trip Section --}}
                        <div id="economyTrips" class="">
                            <div class="col-md-6 economy-container">
                                @include('pages.economy-trip.economy-trip-form')
                                {{-- @include('components.reserve-eco') --}}
                            </div>
                            {{-- <h3 class="no-eco-trips">
                                @lang('language.noEcoTrips')
                            </h3> --}}
                            <p class="no-eco-trips">
                                @lang('language.onlyMincar')
                            </p>
                            {{-- End Economy Trip Section --}}

                        </div>{{-- .card-body --}}
                    </div>{{-- .card --}}
                </div>{{-- .col-md-12 --}}
            </div>{{-- .row --}}
        </div>
    </div>
</div>
{{-- End .container --}}

{{-- Start Footer --}}
@include('components.footer')
{{-- End Footer --}}

<!-- Modal -->
<div class="modal fade" dir="rtl" id="confirmPrivate" tabindex="-1" role="dialog" aria-labelledby="confirmPrivateLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmPrivateLabel">@lang('language.confirm')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="    margin: -1rem  auto -1rem -1rem; ">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @lang('language.areYouSureYouWantToRequestPrivate')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('language.cancelOrder')</button>
                <button type="button" class="btn btn-primary confirm-private">@lang('language.confirm')</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')

    <script type="text/javascript" src="{!! asset('js/picker.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.date.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.time.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/ar.js') !!}"></script>

    @include('pages.economy-trip.scripts')

@endsection
