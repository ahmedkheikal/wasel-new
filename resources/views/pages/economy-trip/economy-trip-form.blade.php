<form class="form-inline reserve-eco" action="/confirm-economy" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="trip_id" value="">

    {{-- start trip date and time --}}
    <div class="col-6 text-right" dir="rtl">
        <p style="text-align: right">@lang('titles.date')</p>
        <select class="full-width form-control eco-date" name="date">
            @foreach ($ecoTrips as $trip)
                <option value="{{ (new DateTime($trip->datetime))->format('Y-m-d') }}">{{ (new DateTime($trip->datetime))->format('d M Y')  }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-6 text-right" dir="rtl">
        <p style="text-align: right">
            @lang('titles.time')
        </p>
        <select class="form-control full-width eco-time" name="time" dir="ltr">
            @foreach ($ecoTrips as $trip)
                <option value="{{ (new DateTime($trip->datetime))->format('H:i:s') }}">{{ (new DateTime($trip->datetime))->format('h:i A') }}</option>
            @endforeach
        </select>
    </div>
    {{-- end trip date and time --}}

    {{-- start car, promo and price --}}
    <div class="row mt-4">
        <div class="col-12 text-right" dir="rtl">
            <p style="text-align: right">
                @lang('titles.selectSeat')
                <sup style="color: red">*</sup>
            </p>
        </div>
        <div class="col-md-1">

        </div>
        <div class="col-md-10" dir="ltr">
            <div id="car_load">

            </div>
            <div class="car-model clearfix">
                <img src="{!! asset('images/car.png') !!}" class="car-model-inner" alt="car model">
                <div class="input-group seats">
                    <div id="frontseat" class="frontseat seat">
                        <img src="{!! asset('images/seat.png') !!}" alt="">
                        <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                        <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
                        <input class="seat-check" value="1" type="checkbox" id="frontseat_in" name="seat_1">
                    </div>

                    <div id="backseat_right" class="backseat-right seat">
                        <img src="{!! asset('images/seat.png') !!}" alt="">
                        <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                        <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
                        <input class="seat-check" value="1" type="checkbox" id="backseat_right_in" name="seat_2">
                    </div>

                    <div id="backseat_left" class="backseat-left seat">
                        <img src="{!! asset('images/seat.png') !!}" alt="">
                        <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                        <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
                        <input class="seat-check" value="1" type="checkbox" id="backseat_left_in" name="seat_3">
                    </div>
                </div>
            </div>

            @auth
                @if (Auth::user()->account_type == 'passenger')
                    <div class="row mt-4" style="width: 100%">
                        <div class="col-10 text-right">
                            <span>
                                @lang('language.someOneElseWillUseThisReservation')
                            </span>
                        </div>
                        <div class="col-2" dir="rtl">
                            <label class="switch">
                                <input class="setfor-economy-toggler" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <input type="hidden" name="reservation_type" value="self">
                    <div class="other-person-info" style="display: none">
                        <div class="row" style="margin: 0 ;" dir="rtl">
                            <div class="col-md-6 setfor_name" dir="rtl">
                                <input type="text" class="form-control" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                            </div>
                            <div class="col-md-6 setfor_phone">
                                <input type="tel" class="form-control" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                            </div>
                        </div>
                    </div>
                @endif

                @if (Auth::user()->account_type == 'partner')
                    <input type="hidden" name="reservation_type" value="other">
                    <div class="other-person-info mb-md-4">
                        <div class="row" style="margin: 15px 0;">
                            <div class="col-md-6 setfor_name">
                                <input type="text" class="form-control" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                            </div>
                            <div class="col-md-6 setfor_phone">
                                <input type="tel" class="form-control" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                            </div>
                        </div>
                    </div>
                @endif
            @endauth
            <!-- start economy price  -->
            <div class="economy-price row mt-4">

                <div class="row" style="width: 100%; margin: 10px;" dir="rtl">
                    {{-- olod location reserve for other  --}}

                    <div class="col-8" style="padding-right: 0">
                        @auth
                            @if (Auth::user()->account_type !== 'partner')
                                <div style="z-index: 600; position: relative; {{ Auth::user()->affiliate_balance + Auth::user()->own_balance >= 50 ? '' : 'margin-top: 20px;'}}">
                                    <input class="form-control" type="text" name="promo_code"
                                        style="width: 100%; float:right; color: #222;" id="promo_code"
                                        placeholder="@lang('language.promoCode')" value="">
                                    <button type="button" class="btn btn-primary check-promo">@lang('language.check')</button>
                                </div>
                                @if (Auth::user()->affiliate_balance + Auth::user()->own_balance >= 50)
                                    <div style="position: relative; margin: 10px 0; text-align: right">
                                        <label class="switch">
                                            <input class="eco-use50egpfromwallet" name="eco_use_50EGPfROMWallet" type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                        <div class="use50egpfromwallet-label" style="margin-top: -30px; text-align: left">
                                            @lang('language.use25EgpFromWallet')
                                        </div>
                                    </div>
                                @endif
                            @else
                                <input type="hidden" id="promo_code" name="promo_code" value="">
                            @endif
                        @else
                            <div style="position: relative; margin-top: 20px;">
                                <input class="form-control" style="width: 100%" type="text" name="promo_code" placeholder="@lang('language.promoCode')" value="">
                                <button type="button" class="btn btn-primary check-promo">@lang('language.check')</button>
                            </div>
                        @endauth
                    </div>
                    <div id="tick_total" class="col-4 pull-left economy-cost" style="font-size: 20px; margin: 10px 0 30px 0;">
                        0 @lang('language.egp')
                    </div>
                </div>


                <p style="font-size: 14px; width: 100%; text-align: center" >@lang('language.paymentMethod'): خدمة فورى</p>

                @auth
                    @if (Auth::user()->account_type != 'driver')
                        <div style="margin-top: 20px;width: 100%;" class="center-block">
                            <input style="min-width: 200px; margin: auto; display: block" disabled type="submit" name="book_trip" value="@lang('language.book')"
                            class="btn btn-xl btn-primary center-block" style="width: 50%">
                        </div>
                    @endif
                @else
                    <div style="margin-top: 20px;width: 100%;" class="offset 3col-md-6 center-block">
                        <input style="min-width: 200px; margin: auto; display: block" disabled type="submit" name="book_trip" value="@lang('language.book')"
                        class="btn btn-xl btn-primary center-block" style="width: 50%">
                    </div>
                @endauth
        </div>
        <!-- end economy price  -->

    </div>
    {{-- end car, promo and price --}}
</form>
