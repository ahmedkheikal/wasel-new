<script type="text/javascript"> // select change scripts
    // start generic fromto

    $.ajaxSetup({
        // async: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function () {
        checkEconomy();
    })


    $('select[name=from_city]').change(function () {
        // getAlltos();
        var from = $('select[name=from_city]').val();

        if (from == 'Cairo') {
            $('select[name=to_city]').empty();
            $('select[name=to_city]').append(`<option value="Minya">المنيا</option>`);
            checkEconomy();
        } else if (from == 'Minya') {
            $('select[name=to_city]').empty();
            $('select[name=to_city]').append(`<option value="Cairo">القاهرة</option>`);
            checkEconomy();
        }
    });

    $('select[name=to_city]').change(function () {
        checkEconomy();
    });
    // end  generic fromto


    // start economy script
    var availableTripsByDate;
    $('.eco-date').change(function () {
        $('.eco-time').html('');
        for (day of availableTripsByDate[$('.eco-date').val()]) {
            $('.eco-time').append(`<option value="${day.departure_time}">${day.formatted_departure_time}</option>`)
        }

        checkAvailableSeats()
        updateEconomyPrice()
    });
    $('.eco-time').change(function () {
        checkAvailableSeats();
        updateEconomyPrice()
    })

    $('.seats  input[type=checkbox]').change(function () {
        updateEconomyPrice();
    });

    $('input[name=eco_use_50EGPfROMWallet]').change(function () {
        applyPromo();
    })

    $('.reserve-eco').submit(function (e) {
        e.preventDefault();
        $('input[name="book_trip"]').attr('disabled', 'disabled');
        $('input[name="book_trip"]').val('@lang('language.loading')');
        $('#loader').fadeIn();


        $.ajax({
            method: 'POST',
            url: '/confirm-economy',
            data: {
                trip_id: $('input[name=trip_id]').val(),
                seat_1: $('input[name=seat_1]:checked').length,
                seat_2: $('input[name=seat_2]:checked').length,
                seat_3: $('input[name=seat_3]:checked').length,
                promo_code: $('#promo_code').val(),
                use_50EGP_credit: $('input[name=eco_use_50EGPfROMWallet]:checked').length,
                reservation_type: $('input[name=reservation_type]').val(),
                setfor_name: $('input[name=setfor_name]').val(),
                setfor_phone: $('input[name=setfor_phone]').val()
            },
            success: function (data) {
                console.log(data);
                if (data.code == '401')
                window.location.href = '/login';

                if (data.code == '403')
                window.location.href = '/phone/verify';

                if (data.code == '200')
                window.location.href = '/confirm-economy';
                $('#loader').fadeOut();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (errorThrown.toString() == 'Unauthorized')
                window.location.href = '/login';

                console.error(textStatus);
                alert(errorThrown.toString())
                $('#loader').fadeOut();
                $('input[name="book_trip"]').removeAttr('disabled');
                $('input[name="book_trip"]').val('@lang('language.book')');
            }
        })
    })

    function checkEconomy() {
        $('#loader').fadeIn()
        $.ajax({
            method: 'POST',
            url: '/api/dest/economy-trips',
            data: {
                from_city: $('select[name=from_city]').val(),
                to_city: $('select[name=to_city]').val()
            },
            success: function (data) {
                if (data.code == '200') {
                    if (data.response.length == 0) {
                        $('.economy-container form').addClass('d-none');
                        $('.only-private').removeClass('d-none');
                        $('.nav-tabs').addClass('d-none');
                        $('.no-eco-trips').css({'display': 'block'});
                        $('.privateTrips-btn').click();
                    } else {
                        $('.economyTrips-btn').click();
                        $('.only-private').addClass('d-none');
                        $('.nav-tabs').removeClass('d-none');
                        $('.economy-container form').removeClass('d-none');
                        $('.no-eco-trips').css({'display': 'none'});

                        availableTripsByDate = data.response;
                        $('.eco-date').html('');
                        for (var date in data.response) {
                            if (data.response.hasOwnProperty(date)) {
                                $('.eco-date').append(`<option value="${date}">${formatDate(new Date(date))}</option>`);
                            }
                        }
                        $('.eco-time').html('');
                        for (time of data.response[$('.eco-date').val()]) {
                            $('.eco-time').append(`<option value="${time.departure_time}">${time.formatted_departure_time}</option>`);
                        }
                    }
                }
                checkAvailableSeats();

                $('#loader').fadeOut()
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown);
            }
        })
    }

    function checkAvailableSeats() {
        $('#loader').fadeIn();
        $('#frontseat_in').prop('checked', false);
        $('#frontseat').removeClass('checked');
        $('#frontseat_in').attr('disabled', true);
        $('#frontseat').css('opacity', '.5');

        $('#backseat_right_in').prop('checked', false);
        $('#backseat_right').removeClass('checked');
        $('#backseat_right_in').attr('disabled', true);
        $('#backseat_right').css('opacity', '.5');

        $('#backseat_left_in').prop('checked', false);
        $('#backseat_left').removeClass('checked');
        $('#backseat_left_in').attr('disabled', true);
        $('#backseat_left').css('opacity', '.5');
        $.ajax({
            method: 'POST',
            url: '/api/economy/trip-seats',
            data: {
                from_city: $('select[name=from_city]').val(),
                to_city: $('select[name=to_city]').val(),
                date: $('.eco-date').val(),
                time: $('.eco-time').val()
            },
            success: function (data) {

                if (data.response != null) {
                    $('input[name=trip_id]').val(data.response.id);

                    $('#car_load').css({display: 'none'});
                    if (data.response.seat_1 == '0') {
                        $('#frontseat_in').removeAttr('disabled');
                        $('#frontseat').css('opacity', '1');
                    }
                    if (data.response.seat_2 == '0') {
                        $('#backseat_right_in').removeAttr('disabled');
                        $('#backseat_right').css('opacity', '1');
                    }
                    if (data.response.seat_3 == '0') {
                        $('#backseat_left_in').removeAttr('disabled');
                        $('#backseat_left').css('opacity', '1');
                    }
                }

                $('#loader').fadeOut();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown);
            }
        })
    }

    var totalPrice = 0; // tptal economy price
    function updateEconomyPrice() {
        var tickets = 0;
        for (var i = 1; i <= 3; i++) {
            if ($('.seat:nth-child('+ i +') .seat-check').prop('disabled') == false && $('.seat:nth-child('+ i +') .seat-check').prop('checked') == true) {
                tickets ++;
            }
        }
        if (tickets) {
            $('input[name=book_trip]').removeAttr('disabled');
            $('.check-promo').removeAttr('disabled');
        } else {
            $('input[name=book_trip]').attr('disabled', true);
            $('.check-promo').attr('disabled', true);
        }


        $.ajax({
            method: 'POST',
            url: '/api/economy/price',
            data: {
                from_city: $('select[name=from_city]').val(),
                to_city: $('select[name=to_city]').val()
            },
            success: function (data) {
                if (data.code == '200') {
                    console.log(totalPrice);
                    totalPrice = tickets * data.response.price;
                    if ($('input[name=eco_use_50EGPfROMWallet]:checked').length && tickets)
                    totalPrice -= 25;
                    $('#tick_total').html(`${totalPrice} @lang('language.egp')`)
                } else if (data.code == '422') {
                    $('#tick_total').html('@lang('language.contactAdminAboutThePrice')')
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown);
            }
        })
    }
    // end economy script

    // car clicks
    $('.nav-tabs li a').addClass('p-2')
    $('#frontseat').click(function () {
        $('#frontseat').toggleClass('checked');
        if ($('#frontseat').hasClass('checked')) {
            $('#frontseat_in').attr('checked', true);
        } else {
            $('#frontseat_in').removeAttr('checked');
        }
    });

    $('#backseat_right').click(function () {
        $('#backseat_right').toggleClass('checked');
        if ($('#backseat_right').hasClass('checked')) {
            $('#backseat_right_in').attr('checked', true);
        } else {
            $('#backseat_right_in').removeAttr('checked');
        }
    });

    $('#backseat_left').click(function () {
        $('#backseat_left').toggleClass('checked');
        if ($('#backseat_left').hasClass('checked')) {
            $('#backseat_left_in').attr('checked', true);
        } else {
            $('#backseat_left_in').removeAttr('checked');
        }
    });

    $('.check-promo').click(function () {
        applyPromo();
    })

    function applyPromo() {
        updateEconomyPrice();
        $('.check-promo').attr('disabled', true);
        if (totalPrice)
        $.ajax({
            method: 'POST',
            url: '/api/economy/promo',
            data: {
                from_city: $('select[name=from_city]').val(),
                to_city: $('select[name=to_city]').val(),
                // promo_code: $('#promo_code').val()
                promo_code: $('input[name=promo_code]').val()
            },
            success: function (data) {
                console.log(data);
                if (data.code == '200') {
                    if (data.response.type == 'percent')
                    totalPrice = Math.floor(
                        totalPrice - (totalPrice * (data.response.amount / 100) )
                    );
                    else if (data.response.type == 'flat_rate')
                    totalPrice = totalPrice - data.response.amount;
                    $('#tick_total').html(`${totalPrice} @lang('language.egp')`);
                    // $('.check-promo').css({'color': 'green'});
                    $('input[name=promo_code]').addClass('is-valid').removeClass('is-invalid');
                    $('input[name=promo_code]').attr('disabled', true);

                    $('.check-promo').addClass('btn-success').removeClass('btn-primary').removeClass('btn-danger');
                    $('.check-promo').attr('disabled', true);
                } else {
                    $('input[name=promo_code]').addClass('is-invalid').removeClass('is-valid');
                    $('.check-promo').addClass('btn-danger').removeClass('btn-primary').removeClass('btn-success');

                    $('input[name=promo_code]').removeAttr('disabled');
                    $('.check-promo').removeAttr('disabled');
                }
            }
        });
    }

    function formatDate(date) {
        var monthNames = [
            "@lang('language.Jan')", "@lang('language.Feb')", "@lang('language.Mar')",
            "@lang('language.Apr')", "@lang('language.May')", "@lang('language.Jun')", "@lang('language.Jul')",
            "@lang('language.Aug')", "@lang('language.Sep')", "@lang('language.Oct')",
            "@lang('language.Nov')", "@lang('language.Dec')"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' '
        /*+ year*/;
    }

    function removeDuplicate(selector) {
        var liText = '', liList = $(selector), listForRemove = [];
        $(liList).each(function () {
            var text = $(this).text();
            if (liText.indexOf('|'+ text + '|') == -1) {
                liText += '|'+ text + '|';
            }
            else {
                listForRemove.push($(this));
            }
        });
        $(listForRemove).each(function () { $(this).remove(); });

    }


    var today = new Date().toISOString().split('T')[0];
    $('.reserve-for-others button').click(function () {
        $('.reserve-for-others button').removeClass('active');
        $(this).addClass('active');
        if ($(this).attr('reserve-for-other') == 'true') {
            $('.other-person-info').slideDown('fast')
            // $('.other-person-info').css({'display': 'block'});
            $('input[name=reservation_type]').val('other')
        } else {
            $('.other-person-info').slideUp('fast')
            // $('.other-person-info').css({'display': 'none'});
            $('input[name=reservation_type]').val('self')
        }
    })

    $('.setfor-economy-toggler').change(function () {
        if ($('.setfor-economy-toggler:checked').length) {
            $('.other-person-info').slideDown('fast')
            $('input[name=reservation_type]').val('other')
        } else {
            $('.other-person-info').slideUp('fast')
            $('input[name=reservation_type]').val('self')
        }
    })
    $('.setfor-private-toggler').change(function () {
        if ($('.setfor-private-toggler:checked').length) {
            $('.other-person-info').slideDown('fast')
            $('input[name=reservation_type]').val('other')
        } else {
            $('.other-person-info').slideUp('fast')

            $('input[name=reservation_type]').val('self')
        }
    })

    function getAlltos() {
        $.ajax({
            url: '/getAlltos',
            method: 'POST',
            data: {
                from_city: $('select[name=from_city]').val()
            },
            success: function (data) {
                if (data.code == '200') {
                    $('select[name=to_city]').html('');
                    for (city of data.response) {
                        $('select[name=to_city]').append(`
                            <option value="${city.city_name}">
                                ${city.city_name_ar}
                            </option>
                        `);
                    }
                    removeDuplicate('select[name=to_city] option');
                    $('select[name=to_city]').change();
                } else if (data.code == '422') {
                    for (var error in data.response) {
                        if (data.response.hasOwnProperty(error)) {
                            alert(data.response[error]);
                        }
                    }
                }
            }
        })
    }

</script>
