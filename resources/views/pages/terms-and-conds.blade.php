@extends('layouts.page')

@section('styles')
    <style media="screen">
        .justify-content-center * {
            text-align: right;
        }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>

        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-8">
                <div class="card" dir="rtl">
                    <div class="card-header text-right">@lang('titles.termsAndConds')</div>

                    <div class="card-body">

                        <style type="text/css">
                            @page {
                                margin-left: 1.25in;
                                margin-right: 1.25in;
                                margin-top: 1in;
                                margin-bottom: 1in
                            }

                            p {
                                margin-bottom: 0.1in;
                                direction: rtl;
                                line-height: 120%;
                                text-align: right;
                                orphans: 2;
                                widows: 2
                            }

                            h3 {
                                margin-top: 0.17in;
                                direction: rtl;
                                line-height: 115%;
                                text-align: right;
                                orphans: 2;
                                widows: 2
                            }

                            h3.western {
                                font-family: "Liberation Sans", sans-serif;
                                font-weight: normal
                            }

                            h3.cjk {
                                font-family: "Noto Sans CJK SC Regular";
                                font-weight: normal
                            }

                            h3.ctl {
                                font-family: "FreeSans";
                                font-weight: normal
                            }

                            a:link {
                                so-language: zxx
                            }
                        </style>


                        <div class="row">
                            <div class="col-sm-6">

                                <h3 class="western">
                                    <font face="FreeSans"><span lang="ar-SA">ما هى
                                            التعليمات الواجب على السائق الالتزام
                                            بها ؟</span></font>
                                </h3>
                                <ol>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">التواجد
                                                            فى نطاق محطة القيام المحدده مسبقا قبل
                                                            بدء الرحله بوقت كافى </span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">(10
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">دقائق
                                                            على الاقل</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">).</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">ابراز
                                                            هويتك للمسافر عند طلبه التأكد منها قبل
                                                            التحرك</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">التحرك
                                                            فى ميعاد الرحله المحدد مسبقا مع اعطاء
                                                            فترة سماح للمسافر المتأخر </span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">(</font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">لا
                                                            تتجاوز ال </span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">10
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">دقائق</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">).</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">الالتزام
                                                            بأنزال كل مسافر فى محطات الوصول المحدده
                                                            مسبقا بالترتيب من المحطه الاقرب إلى
                                                            المحطه الابعد</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">الالتزام
                                                            بمعايير السلامة اثناء القيادة مع قيادة
                                                            غير متهورة وهادئة</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">عدم
                                                            التحدث فى الهاتف اثناء القيادة و كذلك
                                                            عدم التدخين</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">الوقوف
                                                            فى محطات الراحه بمعدل مره واحده كل </span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">150
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">كيلومتر،
                                                            او عند طلب المسافر ذلك مراعاة للحاله
                                                            الصحيه لمرضى السكر</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">الالتزام
                                                            بالطريق المحدد وعدم المخالفه الا بامر
                                                            العميل</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">التأكد
                                                            من حصولك على الوقود الكافى للرحله كامله
                                                            قبل ميعاد قيام الرحله</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">عمل
                                                            فحص سريع للسياره ويشمل </span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">(</font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">منسوب
                                                            المياه – منسوب الزيت – الاطارات
                                                            –الفرامل</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">)
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">قبل
                                                            كل رحله</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">التأكد
                                                            من وجود اطار احتياطى وكذلك عدّة الصيانه
                                                            الاحتياطيه بشنطة السياره</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">التأكد
                                                            من وجود شنطة الاسعافات الاوليه ومطفأة
                                                            الحريق بالسياره</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">الالتزام
                                                            بتنفيذ طلب المسافرين فى حالة طلب تشغيل
                                                            او ايقاف التكييف</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">عدم
                                                            التحدث مع المسافر الا اذا تم توجيه
                                                            السؤال لك</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">الا
                                                            تقوم بتشغيل الرديو او الكاسيت الا اذا
                                                            طلب المسافر ذلك</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">الا
                                                            تقبل اى بقشيش او اموال زياده عن سعر
                                                            الرحله المحدد من قبل الشركه، الا في
                                                            حالة اصرار العميل على ذلك</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA">
                                                    <font face="Arial"><span lang="ar-EG">ان
                                                            تكون على علم انه بعد انتهاء كل رحلة
                                                            سيقوم الراكب بتقييم ادائك، وانه سيتم
                                                            ايقافك عن العمل فى حالة حصولك على شكاوى
                                                            او تقييمات سيئه وخاصة الشكاوى التى
                                                            تتعلق بتخطيك السرعات المحدده او القياده
                                                            المتهوره</span></font>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px">
                                                <span lang="ar-SA">
                                                    ان تكون على علم ان الشركه غير مسئوله قانونيا او جنائيا او حتى صرف اى تعوضيات ماليه او عينيه بمختلف انواعها فى الحالات التاليه (على سبيل المثال لا الحصر) الحوادث والاصابات وحالات الوفاه والمخالفات بمختلف انواعها او فقد اى متعلقات شخصيه ، وان سلامتك الشخصيه وسلامة المسافرين معك وسلامة سيارتك ، هى مسئوليتك مسئوليه كامله، فلذلك عليك توخى الحذر حفاظا على سلامتكم جميعا.
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                </ol>
                                <h3 class="western"><br />
                                    <br />
                                </h3>
                            </div>

                            <div class="col-sm-6">

                                <h3 class="western">
                                    <font face="FreeSans"><span lang="ar-SA">ما هى
                                            التعليمات الواجب على المسافر الالتزام
                                            بها ؟</span></font>
                                </h3>
                                <ol>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">التواجد
                                                        فى مكان محطة القيام المحدده مسبقا قبل
                                                        بدء الرحله بوقت كافى</span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">التأكد
                                                        من هوية السائق والسياره </span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">(</font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">مره
                                                        واحده فقط لكل مسافر</span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">)
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">مطابقة
                                                        لبيانات التذكره المحجوزه والمرسله على
                                                        الايميل، وابلاغ خدمة العملاء فى حالة
                                                        اختلاف احد تلك البيانات قبل التحرك</span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">اعطاء
                                                        السائق قيمة التذكره قبل بدء الرحله</span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">عدم
                                                        التدخين داخل السياره، او التحدث بصوت
                                                        عإلى يزعج باقى المسافرين او يشتت انتباه
                                                        السائق</span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">عدم
                                                        اصطحاب الحيوانات الاليفه والمواد
                                                        القابله للأشتعال</span></span>
                                            </font>
                                        </font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">يمكنك
                                                        طلب الوقوف بأحد محطات الراحه بمعدل مره
                                                        واحده كل </span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">150
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">كيلومتر
                                                        او عند الحاجه بحد اقصى </span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">3
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">مرات
                                                        فى الرحله الواحده</span></span>
                                            </font>
                                        </font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">الالتزام
                                                        بمحطة الوصول المحدده مسبقا، او الانتظار
                                                        لحين انزال باقى المسافرين اولا، وذلك
                                                        عند تحديدك لنقطة وصول جديده فى نفس طريق
                                                        السائق بعد موافقتة</span></span>
                                            </font>
                                        </font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">يتم
                                                        حجز تذكره كامله للأطفال ما عدا الاطفال
                                                        الرضّع وحتى </span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">3
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">اعوام</span></span>
                                            </font>
                                        </font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">عند
                                                        حجز السياره بالكامل يجب الا يتعدى عدد
                                                        الركاب عن </span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">4
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">افراد،
                                                        بحد اقصى </span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">3
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">افراد
                                                        فى الكراسى الخلفيه</span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">مسموح
                                                        بشنطة سفر واحده لكل مسافر تتناسب مع ثلث
                                                        حجم شنطة السياره، بالأضافه إلى شنطة يد
                                                        او شنطة ظهر او لاب توب داخل السياره</span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">يمكنك
                                                        الغاء حجز التذكره قبل ميعاد الرحله ب
                                                    </span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">24
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">ساعه
                                                        فقط، بعدها يتم احتساب غرامه قدرها </span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">5
                                        </font>
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">جنيهات
                                                        عن كل ساعه </span>
                                                    <font face="Arial"><span lang="ar-EG">عند
                                                            الغاءك هذه الرحله</span></font><span lang="ar-EG">،
                                                        ويتم تحصيلها عند حجزك لتذكرة الرحله
                                                        التى تليها</span>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">عند
                                                        حجزك للتذكره وعدم حضورك فى الميعاد
                                                        المحدد للرحله او عدم الغائها، سيتم حظر
                                                        ملفك الشخصى بكل بياناته عن الحجز مره
                                                        اخرى الا فى حالة توفير الشركه لامكانية
                                                        الدفع الالكترونى المسبق للتذكره عن
                                                        طريق كارت الفيزا الخاص بك مع تحصيل
                                                        الغرامه المسبقه</span></span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px"><span lang="ar-SA"><span lang="ar-EG">يمكنك
                                                        تقييم السائق والرحله لأستبعاد السائقين
                                                        الغير كفوئين، </span>
                                                    <font face="Arial"><span lang="ar-EG">وكذلك
                                                            تقديم الشكاوى والاقتراحات</span></font><span lang="ar-EG">
                                                        لتحسين مستوى الخدمه</span>
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                    <li />
                                    <p style="margin-bottom: 0.14in; line-height: 150%">
                                        <font face="Arial">
                                            <font size="4" style="font-size: 16px">
                                                <span lang="ar-SA">
                                                    ان تكون على علم ان الشركه غير مسئوله قانونيا او جنائيا او حتى صرف اى تعوضيات ماليه او عينيه بمختلف انواعها فى الحالات التاليه (على سبيل المثال لا الحصر) الحوادث والاصابات وحالات الوفاه والمخالفات بمختلف انواعها او فقد اى متعلقات شخصيه.
                                                </span>
                                            </font>
                                        </font>
                                        <font size="4" style="font-size: 16px">.</font>
                                    </p>
                                </ol>

                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('components.footer')
@endsection

@section('page-scripts')
@endsection
