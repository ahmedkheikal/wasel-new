@extends('layouts.page')

@section('styles')
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.date.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.time.css') !!}">
    <style media="screen">
        .modal-footer>:last-child {
            margin-right: .25rem;
        }
        .modal-footer>:not(:last-child) {
            margin-left: .25rem;
        }
        .only-private {
            color: #444;
            text-align: center;
        }
        ::placeholder {
            color: #888 !important
        }
        sup {
            font-size: 20px;
            top: auto;
        }
        @media (min-width: 845px) {
            .economy-container {
                margin-right: 25%;
            }
        }
        @media (max-width: 361px) {
            .use50egpfromwallet-label {
                font-size: 12px;
            }
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: white !important;
        }
        /* start tabs  */

        .nav-tabs {
            padding: 0
        }
        .nav-tabs li {
            width: 50%;
            display: inline-block;
        }

        .nav-tabs li a {
            width: 100%;
            display: block;
            text-align: center;
            font-size: 17px;
        }
        .nav-tabs li a:hover {
            text-decoration: none;
        }

        .nav-tabs .active {
            border-bottom: 2px #fba829 solid;
        }
        /* end tabs  */

        /* #f8fafc */
        .from-region, .to-region {
            width: 100% !important;
        }
        .bg-light {
            background: white !important;
        }
        .fromto {
            box-shadow: inset 0 3px 1px rgba(0,0,0,0.13);
            padding: 10px;
            border-radius: 5px
        }

        .no-eco-trips {
            color: rgba(227, 52, 47, 0.5);
            font-weight: lighter;
        }
        .request-cost, .request-cost:focus {
            border: none;
            cursor: default;
            background: #5cb85c;
            color: white;
            display: block;
            text-align: center;
            padding: 10px;
            border-radius: 3px;
        }

        .request-cost:hover {
            background: #5cb85c;
            border: none
        }

        .economy-cost, .economy-cost:focus {
            border: none;
            cursor: default;
            background: #5cb85c;
            color: white;
            display: block;
            text-align: center;
            padding: 10px;
            border-radius: 3px;
        }

        .economy-cost:hover {
            background: #5cb85c;
            border: none
        }

        .private-check-promo {
            position: absolute;
            color: grey;
            top: 0px;
            left: 0;
            cursor: pointer;
            color: white;
        }
        .check-promo {
            position: absolute !important;
            color: white;
            top: 0;
            left: 0;
            cursor: pointer;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/car.css') }}">
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row" dir="rtl">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <ul class="nav nav-tabs" dir="rtl">
                            <li><a class="economyTrips-btn" data-toggle="tab" href="#economyTrips">@lang('titles.economyTrip')</a></li>
                            <li><a class="privateTrips-btn" data-toggle="tab" href="#privateTrips">@lang('titles.privateTrip')</a></li>
                        </ul>
                        <p class="only-private d-none">@lang('language.orderPrivateTrip')</p>
                        <div class="row text-center">
                            <hr>
                            <div class="mb-4 col-md-12 text-right fromto" style="background: #fbfbfb;">
                                <div class="row ">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-8">
                                        <form class="main-form" action="/add-trip" method="post" dir="{{ Config::get('app.locale') == 'ar' ? 'rtl' : 'ltr' }}">
                                            {{ csrf_field() }}
                                            <div class="row" style="font-weight: bolder; font-size: 17px">
                                                <div class="col-md-6 account_type == 'driverity">
                                                    <label for="from_city" class="float-right">
                                                        @lang('titles.from')
                                                    </label>
                                                    <select class="form-control" name="from_city" style="font-size: 17px; height: 3rem">
                                                        <option disabled selected value="">@lang('titles.from')</option>
                                                        @foreach ($cities as $city)
                                                            <option {{ $from_city == $city->city_name ? 'selected' : '' }} value="{{ $city->city_name }}">@lang("db.{$city->city_name}")</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 to_city">
                                                    <label for="to_city" class="float-right">
                                                        @lang('titles.to')
                                                    </label>
                                                    <select class="form-control" name="to_city" style="font-size: 17px; height: 3rem">
                                                        <option disabled selected value="">@lang('titles.to')</option>
                                                        @foreach ($toCities as $city)
                                                            <option {{ $to_city == $city->city_name ? 'selected' : '' }} value="{{ $city->city_name }}">@lang("db.{$city->city_name}")</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="tab-content" style="width: 100%">
                                <div id="economyTrips" class="tab-pane fade in active">
                                    <div class="col-md-6 economy-container">
                                        <form class="form-inline reserve-eco" action="/confirm-economy" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="trip_id" value="">
                                            {{-- start trip date and time --}}
                                            <div class="col-6 text-right" dir="rtl">
                                                <p style="text-align: right">@lang('titles.date')</p>
                                                <select class="full-width form-control eco-date" name="date">
                                                    @foreach ($ecoTrips as $trip)
                                                        <option value="{{ (new DateTime($trip->datetime))->format('Y-m-d') }}">{{ (new DateTime($trip->datetime))->format('d M Y')  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-6 text-right" dir="rtl">
                                                <p style="text-align: right">
                                                    @lang('titles.time')
                                                </p>
                                                <select class="form-control full-width eco-time" name="time" dir="ltr">
                                                    @foreach ($ecoTrips as $trip)
                                                        <option value="{{ (new DateTime($trip->datetime))->format('H:i:s') }}">{{ (new DateTime($trip->datetime))->format('h:i A') }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            {{-- start trip date and time --}}


                                            {{-- start car, promo and price --}}
                                            <div class="row mt-4">
                                                <div class="col-12 text-right" dir="rtl">
                                                    <p style="text-align: right">
                                                        @lang('titles.selectSeat')
                                                        <sup style="color: red">*</sup>
                                                    </p>
                                                </div>
                                                <div class="col-md-1">

                                                </div>
                                                <div class="col-md-10" dir="ltr">
                                                    <div id="car_load">

                                                    </div>
                                                    <div class="car-model clearfix">
                                                        <img src="{!! asset('images/car.png') !!}" class="car-model-inner" alt="car model">
                                                        <div class="input-group seats">
                                                            <div id="frontseat" class="frontseat seat">
                                                                <img src="{!! asset('images/seat.png') !!}" alt="">
                                                                <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                                                                <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
                                                                <input class="seat-check" value="1" type="checkbox" id="frontseat_in" name="seat_1">
                                                            </div>

                                                            <div id="backseat_right" class="backseat-right seat">
                                                                <img src="{!! asset('images/seat.png') !!}" alt="">
                                                                <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                                                                <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
                                                                <input class="seat-check" value="1" type="checkbox" id="backseat_right_in" name="seat_2">
                                                            </div>

                                                            <div id="backseat_left" class="backseat-left seat">
                                                                <img src="{!! asset('images/seat.png') !!}" alt="">
                                                                <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                                                                <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
                                                                <input class="seat-check" value="1" type="checkbox" id="backseat_left_in" name="seat_3">
                                                            </div>


                                                        </div>
                                                    </div>

                                                    @auth
                                                        @if (Auth::user()->account_type == 'passenger')
                                                            <div class="row mt-4" style="width: 100%">
                                                                <div class="col-10 text-right">
                                                                    <span>
                                                                        @lang('language.someOneElseWillUseThisReservation')
                                                                    </span>
                                                                </div>
                                                                <div class="col-2" dir="rtl">
                                                                    <label class="switch">
                                                                        <input class="setfor-economy-toggler" type="checkbox">
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="reservation_type" value="self">
                                                            <div class="other-person-info" style="display: none">
                                                                <div class="row" style="margin: 0 ;" dir="rtl">
                                                                    <div class="col-md-6 setfor_name" dir="rtl">
                                                                        <input type="text" class="form-control" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                                                                    </div>
                                                                    <div class="col-md-6 setfor_phone">
                                                                        <input type="tel" class="form-control" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        @endif
                                                        @if (Auth::user()->account_type == 'partner')
                                                            <input type="hidden" name="reservation_type" value="other">
                                                            <div class="other-person-info mb-md-4">
                                                                <div class="row" style="margin: 15px 0;">
                                                                    <div class="col-md-6 setfor_name">
                                                                        <input type="text" class="form-control" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                                                                    </div>
                                                                    <div class="col-md-6 setfor_phone">
                                                                        <input type="tel" class="form-control" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endauth
                                                    <!-- start economy price  -->
                                                    <div class="economy-price row mt-4">

                                                        <div class="row" style="width: 100%; margin: 10px;" dir="rtl">
                                                            {{-- olod location reserve for other  --}}

                                                            <div class="col-8" style="padding-right: 0">
                                                                @auth
                                                                    @if (Auth::user()->account_type !== 'partner')
                                                                        <div style="z-index: 600; position: relative; {{ Auth::user()->affiliate_balance + Auth::user()->own_balance >= 50 ? '' : 'margin-top: 20px;'}}">
                                                                            <input class="form-control" type="text" name="promo_code"
                                                                                style="width: 100%; float:right; color: #222;" id="promo_code"
                                                                                placeholder="@lang('language.promoCode')" value="">
                                                                            <button type="button" class="btn btn-primary check-promo">@lang('language.check')</button>
                                                                        </div>
                                                                        @if (Auth::user()->affiliate_balance + Auth::user()->own_balance >= 50)
                                                                            <div style="position: relative; margin: 10px 0; text-align: right">
                                                                                <label class="switch">
                                                                                    <input class="eco-use50egpfromwallet" name="eco_use_50EGPfROMWallet" type="checkbox">
                                                                                    <span class="slider round"></span>
                                                                                </label>
                                                                                <div class="use50egpfromwallet-label" style="margin-top: -30px; text-align: left">
                                                                                    @lang('language.use50EgpFromWallet')
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @else
                                                                        <input type="hidden" id="promo_code" name="promo_code" value="">
                                                                    @endif
                                                                @else
                                                                    <div style="position: relative; margin-top: 20px;">
                                                                        <input class="form-control" style="width: 100%" type="text" name="promo_code" placeholder="@lang('language.promoCode')" value="">
                                                                        <button type="button" class="btn btn-primary check-promo">@lang('language.check')</button>
                                                                    </div>
                                                                @endauth
                                                            </div>
                                                            <div id="tick_total" class="col-4 pull-left economy-cost" style="font-size: 20px; margin: 10px 0 30px 0;">
                                                                0 @lang('language.egp')
                                                            </div>
                                                        </div>


                                                        <p style="font-size: 14px; width: 100%;" >@lang('language.paymentMethod'): كاش للكابتن</p>

                                                        @auth
                                                            @if (Auth::user()->account_type != 'driver')
                                                                <div style="margin-top: 20px;width: 100%;" class="center-block">
                                                                    <input disabled type="submit" name="book_trip" value="@lang('language.book')"
                                                                    class="btn btn-xl btn-primary center-block" style="width: 50%">
                                                                </div>
                                                            @endif
                                                        @else
                                                            <div style="margin-top: 20px;width: 100%;" class="offset 3col-md-6 center-block">
                                                                <input disabled type="submit" name="book_trip" value="@lang('language.book')"
                                                                class="btn btn-xl btn-primary center-block" style="width: 50%">
                                                            </div>
                                                        @endauth

                                                </div>
                                                <!-- end economy price  -->

                                            </div>
                                        </div>
                                        {{-- end car, promo and price --}}
                                    </form>
                                    {{-- @include('components.reserve-eco') --}}
                                    <h3 class="no-eco-trips">
                                        @lang('language.noEcoTrips')
                                    </h3>
                                    <p class="no-eco-trips">
                                        @lang('language.onlyMincar')
                                    </p>
                                </div>
                            </div>
                            <div id="privateTrips" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-2">

                                    </div>
                                    <div class="col-md-8">
                                        <form class="form-inline" action="/add-private-trip" method="post">
                                            {{-- start fromto regions --}}

                                            <div class="col-md-6 from_city_region from_city_address region-address form-inline">
                                                <p style="text-align: right; width: 100%">
                                                    @lang('titles.departure')
                                                    <sup style="color: red">*</sup>
                                                </p>
                                                <select class="form-control from-region" name="from_region">
                                                    {{-- <option value="">lorem lorem</option> --}}
                                                </select>
                                                <input type="text" name="from_pri_address" class="form-control" value="" placeholder="@lang('titles.writeFromAddress')">
                                            </div>
                                            <div class="col-md-6 to_city_region to_city_address region-address form-inline">
                                                <p style="text-align: right; width: 100%">
                                                    @lang('titles.arrival')
                                                    <sup style="color: red">*</sup>
                                                </p>
                                                <select class="form-control to-region" name="to_region">
                                                    {{-- <option value="">lorem lorem</option> --}}
                                                </select>
                                                <input type="text" name="to_pri_address" class="form-control" value="" placeholder="@lang('titles.writeToAddress')">
                                            </div>
                                            {{-- end fromto regions --}}

                                            {{-- start datetime --}}
                                            <div class="col-6 mt-3 private-date justdate date">
                                                <p style="text-align: right">
                                                    @lang('titles.date')
                                                    <sup style="color: red">*</sup>
                                                </p>
                                                <div class="row justdate date" style="position: relative">
                                                    <input name="private_date" class="form-control" type="date" min="2013-12-25" placeholder="@lang('titles.date')">
                                                </div>
                                            </div>
                                            <div class="col-6 mt-3 private-date">
                                                <p style="text-align: right">
                                                    @lang('titles.time')
                                                    <sup style="color: red">*</sup>
                                                </p>
                                                <div class="row time">
                                                    <input type="time" name="private_time" class="form-control" value="" placeholder="@lang('titles.time')">
                                                </div>
                                            </div>
                                            {{-- end datetime --}}

                                            {{-- start reserve for others --}}
                                            <div class="row mt-4" style="width: 100%; margin: 0">
                                                @auth
                                                    @if (Auth::user()->account_type == 'partner')
                                                        <input type="hidden" name="reservation_type" value="other">
                                                        <div class="other-person-info">
                                                            <div class="row" style="margin: 15px 0;">
                                                                <div class="col-md-6 setfor_name">
                                                                    <input type="text" class="form-control private-setfor_name" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                                                                </div>
                                                                <div class="col-md-6 setfor_phone">
                                                                    <input type="tel" class="form-control private-setfor_phone" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @elseif (Auth::user()->account_type == 'passenger')
                                                        <input type="hidden" name="reservation_type" value="self">

                                                        <div class="row mt-2 mb-2" style="width: 100%; margin: 0">
                                                            <div class="col-2" dir="rtl">
                                                                <label class="switch">
                                                                    <input class="setfor-private-toggler" type="checkbox">
                                                                    <span class="slider round"></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-10 text-right">
                                                                <span>
                                                                    @lang('language.someOneElseWillUseThisReservation')
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="other-person-info" style="display: none">
                                                            <div class="row" style="margin: 0 ;" dir="rtl">
                                                                <div class="col-md-6 setfor_name" dir="rtl">
                                                                    <input type="text" class="form-control private-setfor_name" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                                                                </div>
                                                                <div class="col-md-6 setfor_phone">
                                                                    <input type="tel" class="form-control private-setfor_phone" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endauth
                                            </div>
                                            {{-- end reserve for others --}}
                                            <div class="col-sm-12 mb-4 passenger_notes">
                                                <p style="text-align: right">@lang('titles.notes')</p>
                                                <textarea style="width: 100%" name="passenger_notes" rows="2" class="form-control" placeholder="@lang('language.notes')"></textarea>
                                            </div>

                                            <div class="col-12">
                                                <hr>
                                            </div>
                                            <div class="col-12" style="text-align: right">
                                                <div class="pull-right">
                                                    <p>
                                                        <input type="radio" id="going" name="goingandcoming" value="going" checked>
                                                        <label for="going">
                                                            @lang('language.going')
                                                        </label>
                                                    </p>
                                                    <p>
                                                        <input type="radio" id="going_and_comingback" name="goingandcoming" value="going_and_comingback">
                                                        <label for="going_and_comingback">
                                                            @lang('language.goingAndComingback')
                                                            <small class="waiting-time" style="font-size: 10px">(انتظار حتى 4 ساعات)</small> <br>
                                                        </label>
                                                    </p>
                                                    <p>
                                                        <input type="radio" id="going_and_comingback_otherday" name="goingandcoming" value="going_and_comingback_otherday">
                                                        <label for="going_and_comingback_otherday">
                                                            @lang('language.goingAndComingbackOtherDay')
                                                            <br>
                                                        </label>
                                                    </p>

                                                    <div class="row return-trip mb-4" style="display: none">
                                                        {{-- start return datetime --}}
                                                        <div class="col-6 mt-3 private-date date return_date">
                                                            <p style="text-align: right">
                                                                @lang('titles.returnDate')
                                                                <sup style="color: red">*</sup>
                                                            </p>
                                                            <div class="row justdate date" style="position: relative">
                                                                <input name="return_private_date" class="form-control" type="date" min="2013-12-25" placeholder="@lang('titles.returnDate')">
                                                            </div>
                                                        </div>
                                                        <div class="col-6 mt-3 private-date return_time">
                                                            <p style="text-align: right">
                                                                @lang('titles.returnTime')
                                                                <sup style="color: red">*</sup>
                                                            </p>
                                                            <div class="row time">
                                                                <input type="time" name="return_private_time" class="form-control" value="" placeholder="@lang('titles.returnTime')">
                                                            </div>
                                                        </div>
                                                        {{-- end return datetime --}}

                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row" style="width: 100%; margin: 0;">
                                                {{-- old location reserve for other  --}}
                                                <div class="col-md-3"></div>

                                                <div class="col-md-4 col-8">
                                                    @auth
                                                        @if (Auth::user()->account_type !== 'partner')
                                                            <div style="position: relative; {{ Auth::user()->affiliate_balance + Auth::user()->own_balance >= 50 ? '' : 'margin-top: 20px;'}}">
                                                                <input class="form-control" style="width: 100%" type="text" name="private_promo_code" placeholder="@lang('language.promoCode')" value="">
                                                                <button type="button" class="btn btn-primary private-check-promo">@lang('language.check')</button>
                                                            </div>
                                                            @if (Auth::user()->affiliate_balance + Auth::user()->own_balance >= 50)
                                                                <div style="position: relative; margin: 10px 0; text-align: right">
                                                                    <label class="switch">
                                                                        <input class="eco-use50egpfromwallet" name="use_50EGPfROMWallet" type="checkbox">
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                    <div style="margin-top: -30px; text-align: left" class="use50egpfromwallet-label">
                                                                        @lang('language.use50EgpFromWallet')
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @else
                                                            <input type="hidden" name="private_promo_code" value="">
                                                        @endif
                                                    @else
                                                        <div style="position: relative; margin-top: 20px;">
                                                            <input class="form-control" style="width: 100%" type="text" name="private_promo_code" placeholder="@lang('language.promoCode')" value="">
                                                            <button type="button" class="btn btn-primary private-check-promo">@lang('language.check')</button>
                                                        </div>
                                                    @endauth
                                                </div>
                                                <div class="col-md-3 col-4 pull-left request-cost" style="font-size: 20px; margin: 10px 0 30px 0;">
                                                    700 @lang('language.egp')
                                                </div>
                                            </div>
                                            <div class="row" style="width: 100%; margin: 0">
                                                <div class="col-sm-12">
                                                    <p style="font-size: 14px; width: 100%;" >
                                                        يتم اضافة 50 ج م لكل ساعة انتظار اضافيه
                                                        او تأخير
                                                    </p>
                                                </div>
                                                <div class="col-sm-4"></div>
                                                @auth
                                                    @if (Auth::user()->account_type !== 'driver')
                                                        <div class="col-sm-4 mt-3 private-submit-container">
                                                            <input style="width: 100%" class="btn btn-primary float-right" type="submit" name="request_private" value="@lang('language.requestPrivate')">
                                                        </div>
                                                    @endif
                                                @else
                                                    <div class="col-sm-4 mt-3 private-submit-container">
                                                        <input style="width: 100%" class="btn btn-primary float-right" type="submit" name="request_private" value="@lang('language.requestPrivate')">
                                                    </div>
                                                @endauth
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components.footer')

@endsection

@section('page-scripts')

    <script type="text/javascript" src="{!! asset('js/picker.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.date.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.time.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/ar.js') !!}"></script>


    <!-- Modal -->
    <div class="modal fade" dir="rtl" id="confirmPrivate" tabindex="-1" role="dialog" aria-labelledby="confirmPrivateLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmPrivateLabel">@lang('language.confirm')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="    margin: -1rem  auto -1rem -1rem; ">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @lang('language.areYouSureYouWantToRequestPrivate')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('language.cancelOrder')</button>
                    <button type="button" class="btn btn-primary confirm-private">@lang('language.confirm')</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript"> // select change scripts
        // start generic fromto

        $.ajaxSetup({
            // async: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function () {
            checkEconomy();
            getPrivateRegions();
            // $('.reserve-for-others button').click();
        })
        var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);

        var privateDatePicker = $('input[name=private_date]').pickadate({
            disable: [
                { from: [0,0,0], to: yesterday }
            ]
        });
        var privateDatePicker2 = $('input[name=return_private_date]').pickadate({
            disable: [
                { from: [0,0,0], to: yesterday }
            ]
        });

        var privateTimePicker = $('input[name=private_time]').pickatime()
        var privateTimePicker2 = $('input[name=return_private_time]').pickatime()

        $('select[name=from_city]').change(function () {
            getAlltos();
        });

        $('select[name=to_city]').change(function () {
            checkEconomy();
            getPrivateRegions();
            updatePrivatePrice();
        });

        $('.from-region').change(function () {
            updatePrivatePrice();
        });
        $('.to-region').change(function () {
            updatePrivatePrice();
        });
        // end  generic fromto


        // start economy script
        var availableTripsByDate;
        $('.eco-date').change(function () {
            $('.eco-time').html('');
            for (day of availableTripsByDate[$('.eco-date').val()]) {
                $('.eco-time').append(`<option value="${day.departure_time}">${day.formatted_departure_time}</option>`)
            }

            checkAvailableSeats()
            updateEconomyPrice()
        });
        $('.eco-time').change(function () {
            checkAvailableSeats();
            updateEconomyPrice()
        })

        $('.seats  input[type=checkbox]').change(function () {
            updateEconomyPrice();
        });

        $('input[name=use_50EGPfROMWallet]').change(function () {
            applyPrivatePromo();
        })

        $('input[name=eco_use_50EGPfROMWallet]').change(function () {
            applyPromo();
        })

        $('input[name=goingandcoming]').change(function () {
            updatePrivatePrice();
            console.log($('input[name=goingandcoming]:checked').val());
            if ($('input[name=goingandcoming]:checked').val() == 'going_and_comingback_otherday')
            $('.return-trip').slideDown('fast');
            else
            $('.return-trip').slideUp('fast');
        })

        $('.reserve-eco').submit(function (e) {
            e.preventDefault();
            $('input[name="book_trip"]').attr('disabled', 'disabled');
            $('input[name="book_trip"]').val('@lang('language.loading')');
            $('#loader').fadeIn();


            $.ajax({
                method: 'POST',
                url: '/confirm-economy',
                data: {
                    trip_id: $('input[name=trip_id]').val(),
                    seat_1: $('input[name=seat_1]:checked').length,
                    seat_2: $('input[name=seat_2]:checked').length,
                    seat_3: $('input[name=seat_3]:checked').length,
                    promo_code: $('#promo_code').val(),
                    use_50EGP_credit: $('input[name=eco_use_50EGPfROMWallet]:checked').length,
                    reservation_type: $('input[name=reservation_type]').val(),
                    setfor_name: $('input[name=setfor_name]').val(),
                    setfor_phone: $('input[name=setfor_phone]').val()
                },
                success: function (data) {
                    console.log(data);
                    if (data.code == '401')
                    window.location.href = '/login';

                    if (data.code == '403')
                    window.location.href = '/phone/verify';

                    if (data.code == '200')
                    window.location.href = '/confirm-economy';
                    $('#loader').fadeOut();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    console.error(textStatus);
                    alert(errorThrown.toString())
                    $('#loader').fadeOut();
                    $('input[name="book_trip"]').removeAttr('disabled');
                    $('input[name="book_trip"]').val('@lang('language.book')');
                }
            })
        })


        function getPrivateDate(dateElement) {
            var privateDateArray = dateElement.val().split(' ');
            var months = [ 'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر' ];
            privateDateArray[1] = months.indexOf(privateDateArray[1]) + 1;

            if (privateDateArray[1] < 10)
            privateDateArray[1] = '0' + privateDateArray[1];
            finalPrivateDate = '';
            for (var i = 2; i >= 0; i--) {
                if (i == 0) {
                    finalPrivateDate += privateDateArray[i];
                    continue ;
                }
                finalPrivateDate += privateDateArray[i] + '-';
            }
            return finalPrivateDate;
        }

        $('form[action="/add-private-trip"]').submit(function (e) {
            e.preventDefault();
            $('#confirmPrivate').modal();
        })

        $('.confirm-private').click(function (e) {
            confirmPrivate();
        })

        function confirmPrivate() {
            $('.private-submit-container input').attr("disabled","disable");
            $('.private-submit-container input').val('@lang('language.pleaseWait')');
            $('.error').remove();

            $('#loader').fadeIn();

            var finalPrivateDate = getPrivateDate($('input[name=private_date]'));
            var privateReturnDate = getPrivateDate($('input[name=return_private_date]'));

            $.ajax({
                url: '/add-private-trip',
                method: 'POST',
                data: {
                    from_city: $('select[name=from_city]').val(),
                    to_city: $('select[name=to_city]').val(),
                    from_city_region: $('.from-region').val(),
                    to_city_region: $('.to-region').val(),
                    from_city_address: $('input[name=from_pri_address]').val(),
                    to_city_address: $('input[name=to_pri_address]').val(),
                    request_type: $('input[name=reservation_type]').val(),
                    setfor_name: $('.private-setfor_name').val(),
                    setfor_phone: $('.private-setfor_phone').val(),
                    date: finalPrivateDate,
                    return_date: privateReturnDate,
                    time: privateTimePicker.val(),
                    return_time: privateTimePicker2.val(),
                    trip_type: $('input[name=goingandcoming]:checked').val(),
                    price: totalPrivatePrice,
                    price_going: privatePriceGoing,
                    price_going_and_comingback: privatePriceGoingAndComingback,
                    price_going_and_comingback_otherday: privatePriceGoingAndComingbackOtherday,
                    use_50EGP_credit: $('input[name=use_50EGPfROMWallet]:checked').length,
                    private_promo: $('input[name=private_promo_code]').val(),
                    passenger_notes: $('textarea[name=passenger_notes]').val()
                },
                success: function (data) {
                    console.log(data);
                    $('.private-submit-container input').removeAttr('disabled');
                    $('.private-submit-container input').val('@lang('language.requestPrivate')');
                    if (data.code == '422') {
                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                $('.' + field).append(``);
                                if (field == 'price')
                                alert('@lang('language.cantMakePrivateTripWithoutPrice')');
                                $('.' + field).append(`
                                <small class="error">
                                    <br>
                                    ${data.response[field]}
                                </small>
                                `);
                                $(window).scrollTop($('.' + field).offset().top - 60);

                            }
                        }
                        $('#confirmPrivate').modal('hide');
                    }

                    if (data.code == '401')
                    window.location.href = '/login';

                    if (data.code == '403')
                    window.location.href = '/phone/verify';

                    if (data.code == '200')
                    window.location.href = data.response.url;

                    $('#loader').fadeOut();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus);
                    alert(errorThrown.toString());
                    $('#loader').fadeOut();
                    $('input[name="book_trip"]').removeAttr('disabled');
                    $('input[name="book_trip"]').val('@lang('language.book')');
                }
            })
        }

        function checkEconomy() {
            $('#loader').fadeIn()
            $.ajax({
                method: 'POST',
                url: '/api/dest/economy-trips',
                data: {
                    from_city: $('select[name=from_city]').val(),
                    to_city: $('select[name=to_city]').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        if (data.response.length == 0) {
                            $('.economy-container form').addClass('d-none');
                            $('.fromto').after('عذرا، الرحلات الاقتصادية غير متاحة مؤقتا يمكنك طلب رحلة اقتصادية في اي وقت');
                            $('.only-private').removeClass('d-none');
                            $('.nav-tabs').addClass('d-none');
                            $('.no-eco-trips').css({'display': 'block'});
                            $('.privateTrips-btn').click();
                        } else {
                            $('.economyTrips-btn').click();
                            $('.only-private').addClass('d-none');
                            $('.nav-tabs').removeClass('d-none');
                            $('.economy-container form').removeClass('d-none');
                            $('.no-eco-trips').css({'display': 'none'});

                            availableTripsByDate = data.response;
                            $('.eco-date').html('');
                            for (var date in data.response) {
                                if (data.response.hasOwnProperty(date)) {
                                    $('.eco-date').append(`<option value="${date}">${formatDate(new Date(date))}</option>`);
                                }
                            }
                            $('.eco-time').html('');
                            for (time of data.response[$('.eco-date').val()]) {
                                $('.eco-time').append(`<option value="${time.departure_time}">${time.formatted_departure_time}</option>`);
                            }
                        }
                    }
                    checkAvailableSeats();

                    $('#loader').fadeOut()
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus, errorThrown);
                }
            })
        }

        function checkAvailableSeats() {
            $('#loader').fadeIn();
            $('#frontseat_in').prop('checked', false);
            $('#frontseat').removeClass('checked');
            $('#frontseat_in').attr('disabled', true);
            $('#frontseat').css('opacity', '.5');

            $('#backseat_right_in').prop('checked', false);
            $('#backseat_right').removeClass('checked');
            $('#backseat_right_in').attr('disabled', true);
            $('#backseat_right').css('opacity', '.5');

            $('#backseat_left_in').prop('checked', false);
            $('#backseat_left').removeClass('checked');
            $('#backseat_left_in').attr('disabled', true);
            $('#backseat_left').css('opacity', '.5');
            $.ajax({
                method: 'POST',
                url: '/api/economy/trip-seats',
                data: {
                    from_city: $('select[name=from_city]').val(),
                    to_city: $('select[name=to_city]').val(),
                    date: $('.eco-date').val(),
                    time: $('.eco-time').val()
                },
                success: function (data) {

                    if (data.response != null) {
                        $('input[name=trip_id]').val(data.response.id);

                        $('#car_load').css({display: 'none'});
                        if (data.response.seat_1 == '0') {
                            $('#frontseat_in').removeAttr('disabled');
                            $('#frontseat').css('opacity', '1');
                        }
                        if (data.response.seat_2 == '0') {
                            $('#backseat_right_in').removeAttr('disabled');
                            $('#backseat_right').css('opacity', '1');
                        }
                        if (data.response.seat_3 == '0') {
                            $('#backseat_left_in').removeAttr('disabled');
                            $('#backseat_left').css('opacity', '1');
                        }
                    }

                    $('#loader').fadeOut();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus, errorThrown);
                }
            })
        }

        var totalPrice = 0; // tptal economy price
        function updateEconomyPrice() {
            var tickets = 0;
            for (var i = 1; i <= 3; i++) {
                if ($('.seat:nth-child('+ i +') .seat-check').prop('disabled') == false && $('.seat:nth-child('+ i +') .seat-check').prop('checked') == true) {
                    tickets ++;
                }
            }
            if (tickets) {
                $('input[name=book_trip]').removeAttr('disabled');
                $('.check-promo').removeAttr('disabled');
            } else {
                $('input[name=book_trip]').attr('disabled', true);
                $('.check-promo').attr('disabled', true);
            }


            $.ajax({
                method: 'POST',
                url: '/api/economy/price',
                data: {
                    from_city: $('select[name=from_city]').val(),
                    to_city: $('select[name=to_city]').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        console.log(totalPrice);
                        totalPrice = tickets * data.response.price;
                        if ($('input[name=eco_use_50EGPfROMWallet]:checked').length && tickets)
                        totalPrice -= 25;
                        $('#tick_total').html(`${totalPrice} @lang('language.egp')`)
                    } else if (data.code == '422') {
                        $('#tick_total').html('@lang('language.contactAdminAboutThePrice')')
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus, errorThrown);
                }
            })
        }
        // end economy script

        // start private scripts
        function getPrivateRegions() {
            // from
            $.ajax({
                method: 'POST',
                url: '/api/private/regions',
                data: {
                    city_name: $('select[name=from_city]').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('.from-region').html('');
                        for (city of data.response) {
                            let selected = city.main_region == '1' ? 'selected' : '';
                            $('.from-region').append(`
                            <option ${selected} value="${city.region}">${city.region}</option>
                            `);
                        }
                    } else if (data.code == '422') {
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                alert(data.response[error]);
                            }
                        }
                    }
                }
            })
            // to
            $.ajax({
                method: 'POST',
                url: '/api/private/regions',
                data: {
                    city_name: $('select[name=to_city]').val()
                },
                success: function (data) {
                    $('.to-region').html('');
                    for (city of data.response) {
                        let selected = city.main_region == '1' ? 'selected' : '';
                        $('.to-region').append(`
                            <option ${selected} value="${city.region}">${city.region}</option>
                        `);
                    }
                    updatePrivatePrice();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus, errorThrown);
                }
            })
        }

        var totalPrivatePrice;
        var privatePriceGoing;
        var privatePriceGoingAndComingback;
        var privatePriceGoingAndComingbackOtherday;
        function updatePrivatePrice() {
            $.ajax({
                method: 'POST',
                url: '/api/private/price',
                data: {
                    from_city: $('select[name=from_city]').val(),
                    to_city: $('select[name=to_city]').val(),
                    from_region: $('.from-region').val(),
                    to_region: $('.to-region').val(),
                    going_and_comingback: $('input[name=goingandcoming]:checked').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        var base_price = data.response.base_price;
                        var additional_amount = data.response.additional_amount_from + data.response.additional_amount_to;
                        if ($('input[name=use_50EGPfROMWallet]:checked').length)
                        base_price -= 50;

                        $('.request-cost').html(`${base_price + additional_amount} @lang('language.egp')`);
                        totalPrivatePrice = base_price + additional_amount;
                        privatePriceGoing = data.response.price_going;
                        privatePriceGoingAndComingback = data.response.price_going_and_comingback;
                        privatePriceGoingAndComingbackOtherday = data.response.price_going_and_comingback_otherday;
                    } else {
                        $('.request-cost').html(`<small>@lang('language.contactAdminAboutThePrice') </small>`);
                        updatePrivatePrice()
                    }
                }
            })
        }
        // end   private scripts



        // car clicks
        $('.nav-tabs li a').addClass('p-2')
        $('#frontseat').click(function () {
            $('#frontseat').toggleClass('checked');
            if ($('#frontseat').hasClass('checked')) {
                $('#frontseat_in').attr('checked', true);
            } else {
                $('#frontseat_in').removeAttr('checked');
            }
        });

        $('#backseat_right').click(function () {
            $('#backseat_right').toggleClass('checked');
            if ($('#backseat_right').hasClass('checked')) {
                $('#backseat_right_in').attr('checked', true);
            } else {
                $('#backseat_right_in').removeAttr('checked');
            }
        });

        $('#backseat_left').click(function () {
            $('#backseat_left').toggleClass('checked');
            if ($('#backseat_left').hasClass('checked')) {
                $('#backseat_left_in').attr('checked', true);
            } else {
                $('#backseat_left_in').removeAttr('checked');
            }
        });

        $('.private-check-promo').click(function () {
            applyPrivatePromo();
        })

        $('.check-promo').click(function () {
            applyPromo();
        })

        function applyPromo() {
            updateEconomyPrice();
            $('.check-promo').attr('disabled', true);
            if (totalPrice)
            $.ajax({
                method: 'POST',
                url: '/api/economy/promo',
                data: {
                    from_city: $('select[name=from_city]').val(),
                    to_city: $('select[name=to_city]').val(),
                    // promo_code: $('#promo_code').val()
                    promo_code: $('input[name=promo_code]').val()
                },
                success: function (data) {
                    console.log(data);
                    if (data.code == '200') {
                        if (data.response.type == 'percent')
                        totalPrice = Math.floor(
                            totalPrice - (totalPrice * (data.response.amount / 100) )
                        );
                        else if (data.response.type == 'flat_rate')
                        totalPrice = totalPrice - data.response.amount;
                        $('#tick_total').html(`${totalPrice} @lang('language.egp')`);
                        // $('.check-promo').css({'color': 'green'});
                        $('input[name=promo_code]').addClass('is-valid').removeClass('is-invalid');
                        $('input[name=promo_code]').attr('disabled', true);

                        $('.check-promo').addClass('btn-success').removeClass('btn-primary').removeClass('btn-danger');
                        $('.check-promo').attr('disabled', true);
                    } else {
                        $('input[name=promo_code]').addClass('is-invalid').removeClass('is-valid');
                        $('.check-promo').addClass('btn-danger').removeClass('btn-primary').removeClass('btn-success');

                        $('input[name=promo_code]').removeAttr('disabled');
                        $('.check-promo').removeAttr('disabled');
                    }
                }
            });
        }

        function applyPrivatePromo() {
            updatePrivatePrice();
            setTimeout(function () {
                $.ajax({
                    method: 'POST',
                    url: '/api/private/promo',
                    data: {
                        from_city: $('select[name=from_city]').val(),
                        to_city: $('select[name=to_city]').val(),
                        promo_code: $('input[name=private_promo_code]').val()
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.code == '200') {
                            if (data.response.type == 'percent')
                            totalPrivatePrice = Math.floor(
                                totalPrivatePrice - (totalPrivatePrice * (data.response.amount / 100) )
                            );
                            else if (data.response.type == 'flat_rate')
                            totalPrivatePrice = totalPrivatePrice - data.response.amount;

                            $('.request-cost').html(`${totalPrivatePrice} @lang('language.egp')`);

                            $('input[name=private_promo_code]').attr('disabled', true);
                            $('.private-check-promo').attr('disabled', true);

                            $('input[name=private_promo_code]').addClass('is-valid').removeClass('is-invalid');
                            $('.private-check-promo').addClass('btn-success').removeClass('btn-primary').removeClass('btn-danger');
                        } else {
                            $('input[name=private_promo_code]').removeAttr('disabled');
                            $('.private-check-promo').removeAttr('disabled');

                            $('input[name=private_promo_code]').addClass('is-invalid').removeClass('is-valid');
                            $('.private-check-promo').addClass('btn-danger').removeClass('btn-primary').removeClass('btn-success');
                        }
                    }
                });

            }, 500);
        }

        function formatDate(date) {
            var monthNames = [
                "@lang('language.Jan')", "@lang('language.Feb')", "@lang('language.Mar')",
                "@lang('language.Apr')", "@lang('language.May')", "@lang('language.Jun')", "@lang('language.Jul')",
                "@lang('language.Aug')", "@lang('language.Sep')", "@lang('language.Oct')",
                "@lang('language.Nov')", "@lang('language.Dec')"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' '
            /*+ year*/;
        }

        function removeDuplicate(selector) {
            var liText = '', liList = $(selector), listForRemove = [];
            $(liList).each(function () {
                var text = $(this).text();
                if (liText.indexOf('|'+ text + '|') == -1) {
                    liText += '|'+ text + '|';
                }
                else {
                    listForRemove.push($(this));
                }
            });
            $(listForRemove).each(function () { $(this).remove(); });

        }

        function getAlltos() {
            $.ajax({
                url: '/getAlltos',
                method: 'POST',
                data: {
                    from_city: $('select[name=from_city]').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('select[name=to_city]').html('');
                        for (city of data.response) {
                            $('select[name=to_city]').append(`
                                <option value="${city.city_name}">
                                    ${city.city_name_ar}
                                </option>
                            `);
                        }
                        removeDuplicate('select[name=to_city] option');
                        $('select[name=to_city]').change();
                    } else if (data.code == '422') {
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                alert(data.response[error]);
                            }
                        }
                    }
                }
            })
        }



        var today = new Date().toISOString().split('T')[0];
        document.getElementsByName("private_date")[0].setAttribute('min', today);
        $('input[name=privaeDateViewer]').click(function () {
            $('input[name=private_date]').click();
        })
        $('.reserve-for-others button').click(function () {
            $('.reserve-for-others button').removeClass('active');
            $(this).addClass('active');
            if ($(this).attr('reserve-for-other') == 'true') {
                $('.other-person-info').slideDown('fast')
                // $('.other-person-info').css({'display': 'block'});
                $('input[name=reservation_type]').val('other')
            } else {
                $('.other-person-info').slideUp('fast')
                // $('.other-person-info').css({'display': 'none'});
                $('input[name=reservation_type]').val('self')
            }
        })

        $('.setfor-economy-toggler').change(function () {
            if ($('.setfor-economy-toggler:checked').length) {
                $('.other-person-info').slideDown('fast')
                $('input[name=reservation_type]').val('other')
            } else {
                $('.other-person-info').slideUp('fast')
                $('input[name=reservation_type]').val('self')
            }
        })
        $('.setfor-private-toggler').change(function () {
            if ($('.setfor-private-toggler:checked').length) {
                $('.other-person-info').slideDown('fast')
                $('input[name=reservation_type]').val('other')
            } else {
                $('.other-person-info').slideUp('fast')

                $('input[name=reservation_type]').val('self')
            }
        })
    </script>
@endsection
