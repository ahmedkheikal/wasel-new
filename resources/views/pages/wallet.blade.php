@extends('layouts.page')

@section('styles')
    <style media="screen">
        .copy-promo-code {
            cursor: pointer;
        }
        .table * {
            text-align: right;
        }
        .wallet-element {
            color: #444;
            margin: 30px auto;
        }
        .wallet-element span {
            background: #5cb85c;
            color: white;
            border-radius: 20px;
            padding: 5px 10px;
            margin-right: 10px;
            font-weight: bold;
        }

        .partner-promo {
            color: #444;
            margin: 30px auto;
        }
        .partner-promo span {
            background: #fba829;
            color: white;
            border-radius: 20px;
            padding: 5px 10px;
            margin-right: 10px;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
    @include('components.header')

    <div class="container" dir="rtl" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-right">@lang('titles.wallet')</div>

                    <div class="card-body text-right" dir="rtl">
                        @if (Auth::user()->account_type == 'partner')
                            @if ($partnerPromoCode !== null)
                                <p class="partner-promo">
                                    @lang('language.promoCode'):
                                    <span class="copy-promo-code">
                                        {{ $partnerPromoCode->promo_code }}
                                        <button type="button" class="btn btn-success copy-promo-code" style="background: transparent; border: none; outline: none;top: 0; border-radius: 50%;">
                                            <i class="fa fa-copy"></i>
                                        </button>
                                    </span>
                                    <br>
                                    <span style="background: transparent; color: #888; font-weight: normal; width: 40%; font-size: 14px">
                                        {{ 'يتم خصم '. $partnerPromoCode->amount .' ج.م من تكلفة الرحلة للمسافر عند استخدامه هذا الكود ويتم إضافة '. 5 .'% من إجمالي قيمة الرحلة بعد إتمامها بنجاح إلي محفظتك خلال 24 الساعة' }}
                                    </span>
                                </p>
                            @endif
                        @endif
                        <p class="wallet-element">
                            @lang('language.balance'):  <span>{{ Auth::user()->affiliate_balance + Auth::user()->own_balance }} @lang('language.egp')</span>
                        </p>
                        <p class="wallet-element">
                            @lang('language.holdBalance'):
                            {{ Auth::user()->hold_balance }} @lang('language.egp')
                        </p>
                    </div>
                    <div class="m-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>@lang('language.amount')</th>
                                    <th>@lang('language.description')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($paidTransactions as $transaction)
                                    <tr>
                                        <td>{{ $transaction->amount }} @lang('language.egp')</td>
                                        <td>{{ $transaction->description }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">@lang('language.noTransaction')</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $paidTransactions->links() }}
                        <hr>
                        <p class="text-center" style="color: #666 ">
                            @lang('language.affiliate.inviteFriendsToGet50EGP')
                            <br> <br>
                            <a href="/invite-friends">
                                <button type="button" class="btn btn-primary center-block">@lang('language.affiliate.inviteFriends')</button>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('components.footer')
@endsection

@section('scripts')
    @if (Auth::user()->account_type == 'partner')
        @if ($partnerPromoCode !== null)
            <input type="text" value="{{ $partnerPromoCode->promo_code }}" style="position: absolute; left: -200px; opacity: 0; z-index: -10" id="partnerPromoCode">
            <script type="text/javascript">
            $('.copy-promo-code').click(function (e) {
                alert('@lang('language.copiedSuccessfully')')
                $('#partnerPromoCode').select();
                document.execCommand('copy');
                // TODO: prompt copied successfully
                $('#partnerPromoCode').blur();
            })
            </script>
        @endif
    @endif
@endsection
