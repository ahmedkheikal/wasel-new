@extends('layouts.page')

@section('styles')
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.date.css') !!}">
    <style media="screen">
    .form-control:disabled, .form-control[readonly] {
        background-color: white !important;
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" dir="rtl" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body text-right" dir="rtl">
                        <div class="row">
                            <div class="col-sm-3">
                            </div>
                            <div class="offset-sm-3 col-sm-6">
                                <div class="alert alert-success text-center">
                                    @lang('language.incity.success')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('components.footer')
@endsection
