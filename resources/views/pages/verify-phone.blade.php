@extends('layouts.page')

@section('styles')
    <style media="screen">
        form input[type=text] {
            font-size: 20px !important;
            letter-spacing: 20px;
            text-align: center;
        }
        [placeholder] {
            letter-spacing: 0
        }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div id="loader" style="display: block">
        <img src="{!! asset('images/loader.svg') !!}" alt="">
    </div>
    <div class="container" style="margin-top: 80px; min-height: 70vh">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header text-right">
                        @lang('language.verifyPhone')
                    </div>
                    <div class="row my-3 mx-2" dir="rtl">
                        <div class="col-md-3">
                        </div>
                        <div class="offset-md-3 col-md-6">
                            <form class="update-phone-and-resendCode mb-2" action="index.html" method="post">
                                <input type="tel" name="phone" class="form-control mb-1" value="{{ Auth::user()->phone }}">
                                <br>
                                @lang('language.didntReceiveVerCode')?
                                <input type="submit" class="btn btn-primary" name="update_phone" value="@lang('language.resendCode')">
                                <div class="alert alert-success resend-code-alert" style="display: none">
                                    @lang('language.sentSuccessfully')
                                </div>
                            </form>
                            <form class="text-center" method="post">
                                <p>@lang('language.verificationCodeSent')</p>
                                <input size="4" type="text" name="verification_code" class="form-control" value="">
                                <input type="submit" name="send_code" class="mt-3 btn btn-primary" value="@lang('language.send')">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
    $(document).ready(function () {
        sendCode();
    })
    function sendCode() {
        $.ajax({
            url: '/phone/send-code',
            method: 'POST',
            data: {
                user_id: '{{ Auth::user()->id }}'
            },
            success: function (data) {
                if (data.code == '200') {
                    $('#loader').fadeOut();
                }
            }
        })
    }
    $('form').submit(function (e) {
        e.preventDefault();

        $('#loader').fadeIn();

        $.ajax({
            method: 'POST',
            url: '/phone/verify',
            processData: false,
            cache: false,
            contentType: false,
            data: new FormData(this),
            success: function (data) {

                if (data.code == '200') {
                    if (data.response.ajax == undefined || data.response.ajax == false) {
                        window.location.href = data.response.url;
                    } else {
                        $('#loader').fadeIn();
                        if (data.response.ajax == true) {
                            $.ajax({
                                method: data.response.method,
                                url: data.response.url,
                                data: data.response.params,
                                success: function (data) {
                                    window.location.href = data.response.url;
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.error(textStatus);
                                    console.error(errorThrown);
                                    $('#loader').fadeOut();
                                }
                            })
                        }
                        // else {
                        //     window.location.href = data.response.url;
                        // }
                    }
                } else {
                    $('#loader').fadeOut();
                    if (typeof data.response != 'string') {
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                if (error == 'agree_terms_and_conds') {
                                    $('input[name='+ error +'] + label').after(`
                                        <small class="error">
                                            <br>
                                            @lang('language.youHaveToAgreeTermsAndConds')
                                        </small>
                                    `);
                                    $('input[name='+ error +']').focus();
                                } else {
                                    $('input[name='+ error +']').after(`
                                        <small class="error">${data.response[error]}</small>
                                    `);
                                    $('input[name='+ error +']').focus();
                                }
                            }
                        }
                    } else {
                        alert(data.response)
                    }
                }
            }
        })
    })

    $('.update-phone-and-resendCode').submit(function (e) {
        $.ajax({
            url: '/phone/update',
            data: new FormData(this),
            processData: false,
            cache: false,
            contentType: false,
            success: function (data) {
                if (data.code == '200') {
                    $('input[name=update_phone]').attr('disabled', 'disabled');
                    $('.resend-code-alert').slideDown('fast');
                    setTimeout(function () {
                        $('.resend-code-alert').slideUp('fast');
                    }, 2000)
                } else if (data.code == '422') {
                    $('input[name=update_phone]').attr('disabled', 'disabled');
                    $('.resend-code-alert').removeClass('alert-success').addClass('alert-danger');
                    $('.resend-code-alert').html(data.response);
                    $('.resend-code-alert').slideDown('fast');
                    setTimeout(function () {
                        $('.resend-code-alert').slideUp('fast');
                    }, 2000)
                }
            }
        });
        setTimeout(function () {
            sendCode();
        }, 1000)
    })
</script>
@endsection
