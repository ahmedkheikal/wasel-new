<form class="form-inline" action="/add-private-trip" method="post">
    {{-- start fromto regions --}}

    <div class="col-md-6 from_city_region from_city_address region-address form-inline">
        <p style="text-align: right; width: 100%">
            @lang('titles.departure')
            <sup style="color: red">*</sup>
        </p>
        <select class="form-control from-region" name="from_region">
            {{-- <option value="">lorem lorem</option> --}}
        </select>
        <input type="text" name="from_pri_address" class="form-control" value="" placeholder="@lang('titles.writeFromAddress')">
    </div>
    <div class="col-md-6 to_city_region to_city_address region-address form-inline">
        <p style="text-align: right; width: 100%">
            @lang('titles.arrival')
            <sup style="color: red">*</sup>
        </p>
        <select class="form-control to-region" name="to_region">
            {{-- <option value="">lorem lorem</option> --}}
        </select>
        <input type="text" name="to_pri_address" class="form-control" value="" placeholder="@lang('titles.writeToAddress')">
    </div>
    {{-- end fromto regions --}}

    {{-- start datetime --}}
    <div class="col-6 mt-3 private-date justdate date">
        <p style="text-align: right">
            @lang('titles.date')
            <sup style="color: red">*</sup>
        </p>
        <div class="row justdate date" style="position: relative">
            <input name="private_date" class="form-control" type="date" min="2013-12-25" placeholder="@lang('titles.date')">
        </div>
    </div>
    <div class="col-6 mt-3 private-date">
        <p style="text-align: right">
            @lang('titles.time')
            <sup style="color: red">*</sup>
        </p>
        <div class="row time">
            {{-- <input type="time" name="private_time" class="form-control" value="" placeholder="@lang('titles.time')"> --}}
            <select name="private_time" class="form-control">
                <?php
                    $hour = date('h');
                    $type = date('A');

                    if ($type == 'PM') {
                        $start = $hour + 12 + 6;
                    } else {
                        $start = $hour + 2;
                        $start = date('h', strtotime($start));
                    }
                    
                    $time = [
                        '32' => "12:30 AM", '33' => "01:00 AM", '34' => "01:30 AM",
                        '35' => "02:00 AM", '36' => "02:30 AM", '37' => "03:00 AM",
                        '38' => "03:30 AM", '39' => "04:00 AM", '40' => "04:30 AM",
                        '41' => "05:00 AM", '42' => "05:30 AM", '43' => "06:00 AM",
                        '44' => "06:30 AM", '45' => "07:00 AM", '46' => "07:30 AM",
                        '47' => "08:00 AM", '48' => "08:30 AM", '1' => "09:00 AM",
                        
                        '2' => "09:30 AM", '3' => "10:00 AM", '4' => "10:30 AM",
                        '5' => "11:00 AM", '6' => "11:30 AM", '7' => "12:00 PM", 
                        '8' => "12:30 PM", '9' => "01:00 PM", '10' => "01:30 PM", 
                        
                        '11' => "02:00 PM", '12' => "02:30 PM", '13' => "03:00 PM",
                        '14' => "03:30 PM", '15' => "04:00 PM", '16' => "04:30 PM",
                        '17' => "05:00 PM", '18' => "05:30 PM", '19' => "06:00 PM",
                        '20' => "06:30 PM", '21' => "07:00 PM", '22' => "07:30 PM",
                        '23' => "08:00 PM", '24' => "08:30 PM", '25' => "09:00 PM",
                        '26' => "09:30 PM", '27' => "10:00 PM", '28' => "10:30 PM",
                        '29' => "11:00 PM", '30' => "11:30 PM", '31' => "12:00 AM"
                    ];
                ?>
                @for($i = $start; $i <= 30; $i += 1)
                    @if(array_key_exists($i, $time))
                        <option value="{{ date('h:i A', strtotime($time[$i])) }}">{{ date('h:i A', strtotime($time[$i])) }}</option>
                    @endif
                @endfor
            </select>
        </div>
    </div>
    {{-- end datetime --}}

    {{-- start reserve for others --}}
    <div class="row mt-4" style="width: 100%; margin: 0">
        @auth
            @if (Auth::user()->account_type == 'partner')
                <input type="hidden" name="reservation_type" value="other">
                <div class="other-person-info">
                    <div class="row" style="margin: 15px 0;">
                        <div class="col-md-6 setfor_name">
                            <input type="text" class="form-control private-setfor_name" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                        </div>
                        <div class="col-md-6 setfor_phone">
                            <input type="tel" class="form-control private-setfor_phone" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                        </div>
                    </div>
                </div>
            @elseif (Auth::user()->account_type == 'passenger')
                <input type="hidden" name="reservation_type" value="self">

                <div class="row mt-2 mb-2" style="width: 100%; margin: 0">
                    <div class="col-2" dir="rtl">
                        <label class="switch">
                            <input class="setfor-private-toggler" type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <div class="col-10 text-right">
                        <span>
                            @lang('language.someOneElseWillUseThisReservation')
                        </span>
                    </div>
                </div>

                <div class="other-person-info" style="display: none">
                    <div class="row" style="margin: 0 ;" dir="rtl">
                        <div class="col-md-6 setfor_name" dir="rtl">
                            <input type="text" class="form-control private-setfor_name" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                        </div>
                        <div class="col-md-6 setfor_phone">
                            <input type="tel" class="form-control private-setfor_phone" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                        </div>
                    </div>
                </div>
            @endif
        @endauth
    </div>
    {{-- end reserve for others --}}
    <div class="col-sm-12 mb-4 passenger_notes">
        <p style="text-align: right">@lang('titles.notes')</p>
        <textarea style="width: 100%" name="passenger_notes" rows="2" class="form-control" placeholder="@lang('language.notes')"></textarea>
    </div>

    <div class="col-12">
        <hr>
    </div>
    <div class="col-12" style="text-align: right">
        <div class="pull-right">
            <p>
                <input type="radio" id="going" name="goingandcoming" value="going" checked>
                <label for="going">
                    @lang('language.going')
                </label>
            </p>
            <p>
                <input type="radio" id="going_and_comingback" name="goingandcoming" value="going_and_comingback">
                <label for="going_and_comingback">
                    @lang('language.goingAndComingback')
                    <small class="waiting-time" style="font-size: 10px">(انتظار حتى 4 ساعات)</small> <br>
                </label>
            </p>
            <p>
                <input type="radio" id="going_and_comingback_otherday" name="goingandcoming" value="going_and_comingback_otherday">
                <label for="going_and_comingback_otherday">
                    @lang('language.goingAndComingbackOtherDay')
                    <br>
                </label>
            </p>

            <div class="row return-trip mb-4" style="display: none">
                {{-- start return datetime --}}
                <div class="col-6 mt-3 private-date date return_date">
                    <p style="text-align: right">
                        @lang('titles.returnDate')
                        <sup style="color: red">*</sup>
                    </p>
                    <div class="row justdate date" style="position: relative">
                        <input name="return_private_date" class="form-control" type="date" min="2013-12-25" placeholder="@lang('titles.returnDate')">
                    </div>
                </div>
                <div class="col-6 mt-3 private-date return_time">
                    <p style="text-align: right">
                        @lang('titles.returnTime')
                        <sup style="color: red">*</sup>
                    </p>
                    <div class="row time">
                        <input type="time" name="return_private_time" class="form-control" value="" placeholder="@lang('titles.returnTime')">
                    </div>
                </div>
                {{-- end return datetime --}}

            </div>

        </div>
    </div>

    <div class="row" style="width: 100%; margin: 0;">
        {{-- old location reserve for other  --}}
        <div class="col-md-3"></div>

        <div class="col-md-4 col-8">
            @auth
                @if (Auth::user()->account_type !== 'partner')
                    <div style="position: relative; {{ Auth::user()->affiliate_balance + Auth::user()->own_balance >= 50 ? '' : 'margin-top: 20px;'}}">
                        <input class="form-control" style="width: 100%" type="text" name="private_promo_code" placeholder="@lang('language.promoCode')" value="">
                        <button type="button" class="btn btn-primary private-check-promo">@lang('language.check')</button>
                    </div>
                    @if (Auth::user()->affiliate_balance + Auth::user()->own_balance >= 50)
                        <div style="position: relative; margin: 10px 0; text-align: right">
                            <label class="switch">
                                <input class="eco-use50egpfromwallet" name="use_50EGPfROMWallet" type="checkbox">
                                <span class="slider round"></span>
                            </label>
                            <div style="margin-top: -30px; text-align: left" class="use50egpfromwallet-label">
                                @lang('language.use50EgpFromWallet')
                            </div>
                        </div>
                    @endif
                @else
                    <input type="hidden" name="private_promo_code" value="">
                @endif
            @else
                <div style="position: relative; margin-top: 20px;">
                    <input class="form-control" style="width: 100%" type="text" name="private_promo_code" placeholder="@lang('language.promoCode')" value="">
                    <button type="button" class="btn btn-primary private-check-promo">@lang('language.check')</button>
                </div>
            @endauth
        </div>
        <div class="col-md-3 col-4 pull-left request-cost" style="font-size: 20px; margin: 10px 0 30px 0;">
            700 @lang('language.egp')
        </div>
    </div>
    <div class="row" style="width: 100%; margin: 0">
        <div class="col-sm-12">
            <p style="font-size: 14px; width: 100%;" class="text-center">
                يتم اضافة 50 ج م لكل ساعة انتظار اضافيه
                او تأخير
            </p>
        </div>
        <div class="col-sm-4"></div>
        @auth
            @if (Auth::user()->account_type !== 'driver')
                <div class="col-sm-4 mt-3 private-submit-container">
                    <input style="width: 100%" class="btn btn-primary float-right" type="submit" name="request_private" value="@lang('language.requestPrivate')">
                </div>
            @endif
        @else
            <div class="col-sm-4 mt-3 private-submit-container">
                <input style="width: 100%" class="btn btn-primary float-right" type="submit" name="request_private" value="@lang('language.requestPrivate')">
            </div>
        @endauth
    </div>
</form>