<link rel="stylesheet" href="{!! asset('css/pickadate-theme.css') !!}">
<link rel="stylesheet" href="{!! asset('css/pickadate-theme.date.css') !!}">
<link rel="stylesheet" href="{!! asset('css/pickadate-theme.time.css') !!}">
<style media="screen">
    .modal-footer>:last-child {
        margin-right: .25rem;
    }
    .modal-footer>:not(:last-child) {
        margin-left: .25rem;
    }
    .only-private {
        color: #444;
        text-align: center;
    }
    ::placeholder {
        color: #888 !important
    }
    sup {
        font-size: 20px;
        top: auto;
    }
    @media (min-width: 845px) {
        .economy-container {
            margin-right: 25%;
        }
    }
    @media (max-width: 361px) {
        .use50egpfromwallet-label {
            font-size: 12px;
        }
    }
    .form-control:disabled, .form-control[readonly] {
        background-color: white !important;
    }
    /* start tabs  */

    .nav-tabs {
        padding: 0
    }
    .nav-tabs li {
        width: 50%;
        display: inline-block;
    }

    .nav-tabs li a {
        width: 100%;
        display: block;
        text-align: center;
        font-size: 17px;
    }
    .nav-tabs li a:hover {
        text-decoration: none;
    }

    .nav-tabs .active {
        border-bottom: 2px #fba829 solid;
    }
    /* end tabs  */

    /* #f8fafc */
    .from-region, .to-region {
        width: 100% !important;
    }
    .bg-light {
        background: white !important;
    }
    .fromto {
        box-shadow: inset 0 3px 1px rgba(0,0,0,0.13);
        padding: 10px;
        border-radius: 5px
    }

    .no-eco-trips {
        color: rgba(227, 52, 47, 0.5);
        font-weight: lighter;
    }
    .request-cost, .request-cost:focus {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 10px;
        border-radius: 3px;
    }

    .request-cost:hover {
        background: #5cb85c;
        border: none
    }

    .economy-cost, .economy-cost:focus {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 10px;
        border-radius: 3px;
    }

    .economy-cost:hover {
        background: #5cb85c;
        border: none
    }

    .private-check-promo {
        position: absolute;
        color: grey;
        top: 0px;
        left: 0;
        cursor: pointer;
        color: white;
    }
    .check-promo {
        position: absolute !important;
        color: white;
        top: 0;
        left: 0;
        cursor: pointer;
    }
</style>
<link rel="stylesheet" href="{{ asset('css/car.css') }}">