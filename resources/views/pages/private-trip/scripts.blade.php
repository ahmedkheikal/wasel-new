<script type="text/javascript"> // select change scripts
    // start generic fromto

    $.ajaxSetup({
        // async: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function () {
        getPrivateRegions();
    })
    // Edit the time
    $('input[name=private_date]').on('change', function() {
        var optionValue = $(this).val();
        var arr = optionValue.split(" ");
        var date = arr[0];
        var current = "{{ date("d") }}";
        
        var currentHour = "{{ date('h') }}";
        var currentType = "{{ date('A') }}";

        var time = {
            32: "12:30 AM", 33: "01:00 AM", 34: "01:30 AM",
            35: "02:00 AM", 36: "02:30 AM", 37: "03:00 AM",
            38: "03:30 AM", 39: "04:00 AM", 40: "04:30 AM",
            41: "05:00 AM", 42: "05:30 AM", 43: "06:00 AM",
            44: "06:30 AM", 45: "07:00 AM", 46: "07:30 AM",
            47: "08:00 AM", 48: "08:30 AM", 1: "09:00 AM",
            
            2: "09:30 AM", 3: "10:00 AM", 4: "10:30 AM",
            5: "11:00 AM", 6: "11:30 AM", 7: "12:00 PM", 
            8: "12:30 PM", 9: "01:00 PM", 10: "01:30 PM", 
            
            11: "02:00 PM", 12: "02:30 PM", 13: "03:00 PM",
            14: "03:30 PM", 15: "04:00 PM", 16: "04:30 PM",
            17: "05:00 PM", 18: "05:30 PM", 19: "06:00 PM",
            20: "06:30 PM", 21: "07:00 PM", 22: "07:30 PM",
            23: "08:00 PM", 24: "08:30 PM", 25: "09:00 PM",
            26: "09:30 PM", 27: "10:00 PM", 28: "10:30 PM",
            29: "11:00 PM", 30: "11:30 PM", 31: "12:00 AM"
        };

        var time1 = {
            1: "12:00 AM", 2: "12:30 AM", 3: "01:00 AM", 4: "01:30 AM",
            5: "02:00 AM", 6: "02:30 AM", 7: "03:00 AM",
            8: "03:30 AM", 9: "04:00 AM", 10: "04:30 AM",
            11: "05:00 AM", 12: "05:30 AM", 13: "06:00 AM",
            14: "06:30 AM", 15: "07:00 AM", 16: "07:30 AM",
            17: "08:00 AM", 18: "08:30 AM", 19: "09:00 AM",
            20: "09:30 AM", 21: "10:00 AM", 22: "10:30 AM",
            23: "11:00 AM", 24: "11:30 AM", 25: "12:00 PM", 
            26: "12:30 PM", 27: "01:00 PM", 28: "01:30 PM", 
            29: "02:00 PM", 30: "02:30 PM", 31: "03:00 PM",
            32: "03:30 PM", 33: "04:00 PM", 34: "04:30 PM",
            35: "05:00 PM", 36: "05:30 PM", 37: "06:00 PM",
            38: "06:30 PM", 39: "07:00 PM", 40: "07:30 PM",
            41: "08:00 PM", 42: "08:30 PM", 43: "09:00 PM",
            44: "09:30 PM", 45: "10:00 PM", 46: "10:30 PM",
            47: "11:00 PM", 48: "11:30 PM", 
        };
        
        start = 12;
        if (currentType == 'PM') {
            start = parseInt(currentHour) + 12 + 6;
        } else {
            start = currentHour + 2;
            <?php $start = date('h') + 2; ?>
            start = parseInt("{{ date('h', strtotime($start)) }}");
        }

        $('select[name=private_time]').empty();
        // var time = '';
        var opt = '';

        /*for(var i = $start; $i <= 30; $i += 1) {
            
        }*/

        if (date == current) {
            for (var i = start; i <= 30; i++){
                if(time[i] != undefined) {
                    opt += "<option value='" + time[i] + "'>"+ time[i] +"</option>";
                }
                
            }
        } else {
            for (var i = 1; i <= 48; i++){
                if(time1[i] != undefined) {
                    opt += "<option value='" + time1[i] + "'>"+ time1[i] +"</option>";
                }
            }
        }

        $('select[name=private_time]').append(opt);
    });

    var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);

    var privateDatePicker = $('input[name=private_date]').pickadate({
        disable: [
            { from: [0,0,0], to: yesterday }
        ]
    });
    var privateDatePicker2 = $('input[name=return_private_date]').pickadate({
        disable: [
            { from: [0,0,0], to: yesterday }
        ]
    });

    var privateTimePicker = $('input[name=private_time]').pickatime()
    var privateTimePicker2 = $('input[name=return_private_time]').pickatime()

    $('select[name=from_city]').change(function () {
        getAlltos();
    });

    $('select[name=to_city]').change(function () {
        getPrivateRegions();
        updatePrivatePrice();
    });

    $('.from-region').change(function () {
        updatePrivatePrice();
    });
    $('.to-region').change(function () {
        updatePrivatePrice();
    });
    // end  generic fromto

    $('input[name=use_50EGPfROMWallet]').change(function () {
        applyPrivatePromo();
    })

    $('input[name=goingandcoming]').change(function () {
        updatePrivatePrice();
        console.log($('input[name=goingandcoming]:checked').val());
        if ($('input[name=goingandcoming]:checked').val() == 'going_and_comingback_otherday')
        $('.return-trip').slideDown('fast');
        else
        $('.return-trip').slideUp('fast');
    })

    function getPrivateDate(dateElement) {
        var privateDateArray = dateElement.val().split(' ');
        var months = [ 'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر' ];
        privateDateArray[1] = months.indexOf(privateDateArray[1]) + 1;

        if (privateDateArray[1] < 10)
        privateDateArray[1] = '0' + privateDateArray[1];
        finalPrivateDate = '';
        for (var i = 2; i >= 0; i--) {
            if (i == 0) {
                finalPrivateDate += privateDateArray[i];
                continue ;
            }
            finalPrivateDate += privateDateArray[i] + '-';
        }
        return finalPrivateDate;
    }

    $('form[action="/add-private-trip"]').submit(function (e) {
        e.preventDefault();
        $('#confirmPrivate').modal();
    })

    $('.confirm-private').click(function (e) {
        confirmPrivate();
    })

    function confirmPrivate() {
        $('.private-submit-container input').attr("disabled","disable");
        $('.private-submit-container input').val('@lang('language.pleaseWait')');
        $('.error').remove();

        $('#loader').fadeIn();

        var finalPrivateDate = getPrivateDate($('input[name=private_date]'));
        var privateReturnDate = getPrivateDate($('input[name=return_private_date]'));

        $.ajax({
            url: '/add-private-trip',
            method: 'POST',
            data: {
                from_city: $('select[name=from_city]').val(),
                to_city: $('select[name=to_city]').val(),
                from_city_region: $('.from-region').val(),
                to_city_region: $('.to-region').val(),
                from_city_address: $('input[name=from_pri_address]').val(),
                to_city_address: $('input[name=to_pri_address]').val(),
                request_type: $('input[name=reservation_type]').val(),
                setfor_name: $('.private-setfor_name').val(),
                setfor_phone: $('.private-setfor_phone').val(),
                date: finalPrivateDate,
                return_date: privateReturnDate,
                time: $('select[name=private_time]').val(),
                return_time: privateTimePicker2.val(),
                trip_type: $('input[name=goingandcoming]:checked').val(),
                price: totalPrivatePrice,
                price_going: privatePriceGoing,
                price_going_and_comingback: privatePriceGoingAndComingback,
                price_going_and_comingback_otherday: privatePriceGoingAndComingbackOtherday,
                use_50EGP_credit: $('input[name=use_50EGPfROMWallet]:checked').length,
                private_promo: $('input[name=private_promo_code]').val(),
                passenger_notes: $('textarea[name=passenger_notes]').val()
            },
            success: function (data) {
                console.log(data);
                $('.private-submit-container input').removeAttr('disabled');
                $('.private-submit-container input').val('@lang('language.requestPrivate')');
                if (data.code == '422') {
                    for (var field in data.response) {
                        if (data.response.hasOwnProperty(field)) {
                            $('.' + field).append(``);
                            if (field == 'price')
                            alert('@lang('language.cantMakePrivateTripWithoutPrice')');
                            $('.' + field).append(`
                            <small class="error">
                                <br>
                                ${data.response[field]}
                            </small>
                            `);
                            $(window).scrollTop($('.' + field).offset().top - 60);

                        }
                    }
                    $('#confirmPrivate').modal('hide');
                }

                if (data.code == '401')
                window.location.href = '/login';

                if (data.code == '403')
                window.location.href = '/phone/verify';

                if (data.code == '200')
                window.location.href = data.response.url;

                $('#loader').fadeOut();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(textStatus);
                alert(errorThrown.toString());
                $('#loader').fadeOut();
                $('input[name="book_trip"]').removeAttr('disabled');
                $('input[name="book_trip"]').val('@lang('language.book')');
            }
        })
    }

    // start private scripts
    function getPrivateRegions() {
        // from
        $.ajax({
            method: 'POST',
            url: '/api/private/regions',
            data: {
                city_name: $('select[name=from_city]').val()
            },
            success: function (data) {
                if (data.code == '200') {
                    $('.from-region').html('');
                    for (city of data.response) {
                        let selected = city.main_region == '1' ? 'selected' : '';
                        $('.from-region').append(`
                        <option ${selected} value="${city.region}">${city.region}</option>
                        `);
                    }
                } else if (data.code == '422') {
                    for (var error in data.response) {
                        if (data.response.hasOwnProperty(error)) {
                            alert(data.response[error]);
                        }
                    }
                }
            }
        })
        // to
        $.ajax({
            method: 'POST',
            url: '/api/private/regions',
            data: {
                city_name: $('select[name=to_city]').val()
            },
            success: function (data) {
                $('.to-region').html('');
                for (city of data.response) {
                    let selected = city.main_region == '1' ? 'selected' : '';
                    $('.to-region').append(`
                        <option ${selected} value="${city.region}">${city.region}</option>
                    `);
                }
                updatePrivatePrice();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown);
            }
        })
    }

    var totalPrivatePrice;
    var privatePriceGoing;
    var privatePriceGoingAndComingback;
    var privatePriceGoingAndComingbackOtherday;
    function updatePrivatePrice() {
        $.ajax({
            method: 'POST',
            url: '/api/private/price',
            data: {
                from_city: $('select[name=from_city]').val(),
                to_city: $('select[name=to_city]').val(),
                from_region: $('.from-region').val(),
                to_region: $('.to-region').val(),
                going_and_comingback: $('input[name=goingandcoming]:checked').val()
            },
            success: function (data) {
                if (data.code == '200') {
                    var base_price = data.response.base_price;
                    var additional_amount = data.response.additional_amount_from + data.response.additional_amount_to;
                    if ($('input[name=use_50EGPfROMWallet]:checked').length)
                    base_price -= 50;

                    $('.request-cost').html(`${base_price + additional_amount} @lang('language.egp')`);
                    totalPrivatePrice = base_price + additional_amount;
                    privatePriceGoing = data.response.price_going;
                    privatePriceGoingAndComingback = data.response.price_going_and_comingback;
                    privatePriceGoingAndComingbackOtherday = data.response.price_going_and_comingback_otherday;
                } else {
                    $('.request-cost').html(`<small>@lang('language.contactAdminAboutThePrice') </small>`);
                    updatePrivatePrice()
                }
            }
        })
    }
    // end   private scripts




    $('.private-check-promo').click(function () {
        applyPrivatePromo();
    })

    function applyPrivatePromo() {
        updatePrivatePrice();
        setTimeout(function () {
            $.ajax({
                method: 'POST',
                url: '/api/private/promo',
                data: {
                    from_city: $('select[name=from_city]').val(),
                    to_city: $('select[name=to_city]').val(),
                    promo_code: $('input[name=private_promo_code]').val()
                },
                success: function (data) {
                    console.log(data);
                    if (data.code == '200') {
                        if (data.response.type == 'percent')
                        totalPrivatePrice = Math.floor(
                            totalPrivatePrice - (totalPrivatePrice * (data.response.amount / 100) )
                        );
                        else if (data.response.type == 'flat_rate')
                        totalPrivatePrice = totalPrivatePrice - data.response.amount;

                        $('.request-cost').html(`${totalPrivatePrice} @lang('language.egp')`);

                        $('input[name=private_promo_code]').attr('disabled', true);
                        $('.private-check-promo').attr('disabled', true);

                        $('input[name=private_promo_code]').addClass('is-valid').removeClass('is-invalid');
                        $('.private-check-promo').addClass('btn-success').removeClass('btn-primary').removeClass('btn-danger');
                    } else {
                        $('input[name=private_promo_code]').removeAttr('disabled');
                        $('.private-check-promo').removeAttr('disabled');

                        $('input[name=private_promo_code]').addClass('is-invalid').removeClass('is-valid');
                        $('.private-check-promo').addClass('btn-danger').removeClass('btn-primary').removeClass('btn-success');
                    }
                }
            });

        }, 500);
    }

    function formatDate(date) {
        var monthNames = [
            "@lang('language.Jan')", "@lang('language.Feb')", "@lang('language.Mar')",
            "@lang('language.Apr')", "@lang('language.May')", "@lang('language.Jun')", "@lang('language.Jul')",
            "@lang('language.Aug')", "@lang('language.Sep')", "@lang('language.Oct')",
            "@lang('language.Nov')", "@lang('language.Dec')"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' '
        /*+ year*/;
    }

    function removeDuplicate(selector) {
        var liText = '', liList = $(selector), listForRemove = [];
        $(liList).each(function () {
            var text = $(this).text();
            if (liText.indexOf('|'+ text + '|') == -1) {
                liText += '|'+ text + '|';
            }
            else {
                listForRemove.push($(this));
            }
        });
        $(listForRemove).each(function () { $(this).remove(); });

    }

    function getAlltos() {
        $.ajax({
            url: '/getAlltos',
            method: 'POST',
            data: {
                from_city: $('select[name=from_city]').val()
            },
            success: function (data) {
                if (data.code == '200') {
                    $('select[name=to_city]').html('');
                    for (city of data.response) {
                        $('select[name=to_city]').append(`
                            <option value="${city.city_name}">
                                ${city.city_name_ar}
                            </option>
                        `);
                    }
                    removeDuplicate('select[name=to_city] option');
                    $('select[name=to_city]').change();
                } else if (data.code == '422') {
                    for (var error in data.response) {
                        if (data.response.hasOwnProperty(error)) {
                            alert(data.response[error]);
                        }
                    }
                }
            }
        })
    }



    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("private_date")[0].setAttribute('min', today);
    $('input[name=privaeDateViewer]').click(function () {
        $('input[name=private_date]').click();
    })
    $('.reserve-for-others button').click(function () {
        $('.reserve-for-others button').removeClass('active');
        $(this).addClass('active');
        if ($(this).attr('reserve-for-other') == 'true') {
            $('.other-person-info').slideDown('fast')
            // $('.other-person-info').css({'display': 'block'});
            $('input[name=reservation_type]').val('other')
        } else {
            $('.other-person-info').slideUp('fast')
            // $('.other-person-info').css({'display': 'none'});
            $('input[name=reservation_type]').val('self')
        }
    })

    $('.setfor-private-toggler').change(function () {
        if ($('.setfor-private-toggler:checked').length) {
            $('.other-person-info').slideDown('fast')
            $('input[name=reservation_type]').val('other')
        } else {
            $('.other-person-info').slideUp('fast')

            $('input[name=reservation_type]').val('self')
        }
    })

    document.getElementsByName("private_date")[0].setAttribute('min', today);
    $('input[name=privaeDateViewer]').click(function () {
        $('input[name=private_date]').click();
    })
</script>
