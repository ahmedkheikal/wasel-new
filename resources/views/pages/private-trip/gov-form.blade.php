<form class="main-form" action="/add-trip" method="post" dir="{{ Config::get('app.locale') == 'ar' ? 'rtl' : 'ltr' }}">
    {{ csrf_field() }}
    <div class="row" style="font-weight: bolder; font-size: 17px">
        <div class="col-md-6 account_type == 'driverity">
            <label for="from_city" class="float-right">
                @lang('titles.from')
            </label>
            <select class="form-control" name="from_city" style="font-size: 17px; height: 3rem">
                <option disabled selected value="">@lang('titles.from')</option>
                @foreach ($cities as $city)
                    {{-- <option {{ $from_city == $city->city_name ? 'selected' : '' }} value="{{ $city->city_name }}">@lang("db.{$city->city_name}")</option> --}}
                    <option {{ $from_city == $city['city_name'] ? 'selected' : '' }} value="{{ $city['city_name'] }}">@lang("db.{$city['city_name']}")</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6 to_city">
            <label for="to_city" class="float-right">
                @lang('titles.to')
            </label>
            <select class="form-control" name="to_city" style="font-size: 17px; height: 3rem">
                <option disabled selected value="">@lang('titles.to')</option>
                @foreach ($toCities as $city)
                    <option {{ $to_city == $city['city_name'] ? 'selected' : '' }} value="{{ $city['city_name'] }}">@lang("db.{$city['city_name']}")</option>
                @endforeach
            </select>
        </div>

    </div>
</form>