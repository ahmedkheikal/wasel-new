@extends('layouts.page')

@section('styles')
    <meta name="og:type" content="Invitaion"/>
    <meta name="og:url" content="{{ env('APP_URL', 'https://www.waselegypt.com') }}"/>
    <meta name="og:image" content="http://ia.media-imdb.com/rock.jpg"/>
    <meta name="og:site_name" content="WASEL"/>

    @auth
        <meta name="og:title" content="{{ $user->username }}"/>
        <meta name="og:description" content="Join {{ $user->username }} and ride with WASEL with 50EGP off on your first trip"/>
    @else
        <meta name="og:title" content="WASEL"/>
        <meta name="og:description" content="Join me and ride with WASEL with 50EGP off on your first trip"/>
    @endauth
    <style media="screen">

        .overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.5);
        }
        .share-referal-code img {
            width: 10%;
            margin: 11%;
            margin-top: 4%;
        }
        .share-referal-code {
            width: 40%
        }
        @media (max-width: 890px) {
            .share-referal-code {
                width: 100%;
            }
        }
        .inviteFriends {
            display: none;
        }
        @media (min-width: 810px) {
            .sh-ref-code-popup {
                display: none;
            }
        }
        @media (max-width: 810px) {
            html, body {
                overflow: hidden;
            }
            .inviteFriends {
                display: block;
            }
        }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>

        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-12">
                <div class="card" dir="rtl">
                    <div class="card-header text-right">@lang('titles.inviteFriends')</div>

                    <div class="card-body">
                        @guest
                            <p style="color: #888; text-align: center">
                                @lang('language.affiliate.inviteFriendsAndGet50EGPOff')
                            </p>
                        @endguest
                        <div class="row" style="position: relative">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <p class="referal-code text-center form-control">
                                    {!! $htmlInvitationText !!}
                                </p>
                            </div>
                            <div class="col-md-3">
                            </div>


                            <div class="col-md-3"></div>
                            <div class="col-md-6" onclick="showAffshare()">
                                <button style="width: 100%" type="button" class="btn btn-primary inviteFriends">@lang('language.affiliate.inviteFriends')</button>
                                <p style="margin: 5px 0; text-align: center">أو</p>
                            </div>
                            <div class="col-md-3">
                            </div>


                            <div class="col-md-3">
                            </div>
                        </div>
                        <div class="share-referal-code text-center" dir="rtl" style="margin: auto">

                            <button style="width: 100%; font-size: 18px" type="button" class="btn btn-success copy-ref-code">
                                <i class="fa fa-copy"></i>
                                @lang('language.copy')
                            </button>
                            <div class="alert alert-success" style="width: 100%; text-align: center ; margin:  10px auto; display: none">
                                @lang('language.copiedSuccessfully')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('components.footer')
@endsection

@section('scripts')
    <div class="sh-ref-code-popup">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>
                        <a href="https://api.whatsapp.com/send?text={{ rawurlencode($invitationText) }}">
                            <img src="{!! asset('images/social/whatsapp.png') !!}" alt="whatsapp">
                            WhatsApp
                        </a>
                    </li>
                    <li>
                        <a href="sms:?body={!! rawurlencode($invitationText) !!}">
                            <img src="{!! asset('images/social/sms.png') !!}" alt="sms">
                            SMS
                        </a>
                    </li>
                    {{-- <li>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={!! $invitationText !!}">
                            <img src="{!! asset('images/social/facebook.png') !!}" alt="facebook">
                            Facebook
                        </a>
                    </li> --}}
                    <li>
                        <a href="fb-messenger://share?link={!! rawurlencode($invitationText) !!}">
                            <img src="{!! asset('images/social/messenger.png') !!}" alt="messenger">
                            Messenger
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/home?status={!! rawurlencode($invitationText) !!}">
                            <img src="{!! asset('images/social/twitter.png') !!}" alt="twitter">
                            Twitter
                        </a>
                    </li>
                    <li>
                        <a class="native-android-share">
                            Other
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div onclick="hideAffshare()" class="overlay"></div>
    <textarea rows="8" cols="80" id="invitationText"
        style="position: absolute;top: -200px; z-index: -2;">
        {{ $invitationText }}
    </textarea>
    <script type="text/javascript">
        var Affshare = $('.sh-ref-code-popup');
        $('.overlay').fadeOut('fast');
        function showAffshare() {
            $('.overlay').fadeIn('fast');
            Affshare.animate({bottom: 0});
        }
        function hideAffshare() {
            $('.overlay').fadeOut('fast');
            Affshare.animate({bottom: '-100vw'});

        }
        $('.copy-ref-code').click(function (e) {
            $('#invitationText').select();
            document.execCommand('copy');
            // TODO: prompt copied successfully
            $('#invitationText').blur();
            $('.alert-success').slideDown();
            setTimeout(function () {
                $('.alert-success').slideUp();
            }, 3000);
        })

        $('.inviteFriends').click(function (e) {
            e.preventDefault();

        })

        $('.native-android-share').click(function () {
            if (navigator.share) {
                navigator.share({
                    title: 'واصل',
                    text: $('#invitationText').html(),
                    // url: '{{ $invitationURL }}',
                })
                .then(() => console.log('Successful share'))
                .catch((error) => console.log('Error sharing', error));
            }
        })
    </script>
@endsection
