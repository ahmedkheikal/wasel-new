@extends('layouts.page')

@section('styles')
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.date.css') !!}">
    <style media="screen">
    .form-control:disabled, .form-control[readonly] {
        background-color: white !important;
    }
    @media (max-width: 765px) {
        .fedex-logo {
            margin-top: 20px;
        }
        .margin-bottom-small {
            margin-bottom: 5px;
        }
    }
    .dimentions .form-control {
        padding-left: 0 !important;
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" dir="rtl" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-right">@lang('language.shipping.title')</div>
                    <div class="card-body text-right" dir="rtl">
                        <div class="row">
                            <div class="col-6">
                                <p style="margin-top: 15px; text-align: left">هذه الخدمه مقدمه من واصل بالتعاقد مع</p>
                            </div>
                            <div class="col-6 fedex-logo">
                                <img src="{!! asset('images/fedex.png') !!}" style="text-align: right; width: 100%; max-width: 200px" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <form class="send-request" action="/express/add" method="post">
                                    {{-- from --}}
                                    <label for="from_city">@lang('titles.from')</label>
                                    <div class="mb-3 card">
                                        <div class="row card-body">
                                            <div class="col-md-6 margin-bottom-small">
                                                <select class="form-control" id="from_city" name="from_city" >
                                                    <option value="Minya">@lang('cities.Minya')</option>
                                                    <option value="Assiut">@lang('cities.Assiut')</option>
                                                    <option value="Bani Suef">@lang('cities.Bani Suef')</option>
                                                    <option value="Cairo">@lang('cities.Cairo')</option>
                                                    <option value="Giza">@lang('cities.Giza')</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 mb-2">
                                                <select id="from_city_address" name="from_city_address" class="form-control"  readonly>
                                                    <option selected value="مدينة المنيا">
                                                        مدينة المنيا
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-12">
                                                <input type="text" id="pickup_address" name="pickup_address" class="form-control" placeholder="@lang('language.shipping.pickupLocation')" value="">
                                                {{-- <input type="text" id="pickup_address" name="pickup_address" class="form-control" value="فرع واصل بالمنيا: أمام مسجد صلاح الدين بجوار كوبري الاخصاص"> --}}
                                            </div>
                                        </div>
                                    </div>
                                    {{-- /.from --}}

                                    {{-- to --}}
                                    <label for="from_city">@lang('titles.to')</label>
                                    <div class="card mb-3">
                                        <div class="row card-body">
                                            {{-- recepient --}}
                                            <div class="margin-bottom-small col-md-6">
                                                <input type="text" name="receiver_name" class="form-control" placeholder="@lang('language.receiver_name')" value="">
                                            </div>
                                            <div class="col-md-6 mb-2">
                                                <input type="text" name="receiver_phone" class="form-control" placeholder="@lang('language.receiver_phone')" value="">
                                            </div>
                                            {{-- /.recepient --}}
                                            <div class="col-md-6 margin-bottom-small">
                                                <select class="form-control" id="to_city" name="to_city">
                                                    <option value="Cairo">@lang('cities.Cairo')</option>
                                                    <option value="Giza">@lang('cities.Giza')</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 mb-2 margin-bottom-small">
                                                <select class="form-control" name="to_city_address">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                            <div class="col-12">
                                                <input type="text" id="arrival_address" name="arrival_address"
                                                    class="form-control" value=""
                                                    placeholder="@lang('language.shipping.dropOffLocation')">
                                            </div>
                                        </div>
                                    </div>
                                    {{-- /.to --}}

                                    {{-- details --}}
                                    <div class="row card-body">
                                        {{-- <div class="col-12 mb-3">
                                            <input name="datetime" class="form-control" type="date" placeholder="@lang('language.shipping.orderDate')">
                                        </div> --}}
                                        <div class="col-md-4 mb-3">
                                            <select class="form-control" name="order_type">
                                                <option disabled selected value="">@lang('language.shipping.orderType')</option>
                                                <option value="document">@lang('language.shipping.document')</option>
                                                <option value="gadget">@lang('language.shipping.gadgets')</option>
                                                <option value="box">@lang('language.shipping.box')</option>
                                                <option value="other">@lang('language.shipping.other')</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8 mb-3">
                                            <select class="form-control" name="weight">
                                                <option selected disabled value="">@lang('language.shipping.pleaseSpecifyTypeFirst')</option>
                                                {{-- text --}}
                                                {{-- <option value="1 kg">1 كجم أو أقل</option>
                                                <option value="2 kg">2 كجم أو أقل</option>
                                                <option value="3 kg">3 كجم أو أقل</option>
                                                <option value="4 kg">4 كجم أو أقل</option>
                                                <option value="5 kg">5 كجم أو أقل</option>
                                                <option value="6 kg">6 كجم أو أقل</option>
                                                <option value="7 kg">7 كجم أو أقل</option>
                                                <option value="8 kg">8 كجم أو أقل</option>
                                                <option value="9 kg">9 كجم أو أقل</option>
                                                <option value="10 kg">10 كجم أو أقل</option> --}}
                                                {{-- /.text --}}
                                            </select>
                                            {{-- dimentions --}}
                                            <div class="row dimentions mt-3">
                                                <div class="col-4">
                                                    <select class="form-control" name="length" placeholder="@lang('language.shipping.length')">
                                                        <option selected disabled value="">@lang('language.shipping.length')</option>
                                                        <option value="20 cm">20 سم</option>
                                                        <option value="30 cm">30 سم</option>
                                                        <option value="40 cm">40 سم</option>
                                                        <option value="50 cm">50 سم</option>
                                                    </select>
                                                </div>
                                                <div class="col-4">
                                                    <select class="form-control" name="width" placeholder="@lang('language.shipping.width')">
                                                        <option selected disabled value="">@lang('language.shipping.width')</option>
                                                        <option value="20 cm">20 سم</option>
                                                        <option value="30 cm">30 سم</option>
                                                        <option value="40 cm">40 سم</option>
                                                        <option value="50 cm">50 سم</option>
                                                    </select>
                                                </div>
                                                <div class="col-4">
                                                    <select class="form-control" name="height" placeholder="@lang('language.shipping.height')">
                                                        <option selected disabled value="">@lang('language.shipping.height')</option>
                                                        <option value="20 cm">20 سم</option>
                                                        <option value="30 cm">30 سم</option>
                                                        <option value="40 cm">40 سم</option>
                                                        <option value="50 cm">50 سم</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {{-- /.dimentions --}}
                                        </div>
                                        <div class="col-12">
                                            <textarea name="order_description" rows="4" class="form-control" placeholder="@lang('language.shipping.orderDescription')"></textarea>
                                        </div>
                                    </div>
                                    {{-- /.details --}}

                                    <div class="row">
                                        {{-- instructions  --}}
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <p style="color: #999; font-size: 15px; text-align: center; margin-top: 20px">
                                                يتم تجميع و توزيع الشحنات يوميا فى الفتره من الساعه ٩ صباحا وحتى ٣ مساء ما عدا ايام الجمعه، اذا قمت بطلب الشحن بعد الساعه ٣ مساءا يتم تجميع الشحنه فى اليوم التالى.
                                            </p>
                                        </div>
                                        <div class="col-md-3"></div>
                                        {{-- /.instructions  --}}
                                        {{-- price --}}
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8 offset-md-2">
                                            <div id="tick_total" class="request-cost" style="width: 100%; font-size: 20px; margin: 10px 0 30px 0;">
                                                0 @lang('language.egp')
                                            </div>
                                        </div>
                                        {{-- /.price --}}

                                        {{-- submit --}}
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="form-check row mb-3">
                                                <div class="col-md-12 agree_terms_and_conds">
                                                    <input class="form-check-input" type="checkbox" value="1" id="agree_terms_and_conds" name="agree_terms_and_conds">
                                                    <label class="form-check-label mr-4" for="agree_terms_and_conds">
                                                        أوافق على
                                                        <a href="/express/terms-and-conditions" target="_blank">
                                                            الشروط والأحكام
                                                        </a>
                                                    </label>
                                                </div>
                                            </div>
                                            <input type="submit" name="add_shipping_request" value="@lang('language.send')" class="form-control btn btn-primary">
                                        </div>
                                        {{-- .submit --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('components.footer')
@endsection
@section('page-scripts')
    <script type="text/javascript" src="{!! asset('js/picker.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.date.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/ar.js') !!}"></script>

    <script type="text/javascript">
    $('#loader').show();
    $(document).ready(function () {
        $('#loader').fadeOut();
        $('.dimentions').fadeOut();
    })

    $('select[name=from_city]').change(function () {
        getFromRegions();
    })

    $('select[name=to_city]').change(function () {
        getToRegions();
    })

    $('form.send-request').submit(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'POST',
            url: '/express/add',
            data: {
                from_city: $('select[name=from_city]').val(),
                from_city_address: $('select[name=from_city_address]').val(),
                pickup_address: $('input[name=pickup_address]').val(),

                to_city: $('select[name=to_city]').val(),
                to_city_address: $('select[name=to_city_address]').val(),
                arrival_address: $('input[name=arrival_address]').val(),

                receiver_name: $('input[name=receiver_name]').val(),
                receiver_phone: $('input[name=receiver_phone]').val(),

                // datetime: getFullDate($('input[name=datetime]')),

                order_type: $('select[name=order_type]').val(),
                weight: $('select[name=weight]').val(),
                order_description: $('textarea[name=order_description]').val(),
                length: $('select[name=length]').val(),
                width: $('select[name=width]').val(),
                height: $('select[name=height]').val()
            },
            success: function (data) {
                $('.error').remove();
                if (data.code == '200') {
                    location.href = '/express/success';
                } else if (data.code == '422') {
                    console.log(data.response);
                    for (var error in data.response) {
                        if (data.response.hasOwnProperty(error)) {
                            for (var i = 0; i < data.response[error].length; i++) {
                                $('[name='+ error +']').after(`
                                    <small class="error"> ${data.response[error][i]} </small>
                                `);
                            }
                        }
                    }
                } else if (data.code == '401') {
                    location.href = '/login';
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (errorThrown.toString() == 'Unauthorized')
                window.location.href = '/login';

                console.error(textStatus);
                alert(errorThrown.toString())
                $('#loader').fadeOut();
                $('input[name="book_trip"]').removeAttr('disabled');
                $('input[name="book_trip"]').val('@lang('language.book')');
            }
        })
    })

    $('select[name=weight]').change(function () {
        calculatePrice()
    });

    $('select[name=length]').change(function () {
        calculatePrice()
    });
    $('select[name=width]').change(function () {
        calculatePrice()
    });
    $('select[name=height]').change(function () {
        calculatePrice()
    });

    function calculatePrice() {
        $.ajax({
            method: 'POST',
            url: '/express/cost',
            data: {
                from: $('#from_city').val(),
                to: $('#to_city').val(),
                weight: $('select[name=weight]').val(),
                type: $('select[name=order_type]').val(),
                length: $('select[name=length]').val(),
                width: $('select[name=width]').val(),
                height: $('select[name=height]').val()
            },
            success: function (data) {
                if (data.code == '200') {
                    $('#tick_total').html(`@lang('language.shipping.approximatePrice') ${data.response.price} @lang('language.egp')`)
                } else if (data.code == '422') {
                    $('#tick_total').html(`@lang('language.shipping.pleaseCompleteOrderDetails')`)
                }
            }
        })
    }
    function getFromRegions() {
        $.ajax({
            method: 'POST',
            url: '/api/private/regions',
            data: {
                city_name: $('select[name=from_city]').val()
            },
            success: function (data) {
                $('select[name=from_city_address]').html('');
                if ($('select[name=from_city]').val() == 'Minya')
                $('select[name=to_city]').html(`
                    <option value="Cairo">@lang('cities.Cairo')</option>
                    <option value="Giza">@lang('cities.Giza')</option>
                `);
                else if ($('select[name=from_city]').val() == 'Cairo' || $('select[name=from_city]').val() == 'Giza')
                $('select[name=to_city]').html(`
                    <option value="Minya">@lang('cities.Minya')</option>
                    <option value="Assiut">@lang('cities.Assiut')</option>
                    <option value="Bani Suef">@lang('cities.Bani Suef')</option>
                `);

                for (city of data.response) {
                    let selected = city.main_region == '1' ? 'selected' : '';
                    $('select[name=from_city_address]').append(`
                        <option ${selected} value="${city.region}">${city.region}</option>
                    `);
                }
                $('select[name=to_city]').change();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown);
            }
        })
    }

    function getToRegions() {
        $.ajax({
            method: 'POST',
            url: '/api/private/regions',
            data: {
                city_name: $('select[name=to_city]').val()
            },
            success: function (data) {
                $('select[name=to_city_address]').html('');
                for (city of data.response) {
                    let selected = city.main_region == '1' ? 'selected' : '';
                    $('select[name=to_city_address]').append(`
                        <option ${selected} value="${city.region}">${city.region}</option>
                    `);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown);
            }
        })
    }
    $('select[name=to_city]').change()


    $('select[name=order_type]').change(function () {
        if ($(this).val() == 'document') {
            $('select[name=weight]').html(`
                <option disabled selected value="">@lang('language.shipping.weight')</option>
                <option value="1 kg">1 كجم أو أقل</option>
                <option value="2 kg">2 كجم أو أقل</option>
                <option value="3 kg">3 كجم أو أقل</option>
                <option value="4 kg">4 كجم أو أقل</option>
                <option value="5 kg">5 كجم أو أقل</option>
            `);
            $('.dimentions').fadeOut()
        } else if ($(this).val() == 'gadget') {
            $('select[name=weight]').html(`
                <option disabled selected value="">@lang('language.shipping.weight')</option>
                <option value="1 kg">1 كجم أو أقل</option>
                <option value="2 kg">2 كجم أو أقل</option>
                <option value="3 kg">3 كجم أو أقل</option>
                <option value="4 kg">4 كجم أو أقل</option>
                <option value="5 kg">5 كجم أو أقل</option>
            `);
            $('.dimentions').fadeOut()
        } else if ($(this).val() == 'box') {
            $('select[name=weight]').html(`
                <option disabled selected value="">@lang('language.shipping.weight')</option>
                <option value="5 kg">5 كجم أو أقل</option>
                <option value="10 kg">10 كجم أو أقل</option>
                <option value="15 kg">15 كجم أو أقل</option>
                <option value="20 kg">20 كجم أو أقل</option>
                <option value="25 kg">25 كجم أو أقل</option>
            `);
            $('.dimentions').fadeIn()
        } else if ($(this).val() == 'other') {
            $('.upload-photo').fadeIn();
            $('select[name=weight]').html(`
                <option disabled selected value="">@lang('language.shipping.weight')</option>
                <option value="1 kg">1 كجم أو أقل</option>
                <option value="2 kg">2 كجم أو أقل</option>
                <option value="3 kg">3 كجم أو أقل</option>
                <option value="4 kg">4 كجم أو أقل</option>
                <option value="5 kg">5 كجم أو أقل</option>
                <option value="10 kg">10 كجم أو أقل</option>
                <option value="15 kg">15 كجم أو أقل</option>
                <option value="20 kg">20 كجم أو أقل</option>
                <option value="25 kg">25 كجم أو أقل</option>
            `);
            $('.dimentions').fadeOut()
        }
    })

    // var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
    //
    // var privateDatePicker = $('input[name=datetime]').pickadate({
    //     disable: [
    //         { from: [0,0,0], to: yesterday }
    //     ]
    // });


    // function getFullDate(dateElement) {
    //     var privateDateArray = dateElement.val().split(' ');
    //     var months = [ 'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر' ];
    //     privateDateArray[1] = months.indexOf(privateDateArray[1]) + 1;
    //
    //     if (privateDateArray[1] < 10)
    //     privateDateArray[1] = '0' + privateDateArray[1];
    //     finalPrivateDate = '';
    //     for (var i = 2; i >= 0; i--) {
    //         if (i == 0) {
    //             finalPrivateDate += privateDateArray[i];
    //             continue ;
    //         }
    //         finalPrivateDate += privateDateArray[i] + '-';
    //     }
    //     return finalPrivateDate;
    // }
    </script>
@endsection
