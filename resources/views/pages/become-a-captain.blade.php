@extends('layouts.page')

@section('styles')
    <style media="screen">
        .justify-content-center * {
            text-align: right;
        }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>

        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-8">
                <div class="card" dir="rtl">
                    <div class="card-header text-right">@lang('language.becomeACaptain')</div>

                    <div class="card-body">
                        <form class="become-a-captain-form" method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">@lang('language.name')</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">@lang('language.phone')</label>

                                <div class="col-md-6">
                                    <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>

                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('language.email') (@lang('language.optional'))</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">@lang('language.city')</label>

                                <div class="col-md-6">
                                    <select id="city" class="govs form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="city">
                                        <option selected disabled value="">@lang('language.city')</option>
                                        <option value="Alexandria">@lang('cities.Alexandria')</option>
                                        <option value="Cairo">@lang('cities.Cairo')</option>
                                        <option value="Giza">@lang('cities.Giza')</option>
                                        <option value="Faiyum">@lang('cities.Faiyum')</option>
                                        <option value="Hurghada">@lang('cities.Hurghada')</option>
                                        <option value="Bani Suef"> @lang('cities.bani_suef')</option>
                                        <option value="Minya">@lang('cities.Minya')</option>
                                        <option value="Assiut">@lang('cities.Asyut')</option>
                                        <option value="Sohag">@lang('cities.Sohag')</option>
                                        <option value="Qena">@lang('cities.Qena')</option>
                                        <option value="New_Valley">@lang('cities.New_Valley')</option>
                                        <option value="Matruh">@lang('cities.Matruh')</option>
                                        <option value="Red_Sea">@lang('cities.Red_Sea')</option>
                                        <option value="South_Sinai">@lang('cities.South_Sinai')</option>
                                        <option value="North_Sinai">@lang('cities.North_Sinai')</option>
                                        <option value="Suez">@lang('cities.Suez')</option>
                                        <option value="Beheira">@lang('cities.Beheira')</option>
                                        <option value="Helwan">@lang('cities.Helwan')</option>
                                        <option value="Sharqia">@lang('cities.Sharqia')</option>
                                        <option value="Dakahlia">@lang('cities.Dakahlia')</option>
                                        <option value="Kafr_el_Sheikh">@lang('cities.Kafr_el_Sheikh')</option>
                                        <option value="Monufia">@lang('cities.Monufia')</option>
                                        <option value="Gharbia">@lang('cities.Gharbia')</option>
                                        <option value="Ismailia">@lang('cities.Ismailia')</option>
                                        <option value="Qalyubia">@lang('cities.Qalyubia')</option>
                                        <option value="Luxor">@lang('cities.Luxor')</option>
                                        <option value="Aswan">@lang('cities.Aswan')</option>
                                        <option value="Damietta">@lang('cities.Damietta')</option>
                                        <option value="Port_Said">@lang('cities.Port_Said')</option>
                                    </select>
                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('language.password')</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('language.confirmPassword')</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-check row mb-3">
                                <div class="col-md-6 offset-md-4 agree_terms_and_conds">
                                    <input class="form-check-input" type="checkbox" value="1" id="agree_terms_and_conds" name="agree_terms_and_conds">
                                    <label class="form-check-label mr-4" for="agree_terms_and_conds">
                                        {{-- @lang('language.agreeTermsAndConds') --}}
                                        أوافق على
                                        <a href="/terms-and-conditions" target="_blank">
                                            الشروط والأحكام
                                        </a>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('language.becomeACaptain')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('.become-a-captain-form').submit(function (e) {
            e.preventDefault();
            $('.error').remove();
            $.ajax({
                method: 'POST',
                url: 'become-a-captain',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    if (data.code == '200') {
                        window.location.href = '/account-settings'
                    } else if (data.code == '422') {
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                if (error == 'agree_terms_and_conds') {
                                    $('input[name='+ error +'] + label').after(`
                                        <small class="error">
                                            <br>
                                            @lang('language.youHaveToAgreeTermsAndConds')
                                        </small>
                                    `);
                                    $('input[name='+ error +']').focus();
                                } else {
                                    $('input[name='+ error +']').after(`
                                        <small class="error">${data.response[error]}</small>
                                    `);
                                    $('input[name='+ error +']').focus();
                                }
                                // $('.form-control[name="'+ field +'"]').after(`
                                //     <small class="error">${data.response[field]}</small>
                                // `);
                                // $('.form-control[name="'+ field +'"]').focus();
                            }
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus);
                    alert(errorThrown.toString())
                }
            });
        })
    </script>
@endsection
