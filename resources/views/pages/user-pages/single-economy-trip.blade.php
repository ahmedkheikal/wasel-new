@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('/css/user-dashboard.css') !!}">
    <style media="screen">
    input[name=add_feedback] {
        text-align: center;
        margin: auto;
        display: block;
    }
    @media (max-width: 845px) {
        .captain-block {
            margin-top: 50px
        }
    }
    .price {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 5px;
        padding-top: 4px;
        border-radius: 3px;
        width: 50%;
        margin: auto;
    }
    .chevron-down {
        position: relative;
        width: 20px;
        height: 20px;
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <script src="{!! asset('js/jquery.min.js') !!}"></script>

    <div class="container" style="margin-top: 80px" dir="rtl">
        <div class="card pb-5">

            <ul class="trip-list">


                <li data-tripId="{{ $reservation->trip->id }}">
                    <span class="float-left chevron-down info-toggler">
                        <i class="fa fa-chevron-down"></i>
                    </span>
                    <div class="row float-md-right">
                        <div class="col-md-3">
                            <span class="from">
                                <i class="fa fa-map-marker"></i>
                                @lang("db.{$reservation->trip->from_city}")
                            </span> -
                            <span class="to">
                                @lang("db.{$reservation->trip->to_city}")
                            </span>
                            <br>
                            <span class="date">
                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                {{ (new DateTime($reservation->trip->datetime))->format('d ') }}
                                @php
                                    $month = (new DateTime($reservation->trip->datetime))->format('M')
                                @endphp
                                @lang("language.{$month}")
                                {{ (new DateTime($reservation->trip->datetime))->format(' Y') }}
                            </span>
                            <br>
                            <span class="time">
                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                {{ (new DateTime($reservation->trip->datetime))->format('h:i') }}
                                @php
                                    $ampm = (new DateTime($reservation->trip->datetime))->format('A')
                                @endphp
                                @lang("language.{$ampm}")
                            </span>
                        </div>
                        <div class="col-md-3 mt-2">
                            <span class="trip-seats">
                                <img src="{!! asset('/images/seat.png') !!}" alt="seat" class="seat-icon"> &nbsp;
                                @if ($reservation->seat_1 == '1')
                                    @lang('language.frontSeat'),
                                @endif
                                @if ($reservation->seat_2 == '1')
                                    @lang('language.backSeatRight'),
                                @endif
                                @if ($reservation->seat_3 == '1')
                                    @lang('language.backSeatLeft')
                                @endif
                            </span>
                        </div>
                        <div class="col-md-3 mt-2">
                            <span class="captain">
                                <i class="fa fa-user"></i> &nbsp;
                                @lang('language.captain'): {{ $reservation->trip->captain->username }}
                            </span>
                        </div>
                    </div>
                    <div class="row full-info-inner pb-2 captain-block" style="margin: 50px -20px">
                        <div class="col-md-4">
                            @include('components.captain', [
                            'captain' => $reservation->trip->captain
                            ])
                        </div>
                        <div class="col-md-4">
                            @if ($reservation->reservation_type == 'other')
                                <h3 class="text-center">@lang('titles.reservedFor'): </h3>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>@lang('language.name')</td>
                                            <td>{{ $reservation->setfor_name }}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('language.phone')</td>
                                            <td>{{ $reservation->setfor_phone }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                            <h3 class="text-center">@lang('titles.price')</h3>
                            <div class="price">
                                <span class="price" style="width: 100%; font-size: 20px; ">
                                    {{ $reservation->price }} @lang('language.egp')
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            @if ($reservation->reservation_status == 'canceled')
                                <div style="max-width: 120px; margin:auto;">
                                    @lang("db.canceledReservation")
                                </div>
                            @else
                                @if ($reservation->feedback !== null)
                                    <div style="color: #888; margin-top: 20px;">
                                        <div class="form-group">
                                            <label for="">@lang('language.feedback.timing')</label>
                                            @include('components.five-stars', [
                                                'inForm' => false,
                                                'rating' => $reservation->feedback->timing
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('language.feedback.behaviour')</label>
                                            @include('components.five-stars', [
                                                'inForm' => false,
                                                'rating' => $reservation->feedback->behaviour
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.safety')</label>
                                            @include('components.five-stars', [
                                                'inForm' => false,
                                                'rating' => $reservation->feedback->safety
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.cleanliness')</label>
                                            @include('components.five-stars', [
                                                'inForm' => false,
                                                'rating' => $reservation->feedback->cleanliness
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.pricing_commitment')</label>
                                            @include('components.five-stars', [
                                                'inForm' => false,
                                                'rating' => $reservation->feedback->pricing_commitment
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.pickup_dropoff_commitment')</label>
                                            @include('components.five-stars', [
                                                'inForm' => false,
                                                'rating' => $reservation->feedback->pickup_dropoff_commitment
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                @lang('language.feedback.overall_rating')
                                                {{ $reservation->feedback->overall_rating }}
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.notes')</label>
                                            <p>
                                                {{ $reservation->feedback->notes }}
                                            </p>
                                        </div>

                                    </div>
                                @else
                                    <form class="mt-3 feedback-form formClass{{ $reservation->id }}" action="index.html" method="post">
                                        <input type="hidden" name="reservation_id" value="{{ $reservation->id }}">
                                        <div class="form-group timing">
                                            <label for="timing">@lang('language.feedback.timing')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'timing',
                                            ])
                                        </div>

                                        <div class="form-group behaviour">
                                            <label for="behaviour">@lang('language.feedback.behaviour')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'behaviour',
                                            ])
                                        </div>

                                        <div class="form-group safety">
                                            <label for="safety">@lang('language.feedback.safety')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'safety',
                                            ])
                                        </div>

                                        <div class="form-group cleanliness">
                                            <label for="safety">@lang('language.feedback.cleanliness')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'cleanliness',
                                            ])
                                        </div>

                                        <div class="form-group pricing_commitment">
                                            <label for="pricing_commitment">@lang('language.feedback.pricing_commitment')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'pricing_commitment',
                                            ])
                                        </div>

                                        <div class="form-group pickup_dropoff_commitment">
                                            <label for="pickup_dropoff_commitment">@lang('language.feedback.pickup_dropoff_commitment')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'pickup_dropoff_commitment',
                                            ])
                                        </div>

                                        <div class="form-group five-stars">
                                            <label for="overall_rating">@lang('language.feedback.overall_rating')</label>
                                            <input type="hidden" name="overall_rating" value="">
                                            <span class="overall_rating_text"></span>
                                        </div>
                                        <div class="form-group" style="padding-top: 0 !important">
                                            <label for="notes">@lang('language.feedback.notes')</label>
                                            <textarea name="notes" rows="3" class="form-control"></textarea>
                                        </div>

                                        <input type="submit" name="add_feedback" class="btn btn-primary w-75" value="@lang('language.send')">
                                        <div class="alert alert-success" style="display: none">
                                            @lang('language.feedback.submittedSuccessfully')
                                        </div>
                                    </form>

                                    <script type="text/javascript">
                                        $('.feedback-form.formClass{{ $reservation->id }} input[type=radio]').change(function (e) {
                                            var sum = 0;
                                            $('.feedback-form input[type=radio]:checked').each(function (index) {
                                                count = index;
                                                sum += parseInt($(this).val());
                                            })
                                            var rating = (sum / (++count * 5) * 5);
                                            $('.feedback-form.formClass{{ $reservation->id }} input[name=overall_rating]').val(Math.round(rating * 100) / 100)
                                            $('.feedback-form.formClass{{ $reservation->id }} .overall_rating_text').html(Math.round(rating * 100) / 100)
                                        })
                                    </script>
                                @endif
                            @endif
                        </div>

                    </div>
                </li>



            </ul>


        </div>
    </div>

    @include('components.footer')
@endsection

@section('page-scripts')

    <!-- Modal -->
    <div class="modal fade" id="cancelReservationModal" tabindex="-1" role="dialog" aria-labelledby="cancelReservationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    {{-- <h5 class="modal-title" id="cancelReservationModalLabel">Modal title</h5> --}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @lang('language.areYouSureYouWantToCancel')
                    <div class="cancelled-Successfully alert alert-success" role="alert" style="display: none">
                        @lang('language.reservationCanceledSuccessfully')
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('language.close')</button>
                    <button type="button" class="btn btn-primary confirm-cancellation" data-id="">@lang('language.cancelReservation')</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.nav-tabs li a').addClass('p-2')
        $(document).ready(function () {
            $('.upcomingTrips-btn').click();
            $('.trip-list li').click()
        })
        $('.trip-list li').click(function (e) {
            if ($(this).hasClass('full-info') == false) {
                $('.trip-list li').scrollTop(0);
                $('.trip-list li').removeClass('full-info');
                $('.trip-list li .info-toggler').html(`
                <i class="fa fa-chevron-down"></i>
                `);
                $('.full-info-inner').css({height: '0'});
                $(this).addClass('full-info');
                $('.trip-list li:hover .info-toggler').html(`
                <i class="fa fa-chevron-up"></i>
                `);
                $('.full-info-inner').css({height: 'auto'});
            } else {
                if (e.target !== this)
                return ;
                $('.trip-list li').scrollTop(0);
                $('.trip-list li').removeClass('full-info');
                $('.full-info-inner').css({height: '0'});
                $('.trip-list li .info-toggler').html(`
                <i class="fa fa-chevron-down"></i>
                `);
            }
            $(window).scrollTop( $(this).offset().top );
        })

        $('.cancel-reservaion').click(function(e) {
            e.preventDefault();
            $('#cancelReservationModal').modal();
            $('.confirm-cancellation').attr('data-id', $(this).attr('data-id'));
        })


        $('.confirm-cancellation').click(function () {
            sendCancellationOrder($(this));
        })
        function sendCancellationOrder(clickedNode) {
            $('#loader').fadeIn();
            $.ajax({
                method: 'POST',
                url: '/cancel-reservation',
                data: {
                    reservation_id: clickedNode.attr('data-id')
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('.cancelled-Successfully').slideDown()
                        $('.confirm-cancellation').attr('disabled', 'disabled');
                        setTimeout(function () {
                            $('#cancelReservationModal').modal('hide');
                            location.reload();
                        }, 3000)
                    } else {
                    }
                    $('#loader').fadeOut();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    console.error(textStatus);
                    alert(errorThrown.toString())

                    $('#loader').fadeOut();
                }
            })
        }

        $('.feedback-form').submit(function (e) {
            e.preventDefault();
            $('#loader').fadeIn();
            $.ajax({
                url: '/feedback/send-economy',
                method: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: new FormData(this),
                success: function (data) {
                    $('.error').remove();
                    if (data.code == '200') {
                        $('.alert-success').slideDown('fast');
                        setTimeout(function () {
                            location.reload();
                        }, 3000)
                    } else if (data.code == '422') {

                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                $('.'+ field).after(`
                                    <small class="error">${data.response[field]}</small>
                                `);
                            }
                        }
                    }
                    $('#loader').fadeOut();

                }
            })
        })
    </script>
@endsection
