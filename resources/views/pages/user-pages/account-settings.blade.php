@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('/css/user-dashboard.css') !!}">
    <style media="screen">
    .alert {
        display: none;
    }
    .verify-phone {
        position: absolute;
        bottom: 10px;
        left: 0;
        z-index: 2;
        height: 36.7px;
    }
    @media (min-width: 790px) {
        .verify-phone {
            position: absolute;
            bottom: 0;
            left: 0;
            z-index: 2;
            height: 36.7px;
        }
    }
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    img.profile-picture {
        margin-top: 15px
    }
    @media (min-width: 465px) {
        img.profile-picture {
            margin-top: -40px
        }
    }

    .btn {
        padding: 8px 20px;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }
    form label {
        float: right;
    }
    .form-control {
        width: 100% !important;
    }

    .hidden-sub {
        width: 0;
        height: 0;
        padding: 0;
        border: none;
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px;"  dir="rtl">
        <div class="card">
            <div class="row" style="text-align: right;">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 mb-4" style="margin-right: 15px">
                            <img class="profile-picture" src="{{ Auth::user()->profile_picture }}" alt="" style="width: 100px; height: 100px;">
                            <div class="upload-btn-wrapper m-3 mt-5">
                                <form class="pp-form" action="/upload-profile-picture" method="post">
                                    <button class="btn btn-primary">@lang('titles.changeProfilePic')</button>
                                    <input type="file" name="profile_picture" value="">
                                    <input type="submit" name="update_profile" value="" class="hidden-sub">
                                </form>
                                <div class="alert alert-success update_profile">
                                    @lang('language.line')
                                </div>
                            </div>
                        </div>
                        @if (Auth::user()->account_type == 'driver')
                            <div class="container m-2">
                                <div class="card official-docs-wrapper" style="width: 100%">
                                    <div class="card-header official-docs-toggle">
                                        @lang('titles.officialDocuments')
                                        <span class="float-left chevron-down">
                                            <i class="fa fa-chevron-down"></i>
                                        </span>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <form class="upload-btn-wrapper driving-license" action="/upload-profile-picture" method="post">
                                                    <button class="btn btn-primary">@lang('titles.updateDrivingLicense')</button>
                                                    <input type="file" name="driving_license" value="">
                                                    <input type="submit" name="update_driving_license" value="" class="hidden-sub">
                                                </form>
                                                <div class="alert alert-success driving_license">
                                                </div>
                                                <br>
                                                <img class="official-doc" src="{{ Auth::user()->driving_license }}">
                                            </div>
                                            <div class="col-sm-6">
                                                <form class="upload-btn-wrapper id-card" action="/upload-profile-picture" method="post">
                                                    <button class="btn btn-primary">@lang('titles.updateIdCard')</button>
                                                    <input type="file" name="id_card" value="">
                                                    <input type="submit" name="update_id_card" value="" class="hidden-sub">
                                                </form>
                                                <div class="alert alert-success id_card">

                                                </div>
                                                <br>
                                                <img class="official-doc" src="{{ Auth::user()->id_card }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <form class="form-inline account-settings m-4" action="/update-account-info" method="post">
                            <div class="col-md-6">
                                <div class="form-group row mb-3">
                                    <div class="col-sm-4">
                                        <label for="exampleFormControlInput1">
                                            <i class="fa fa-user"></i> &nbsp;
                                            @lang('language.fullname')
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="fullname" name="username" placeholder="@lang('language.fullname')" value="{{ Auth::user()->username }}">
                                    </div>
                                </div>
                                <div class="form-group row mb-3" style="position: relative">
                                    @if (Auth::user()->phone_verified == 0)
                                        <a href="/phone/verify">
                                            <button type="button" class="btn btn-primary verify-phone">@lang('language.verifyPhone')</button>
                                        </a>
                                    @endif
                                    <div class="col-sm-4">
                                        <label for="exampleFormControlInput1">
                                            <i class="fa fa-phone"></i> &nbsp;
                                            @lang('language.phone')
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="tel" class="form-control" id="phone" name="phone" placeholder="@lang('language.phone')" value="{{ Auth::user()->phone }}">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="col-sm-4">
                                        <label for="exampleFormControlInput1">
                                            <i class="fa fa-envelope"></i> &nbsp;
                                            @lang('language.email')
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="@lang('language.email')" value="{{ Auth::user()->email }}">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group row mb-3">
                                    <div class="col-sm-4">
                                        <label for="exampleFormControlInput1">
                                            <i class="fa fa-building"></i> &nbsp;
                                            @lang('language.city')
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        {{-- <input type="text" class="form-control" id="city" placeholder="@lang('language.city')"> --}}
                                        <select name="city" class="form-control" id="city" placeholder="@lang('language.city')" name="city">
                                            @include('components.cities', ['selected' => Auth::user()->city])
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="col-sm-4">
                                        <label for="exampleFormControlInput1">
                                            <i class="fa fa-suitcase"></i> &nbsp;
                                            @lang('language.job')
                                        </label>
                                    </div>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="job" name="job_title" placeholder="@lang('language.job')" value="{{ Auth::user()->job_title }}">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="col-sm-4">
                                        <label for="exampleFormControlInput1">
                                            <i class="fa fa-user"></i> &nbsp;
                                            @lang('language.gender')
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="gender">
                                            <option disabled selected value=""> - @lang('language.select') - </option>
                                            <option {{ Auth::user()->gender == 'male' ? 'selected' : '' }} value="male">@lang('language.male')</option>
                                            <option {{ Auth::user()->gender == 'female' ? 'selected' : '' }} value="female">@lang('language.female')</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6 text-center">
                                <div class="form-group row mb-3">
                                    <div class="col-sm-4">
                                        <label for="exampleFormControlInput1">
                                            <i class="fa fa-key"></i> &nbsp;
                                            @lang('language.password')
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" class="btn btn-primary text-center" data-toggle="modal" data-target="#changePasswordPop">
                                            @lang('language.changePassword')
                                        </button>
                                    </div>
                                </div>
                                <!-- Button trigger modal -->


                            </div>
                            <div class="col-sm-6"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-6"></div>
                            @if (Auth::user()->account_type == 'driver')
                                <div class="col-md-6">
                                    <hr>
                                    <div class="form-group row mb-3">
                                        <div class="col-sm-4">
                                            <label for="car_model">@lang('language.carModel')</label>
                                        </div>

                                        <div class="col-sm-6">
                                            <input type="text" name="car_model" value="{{ Auth::user()->car_model }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-3">
                                        <div class="col-sm-4">
                                            <label for="car_model">@lang('language.carColor')</label>
                                        </div>

                                        <div class="col-sm-6">
                                            <input type="text" name="car_color" value="{{ Auth::user()->car_color }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-3">
                                        <div class="col-sm-4">
                                            <label for="chassis_no">@lang('language.plateNo')</label>
                                        </div>

                                        <div class="col-sm-6">
                                            <input type="text" name="chassis_no" value="{{ Auth::user()->chassis_no }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-sm-12"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <input type="submit" name="save_changes" value="@lang('language.saveChanges')" class="btn btn-primary mt-5" style="width: 100%;">
                                <div class="alert alert-success save_changes">
                                    @lang('language.accountInfoUpdatedSuccessfully')
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
@endsection

@section('page-scripts')
    <!-- Modal -->
    <div class="modal fade" id="changePasswordPop" tabindex="-1" role="dialog" aria-labelledby="changePasswordPopLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="changePasswordPopLabel">@lang('language.changePassword')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="update-password" action="/update-password" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="old_password">@lang('language.oldPassword')</label>
                            <input type="password" class="form-control mb-2" id="old_password" name="old_password" placeholder="@lang('language.oldPassword')" value="">
                        </div>
                        <div class="form-group">
                            <label for="new_password">@lang('language.newPassword')</label>
                            <input type="password" class="form-control mb-2" id="new_password" name="new_password" placeholder="@lang('language.newPassword')" value="">
                        </div>
                        <div class="form-group">
                            <label for="new_password_confirmation">@lang('language.confirmNewPassword')</label>
                            <input type="password" class="form-control mb-2" id="new_password_confirmation" name="new_password_confirmation" placeholder="@lang('language.confirmNewPassword')" value="">
                        </div>
                        <div class="alert alert-success update_password">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('language.close')</button>
                        {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                        <input type="submit" class="btn btn-primary" name="update_password" value="@lang('language.changePassword')">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('.account-settings').submit(function (e) {
            e.preventDefault();
            $('.error').remove();
            $.ajax({
                method: 'POST',
                url: '/update-account-info',
                data: new FormData(this),
                processData: false,
                cache: false,
                contentType: false,
                success: function (data) {
                    console.log(data);
                    if (data.code == '200') {
                        $('.save_changes').slideDown('fast');
                        setTimeout(function () {
                            $('.save_changes').slideUp('fast');
                        }, 3000)
                    } else if (data.code == '422') {
                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                try {
                                    $(window).scrollTop($('input[name='+ field +']').offset().top - 60);
                                } catch (e) {
                                    $(window).scrollTop($('select[name='+ field +']').offset().top - 60);
                                }

                                $('input[name='+ field +']').after(`
                                    <small class="error">${data.response[field]}</small>
                                `);
                                $('input[name='+ field +']').focus();

                                $('select[name='+ field +']').after(`
                                    <small class="error">${data.response[field]}</small>
                                `);
                                $('select[name='+ field +']').focus();
                            }
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    console.error(textStatus);
                    alert(errorThrown.toString())
                }
            });
        })

        $('input[name="profile_picture"]').change(function (e) {
            $('.upload-btn-wrapper button').attr('disabled', 'disabled');
            $('.upload-btn-wrapper button').html('@lang('language.uploading')');
            $('.pp-form').submit();
        })

        $('input[name="driving_license"]').change(function (e) {
            $('.upload-btn-wrapper button').attr('disabled', 'disabled');
            $('.upload-btn-wrapper button').html('@lang('language.uploading')');
            $('.driving-license').submit();
        })

        $('input[name="id_card"]').change(function (e) {
            $('.upload-btn-wrapper button').attr('disabled', 'disabled');
            $('.upload-btn-wrapper button').html('@lang('language.uploading')');
            $('.id-card').submit();
        })

        $('.pp-form').submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: '/upload-profile-picture',
                data: new FormData(this),
                processData: false,
                cache: false,
                contentType: false,
                success: function (data) {
                    $('.update_profile').html(data.response);
                    if (data.code !== '200')
                    $('.update_profile').removeClass('alert-success').addClass('alert-danger');

                    $('.update_profile').slideDown();
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    $('.update_profile').html(errorThrown.toString());
                    $('.update_profile').removeClass('alert-success').addClass('alert-danger');
                    $('.update_profile').slideDown();
                    location.reload();
                }
            })
        })

        $('.driving-license').submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: '/upload-driving-license',
                data: new FormData(this),
                processData: false,
                cache: false,
                contentType: false,
                success: function (data) {
                    $('.driving_license').html(data.response);
                    $('.driving_license').slideDown('fast');

                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    $('.driving_license').html(errorThrown.toString());
                    $('.driving_license').slideDown('fast');
                    $('.driving_license').removeClass('alert-success').addClass('alert-danger');
                    $('.driving_license').slideDown('fast');
                    location.reload();
                }
            })
        })

        $('.id-card').submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: '/upload-id-card',
                data: new FormData(this),
                processData: false,
                cache: false,
                contentType: false,
                success: function (data) {
                    $('.id_card').html(data.response);
                    $('.id_card').slideDown('fast');
                    setTimeout(function () {
                        location.reload();
                    }, 3000)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    $('.id_card').removeClass('alert-success').addClass('alert-danger');
                    $('.id_card').html(errorThrown.toString());
                    $('.id_card').slideDown();
                    setTimeout(function () {
                        location.reload();
                    }, 3000)
                }
            })
        })

        $('.official-docs-toggle').click(function () {
            $('.official-docs-wrapper').toggleClass('toggled-on');
        })

        $('.update-password').submit(function (e) {
            $('.error').remove();
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: '/update-password',
                processData: false,
                cache: false,
                contentType: false,
                data: new FormData(this),
                success: function (data) {
                    console.log(data);
                    if (data.code == '422') {
                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                $('#' + field).after(`<small class="error">${data.response[field]}</small>`);
                            }
                        }
                    } else if (data.code == '200') {
                        $('.update_password').html(data.response)
                        $('.update_password').slideDown()
                        setTimeout(function () {
                            location.reload();
                        }, 3000)
                    } else if (data.code == '401') {
                        $('.update_password').html(data.response)
                        $('.update_password').removeClass('alert-success').addClass('alert-danger');
                        $('.update_password').slideDown()
                        setTimeout(function () {
                            location.reload();
                        }, 3000)
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    alert(errorThrown.toString())
                    location.reload();
                }
            });
        })
    </script>
@endsection
