@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('/css/user-dashboard.css') !!}">
    <style media="screen">
    input[name=add_feedback] {
        display: block;
        margin: auto;
        width: 75%;
    }
    .price {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 5px;
        padding-top: 4px;
        border-radius: 3px;
        width: 50%;
        margin: auto;
    }
    .chevron-down {
        position: relative;
        width: 20px;
        height: 20px;
    }
    .nav-tabs li {
        width: 50%;
        display: inline-block;
    }
    @media (max-width: 1250px) {
        .full-info-inner {
            top: auto !important;
        }
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="card pb-5">
            <div class="container p-sm-5">
                <ul class="trip-list" dir="rtl">
                    <script src="{!! asset('js/jquery.min.js') !!}"></script>
                    <li>
                        <div class="row">
                            <div class="col-sm-4 col-8">
                                <span class="from">
                                    <i class="fa fa-map-marker"></i>
                                    @lang("db.{$request->from_city}")
                                </span> -
                                <span class="to">
                                    @lang("db.{$request->to_city}")
                                </span>
                                <br>
                                <span class="date">
                                    <i class="fa fa-calendar" style="font-size: 12px"></i>
                                    {{ (new DateTime($request->datetime))->format('d ') }}
                                    @php
                                    $month = (new DateTime($request->datetime))->format('M')
                                    @endphp
                                    @lang("language.{$month}")
                                    {{ (new DateTime($request->datetime))->format(' Y') }}
                                </span>
                                <br>
                                <span class="time">
                                    <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                    {{ (new DateTime($request->datetime))->format('h:i') }}
                                    @php
                                    $ampm = (new DateTime($request->datetime))->format('A')
                                    @endphp
                                    @lang("language.{$ampm}")
                                </span>
                            </div>
                            @if ($request->request_status == 'canceled')
                                <div class="col-sm-4 col-4">
                                    @lang("db.{$request->request_status}")
                                </div>
                            @endif
                            <div class="col-sm-4 col-6 mt-2">
                                <span class="captain">
                                    {{-- <i class="fa fa-user"></i> &nbsp; --}}
                                    @lang("language.{$request->trip_type}")
                                </span>
                            </div>

                        </div>

                        <div class="row full-info-inner pb-2 mt-4">
                            <div class="col-md-4 text-right">
                                <span class="from">
                                    <i class="fa fa-map-marker"></i>
                                    @lang("db.{$request->from_city}")
                                    <span style="font-weight: lighter !important">
                                        - {{ $request->from_city_address }}, {{ $request->pickup_address }}
                                    </span>
                                </span> <br>
                                <span class="to">
                                    <i class="fa fa-bullseye"></i>
                                    @lang("db.{$request->to_city}")
                                    <span style="font-weight: lighter !important">
                                        - {{ $request->to_city_address }}, {{ $request->arrival_address }}
                                    </span>
                                </span>
                                <br>
                                <span class="date">
                                    <i class="fa fa-calendar" style="font-size: 12px"></i>
                                    {{ (new DateTime($request->datetime))->format('d ') }}
                                    @php
                                        $month = (new DateTime($request->datetime))->format('M')
                                    @endphp
                                    @lang("language.{$month}")
                                    {{ (new DateTime($request->datetime))->format(' Y') }}
                                </span>
                                <br>
                                <span class="time">
                                    <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                    {{ (new DateTime($request->datetime))->format('h:i') }}
                                    @php
                                        $ampm = (new DateTime($request->datetime))->format('A')
                                    @endphp
                                    @lang("language.{$ampm}")
                                </span>

                                @if ($request->request_status == 'scheduled')
                                    @isset($request->reservation)
                                        <div class="text-center" >
                                            @include('components.captain', ['captain' => $request->reservation->trip->captain, 'phoneVisible' => true])
                                        </div>
                                    @endisset
                                @endif
                            </div>

                            <div class="col-md-4">
                                @if ($request->request_type == 'other')
                                    <h3 class="text-center">@lang('titles.reservedFor'): </h3>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>@lang('language.name')</td>
                                                <td>{{ $request->fullname }}</td>
                                            </tr>
                                            <tr>
                                                <td>@lang('language.phone')</td>
                                                <td>{{ $request->phone }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                @endif
                                <h3 class="text-center">@lang('titles.price')</h3>
                                <div class="price">
                                    <span class="price" style="width: 100%; font-size: 20px; ">
                                        {{ $request->price }} @lang('language.egp')
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-4 pt-3">
                                @if ($request->feedback !== null)
                                    <div style="color: #888;">
                                        <div class="form-group">
                                            <label for="">@lang('language.feedback.timing')</label>
                                            @include('components.five-stars', [
                                            'inForm' => false,
                                            'rating' => $request->feedback->timing
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('language.feedback.behaviour')</label>
                                            @include('components.five-stars', [
                                            'inForm' => false,
                                            'rating' => $request->feedback->behaviour
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.safety')</label>
                                            @include('components.five-stars', [
                                            'inForm' => false,
                                            'rating' => $request->feedback->safety
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.cleanliness')</label>
                                            @include('components.five-stars', [
                                            'inForm' => false,
                                            'rating' => $request->feedback->cleanliness
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.pricing_commitment')</label>
                                            @include('components.five-stars', [
                                            'inForm' => false,
                                            'rating' => $request->feedback->pricing_commitment
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.pickup_dropoff_commitment')</label>
                                            @include('components.five-stars', [
                                            'inForm' => false,
                                            'rating' => $request->feedback->pickup_dropoff_commitment
                                            ])
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                @lang('language.feedback.overall_rating')
                                                {{ $request->feedback->overall_rating }}
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>@lang('language.feedback.notes')</label>
                                            <p>
                                                {{ $request->feedback->notes }}
                                            </p>
                                        </div>

                                    </div>
                                @else
                                    <form class="feedback-form formClass{{ $request->id }}" action="index.html" method="post">
                                        <input type="hidden" name="request_id" value="{{ $request->id }}">
                                        <div class="form-group timing">
                                            <label for="timing">@lang('language.feedback.timing')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'timing',
                                            ])
                                        </div>

                                        <div class="form-group behaviour">
                                            <label for="behaviour">@lang('language.feedback.behaviour')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'behaviour',
                                            ])
                                        </div>

                                        <div class="form-group safety">
                                            <label for="safety">@lang('language.feedback.safety')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'safety',
                                            ])
                                        </div>

                                        <div class="form-group cleanliness">
                                            <label for="safety">@lang('language.feedback.cleanliness')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'cleanliness',
                                            ])
                                        </div>

                                        <div class="form-group pricing_commitment">
                                            <label for="pricing_commitment">@lang('language.feedback.pricing_commitment')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'pricing_commitment',
                                            ])
                                        </div>

                                        <div class="form-group pickup_dropoff_commitment">
                                            <label for="pickup_dropoff_commitment">@lang('language.feedback.pickup_dropoff_commitment')</label>
                                            @include('components.five-stars', [
                                            'inForm' => true,
                                            'name' => 'pickup_dropoff_commitment',
                                            ])
                                        </div>

                                        <div class="form-group five-stars">
                                            <label for="overall_rating">@lang('language.feedback.overall_rating')</label>
                                            <input type="hidden" name="overall_rating" value="">
                                            <span class="overall_rating_text"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="notes">@lang('language.feedback.notes')</label>
                                            <textarea name="notes" rows="3" class="form-control"></textarea>
                                        </div>

                                        <input type="submit" name="add_feedback" class="btn btn-primary" value="@lang('language.send')">
                                        <div class="alert alert-success" style="display: none">
                                            @lang('language.feedback.submittedSuccessfully')
                                        </div>
                                    </form>

                                    <script type="text/javascript">
                                        $('.feedback-form.formClass{{ $request->id }} input[type=radio]').change(function (e) {
                                            var sum = 0;
                                            $('.feedback-form input[type=radio]:checked').each(function (index) {
                                                count = index;
                                                sum += parseInt($(this).val());
                                            })
                                            var rating = (sum / (++count * 5) * 5);
                                            $('.feedback-form.formClass{{ $request->id }} input[name=overall_rating]').val(Math.round(rating * 100) / 100)
                                            $('.feedback-form.formClass{{ $request->id }} .overall_rating_text').html(Math.round(rating * 100) / 100)
                                        })
                                    </script>
                                @endif
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('.nav-tabs li a').addClass('p-2')
        $(document).ready(function (e) {
            $('.upcomingTrips-btn').click();
            $('.trip-list li').click()
        })
        $('.trip-list li').click(function (e) {
            if ($(this).hasClass('full-info') == false) {
                $('.trip-list li').scrollTop(0);
                $('.trip-list li').removeClass('full-info');
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
                $('.full-info-inner').css({height: '0'});
                $(this).addClass('full-info');
                $('.trip-list li:hover span.chevron-down').html(`
                    <i class="fa fa-chevron-up"></i>
                `);
                $('.full-info-inner').css({height: 'auto'});
            } else {
                if (e.target !== this)
                return ;
                $('.trip-list li').scrollTop(0);
                $('.trip-list li').removeClass('full-info');
                $('.full-info-inner').css({height: '0'});
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
            }
            $(window).scrollTop( $(this).offset().top );
        })


        {{-- $('.cancel-reservaion').click(function(e) {
        e.preventDefault();
        var conf = confirm('@lang('language.areYouSureYouWantToCancel')')

        if (conf) {
        $('#loader').fadeIn();
        $.ajax({
        method: 'POST',
        url: '/cancel-private-request',
        data: {
        request_id: $(this).attr('data-id')
    },
    success: function (data) {
    console.log(data);
    if (data.code == '200') {
    // location.reload();
} else if (data.code == '422') {
for (var error in data.response) {
if (data.response.hasOwnProperty(error)) {
alert(data.response[error]);
}
}
} else {
alert(data.response);
}
location.reload();
$('#loader').fadeOut();
},
error: function(jqXHR, textStatus, errorThrown) {
if (errorThrown.toString() == 'Unauthorized')
window.location.href = '/login';

console.error(textStatus);
alert(errorThrown.toString())

$('#loader').fadeOut();
}
})
}
}) --}}

$('.feedback-form').submit(function (e) {
    e.preventDefault();
    $('#loader').fadeIn()
    $.ajax({
        url: '/feedback/send-private',
        method: 'POST',
        processData: false,
        cache: false,
        contentType: false,
        data: new FormData(this),
        success: function (data) {
            $('.error').remove();
            if (data.code == '200') {
                $('.alert-success').slideDown('fast');
                setTimeout(function () {
                    location.reload();
                }, 3000)
            } else if (data.code == '422') {

                for (var field in data.response) {
                    if (data.response.hasOwnProperty(field)) {
                        $('.'+ field).after(`
                            <small class="error">${data.response[field]}</small>
                        `);
                    }
                }
            }
            $('#loader').fadeOut()
        }
    })
})
    </script>
@endsection
