@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('/css/user-dashboard.css') !!}">
    <style media="screen">
    input[name=add_feedback] {
        display: block;
        margin: auto;
        width: 75%;
    }
    .price {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 5px;
        padding-top: 4px;
        border-radius: 3px;
        width: 50%;
        margin: auto;
    }
    .chevron-down {
        position: relative;
        width: 20px;
        height: 20px;
    }
    .nav-tabs li {
        width: 50%;
        display: inline-block;
    }
    @media (max-width: 1250px) {
        .full-info-inner {
            top: auto !important;
        }
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="card pb-5" dir="rtl">
            <div class="container p-sm-5">
                @foreach ($orders as $order)
                    <div class="card m-4 p-2">
                        <div class="row">
                            <div class="col-md-4">
                                <p style="text-align: center">
                                    @lang('titles.from') @lang("cities.{$order->from_city}")
                                    <i class="fa fa-arrow-left"></i>
                                    @lang('titles.to') @lang("cities.{$order->to_city}")
                                </p>
                                @if ($order->captain)
                                    <p style="text-align: center">
                                        fulfuled by{{ $order->captain->username }}
                                    </p>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <p style="text-align: center">
                                    @lang('language.shipping.orderType'): @lang("language.shipping.{$order->order_type}") <br>
                                    @lang("db.{$order->order_status}")
                                </p>
                            </div>
                            @if ($order->price)
                                <div class="col-md-4">
                                    <p style="text-align: center">
                                        @lang('language.shipping.approximatePrice') {{ $order->price }} @lang('language.egp')
                                    </p>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('.nav-tabs li a').addClass('p-2')
        $(document).ready(function (e) {
            $('.upcomingOrders-btn').click();
        })
        $('.trip-list li').click(function (e) {
            if ($(this).hasClass('full-info') == false) {
                $('.trip-list li').scrollTop(0);
                $('.trip-list li').removeClass('full-info');
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
                $('.full-info-inner').css({height: '0'});
                $(this).addClass('full-info');
                $('.trip-list li:hover span.chevron-down').html(`
                    <i class="fa fa-chevron-up"></i>
                `);
                $('.full-info-inner').css({height: 'auto'});
            } else {
                if (e.target !== this)
                return ;
                $('.trip-list li').scrollTop(0);
                $('.trip-list li').removeClass('full-info');
                $('.full-info-inner').css({height: '0'});
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
            }
            $(window).scrollTop( $(this).offset().top );
        })


        {{-- $('.cancel-reservaion').click(function(e) {
        e.preventDefault();
        var conf = confirm('@lang('language.areYouSureYouWantToCancel')')

        if (conf) {
        $('#loader').fadeIn();
        $.ajax({
        method: 'POST',
        url: '/cancel-private-request',
        data: {
        request_id: $(this).attr('data-id')
    },
    success: function (data) {
    console.log(data);
    if (data.code == '200') {
    // location.reload();
} else if (data.code == '422') {
for (var error in data.response) {
if (data.response.hasOwnProperty(error)) {
alert(data.response[error]);
}
}
} else {
alert(data.response);
}
location.reload();
$('#loader').fadeOut();
},
error: function(jqXHR, textStatus, errorThrown) {
if (errorThrown.toString() == 'Unauthorized')
window.location.href = '/login';

console.error(textStatus);
alert(errorThrown.toString())

$('#loader').fadeOut();
}
})
}
}) --}}

$('.feedback-form').submit(function (e) {
    e.preventDefault();
    $('#loader').fadeIn()
    $.ajax({
        url: '/feedback/send-private',
        method: 'POST',
        processData: false,
        cache: false,
        contentType: false,
        data: new FormData(this),
        success: function (data) {
            $('.error').remove();
            if (data.code == '200') {
                $('.alert-success').slideDown('fast');
                setTimeout(function () {
                    location.reload();
                }, 3000)
            } else if (data.code == '422') {

                for (var field in data.response) {
                    if (data.response.hasOwnProperty(field)) {
                        $('.'+ field).after(`
                            <small class="error">${data.response[field]}</small>
                        `);
                    }
                }
            }
            $('#loader').fadeOut()
        }
    })
})
    </script>
@endsection
