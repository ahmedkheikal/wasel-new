@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('/css/user-dashboard.css') !!}">
    <style media="screen">
    input[name=add_feedback] {
        display: block;
        margin: auto;
        width: 75%;
    }
    .price {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 5px;
        padding-top: 4px;
        border-radius: 3px;
        width: 50%;
        margin: auto;
    }
    .chevron-down {
        position: relative;
        width: 20px;
        height: 20px;
    }
    .nav-tabs li {
        width: 50%;
        display: inline-block;
    }
    @media (max-width: 1250px) {
        .full-info-inner {
            top: auto !important;
        }
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="card pb-5">
            <ul class="nav nav-tabs" dir="rtl">
                <li><a class="upcomingTrips-btn" data-toggle="tab" href="#upcomingTrips">@lang('titles.upcomingPrivateTrips')</a></li>
                {{-- <li><a data-toggle="tab" href="#upcomingPrivateTrips">@lang('titles.upcomingPrivateTrips')</a></li> --}}
                <li><a data-toggle="tab" href="#previousTrips">@lang('titles.previousTrips')</a></li>
            </ul>

            <div class="container p-sm-5">
                <div class="tab-content">
                    <div id="upcomingTrips" class="tab-pane fade in active">
                        <ul class="trip-list" dir="rtl">
                            @forelse ($upcomingTrips as $trip)
                                <li>
                                    <div class="row">
                                        <div class="col-md-4 col-6">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                            </span> -
                                            <span class="to">
                                                @lang("db.{$trip->to_city}")
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>
                                        <div class="col-sm-4 col-6">
                                            @lang("db.{$trip->request_status}")
                                        </div>
                                        <div class="col-md-4 col-6 mt-2">
                                            <span class="captain">
                                                {{-- <i class="fa fa-user"></i> &nbsp; --}}
                                                @lang("language.{$trip->trip_type}")
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row full-info-inner pb-2 mt-4">
                                        <div class="col-md-4 text-right">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->from_city_address }}, {{ $trip->pickup_address }}
                                                </span>
                                            </span> <br>
                                            <span class="to">
                                                <i class="fa fa-bullseye"></i>
                                                @lang("db.{$trip->to_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->to_city_address }}, {{ $trip->arrival_address }}
                                                </span>
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                    $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                    $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>

                                        @if ($trip->request_status == 'scheduled')
                                            @isset($trip->reservation)
                                                <div class="col-md-4 text-center" >
                                                    @include('components.captain', ['captain' => $trip->reservation->trip->captain, 'phoneVisible' => true])
                                                </div>
                                            @endisset
                                        @endif

                                        <div class="col-md-4">
                                            <h3 class="text-center">@lang('titles.price')</h3>
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ $trip->price }} @lang('language.egp')
                                                </span>
                                            </div>
                                            @if ($trip->request_type == 'other')
                                                <h3 class="text-center mt-3">@lang('titles.reservedFor'): </h3>
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td>@lang('language.name')</td>
                                                            <td>{{ $trip->fullname }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>@lang('language.phone')</td>
                                                            <td>{{ $trip->phone }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            @endif
                                        </div>

                                        @if ($trip->request_status == 'pending')
                                            <div class="col-md-4">
                                                <p style="font-size: 17px; color: #888">
                                                    @lang('language.tripRequestPending')
                                                </p>
                                            </div>
                                        @endif

                                        {{-- @if ($trip->request_status !== 'canceled')
                                            <div class="offset-md-4 col-md-4 mt-2">
                                                <button data-id="{{ $trip->id }}" class="btn btn-danger cancel-reservaion" style="width: 100%;">
                                                    @lang('language.cancelOrder')
                                                </button>
                                            </div>
                                        @endif --}}
                                    </div>
                                </li>
                            @empty
                                <h4 class="text-center">@lang('language.noPendingTrips')</h4>
                            @endforelse
                        </ul>
                    </div>

                    <div id="previousTrips" class="tab-pane fade">
                        <ul class="trip-list" dir="rtl">
                            <script src="{!! asset('js/jquery.min.js') !!}"></script>
                            @forelse ($previousTrips as $trip)
                                <li>
                                    <div class="row">
                                        <div class="col-sm-4 col-8">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                            </span> -
                                            <span class="to">
                                                @lang("db.{$trip->to_city}")
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>
                                        @if ($trip->request_status == 'canceled')
                                            <div class="col-sm-4 col-4">
                                                @lang("db.{$trip->request_status}")
                                            </div>
                                        @endif
                                        <div class="col-sm-4 col-6 mt-2">
                                            <span class="captain">
                                                {{-- <i class="fa fa-user"></i> &nbsp; --}}
                                                @lang("language.{$trip->trip_type}")
                                            </span>
                                        </div>

                                    </div>

                                    <div class="row full-info-inner pb-2 mt-4">
                                        <div class="col-md-4 text-right">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->from_city_address }}, {{ $trip->pickup_address }}
                                                </span>
                                            </span> <br>
                                            <span class="to">
                                                <i class="fa fa-bullseye"></i>
                                                @lang("db.{$trip->to_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->to_city_address }}, {{ $trip->arrival_address }}
                                                </span>
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                    $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                    $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>

                                            @if ($trip->request_status == 'scheduled')
                                                @isset($trip->reservation)
                                                    <div class="text-center" >
                                                        @include('components.captain', ['captain' => $trip->reservation->trip->captain, 'phoneVisible' => true])
                                                    </div>
                                                @endisset
                                            @endif
                                        </div>

                                        <div class="col-md-4">
                                            @if ($trip->request_type == 'other')
                                                <h3 class="text-center">@lang('titles.reservedFor'): </h3>
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td>@lang('language.name')</td>
                                                            <td>{{ $trip->fullname }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>@lang('language.phone')</td>
                                                            <td>{{ $trip->phone }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            @endif
                                            <h3 class="text-center">@lang('titles.price')</h3>
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ $trip->price }} @lang('language.egp')
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-md-4 pt-3">
                                            @if ($trip->feedback !== null)
                                                <div style="color: #888;">
                                                    <div class="form-group">
                                                        <label for="">@lang('language.feedback.timing')</label>
                                                        @include('components.five-stars', [
                                                            'inForm' => false,
                                                            'rating' => $trip->feedback->timing
                                                        ])
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">@lang('language.feedback.behaviour')</label>
                                                        @include('components.five-stars', [
                                                            'inForm' => false,
                                                            'rating' => $trip->feedback->behaviour
                                                        ])
                                                    </div>
                                                    <div class="form-group">
                                                        <label>@lang('language.feedback.safety')</label>
                                                        @include('components.five-stars', [
                                                            'inForm' => false,
                                                            'rating' => $trip->feedback->safety
                                                        ])
                                                    </div>
                                                    <div class="form-group">
                                                        <label>@lang('language.feedback.cleanliness')</label>
                                                        @include('components.five-stars', [
                                                            'inForm' => false,
                                                            'rating' => $trip->feedback->cleanliness
                                                        ])
                                                    </div>
                                                    <div class="form-group">
                                                        <label>@lang('language.feedback.pricing_commitment')</label>
                                                        @include('components.five-stars', [
                                                            'inForm' => false,
                                                            'rating' => $trip->feedback->pricing_commitment
                                                        ])
                                                    </div>
                                                    <div class="form-group">
                                                        <label>@lang('language.feedback.pickup_dropoff_commitment')</label>
                                                        @include('components.five-stars', [
                                                            'inForm' => false,
                                                            'rating' => $trip->feedback->pickup_dropoff_commitment
                                                        ])
                                                    </div>
                                                    <div class="form-group">
                                                        <label>
                                                            @lang('language.feedback.overall_rating')
                                                            {{ $trip->feedback->overall_rating }}
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>@lang('language.feedback.notes')</label>
                                                        <p>
                                                            {{ $trip->feedback->notes }}
                                                        </p>
                                                    </div>

                                                </div>
                                            @else
                                                <form class="feedback-form formClass{{ $trip->id }}" action="index.html" method="post">
                                                    <input type="hidden" name="request_id" value="{{ $trip->id }}">
                                                    <div class="form-group timing">
                                                        <label for="timing">@lang('language.feedback.timing')</label>
                                                        @include('components.five-stars', [
                                                        'inForm' => true,
                                                        'name' => 'timing',
                                                        ])
                                                    </div>

                                                    <div class="form-group behaviour">
                                                        <label for="behaviour">@lang('language.feedback.behaviour')</label>
                                                        @include('components.five-stars', [
                                                        'inForm' => true,
                                                        'name' => 'behaviour',
                                                        ])
                                                    </div>

                                                    <div class="form-group safety">
                                                        <label for="safety">@lang('language.feedback.safety')</label>
                                                        @include('components.five-stars', [
                                                        'inForm' => true,
                                                        'name' => 'safety',
                                                        ])
                                                    </div>

                                                    <div class="form-group cleanliness">
                                                        <label for="safety">@lang('language.feedback.cleanliness')</label>
                                                        @include('components.five-stars', [
                                                        'inForm' => true,
                                                        'name' => 'cleanliness',
                                                        ])
                                                    </div>

                                                    <div class="form-group pricing_commitment">
                                                        <label for="pricing_commitment">@lang('language.feedback.pricing_commitment')</label>
                                                        @include('components.five-stars', [
                                                        'inForm' => true,
                                                        'name' => 'pricing_commitment',
                                                        ])
                                                    </div>

                                                    <div class="form-group pickup_dropoff_commitment">
                                                        <label for="pickup_dropoff_commitment">@lang('language.feedback.pickup_dropoff_commitment')</label>
                                                        @include('components.five-stars', [
                                                        'inForm' => true,
                                                        'name' => 'pickup_dropoff_commitment',
                                                        ])
                                                    </div>

                                                    <div class="form-group five-stars">
                                                        <label for="overall_rating">@lang('language.feedback.overall_rating')</label>
                                                        <input type="hidden" name="overall_rating" value="">
                                                        <span class="overall_rating_text"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="notes">@lang('language.feedback.notes')</label>
                                                        <textarea name="notes" rows="3" class="form-control"></textarea>
                                                    </div>

                                                    <input type="submit" name="add_feedback" class="btn btn-primary" value="@lang('language.send')">
                                                    <div class="alert alert-success" style="display: none">
                                                        @lang('language.feedback.submittedSuccessfully')
                                                    </div>
                                                </form>

                                                <script type="text/javascript">
                                                    $('.feedback-form.formClass{{ $trip->id }} input[type=radio]').change(function (e) {
                                                        var sum = 0;
                                                        $('.feedback-form input[type=radio]:checked').each(function (index) {
                                                            count = index;
                                                            sum += parseInt($(this).val());
                                                        })
                                                        var rating = (sum / (++count * 5) * 5);
                                                        $('.feedback-form.formClass{{ $trip->id }} input[name=overall_rating]').val(Math.round(rating * 100) / 100)
                                                        $('.feedback-form.formClass{{ $trip->id }} .overall_rating_text').html(Math.round(rating * 100) / 100)
                                                    })
                                                </script>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @empty
                                <h4 class="text-center">@lang('language.noPreviousTrips')</h4>
                            @endforelse
                        </ul>
                        {{ $previousTrips->links() }}
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('.nav-tabs li a').addClass('p-2')
        $(document).ready(function (e) {
            $('.upcomingTrips-btn').click();
        })
        $('.trip-list li').click(function (e) {
            if ($(this).hasClass('full-info') == false) {
                $('.trip-list li').scrollTop(0);
                $('.trip-list li').removeClass('full-info');
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
                $('.full-info-inner').css({height: '0'});
                $(this).addClass('full-info');
                $('.trip-list li:hover span.chevron-down').html(`
                    <i class="fa fa-chevron-up"></i>
                `);
                $('.full-info-inner').css({height: 'auto'});
            } else {
                if (e.target !== this)
                return ;
                $('.trip-list li').scrollTop(0);
                $('.trip-list li').removeClass('full-info');
                $('.full-info-inner').css({height: '0'});
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
            }
            $(window).scrollTop( $(this).offset().top );
        })


        {{-- $('.cancel-reservaion').click(function(e) {
        e.preventDefault();
        var conf = confirm('@lang('language.areYouSureYouWantToCancel')')

        if (conf) {
        $('#loader').fadeIn();
        $.ajax({
        method: 'POST',
        url: '/cancel-private-request',
        data: {
        request_id: $(this).attr('data-id')
    },
    success: function (data) {
    console.log(data);
    if (data.code == '200') {
    // location.reload();
} else if (data.code == '422') {
for (var error in data.response) {
if (data.response.hasOwnProperty(error)) {
alert(data.response[error]);
}
}
} else {
alert(data.response);
}
location.reload();
$('#loader').fadeOut();
},
error: function(jqXHR, textStatus, errorThrown) {
if (errorThrown.toString() == 'Unauthorized')
window.location.href = '/login';

console.error(textStatus);
alert(errorThrown.toString())

$('#loader').fadeOut();
}
})
}
}) --}}

$('.feedback-form').submit(function (e) {
    e.preventDefault();
    $('#loader').fadeIn()
    $.ajax({
        url: '/feedback/send-private',
        method: 'POST',
        processData: false,
        cache: false,
        contentType: false,
        data: new FormData(this),
        success: function (data) {
            $('.error').remove();
            if (data.code == '200') {
                $('.alert-success').slideDown('fast');
                setTimeout(function () {
                    location.reload();
                }, 3000)
            } else if (data.code == '422') {

                for (var field in data.response) {
                    if (data.response.hasOwnProperty(field)) {
                        $('.'+ field).after(`
                            <small class="error">${data.response[field]}</small>
                        `);
                    }
                }
            }
            $('#loader').fadeOut()
        }
    })
})
    </script>
@endsection
