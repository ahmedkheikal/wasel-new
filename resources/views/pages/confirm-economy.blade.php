@extends('layouts.page')

@section('styles')
<style>
    .fawry-logo  {
        max-height: 50px;
        margin: 10px 20px;
    }
    .notes-wrapper {
        margin-right: 20px;
        margin-left: 20px;
    }
</style>
@endsection

@section('content')
    @include('components.header')

    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row"  dir="rtl">
            <div class="col-md-12">
                <div class="card">
                    <form class="reserve-eco" action="/reserve-economy-trip" method="post">
                        <div class="row">
                            <div class="mt-5 col-md-6 " dir="rtl">
                                <div style="width: 50%; float: right ">
                                    @include('components.captain', [ 'captain' => $trip->captain, 'phoneVisible' => true ])
                                </div>
                                <div style="width: 50%; float: left ">
                                    <div class="ticket-info text-right">
                                        <p>@lang('titles.from'): @lang("db.{$trip->from_city}")</p>
                                        <p>@lang('titles.to'): @lang("db.{$trip->to_city}")</p>
                                        <p>@lang('titles.date'): <span dir="ltr"> {{ ( new DateTime($trip->date) )->format('d M Y') }} </span> </p>
                                        <p>@lang('titles.time'): {{ (new DateTime($trip->departure_time))->format('h:i A') }}</p>
                                        <p>
                                            @lang('titles.reservedSeats'):
                                            {{ $currentReservation['seat_1'] == '1' ? Lang::get('language.frontSeat') . ', ' : '' }}
                                            {{ $currentReservation['seat_2'] == '1' ? Lang::get('language.backSeatRight'). ', ' : '' }}
                                            {{ $currentReservation['seat_3'] == '1' ? Lang::get('language.backSeatLeft') : '' }}
                                        </p>
                                        <p>@lang('language.promoCode'): {{ $currentReservation['promo_code'] }}</p>
                                        <h3>
                                            @lang('titles.price') {{ $currentReservation['price'] }} @lang('language.egp')
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-5 col-md-6">
                                <div class="text-right">
                                    <img src="{{ url('/images/fawry.jpg') }}" alt="fawry payment" class="fawry-logo">
                                    <h5 style="font-weight: bold;" class="notes-wrapper">ادفع باستخدام فوري</h5>
                                    <p class="notes-wrapper">قم بتأكيد حجز الرحلة مع واصل من خلال الدفع باستخدام خدمة فورى </p>
                                    <p style="color: red; font-weight: bold;" class="notes-wrapper">
                                    سوف يتم الغاء الحجز بعد ساعتين  من  الان اذا لم يتم دفع المبلغ من اى مكان به خدمة فورى</p>
                                    <p style="color: blue; font-weight: bold;" class="notes-wrapper">
                                        فى حالة إلغاء الحجز قبل موعد الرحله ب ٢٤ ساعه على الاقل ، يتم خصم ٥٠ جنيه رسوم الالغاء عن كل مقعد ، واسترداد باقى المبلغ من فرع واصل بالمنيا
                                    </p>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <button type="submit" class="btn btn-primary form-control">
                                        @lang('titles.confirmReservation')
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
    $('.reserve-eco').submit(function (e) {
        e.preventDefault();
        $('input[name="book_trip"]').attr('disabled', 'disabled');
        $('input[name="book_trip"]').val('@lang('language.loading')');
        $('#loader').fadeIn();
        $.ajax({
            method: 'POST',
            url: '/reserve-economy-trip',
            data: {
                trip_id: '{{ $trip->id }}',
                seat_1: '{{ $currentReservation['seat_1'] }}',
                seat_2: '{{ $currentReservation['seat_2'] }}',
                seat_3: '{{ $currentReservation['seat_3'] }}',
                use_50EGP_credit: '{{ $currentReservation['use_50EGP_credit'] }}',
                @if (isset(session('tripToReserve')['reservation_type']))
                reservation_type: '{{ $currentReservation['reservation_type'] }}',
                setfor_name: '{{ $currentReservation['setfor_name'] }}',
                setfor_phone: '{{ $currentReservation['setfor_phone'] }}',
                @endif
                promo_code: '{{ $currentReservation['promo_code'] }}'
            },
            success: function (data) {
                console.log(data);
                if (data.code == '401') {
                    window.location.href = '/login';
                } else if (data.code == '200') {
                    window.location.href = '/reservaion-Successful';
                } else {
                    alert(data.response);
                }
                $('#loader').fadeOut();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (errorThrown.toString() == 'Unauthorized')
                window.location.href = '/login';

                console.error(textStatus);
                alert(errorThrown.toString())
                $('#loader').fadeOut();
                $('input[name="book_trip"]').removeAttr('disabled');
                $('input[name="book_trip"]').val('@lang('language.book')');
            }
        })
    });
    </script>
@endsection
