@extends('layouts.page')

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px; min-height: 70vh">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="my-5 offset-md-3 col-md-6 alert alert-success text-center">
                        @lang('language.reservationSuccessful')
                    </div>
                    {{-- <div class="my-5 offset-md-3 col-md-6 " dir="rtl">
                        <div style="width: 50%; float: right ">
                            @include('components.captain', [ 'captain' => $trip->captain ])
                        </div>
                        <div style="width: 50%; float: left ">
                            <div class="ticket-info text-right">
                                <p>@lang('titles.from'): {{ $trip->from_city }}</p>
                                <p>@lang('titles.to'): {{ $trip->to_city }}</p>
                                <p>@lang('titles.date'): <span dir="ltr"> {{ ( new DateTime($trip->date) )->format('d M Y') }} </span> </p>
                                <p>@lang('titles.time'): {{ (new DateTime($trip->time))->format('h:i A') }}</p>
                                <p>@lang('titles.fromAddress'): </p>
                                <p>@lang('titles.toAddress'): </p>
                                <p>
                                    @lang('titles.reservedSeats'):

                                </p>
                                <h3>@lang('titles.price') {{ $reservation->price }} @lang('language.egp')</h3>
                            </div>
                        </div>
                    </div> --}}
                    <div class="offset-md-4 col-md-4 mb-4">
                        <a href="/my-trips">
                            <button class="btn btn-primary form-control">
                                @lang('titles.myTrips')
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
@endsection
