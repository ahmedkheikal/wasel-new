@extends('layouts.page')

@section('styles')
    <style media="screen">
        .justify-content-center * {
            text-align: right;
        }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>

        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-8">
                <div class="card" dir="rtl">
                    <div class="card-header text-right">@lang('titles.termsAndConds')</div>

                    <div class="card-body">

                        <style type="text/css">
                            @page {
                                margin-left: 1.25in;
                                margin-right: 1.25in;
                                margin-top: 1in;
                                margin-bottom: 1in
                            }

                            p {
                                margin-bottom: 0.1in;
                                direction: rtl;
                                line-height: 120%;
                                text-align: right;
                                orphans: 2;
                                widows: 2
                            }

                            h3 {
                                margin-top: 0.17in;
                                direction: rtl;
                                line-height: 115%;
                                text-align: right;
                                orphans: 2;
                                widows: 2
                            }

                            h3.western {
                                font-family: "Liberation Sans", sans-serif;
                                font-weight: normal
                            }

                            h3.cjk {
                                font-family: "Noto Sans CJK SC Regular";
                                font-weight: normal
                            }

                            h3.ctl {
                                font-family: "FreeSans";
                                font-weight: normal
                            }

                            a:link {
                                so-language: zxx
                            }
                            .page-content * {
                                font-size: 15px !important;
                                line-height: 160%;
                                font-weight: normal;
                            }
                        </style>


                        <div class="row">
                            <div class="col-12">
                                <p dir="rtl" class="page-content">
                                    <p>
                                        هذه الخدمه مقدمه من واصل بالتعاقد مع شركة فيدكس (ايجيبت اكسبريس) لذلك تخضع الخدمه بالكامل لكل الشروط والاحكام الخاصه بشركة فيدكس (ايجبت اكسبريس) وهى كالتالى
                                    </p>
                                    <span style="font-size: large;">
                                        <strong>1- </strong>
                                    </span>
                                    <span style="font-family: Arial;">
                                        <span lang="ar-SA">
                                            <span style="font-size: 16px;">
                                                <span lang="ar-EG">
                                                    <strong>يجب على الراسل ملء جميع البنود الواردة في بوليصة الشحن المحلية بشكل صحيح وواضح،حيث يجب ملء البيانات الخاصة به وبالمرسل إليه وهي</strong>
                                                </span>
                                            </span>
                                        </span>
                                    </span>
                                    <span style="font-size: large;">
                                        <strong>( </strong></span>
                                        <span style="font-family: Arial;">
                                            <span lang="ar-SA">
                                                <span style="font-size: large;">
                                                    <span lang="ar-EG">
                                                        <strong>الأسم الكامل للراسل والمرسل إليه، اسم الشركة والأدارة ،رقم التليفون والعنوان كاملاً ، المعلومات الكاملة الخاصة بالشحنة، نوع الخدمة المطلوبة مثل خدمة التوصيل المبكر وكذلك طريقة الدفع</strong>
                                                    </span></span>
                                                </span>
                                            </span>
                                            <span style="font-size: large;"><strong>).<br /> <br /> 2- </strong></span>
                                            <span style="font-family: Arial; fads ">
                                                <span lang="ar-SA">
                                                    <span style="font-size: large;">
                                                        <span lang="ar-EG">
                                                            <strong>يجب على الراسل أن يقوم بالتوقيع على بوليصة الشحن المحلية في المكان المخصص لذلك</strong>
                                                        </span>
                                                    </span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 3- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>تحتفظ شركة ايجيبت اكسبريس بالحق في رفض أو إعادة أي شحنة ترى أنها قد تسبب ضررا تحدث تأخير لباقي الشحنات أو البضائع أو الأشخاص أو تسبب ضرراً مباشر أو غير مباشر لعملية النقل أو تتعارض مع القانون المصري</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 4- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>هناك بعض المواد المحظورة قانونا مثل المواد المنتجات المصنعة من الحيوانات أو النباتات وهي مواد تقع تحت حماية بنود مؤتمر التجارة العالمية للكائنات المعرضة للانقراض وكذلك النحل السندات لحامله والحوالات والنقود والعملات وكل ما يعادل النقود والدفع النقدي عند توصيل الشحنات والقمار والنفايات السامة والبقايا الآدمية والأسماك الحية والحيوانات الأليفة الحية والقرود الحية وتذاكر اليانصيب والفن الإباحي أو أي شحنات أخرى محظورة قانوناً</strong></span></span></span></span><span style="font-size: large;"><strong><br /> <br /> 5- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>يجب على الراسل أن يقوم بعملية التعبئة والتغليف وذلك لضمان النقل الآمن للشحنة</strong></span></span></span></span><span style="font-size: large;"><strong>. </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>وتقوم شركة ايجيبت اكسبريس بتوفير مواد التعبئة مثل الظرف الصغير والكبير</strong></span></span></span></span><span style="font-size: large;"><strong>. </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>فإذا لم يقم الراسل بإستخدامهم هتقع عليه في هذه الحالة مسئولية تغليف الشحنة بمعرفته تغليفاً نتقبله شركة ايجيبت اكسبريس ويحافظ على الشحنة أثناء نقلها</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 6- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>يحق لشركة ايجيبت اكسبريس القيام بفتح الشحنات أو تفتيشها في حالة طلب السلطات الأمنية</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 7- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>يتعذر تسليم الشحنات إذا كان عنوان المرسل إليه غي كاملاً أو غير صحيحاً أو لم يتم العثور عليه، أو إذا قام المرسل إليه برفض تسلم الشحنة والتوقيع عليها أو في حالة عدم تواجد المرسل إليه</strong></span></span></span></span><span style="font-size: large;"><strong>. </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>أو إذا أدت الشحنة إلى تأخير أو تدمير لشحنات أو أشخاص أو بضائع أخرى، أو إذا إحتوت الشحنة على مواد محظورة، وفي هذه الحالات، سيكون الراسل مسئولا مسئولية كاملة عن عدم وصول الشحنة للمرسل إليه</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 8- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>تعتبر شركة ايجيبت اكسبريس غير مسئولة عن أي شحنة تحتوي على نقود أو عملات أو أي مواد أخرى محظورة لم يتم توضيح هويتها في بوليصة الشحن المحلية، وتعتبر شركة ايجيبت اكسبريس غير مسئولة عن خسارة أو ضياع أو تأخير أو توصيل خاطىء أو فشل في التوصيل لهذا النوع من الشحنات</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 9- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>في حالة أن الوزن الفعلي للشحنة أقل من الوزن البعدي يتم احتساب الوزن البعدي</strong></span></span></span></span><span style="font-size: large;"><strong>. <br /> <br /> 10- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>تعاريف</strong></span></span></span></span><span style="font-size: large;"><strong>: </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>حينما يذكر ببوليصة الشحن المحلي هذه نحن و لنا و بناء و ناء الدالة على الجمع و ايجيبت اكسبريس فهذا كله يشير إلى أن ايجيبت اكسبريس للبريد السريع وفروعها وموظفيها ذوي العلاقة ووكلائها والمتعاقدين المستقلين تشير كلمة انتم أو لكم أو الخاصة بكم إلى الشاحن وموظفيه و مديريه ووكلائه إذا كانت شحنتكم صادرة من داخل جمهورية مصر العربية فإن عقدكم للنقل هو ذلك الذي أجري مع شركة ايجيبت اكسبريس للبريد السريع أو فرع لها أو متعاقد مستقل يقبل</strong></span></span></span></span><span style="font-size: large;"><strong>/</strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>تقبل الشحنة منكم</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 11- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: x-large;"><span lang="ar-EG"><strong>الموافقة على الشروط والاحكام</strong></span></span></span></span><span style="font-size: x-large;"><strong>: </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>بموجب اعطائنا شحنتكم و</strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong>بعض</strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong> النظر عما إذا كنتم قد وقعتم على الصفحة الامامية من بوليصة الشحن بالأصالة عن انفسكم أو بصفتكم وكيلاَ نيابة عن أي شخص اخر له مصلحة في هذه الشحنة فأنكم بذلك توافقون على كافة الشروط والأحكام الواردة ببوليصة الشحن هذة الغير قابلة للتداول، وكافة الشروط المناسبة بأي اتفاق للنقل عن طريق ايجيبت اكسبريس </strong></span></span></span></span><span style="font-size: large;"><strong><br /> </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>بينكم وبينها بما يغطي هذه الشحنة، وبأي تعريفة سارية لديها والواردة بدليل الخدمة الحالي أو بالشروط العادية للنقل، المتوفرة نسخا منها بناء على طلبكم، إذا كان هناك أي تعارض بين ما ورد ببوليصة الشحن وأي مستند أخرى ساري المفعول، أو أي اتفاق نقل أو تعريفه أو دليل خدمة ساري المفعول أو شروط عادية للنقل فأن الذي يسود هو بأسبقية ما ورد ذكره بهذه الفترة لا يجوز لأي شخص أن يغير أو يعدل من شروط واحكام اتفاقنا</strong></span></span></span></span><span style="font-size: large;"><strong>. </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>تعد وثيقة الشحن هذه ملزمة لنا في حالة قبول الشحنة ويجوز لنا أن نضع علامة أو علامات على بوليصة الشحن هذه خاصة برقم لموظف لنا و كتوقيع خاص بنا، أو وضع الاسم المطبوع لنا الذي يتعين اعتباره كاف لكي يكون بمثابة توقيع لنا على بوليصة الشحن هذه</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 12- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: x-large;"><span lang="ar-EG"><strong>التأمين</strong></span></span></span></span><span style="font-size: large;"><strong>: </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>سوف تقوم إيجيبت إكسبريس بتقديم التأمين بالقيمة المعرفة وفي حالة الرغبة في التأمين على الشحنات عن طريق افادتنا بقيمة الشحنات كتابيا</strong></span></span></span></span><span style="font-size: large;"><strong>. </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>وفي حالة حدوث أي خسائر أو ضياع للشحنات تعد مسئولية إيجيبت إكسبريس في كافة الحالات محدودة عند دفع التعويضات ، أي المبالغ التأمينية كما تم تحديدها مسبقاً بواسطة الراسل في الفاتورة وذلك على أن يتم تسديد مصاريف التأمين بالكامل وبصفة مقدمة</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>لا يغطي التأمين أي مسئولية ناتجة عن خطأ الراسل بإرسال شحنات مخالفة للقانون المصري وقواعد وأحكام إيجيبت إكسبريس وكذلك لا يغطي التأمين في حالات القوى القاهرة أو الخارقة</strong></span></span></span></span><span style="font-size: large;"><strong>. </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>تكون عملية التسديد محكومة بالقواعد والأحكام الخاصة بإيجيبت إكسبريس بالإضافة إلى الجهة المانحة للتأمين</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 13- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: x-large;"><span lang="ar-EG"><strong>إلتزامكم</strong></span></span></span></span><span style="font-size: x-large;"><strong>: </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>انكم تضمنون أن كل مادة في الشحنة قد تم بيانها وتوضيحها بصورة مناسبة ببوليصة الشحن هذه وأن أي مستندات تصدر مقبولة للنقل بمعرفتنا وقد وضع عليها ما يتناسب من علامات وعناوين وتم تربيطها للتأكد من نقلها بصورة آمنة وإعطائها الرعاية العادية في التداول</strong></span></span></span></span><span style="font-size: large;"><strong>. </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>إنكم مسئولون عن كافة الرسوم والمصاريف في ذلك مصاريف النقل والمصاريف بما في ذلك المصاريف الإضافية المحتملة، وتقديرات الرسوم بما في ذلك الرسوم المتعلقة بالدفع المسبق لنا والجزاءات والغرامات الحكومية والضرائب وأتعاب المحاماة الخاصة بنا والتكاليف القانونية </strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong>المتعلقة</strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong> بهذه الشحنة</strong></span></span></span></span><span style="font-size: large;"><strong>. <br /> <br /> 14- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>حد المسئولية</strong></span></span></span></span><span style="font-size: large;"><strong>: </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>إن أقصى حد لمسئوليتنا تجاه الخسارة أو الضرر أو النقص أو التسليم الخاطىء أو عدم تقديم معلومات تتعلق بشحنتكم ، وطبقاً لما هو محدد ببوليصة الشحن هذه تكون </strong></span></span></span></span><span style="font-size: large;"><strong>(100</strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>ج مائة جنيه فقط لاغير</strong></span></span></span></span><span style="font-size: large;"><strong>) </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>للشحنة، فإن أقصى حد لمسئوليتنا سيكون أقل من قيمتكم المعلنة للنقل أو الأضرار الفعلية التي لحقت بكم ، نحن لا نقدم أي تأمين ضد مسئولية البضاعة أو تأمين ضد كافة المخاطر</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 15- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: x-large;"><span lang="ar-EG"><strong>الخدمات المحلية المتنوعة التي تقدمها إيجيبت إكسبريس هي كالتالي</strong></span></span></span></span><span style="font-size: x-large;"><strong>: </strong></span><span style="font-size: large;"><strong><br /> 1/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>خدمة التسليم في اليوم التالي حيث يتم تسليم الشحنة قبل نهاية يوم العمل الذي وصلت فيه الشحنة للمدينة المرسل إليها وفقاً للجدول الزمني لمواعيد التسليم بأنحاء الجمهورية</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 2/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>خدمة من محطة إلى محطة وهي تعتمد على أن يقوم العميل بإحضار الشحنة إلى أقرب فرع من الفروع التي تقدم هذه الخدمة حيث يتم الإبقاء عليها في المحطة المرسل إليها ليقوم المرسل إليه بالحضور وإستلامها من فرع إيجيبت إكسبريس</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 3/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>الخدمة الإقتصادية وهي خدمة أقل سعراً ويتم تسليمها خلال مدة يومين زيادة عن مواعيد التسليم العادية لخدمة اليوم التالي</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 4/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>خدمات صناديق إيجيبت إكسبريس الـ </strong></span></span></span></span><span style="font-size: large;"><strong>5 </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>، </strong></span></span></span></span><span style="font-size: large;"><strong>10 </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>، </strong></span></span></span></span><span style="font-size: large;"><strong>25 </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>وهي صناديق محددة السعر على أن يتم شحن المحتويات في حدود هذه الأوزان فقط ، ولا يسمح بزيادة وزن المحتويات لهذه الصناديق لأكثر من </strong></span></span></span></span><span style="font-size: large;"><strong>3 </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>كيلوجرام علماً بأن كل كيلو زيادة به سعر محدد ومعلق في قائمة الأسعار</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> <br /> 16- </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: x-large;"><span lang="ar-EG"><strong>الخدمات الإضافية</strong></span></span></span></span><span style="font-size: x-large;"><strong>: </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>التي تقدمها إيجيبت إكسبريس كالتالي</strong></span></span></span></span><span style="font-size: large;"><strong>:<br /> 1/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>خدمة التسليم في نفس اليوم وهي تعتمد على على أن تقوم إيجيبت إكسبريس بإستلام الشحنة قبل الساعة الحادية عشر صباحا على أن يتم تسليمها داخل المدينة قبل نهاية يوم العمل</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 2/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>الصباح المبكر ، تقوم إيجيبت إكسبريس من خلال </strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong>فروعها</strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong> التي تقدم هذه الخدمة لتسليم قبل الساعة التاسعة والنصف صباحاً مع ضمان إعادة مصاريف الشحن والمصاريف</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 3/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>التحصيل عند التسليم حيث تقوم إيجيبت إكسبريس بتحصيل القيمة المحددة من قبل العميل عند تسليم الشحنة</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 4/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>التوقيع والإعادة، تقوم إيجيبت إكسبريس بإعادة المرفقات المطلوبة بعد توقيعها </strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong>طالما</strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong> إن الراسل قد قام بالتنسيق مع المرسل إليه وكذلك قد قام بإرفاق بوليصة شحن إيجيبت إكسبريس محدد عليها من سيقوم بالمحاسبة على الشحنة المعادة وكذلك رقم البوليصة المعادة لابد من أن يتم الإشارة إليه في البوليصة المرسلة للمرة الأولى</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 5/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>تسليم يوم الجمعة</strong></span></span></span></span><span style="font-size: large;"><strong>: </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>حتى يتم تسليم الشحنات المرسلة يوم الأربعاء أو الخميس خلال يوم الجمعة بعد التنسيق مع مندوب خدمة العملاء بالمقر الرئيسي</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 6/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>خدمة الحفظ بمكتب إيجيبت إكسبريس حيث يتم الإبقاء على الشحنة بمقر إيجيبت إكسبريس ليقوم المرسل إليه بالحضور إلى المقر لإستلامها خلال نفس يوم وصولها</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> 7/ </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>جميع </strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong>الخدمات</strong></span></span><span style="font-size: large;"><span lang="ar-EG"><strong> أعلاه ماعدا خدمة الحفظ بمكتب إيجيبت إكسبريس لها مصاريف إضافية ثابته ولايوجد عليها أي نسبة خصم، لذا يرجى مراجعة أقرب من فروعنا أو خدمة العملاء بالمركز الرئيسي لمعرفة أسعار هذه الخدمة الخاصة</strong></span></span></span></span><span style="font-size: large;"><strong>.<br /> </strong></span><span style="font-family: Arial;"><span lang="ar-SA"><span style="font-size: large;"><span lang="ar-EG"><strong>إيجيبت إكسبريس بقبول الشحنات المغلقة بمعرفة الراسل على كامل مسئوليته وفقاً لنوعية هذه المحتويات </strong></span></span></span></span>
                                                    نرجو منكم التكرم بمراجعة وقت التوصيل الخاص بالشحن المحلي حيث أنه يختلف بإختلاف المناطق سواء المرسل منها أو إليها  <br>
                                                    لمزيد من المعلومات نرجو الإتصال على 19985
                                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
@endsection

@section('page-scripts')
@endsection
