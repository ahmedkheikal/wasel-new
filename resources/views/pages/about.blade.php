@extends('layouts.page')

@section('styles')
    <style media="screen">
        .justify-content-center * {
            text-align: right;
        }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>

        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-8">
                <div class="card" dir="rtl">
                    <div class="card-header text-right">@lang('titles.about')</div>

                    <div class="card-body">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('.become-a-captain-form').submit(function (e) {
            e.preventDefault();
            $('.error').remove();
            $.ajax({
                method: 'POST',
                url: 'become-a-captain',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    console.log(data);
                    if (data.code == '200') {
                        window.location.href = '/account-settings'
                    } else if (data.code == '422') {
                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                $('.form-control[name="'+ field +'"]').after(`
                                    <small class="error">${data.response[field]}</small>
                                `);
                                $('.form-control[name="'+ field +'"]').focus();
                            }
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus);
                    alert(errorThrown.toString())
                }
            });
        })
    </script>
@endsection
