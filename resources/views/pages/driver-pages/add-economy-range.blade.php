@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.date.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.time.css') !!}">
    <style media="screen">
        input[name=range_end]:disabled {
            /* display: none !important; */
            background: #dcdcdc !important;
            color: #dcdcdc !important;
        }
        .time-params p {
            text-align: right;
            font-size: 12px;
            padding: 0; margin: 0;
        }
        span.page-title {
            width: 100% !important;
            padding: 5px 20px;
            width: 50%;
            float: right;
            text-align: right;
        }
        .eco-trip-date-params .form-control {
            display: inline;
            background: white;
        }
        form h5, form h6 {
            text-align: right;
        }
        .opacity-0 {
            opacity: 0
        }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div id="loader">
        <img src="{!! asset('images/loader.svg') !!}" alt="">
    </div>
    <div class="container" style="margin-top: 80px">
        <div class="card pb-5" style="position: relative">
            <span style="font-size: 18px">
                <span class="page-title">
                    @lang('titles.travellingAlone')
                </span>
            </span>

            <div class="container p-sm-5">
                <form class="row" action="/add-new-economy-trip" method="post">
                    <div class="offset-md-3 col-md-6 col-12">
                        <div class="row" dir="rtl">
                            <div class="col-6">
                                <h5>@lang('titles.from')</h5>
                                <select class="govs form-control" name="from_city" style="width: 100%">
                                    <option value="Cairo">@lang("db.Cairo")</option>
                                    <option value="Minya">@lang("db.Minya")</option>
                                </select>
                            </div>
                            {{-- <div class="col-6">
                                <h5 class="opacity-0">.</h5>
                                <select class="form-control pickup_address" name="pickup_address">
                                    <option value=""></option>
                                </select>
                            </div> --}}

                            <div class="col-6">
                                <h5>@lang('titles.to')</h5>
                                <select class="govs form-control" name="to_city" style="width: 100%">
                                    <option value="Minya">@lang("db.Minya")</option>
                                </select>
                            </div>
                            {{-- <div class="col-6">
                                <h5 class="opacity-0">.</h5>
                                <select class="form-control pickup_address" name="arrival_address">
                                    <option value=""></option>
                                </select>
                            </div> --}}
                        </div>

                        <div class="row eco-trip-date-params mt-3" dir="rtl">
                            <div class="col-12">
                                <h5>
                                    @lang('titles.travelDay')
                                    <sup style="color: red">*</sup>
                                </h5>
                                <input name="date" class="form-control" type="date" min="2013-12-25" placeholder="@lang('titles.date')">

                            </div>
                            <div class="col-12 mt-3" style="">
                                <h5>@lang('titles.travelAvailability')</h5>
                                <div class="row time-params">
                                    <div class="col-6">
                                        <p>
                                            من الساعة
                                            <sup style="color: red; font-size: 15px">*</sup>
                                        </p>
                                        <input type="time" name="range_start" class="form-control" value="" placeholder="@lang('language.economyrangeStart')">
                                    </div>
                                    <div class="col-6">
                                        <p>
                                            إلى الساعة
                                            <sup style="color: red; font-size: 15px">*</sup>
                                        </p>
                                        <input type="time" disabled name="range_end" class="form-control" value="" placeholder="@lang('language.economyrangeEnd')">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="offset-md-3 col-md-6 mt-3">
                                <input type="submit" name="add_trips" style="width: 100%" class="btn btn-primary mt-4" value="ارسال بيانات السفر">
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{!! asset('js/picker.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.date.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.time.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/ar.js') !!}"></script>


    <!-- Modal -->
    <div class="modal fade" id="addTripsConfirmation" tabindex="-1" role="dialog" aria-labelledby="addTripsConfirmationLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addTripsConfirmationLabel">@lang('language.confirm')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="text-align: right" dir="rtl">
                    @lang('language.areYouSureYouWantToAddTripRange')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('language.close')</button>
                    <button type="button" class="btn btn-primary confirm-submission">@lang('language.confirm')</button>
                </div>
                <div class="add-eco-alert alert"></div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
        var datePicker = $('input[name=date]').pickadate({
            disable: [
                { from: [0,0,0], to: yesterday }
            ]
        });

        var rangeEnd = $('input[name=range_end]');
        var rangeStart = $('input[name=range_start]').pickatime({
            onSet: function () {
                var rangeStartValue = new Date(getFullDate(datePicker) + " "+ rangeStart.val());
                rangeEnd = $('input[name=range_end]').pickatime({
                    format: 'h:i A',
                    min: [rangeStartValue.getHours(), rangeStartValue.getMinutes()],
                })
                $('input[name=range_end]').css("cssText", "display: inline !important;")
                $('input[name=range_end]').removeAttr('disabled')
            }
        })



        $(document).ready(function () {
            $('select[name=from_city] option[value=Cairo]').attr('selected', 'selected');
            $('select[name=from_city]').change();
        })
        $('select[name=from_city]').change(function () {
            getAlltos();
        })

        $('select[name=to_city]').change(function () {
            getRegions();
        })

        function getRegions() {
            // $('#loader').fadeIn();
            // $.ajax({
            //     method: 'POST',
            //     url: '/api/private/regions',
            //     data: {
            //         city_name: $('select[name=from_city]').val()
            //     },
            //     success: function (data) {
            //         if (data.code == '200') {
            //             $('select[name=pickup_address]').html('');
            //             for (city of data.response) {
            //                 let selected = city.main_region == '1' ? 'selected' : '';
            //                 $('select[name=pickup_address]').append(`
            //                     <option ${selected} value="${city.region}">${city.region}</option>
            //                 `);
            //             }
            //         } else if (data.code == '422') {
            //             for (var error in data.response) {
            //                 if (data.response.hasOwnProperty(error)) {
            //                     alert(data.response[error]);
            //                 }
            //             }
            //         }
            //     }
            // })
            // $.ajax({
            //     method: 'POST',
            //     url: '/api/private/regions',
            //     data: {
            //         city_name: $('select[name=to_city]').val()
            //     },
            //     success: function (data) {
            //         if (data.code == '200') {
            //             $('select[name=arrival_address]').html('');
            //             for (city of data.response) {
            //                 let selected = city.main_region == '1' ? 'selected' : '';
            //                 $('select[name=arrival_address]').append(`
            //                     <option ${selected} value="${city.region}">${city.region}</option>
            //                 `);
            //             }
            //         } else if (data.code == '422') {
            //             for (var error in data.response) {
            //                 if (data.response.hasOwnProperty(error)) {
            //                     alert(data.response[error]);
            //                 }
            //             }
            //         }
            //         $('#loader').fadeOut();
            //     }
            // })
        }

        function getAlltos() {
            // $('#loader').fadeIn();
            // $.ajax({
            //     url: '/getAlltos',
            //     method: 'POST',
            //     data: {
            //         from_city: $('select[name=from_city]').val()
            //     },
            //     success: function (data) {
            //         if (data.code == '200') {
            //             $('select[name=to_city]').html('');
            //             for (city of data.response) {
            //                 $('select[name=to_city]').append(`
            //                     <option value="${city.city_name}">
            //                         ${city.city_name_ar}
            //                     </option>
            //                 `);
            //             }
            //             removeDuplicate('select[name=to_city] option');
            //             $('select[name=to_city]').change();
            //             getRegions();
            //             $('#loader').fadeOut();
            //         } else if (data.code == '422') {
            //             for (var error in data.response) {
            //                 if (data.response.hasOwnProperty(error)) {
            //                     alert(data.response[error]);
            //                 }
            //             }
            //         }
            //         $('#loader').fadeOut();
            //     }
            // })
            if ($('select[name=from_city]').val() == 'Cairo') {
                $('select[name=to_city]').html(`
                    <option value="Minya">@lang('db.Minya')</option>
                `);
            } else {
                $('select[name=to_city]').html(`
                    <option value="Cairo">@lang('db.Cairo')</option>
                `);
            }

            getRegions();
        }
        function removeDuplicate(selector) {
            var liText = '', liList = $(selector), listForRemove = [];
            $(liList).each(function () {
                var text = $(this).text();
                if (liText.indexOf('|'+ text + '|') == -1) {
                    liText += '|'+ text + '|';
                }
                else {
                    listForRemove.push($(this));
                }
            });
            $(listForRemove).each(function () { $(this).remove(); });

        }
        function getFullDate(dateElement) {
            var privateDateArray = dateElement.val().split(' ');
            var months = [ 'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر' ];
            privateDateArray[1] = months.indexOf(privateDateArray[1]) + 1;

            if (privateDateArray[1] < 10)
            privateDateArray[1] = '0' + privateDateArray[1];
            finalPrivateDate = '';
            for (var i = 2; i >= 0; i--) {
                if (i == 0) {
                    finalPrivateDate += privateDateArray[i];
                    continue ;
                }
                finalPrivateDate += privateDateArray[i] + '-';
            }
            return finalPrivateDate;
        }

        $('form[action="/add-new-economy-trip"]').submit(function (e) {
            e.preventDefault();
            $('#addTripsConfirmation').modal()
        })

        $('.confirm-submission').click(function () {
            $.ajax({
                method: 'POST',
                url: '{!! route('captainAddAvailableRange') !!}',
                data: {
                    from_city: $('select[name="from_city"]').val(),
                    to_city:  $('select[name="to_city"]').val(),
                    // pickup_address: $('select[name=pickup_address]').val(),
                    // arrival_address: $('select[name=arrival_address]').val(),
                    date: getFullDate($('input[name=date]')),
                    range_start: rangeStart.val(),
                    range_end: rangeEnd.val(),
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('.add-eco-alert')
                            .removeClass('alert-*')
                            .addClass('alert-success')
                            .html('@lang('language.ecoTripAddedSuccessfully')')
                            .slideDown();
                        setTimeout(function () {
                            location.href = '/my-trips';
                        }, 3000)
                        $('input[type=submit]').attr('disabled', 'disabled');
                    } else if (data.code == '422') {
                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                $('.add-eco-alert')
                                    .removeClass('alert-*')
                                    .addClass('alert-danger')
                                    .html(data.response[field])
                                    .slideDown();
                            }
                        }
                        $('input[type=submit]').removeAttr('disabled');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    console.error(textStatus);
                    alert(errorThrown.toString())
                }
            })
        })
    </script>
@endsection
