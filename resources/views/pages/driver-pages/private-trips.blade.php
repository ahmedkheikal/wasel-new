@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('/css/user-dashboard.css') !!}">
    <style media="screen">
    .price {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 5px;
        padding-top: 4px;
        border-radius: 3px;
        width: 50%;
        margin: auto;
    }
    .chevron-down {
        position: relative;
        width: 20px;
        height: 20px;
    }
    #loader {
        position: absolute;
        top: 0;
        right: 0;
        width: 100%;
        background: transparent;
        z-index: 20;
        display: block;
    }

    #loader img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .nav-tabs li {
        width: 50%;
        display: inline-block;
    }

    .page-title {
        width: 100%
    }
    .page-title span {
        font-size: 18px;
        padding: 5px 20px;
        width: 50%;
        float: right;
        text-align: right;

    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div class="card pb-5">
            <span class="page-title" style="font-size: 18px">
                <span>
                    @lang('titles.privateTrips')
                </span>
            </span>
            <ul class="nav nav-tabs mt-4 " dir="rtl">
                <li><a class="upcomingTrips-btn" data-toggle="tab" href="#upcomingTrips">@lang('titles.upcomingPrivateTrips')</a></li>
                <li><a data-toggle="tab" href="#previousTrips">@lang('titles.previousTrips')</a></li>
            </ul>

            <div class="container p-sm-5">
                <div class="tab-content">
                    <div id="upcomingTrips" class="tab-pane fade in active">
                        <ul class="trip-list" dir="rtl">
                            @forelse ($upcomingPrivateTrips as $trip)
                                <li>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                            </span> -
                                            <span class="to">
                                                @lang("db.{$trip->to_city}")
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new \DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                $month = (new \DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new \DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new \DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                $ampm = (new \DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>

                                        @if ($trip->privateReservation && $trip->privateReservation->request && $trip->privateReservation->request->passenger)
                                            <div class="col-md-4 mt-2">
                                                <span class="captain">
                                                    <i class="fa fa-user"></i> &nbsp;
                                                    @lang('language.client'): {{ $trip->privateReservation->request->passenger->username }}
                                                </span>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="row full-info-inner pb-2 mt-4">
                                        <div class="col-md-4 text-right">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->from_city_address }}, {{ $trip->pickup_address }}
                                                </span>
                                            </span> <br>
                                            <span class="to">
                                                <i class="fa fa-bullseye"></i>
                                                @lang("db.{$trip->to_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->to_city_address }}, {{ $trip->arrival_address }}
                                                </span>
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new \DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                    $month = (new \DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new \DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new \DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                    $ampm = (new \DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>

                                        {{-- <div class="col-md-4 text-center" >
                                            @include('components.captain', ['captain' => $trip->privateReservation->trip->captain])
                                        </div> --}}

                                        <div class="col-md-4">
                                            <h3 class="text-center">@lang('titles.price')</h3>
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ $trip->privateReservation->price }} @lang('language.egp')
                                                </span>
                                            </div>
                                        </div>

                                        @if ($trip->privateReservation->request->passenger)
                                            <div class="col-md-4 text-center" >
                                                @include('components.user', ['user' => $trip->privateReservation->request->passenger, 'phone' => true])
                                            </div>
                                        @endif
                                    </div>
                                </li>
                            @empty
                                <h4 class="text-center">@lang('language.noPendingTrips')</h4>
                            @endforelse
                        </ul>
                    </div>
                    <div id="previousTrips" class="tab-pane fade">
                        <ul class="trip-list" dir="rtl">
                            @forelse ($prevPrivateTrips as $trip)
                                <li>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                            </span> -
                                            <span class="to">
                                                @lang("db.{$trip->to_city}")
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new \DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                $month = (new \DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new \DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new \DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                $ampm = (new \DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>
                                        @if ($trip->privateReservation && $trip->privateReservation->request && $trip->privateReservation->request->passenger)
                                            <div class="col-md-4 mt-2">
                                                <span class="captain">
                                                    <i class="fa fa-user"></i> &nbsp;
                                                    @lang('language.client'): {{ $trip->privateReservation->request->passenger->username }}
                                                </span>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="row full-info-inner pb-2 mt-4">
                                        <div class="col-md-4 text-right">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->from_city_address }}, {{ $trip->pickup_address }}
                                                </span>
                                            </span> <br>
                                            <span class="to">
                                                <i class="fa fa-bullseye"></i>
                                                @lang("db.{$trip->to_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->to_city_address }}, {{ $trip->arrival_address }}
                                                </span>
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new \DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                    $month = (new \DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new \DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new \DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                    $ampm = (new \DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>

                                        @if ($trip->privateReservation->request->passenger)
                                            <div class="col-md-4 text-center" >
                                                @include('components.user', ['user' => $trip->privateReservation->request->passenger, 'phone' => true])
                                            </div>
                                        @endif

                                        <div class="col-md-4">
                                            <h3 class="text-center">@lang('titles.price')</h3>
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ $trip->privateReservation->price }} @lang('language.egp')
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @empty
                                <h4 class="text-center">@lang('language.noPreviousTrips')</h4>
                            @endforelse
                        </ul>
                        {{ $prevPrivateTrips->links() }}
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('.nav-tabs li a').addClass('p-2')
        $(document).ready(function () {
            $('.upcomingTrips-btn').click();
        })
        $('.trip-list li').click(function () {
            $('.trip-list li').scrollTop(0);
            if ($(this).hasClass('full-info') == false) {
                $('.trip-list li').removeClass('full-info');
                $('.full-info-inner').css({height: '0'});
                $(this).addClass('full-info');
                $('.trip-list li:hover span.chevron-down').html(`
                    <i class="fa fa-chevron-up"></i>
                `);
                $('.full-info-inner').css({height: 'auto'});
            } else {
                $('.trip-list li').removeClass('full-info');
                $('.full-info-inner').css({height: '0'});
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
            }
        })

        $('.cancel-reservaion').click(function (e) {
            e.preventDefault();
        })

        $('.cancel-reservaion').click(function(e) {
            e.preventDefault();
            var conf = confirm('@lang('language.areYouSureYouWantToCancel')')

            if (conf) {
                $.ajax({
                    method: 'POST',
                    url: '/cancel-private-request',
                    data: {
                        request_id: $(this).attr('data-id')
                    },
                    success: function (data) {
                        if (data.code == '200') {
                            location.reload();
                        } else if (data.code == '422') {
                            for (var error in data.response) {
                                if (data.response.hasOwnProperty(error)) {
                                    alert(data.response[error]);
                                }
                            }
                        } else {
                            alert(data.response);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if (errorThrown.toString() == 'Unauthorized')
                        window.location.href = '/login';

                        console.error(textStatus);
                        alert(errorThrown.toString())
                    }
                })
            }
        })
    </script>
@endsection
