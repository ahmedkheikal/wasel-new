@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('/css/user-dashboard.css') !!}">
    <style media="screen">
    .add-eco-alert {
        display: none;
    }
    .cancel-trip {
        width: 100%;
    }
    @media (max-width: 480px) {
        .full-info-inner {
            top: 250px !important;
        }
    }
    @media (max-width: 1250px) {
        .full-info-inner {
            top: 250px !important;
        }
    }
    .eco-trip-date-params .form-control {
        width: auto;
        display: inline;
        max-width: 45%;
    }
    .add-new-trip {
        margin-top: 0 !important;
        width: 200px;
        float: left;
        text-align: center;
    }
    span.page-title {
        padding: 5px 20px;
        width: 50%;
        float: right;
        text-align: right;

    }
    .price {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 5px;
        padding-top: 4px;
        border-radius: 3px;
        width: 80%;
        margin: auto;
    }
    .chevron-down {
        position: relative;
        width: 20px;
        height: 20px;
    }
    #loader {
        position: absolute;
        top: 0;
        right: 0;
        width: 100%;

        : 100%;
        background: transparent;
        z-index: 20;
        display: block;
    }

    #loader img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    input[data-target="#addTripPop"] {
        width: 20%;
    }
    @media (max-width: 670px) {
        input[data-target="#addTripPop"] {
            width: 50%;
            /* margin: 30px 25%; */
        }
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div class="card pb-5" style="position: relative">
            <span style="font-size: 18px">
                <span class="page-title">
                    @lang('titles.myTrips')
                </span>
            </span>

            <ul class="nav nav-tabs mt-4" dir="rtl">
                <li><a class="upcomingTrips-btn" data-toggle="tab" href="#upcomingTrips">@lang('titles.upcomingTrips')</a></li>
                <li><a data-toggle="tab" href="#previousTrips">@lang('titles.previousTrips')</a></li>
            </ul>

            <div class="container p-sm-5">
                <div class="tab-content">
                    <div id="upcomingTrips" class="tab-pane fade in active">
                        <ul class="trip-list" dir="rtl">
                            @foreach ($upcomingTrips as $trip)
                                <li data-tripId="{{ $trip->id }}">
                                    <span class="float-left chevron-down">
                                        <i class="fa fa-chevron-down"></i>
                                    </span>
                                    <div class="row float-md-right">
                                        <div class="col-md-3">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                            </span> -
                                            <span class="to">
                                                @lang("db.{$trip->to_city}")
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                    $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                    $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>
                                        <div class="col-md-3 mt-2">
                                            <span class="trip-seats">
                                                <img src="{!! asset('/images/seat.png') !!}" alt="seat" class="seat-icon"> &nbsp;
                                                @if ($trip->seat_1 == '1')
                                                    @lang('language.frontSeat'),
                                                @endif
                                                @if ($trip->seat_2 == '1')
                                                    @lang('language.backSeatRight'),
                                                @endif
                                                @if ($trip->seat_3 == '1')
                                                    @lang('language.backSeatLeft')
                                                @endif
                                            </span>
                                        </div>
                                        <div class="col-md-3 mt-2">
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ \App\Services\TripService::getEcoPrice($trip->id) }} @lang('language.egp')
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 mt-2">
                                            @lang("db.{$trip->trip_status}")
                                        </div>
                                    </div>
                                    <div class="row full-info-inner pb-2">

                                        <div class="col-md-4">
                                            <h3 class="text-center">
                                                @lang('titles.reservedSeats')
                                            </h3>
                                            <div style="max-width: 120px; margin:auto;">
                                                @include('components.car', [
                                                    'seat_1' => $trip->seat_1,
                                                    'seat_2' => $trip->seat_2,
                                                    'seat_3' => $trip->seat_3,
                                                ])
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <h3>@lang('language.clients')</h3>
                                            <div class="row">
                                                @foreach ($trip->reservations as $reservation)
                                                    @if ($reservation->user_id != Auth::user()->id)
                                                        <div class="col-md-4 mt-3" style="text-align: center">
                                                            @if ($reservation->reservation_type == 'self')
                                                                @include('components.user', [
                                                                    'user' => $reservation->user,
                                                                    'phone' => true
                                                                ])
                                                            @else
                                                                @include('components.user', [
                                                                    'user' => $reservation->user,
                                                                    'phone' => true
                                                                ])
                                                                <p>@lang('titles.reservingFor')</p>
                                                                <h3>{{ $reservation->setfor_name }}</h3>

                                                                <small>
                                                                    @lang('language.phone'):
                                                                    <a href="tel:{{ $reservation->setfor_phone }}">
                                                                        {{ $reservation->setfor_phone }}
                                                                    </a>
                                                                </small>
                                                            @endif
                                                            <small style="color: #777; text-align: center">
                                                                @if ($reservation->seat_1 == '1')
                                                                    @lang('language.frontSeat'),
                                                                @endif
                                                                @if ($reservation->seat_2 == '1')
                                                                    @lang('language.backSeatRight'),
                                                                @endif
                                                                @if ($reservation->seat_3 == '1')
                                                                    @lang('language.backSeatLeft')
                                                                @endif
                                                            </small>

                                                            <div class="price">
                                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                                    {{ $reservation->price }} @lang('language.egp')
                                                                </span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            <div class="row mt-4">
                                                <div class="col-sm-6 offset-sm-3">
                                                    @if (count($trip->cancellationRequests))
                                                        <span>@lang('language.cancellationRequestSent')</span>
                                                    @else
                                                        @if ($trip->reservations->count())
                                                            <button type="button" class="btn btn-danger form-control cancel-trip" data-tripId="{{ $trip->id }}" data-toggle="modal" data-target="#confirmCancellationPop">@lang('language.requestCancellation')</button>
                                                        @else
                                                            <button type="button" class="btn btn-danger form-control cancel-trip" data-tripId="{{ $trip->id }}" data-toggle="modal" data-target="#confirmCancellationPop">@lang('language.cancel')</button>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div id="previousTrips" class="tab-pane fade">
                        <ul class="trip-list" dir="rtl">
                            @foreach ($prevTrips as $trip)
                                <li data-tripId="{{ $trip->id }}">
                                    <span class="float-left chevron-down">
                                        <i class="fa fa-chevron-down"></i>
                                    </span>
                                    <div class="row float-md-right">
                                        <div class="col-md-3">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                            </span> -
                                            <span class="to">
                                                @lang("db.{$trip->to_city}")
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                    $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                    $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>
                                        <div class="col-md-3 mt-2">
                                            <span class="trip-seats">
                                                <img src="{!! asset('/images/seat.png') !!}" alt="seat" class="seat-icon"> &nbsp;
                                                @if ($trip->seat_1 == '1')
                                                    @lang('language.frontSeat'),
                                                @endif
                                                @if ($trip->seat_2 == '1')
                                                    @lang('language.backSeatRight'),
                                                @endif
                                                @if ($trip->seat_3 == '1')
                                                    @lang('language.backSeatLeft')
                                                @endif
                                            </span>
                                        </div>
                                        {{-- <div class="col-md-3 mt-2">
                                            <span class="captain">
                                                <i class="fa fa-user"></i> &nbsp;
                                                @lang('language.captain'): {{ $trip->captain->username }}
                                            </span>
                                        </div> --}}
                                        <div class="col-md-3 mt-2">
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ \App\Services\TripService::getEcoPrice($trip->id) }} @lang('language.egp')
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row full-info-inner pb-2">

                                        <div class="col-md-4">
                                            <h3 class="text-center">
                                                @lang('titles.reservedSeats')
                                            </h3>
                                            <div style="max-width: 120px; margin:auto;">
                                                @include('components.car', [
                                                    'seat_1' => $trip->seat_1,
                                                    'seat_2' => $trip->seat_2,
                                                    'seat_3' => $trip->seat_3,
                                                ])
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-4">
                                            <h3 class="text-center">@lang('titles.price')</h3>
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ \App\Services\TripService::getEcoPrice($trip->id) }} @lang('language.egp')
                                                </span>
                                            </div>
                                        </div> --}}
                                        <div class="col-md-8">
                                            <h3>@lang('language.clients')</h3>
                                            <div class="row">
                                                    @foreach ($trip->reservations as $reservation)
                                                        @if ($reservation->user_id != Auth::user()->id)
                                                            <div class="col-md-4 mt-3" style="text-align: center">
                                                                @if ($reservation->reservation_type == 'self')
                                                                    @include('components.user', [
                                                                        'user' => $reservation->user,
                                                                    ])
                                                                @else
                                                                    @include('components.user', [
                                                                        'user' => $reservation->user,
                                                                    ])
                                                                    <p>@lang('titles.reservingFor')</p>
                                                                    <h3>{{ $reservation->setfor_name }}</h3>
                                                                @endif
                                                                <small style="color: #777; text-align: center">
                                                                    @if ($reservation->seat_1 == '1')
                                                                        @lang('language.frontSeat'),
                                                                    @endif
                                                                    @if ($reservation->seat_2 == '1')
                                                                        @lang('language.backSeatRight'),
                                                                    @endif
                                                                    @if ($reservation->seat_3 == '1')
                                                                        @lang('language.backSeatLeft')
                                                                    @endif
                                                                </small>

                                                                <div class="price">
                                                                    <span class="price" style="width: 100%; font-size: 20px; ">
                                                                        {{ $reservation->price }} @lang('language.egp')
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        {{ $prevTrips->links() }}
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    {{-- <!-- Modal -->
    <div class="modal fade" id="addTripPop" tabindex="-1" role="dialog" aria-labelledby="addTripPopLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                        <h5 class="modal-title" id="addTripPopLabel">@lang('language.addNewTrip')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-inline" action="/add-new-economy-trip" method="post" style="text-align: right">
                    <div class="modal-body" dir="rtl">
                        <h4>@lang('titles.chooseDestination')</h4>
                        <div class="row">
                            <div class="col-6">
                                <h5>@lang('titles.from')</h5>
                                <select class="govs form-control" name="from_city" style="width: 100%">
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->city_name }}">@lang("db.{$city->city_name}")</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6">
                                <h5>@lang('titles.to')</h5>
                                <select class="govs form-control" name="to_city" style="width: 100%">
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->city_name }}">@lang("db.{$city->city_name}")</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row eco-trip-date-params">
                            <div class="col-6">
                                <h5>@lang('titles.date')</h5>
                                <select class="date-params form-control" name='day' id='dayddl'>
                                    <option value='1'>1</option>
                                    <option value='2'>2</option>
                                    <option value='3'>3</option>
                                    <option value='4'>4</option>
                                    <option value='5'>5</option>
                                    <option value='6'>6</option>
                                    <option value='7'>7</option>
                                    <option value='8'>8</option>
                                    <option value='9'>9</option>
                                    <option value='10'>10</option>
                                    <option value='11'>11</option>
                                    <option value='12'>12</option>
                                    <option value='13'>13</option>
                                    <option value='14'>14</option>
                                    <option value='15'>15</option>
                                    <option value='16'>16</option>
                                    <option value='17'>17</option>
                                    <option value='18'>18</option>
                                    <option value='19'>19</option>
                                    <option value='20'>20</option>
                                    <option value='21'>21</option>
                                    <option value='22'>22</option>
                                    <option value='23'>23</option>
                                    <option value='24'>24</option>
                                    <option value='25'>25</option>
                                    <option value='26'>26</option>
                                    <option value='27'>27</option>
                                    <option value='28'>28</option>
                                    <option value='29'>29</option>
                                    <option value='30'>30</option>
                                    <option value='31'>31</option>
                                </select>
                                &nbsp;
                                <select class="date-params form-control" name='month' id='monthddl'>
                                    @foreach ($months as $key => $month)
                                        <option value="{{ $key + 1 }}">{{ $month }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6" style="padding-right: 0; padding-left: 0;">
                                <h5>@lang('titles.time')</h5>
                                <select class="date-params form-control" name="hour">
                                    <option value="1:00">1:00</option>
                                    <option value="1:30">1:30</option>
                                    <option value="2:00">2:00</option>
                                    <option value="2:30">2:30</option>
                                    <option value="3:00">3:00</option>
                                    <option value="3:30">3:30</option>
                                    <option value="4:00">4:00</option>
                                    <option value="4:30">4:30</option>
                                    <option value="5:00">5:00</option>
                                    <option value="5:30">5:30</option>
                                    <option value="6:00">6:00</option>
                                    <option value="6:30">6:30</option>
                                    <option value="7:00">7:00</option>
                                    <option value="7:30">7:30</option>
                                    <option value="8:00">8:00</option>
                                    <option value="8:30">8:30</option>
                                    <option value="9:00">9:00</option>
                                    <option value="9:30">9:30</option>
                                    <option value="10:00">10:00</option>
                                    <option value="10:30">10:30</option>
                                    <option value="11:00">11:00</option>
                                    <option value="11:30">11:30</option>
                                    <option value="12:00">12:00</option>
                                    <option value="12:30">12:30</option>
                                </select> <!-- :
                                    <select class="date-params" name="minute">
                                    <option value="00">00</option>
                                    <option value="30">30</option>
                                </select>&nbsp; -->
                                <select class="date-params form-control" name="ampm">
                                    <option value="AM">@lang('language.AM')</option>
                                    <option value="PM">@lang('language.PM')</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-inline"> <!-- date and time -->
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('language.close')</button>
                        <input type="submit" name="book_trip" value="إرسال طلب إضافة الرحلة" class="btn btn-primary pull-right">
                    </div>
                </form>
                <div class="add-eco-alert alert"></div>
            </div>
        </div>
    </div> --}}


    <!-- Modal -->
    <div class="modal fade" id="confirmCancellationPop" tabindex="-1" role="dialog" aria-labelledby="confirmCancellationPopLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmCancellationPopLabel">@lang('language.cancel')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @lang('language.areYouSureYouWantToCancel')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('language.close')</button>
                    <button type="button" class="btn btn-primary cancel-trip-inpop">@lang('language.cancel')</button>
                </div>
                <div class="cancel-eco-trip-alert alert"></div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $('.nav-tabs li a').addClass('p-2')
        $(document).ready(function () {
            $('.upcomingTrips-btn').click();
        })
        $('.trip-list li').click(function () {
            $('.trip-list li').scrollTop(0);
            if ($(this).hasClass('full-info') == false) {
                $('.trip-list li').removeClass('full-info');
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
                $('.full-info-inner').css({height: '0'});
                $(this).addClass('full-info');
                $('.trip-list li:hover span.chevron-down').html(`
                    <i class="fa fa-chevron-up"></i>
                `);
                $('.full-info-inner').css({height: 'auto'});
            } else {
                $('.trip-list li').removeClass('full-info');
                $('.full-info-inner').css({height: '0'});
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
            }
        })


        $('form[action="/add-new-economy-trip"] *').addClass('pb-3`');

        $('select[name=from_city]').change(function () {
            getAlltos();
        })
        $('form[action="/add-new-economy-trip"]').submit(function (e) {
            e.preventDefault();
            $('input[type=submit]').attr('disabled', 'disabled');
            $('.add-eco-alert').slideUp();
            $.ajax({
                method: 'POST',
                url: '/add-new-economy-trip',
                global: true,
        		processData: true,
        		async: true,
                data: {
                    from_city: $('select[name="from_city"]').val(),
                    to_city:  $('select[name="to_city"]').val(),
                    date: '{{ (new DateTime())->format('Y') }}-' + $('select[name="month"]').val() + '-' + $('select[name="day"]').val(),
                    time: $('select[name="hour"]').val() + ' ' + $('select[name="ampm"]').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('.add-eco-alert')
                            .removeClass('alert-*')
                            .addClass('alert-success')
                            .html('@lang('language.ecoTripAddedSuccessfully')')
                            .slideDown();
                        setTimeout(function () {
                            location.reload();
                        }, 3000)
                        $('input[type=submit]').attr('disabled', 'disabled');
                    } else if (data.code == '422') {
                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                $('.add-eco-alert')
                                    .removeClass('alert-*')
                                    .addClass('alert-danger')
                                    .html(data.response[field])
                                    .slideDown();
                            }
                        }
                        $('input[type=submit]').removeAttr('disabled');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (errorThrown.toString() == 'Unauthorized')
                    window.location.href = '/login';

                    console.error(textStatus);
                    alert(errorThrown.toString())
                }
            })
        })

        // getAlltos();
        function getAlltos() {
            $.ajax({
                url: '/api/getAlltos',
                method: 'POST',
                data: {
                    from_city: $('select[name=from_city]').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('select[name=to_city]').html('');
                        for (city of data.response) {
                            $('select[name=to_city]').append(`
                                <option value="${city.city_name}">${city.city_name_ar}</option>
                            `);
                        }
                        removeDuplicate('select[name=to_city] option');
                    } else if (data.code == '422') {
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                alert(data.response[error]);
                            }
                        }
                    }
                }
            })
        }

        function removeDuplicate(selector) {
            var liText = '', liList = $(selector), listForRemove = [];
            $(liList).each(function () {
                var text = $(this).text();
                if (liText.indexOf('|'+ text + '|') == -1) {
                    liText += '|'+ text + '|';
                }
                else {
                    listForRemove.push($(this));
                }
            });
            $(listForRemove).each(function () { $(this).remove(); });

        }

        $('.cancel-trip').click(function (e) {
            $('.cancel-trip-inpop').attr('data-tripId', $(this).attr('data-tripId'));
        })
        $('.cancel-trip-inpop').click(function (e) {
            cancelTrip($(this).attr('data-tripId'))
        })

        function cancelTrip(tripId) {
            $('.cancel-eco-trip-alert').slideUp();
            $.ajax({
                method: 'POST',
                url: '/cancel-economy-trip',
                data: {
                    trip_id: tripId
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('.cancel-eco-trip-alert')
                            .html(data.response)
                            .removeClass('alert-*')
                            .addClass('alert-success')
                            .slideDown();
                        location.reload();
                    } else if (data.code == '422') {
                        $('.cancel-eco-trip-alert')
                            .html('')
                            .removeClass('alert-*')
                            .addClass('alert-danger')
                            .slideDown();
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                $('.cancel-eco-trip-alert').append(data.response[error]);
                            }
                        }
                    }
                }
            })
        }
    </script>
@endsection
