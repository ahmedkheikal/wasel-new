@extends('layouts.page')

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px; min-height: 70vh">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="my-5 offset-md-3 col-md-6 alert alert-success text-center">
                        @lang('language.reservationSuccessful')
                    </div>
                    <div class="offset-md-4 col-md-4 mb-4">
                        <a href="/private-trips">
                            <button class="btn btn-primary form-control">
                                @lang('titles.myPrivateTrips')
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
@endsection
