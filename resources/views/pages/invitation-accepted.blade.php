@extends('layouts.page')

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row" dir="rtl">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <div class="card-body text-center">
                        @lang('language.affiliate.YouNowhave50EGPfromReferer')
                        <a href="/">
                            <button type="button" class="btn btn-primary">
                                @lang('language.returnToHome')
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
@endsection
