@extends('layouts.page')

@section('page-styles')
    <link rel="stylesheet" href="{!! asset('/css/user-dashboard.css') !!}">
    <style media="screen">
    .price {
        border: none;
        cursor: default;
        background: #5cb85c;
        color: white;
        display: block;
        text-align: center;
        padding: 5px;
        padding-top: 4px;
        border-radius: 3px;
        width: 50%;
        margin: auto;
    }
    .chevron-down {
        position: relative;
        width: 20px;
        height: 20px;
    }
    .nav-tabs li {
        width: 50%;
        display: inline-block;
    }
    @media (max-width: 1250px) {
        .full-info-inner {
            top: auto !important;
        }
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="card pb-5">
            <ul class="nav nav-tabs" dir="rtl">
                <li><a class="upcomingTrips-btn" data-toggle="tab" href="#upcomingTrips">@lang('titles.upcomingPrivateTrips')</a></li>
                {{-- <li><a data-toggle="tab" href="#upcomingPrivateTrips">@lang('titles.upcomingPrivateTrips')</a></li> --}}
                <li><a data-toggle="tab" href="#previousTrips">@lang('titles.previousTrips')</a></li>
            </ul>

            <div class="container p-sm-5">
                <div class="tab-content">
                    <div id="upcomingTrips" class="tab-pane fade in active">
                        <ul class="trip-list" dir="rtl">
                            @forelse ($upcomingTrips as $trip)
                                <li>
                                    <div class="row">
                                        <div class="col-md-4 col-6">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                            </span> -
                                            <span class="to">
                                                @lang("db.{$trip->to_city}")
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>
                                        <div class="col-sm-4 col-6">
                                            @lang("db.{$trip->request_status}")
                                        </div>
                                        <div class="col-md-4 col-6 mt-2">
                                            <span class="captain">
                                                {{-- <i class="fa fa-user"></i> &nbsp; --}}
                                                @lang("language.{$trip->trip_type}")
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row full-info-inner pb-2 mt-4">
                                        <div class="col-md-4 text-right">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->from_city_address }}, {{ $trip->pickup_address }}
                                                </span>
                                            </span> <br>
                                            <span class="to">
                                                <i class="fa fa-bullseye"></i>
                                                @lang("db.{$trip->to_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->to_city_address }}, {{ $trip->arrival_address }}
                                                </span>
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                    $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                    $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>

                                        @if ($trip->request_status == 'scheduled')
                                            @isset($trip->reservation)
                                                <div class="col-md-4 text-center" >
                                                    @include('components.captain', ['captain' => $trip->reservation->trip->captain, 'phoneVisible' => true])
                                                </div>
                                            @endisset
                                        @endif

                                        <div class="col-md-4">
                                            <h3 class="text-center">@lang('titles.price')</h3>
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ $trip->price }} @lang('language.egp')
                                                </span>
                                            </div>
                                            @if ($trip->request_type == 'other')
                                                <h3 class="text-center mt-3">@lang('titles.reservedFor'): </h3>
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td>@lang('language.name')</td>
                                                            <td>{{ $trip->fullname }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>@lang('language.phone')</td>
                                                            <td>{{ $trip->phone }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            @endif
                                        </div>

                                        @if ($trip->request_status == 'pending')
                                            <div class="col-md-4">
                                                <p style="font-size: 17px; color: #888">
                                                    @lang('language.tripRequestPending')
                                                </p>
                                            </div>
                                        @endif

                                        {{-- @if ($trip->request_status !== 'canceled')
                                            <div class="offset-md-4 col-md-4 mt-2">
                                                <button data-id="{{ $trip->id }}" class="btn btn-danger cancel-reservaion" style="width: 100%;">
                                                    @lang('language.cancelOrder')
                                                </button>
                                            </div>
                                        @endif --}}
                                    </div>
                                </li>
                            @empty
                                <h4 class="text-center">@lang('language.noPendingTrips')</h4>
                            @endforelse
                        </ul>
                    </div>

                    <div id="previousTrips" class="tab-pane fade">
                        <ul class="trip-list" dir="rtl">
                            @forelse ($previousTrips as $trip)
                                <li>
                                    <div class="row">
                                        <div class="col-sm-4 col-8">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                            </span> -
                                            <span class="to">
                                                @lang("db.{$trip->to_city}")
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>
                                        @if ($trip->request_status == 'canceled')
                                            <div class="col-sm-4 col-4">
                                                @lang("db.{$trip->request_status}")
                                            </div>
                                        @endif
                                        <div class="col-sm-4 col-6 mt-2">
                                            <span class="captain">
                                                {{-- <i class="fa fa-user"></i> &nbsp; --}}
                                                @lang("language.{$trip->trip_type}")
                                            </span>
                                        </div>

                                    </div>

                                    <div class="row full-info-inner pb-2 mt-4">
                                        <div class="col-md-4 text-right">
                                            <span class="from">
                                                <i class="fa fa-map-marker"></i>
                                                @lang("db.{$trip->from_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->from_city_address }}, {{ $trip->pickup_address }}
                                                </span>
                                            </span> <br>
                                            <span class="to">
                                                <i class="fa fa-bullseye"></i>
                                                @lang("db.{$trip->to_city}")
                                                <span style="font-weight: lighter !important">
                                                    - {{ $trip->to_city_address }}, {{ $trip->arrival_address }}
                                                </span>
                                            </span>
                                            <br>
                                            <span class="date">
                                                <i class="fa fa-calendar" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('d ') }}
                                                @php
                                                    $month = (new DateTime($trip->datetime))->format('M')
                                                @endphp
                                                @lang("language.{$month}")
                                                {{ (new DateTime($trip->datetime))->format(' Y') }}
                                            </span>
                                            <br>
                                            <span class="time">
                                                <i class="fa fa-clock-o" style="font-size: 12px"></i>
                                                {{ (new DateTime($trip->datetime))->format('h:i') }}
                                                @php
                                                    $ampm = (new DateTime($trip->datetime))->format('A')
                                                @endphp
                                                @lang("language.{$ampm}")
                                            </span>
                                        </div>

                                        @if ($trip->request_status == 'scheduled')
                                            @isset($trip->reservation)
                                                <div class="col-md-4 text-center" >
                                                    @include('components.captain', ['captain' => $trip->reservation->trip->captain, 'phoneVisible' => true])
                                                </div>
                                            @endisset
                                        @endif

                                        <div class="col-md-4">
                                            @if ($trip->request_type == 'other')
                                                <h3 class="text-center">@lang('titles.reservedFor'): </h3>
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td>@lang('language.name')</td>
                                                            <td>{{ $trip->fullname }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>@lang('language.phone')</td>
                                                            <td>{{ $trip->phone }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            @endif
                                            <h3 class="text-center">@lang('titles.price')</h3>
                                            <div class="price">
                                                <span class="price" style="width: 100%; font-size: 20px; ">
                                                    {{ $trip->price }} @lang('language.egp')
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @empty
                                <h4 class="text-center">@lang('language.noPreviousTrips')</h4>
                            @endforelse
                        </ul>
                        {{ $previousTrips->links() }}
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('.nav-tabs li a').addClass('p-2')
        $(document).ready(function () {
            $('.upcomingTrips-btn').click();
        })
        $('.trip-list li').click(function () {
            $('.trip-list li').scrollTop(0);
            if ($(this).hasClass('full-info') == false) {
                $('.trip-list li').removeClass('full-info');
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
                $('.full-info-inner').css({height: '0'});
                $(this).addClass('full-info');
                $('.trip-list li:hover span.chevron-down').html(`
                    <i class="fa fa-chevron-up"></i>
                `);
                $('.full-info-inner').css({height: 'auto'});
            } else {
                $('.trip-list li').removeClass('full-info');
                $('.full-info-inner').css({height: '0'});
                $('.trip-list li span.chevron-down').html(`
                    <i class="fa fa-chevron-down"></i>
                `);
            }
        })

    </script>
@endsection
