@extends('layouts.page')

@section('styles')
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.date.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/pickadate-theme.time.css') !!}">
    <style media="screen">
        .modal-footer>:last-child {
            margin-right: .25rem;
        }
        .modal-footer>:not(:last-child) {
            margin-left: .25rem;
        }
        .only-private {
            color: #444;
            text-align: center;
        }
        ::placeholder {
            color: #888 !important
        }
        sup {
            font-size: 20px;
            top: auto;
        }
        @media (min-width: 845px) {
            .economy-container {
                margin-right: 25%;
            }
        }
        @media (max-width: 361px) {
            .use10egpfromwallet-label {
                font-size: 12px;
            }
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: white !important;
        }
        /* #f8fafc */
        .from-region, .to-region {
            width: 100% !important;
        }
        .bg-light {
            background: white !important;
        }
        .fromto {
            box-shadow: inset 0 3px 1px rgba(0,0,0,0.13);
            padding: 10px;
            border-radius: 5px
        }

        .no-eco-trips {
            color: rgba(227, 52, 47, 0.5);
            font-weight: lighter;
        }
        .request-cost, .request-cost:focus {
            border: none;
            cursor: default;
            background: #5cb85c;
            color: white;
            display: block;
            text-align: center;
            padding: 10px;
            border-radius: 3px;
        }

        .request-cost:hover {
            background: #5cb85c;
            border: none
        }

        .economy-cost, .economy-cost:focus {
            border: none;
            cursor: default;
            background: #5cb85c;
            color: white;
            display: block;
            text-align: center;
            padding: 10px;
            border-radius: 3px;
        }

        .economy-cost:hover {
            background: #5cb85c;
            border: none
        }

        .private-check-promo {
            position: absolute;
            color: grey;
            top: 0px;
            left: 0;
            cursor: pointer;
            color: white;
        }
        .check-promo {
            position: absolute !important;
            color: white;
            top: 0;
            left: 0;
            cursor: pointer;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/car.css') }}">
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row" dir="rtl">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <p class="only-private d-none">@lang('language.orderPrivateTrip')</p>
                        <div class="row text-center">
                            <div class="row">
                                <div class="col-12">
                                    <p style="color: #555">خدمة واصل للتنقل بين المراكز</p>
                                    <p style="color: #888">الخدمة متاحة حاليا في محافظة المنيا فقط</p>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <form class="form-inline" action="/add-private-trip" method="post">
                                        {{-- start fromto regions --}}
                                        <div class="col-md-6 from_city_region from_address region-address form-inline">
                                            <p style="text-align: right; width: 100%">
                                                @lang('titles.departure')
                                                <sup style="color: red">*</sup>
                                            </p>
                                            <select class="form-control from-region" name="from_region">
                                                {{-- <option value="">lorem lorem</option> --}}
                                            </select>
                                            <input type="text" name="from_address" class="form-control" value="" placeholder="@lang('titles.writeFromAddress')">
                                        </div>
                                        <div class="col-md-6 to_region to_address region-address form-inline">
                                            <p style="text-align: right; width: 100%">
                                                @lang('titles.arrival')
                                                <sup style="color: red">*</sup>
                                            </p>
                                            <select class="form-control to-region" name="to_region">
                                                {{-- <option value="">lorem lorem</option> --}}
                                            </select>
                                            <input type="text" name="to_address" class="to_address form-control" value="" placeholder="@lang('titles.writeToAddress')">
                                        </div>
                                        {{-- end fromto regions --}}

                                        {{-- start datetime --}}
                                        <div class="col-6 mt-3 private-date  date">
                                            <p style="text-align: right">
                                                @lang('titles.date')
                                                <sup style="color: red">*</sup>
                                            </p>
                                            <?php 
                                                $currentDate = date("Y-m-d");
                                                $week = date('Y-m-d', strtotime(' + 6 days'));
                                                $day = date('D', strtotime($currentDate));
                                            ?>
                                            <select class="form-control" name="private_date">
                                                @for($i = 1; $i <= 7; $i++)
                                                    @if($i == 1)
                                                        <option value="{{ date('Y-m-d') }}">اليوم</option>
                                                    @elseif($i == 2)
                                                        <option value="{{ date('Y-m-d', strtotime(' + 1 days')) }}">غدا</option>
                                                    @else
                                                        <option value="{{ $current = date('Y-m-d', strtotime(' + ' . ($i - 1) . ' days')) }}">
                                                            <?php $date = date('D', strtotime($current)) . ' ' . date('m-d', strtotime($current)); ?>
                                                            {{ App\MyHelper::incitySelectDate($date) }}
                                                        </option>
                                                    @endif
                                                @endfor
                                            </select>
                                            {{-- <div class="row" style="position: relative">
                                                <input name="private_date" class="form-control" type="date" min="2013-12-25" placeholder="@lang('titles.date')">
                                            </div> --}}
                                        </div>
                                        <div class="col-6 mt-3 private-date time">
                                            <p style="text-align: right">
                                                @lang('titles.time')
                                                <sup style="color: red">*</sup>
                                            </p>
                                            <select name="private_time" class="form-control">
                                                <?php
                                                    $hour = date('h');
                                                    $type = date('A');
                                                    
                                                    if ($type == 'PM') {
                                                        $start = $hour + 12 + 2;
                                                    } else {
                                                        $start = 10;
                                                    }
                                                ?>
                                                @for($i = $start; $i <= 22; $i++)
                                                    @if($i >= 12)
                                                        @if($i > 12)
                                                            <option value="{{ date('h:i A', strtotime(($i - 12) . ' PM')) }}">{{ date('h:i A', strtotime(($i - 12) . ' PM')) }}</option>
                                                        @else
                                                            <option value="{{ date('h:i A', strtotime(($i) . ' PM')) }}">{{ date('h:i A', strtotime(($i) . ' PM')) }}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{ date('h:i A', strtotime(($i) . ' AM')) }}">{{ date('h:i A', strtotime(($i) . ' AM')) }}</option>
                                                    @endif
                                                @endfor
                                            </select>
                                            {{-- <div class="row">
                                                <input type="time" name="private_time" class="form-control" value="" placeholder="@lang('titles.time')">
                                            </div> --}}
                                        </div>
                                        {{-- end datetime --}}

                                        {{-- start reserve for others --}}
                                        <div class="row mt-4" style="width: 100%; margin: 0">
                                            @auth
                                                @if (Auth::user()->account_type == 'partner')
                                                    <input type="hidden" name="reservation_type" value="other">
                                                    <div class="other-person-info">
                                                        <div class="row" style="margin: 15px 0;">
                                                            <div class="col-md-6 setfor_name">
                                                                <input type="text" class="form-control private-setfor_name" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                                                            </div>
                                                            <div class="col-md-6 setfor_phone">
                                                                <input type="tel" class="form-control private-setfor_phone" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif (Auth::user()->account_type == 'passenger')
                                                    <input type="hidden" name="reservation_type" value="self">

                                                    <div class="row mt-2 mb-2" style="width: 100%; margin: 0">
                                                        <div class="col-2" dir="rtl">
                                                            <label class="switch">
                                                                <input class="setfor-private-toggler" type="checkbox">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-10 text-right">
                                                            <span>
                                                                @lang('language.someOneElseWillUseThisReservation')
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="other-person-info" style="display: none">
                                                        <div class="row" style="margin: 0 ;" dir="rtl">
                                                            <div class="col-md-6 setfor_name" dir="rtl">
                                                                <input type="text" class="form-control private-setfor_name" name="setfor_name" placeholder="@lang('language.clientName')" value="">
                                                            </div>
                                                            <div class="col-md-6 setfor_phone">
                                                                <input type="tel" class="form-control private-setfor_phone" name="setfor_phone" placeholder="@lang('language.clientPhone')" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endauth
                                        </div>
                                        {{-- end reserve for others --}}
                                        {{-- <div class="col-sm-12 mb-4 passenger_notes">
                                            <p style="text-align: right">@lang('titles.notes')</p>
                                            <textarea style="width: 100%" name="passenger_notes" rows="2" class="form-control" placeholder="@lang('language.notes')"></textarea>
                                        </div> --}}

                                        <div class="col-12">
                                            <hr>
                                        </div>
                                        <div class="col-12" style="text-align: right">
                                            <div class="row pull-right">
                                                <p class="col-md-6">
                                                    <input type="radio" id="going" name="goingandcoming" value="going" checked>
                                                    <label for="going">
                                                        @lang('language.going')
                                                    </label>
                                                </p>
                                                <p class="col-md-6">
                                                    <input type="radio" id="going_and_comingback" name="goingandcoming" value="going_and_comingback">
                                                    <label for="going_and_comingback">
                                                        @lang('language.goingAndComingback')
                                                        <small class="waiting-time" style="font-size: 10px">(انتظار حتى ساعة واجدة)</small> <br>
                                                    </label>
                                                </p>
                                                {{-- <p>
                                                    <input type="radio" id="going_and_comingback_otherday" name="goingandcoming" value="going_and_comingback_otherday">
                                                    <label for="going_and_comingback_otherday">
                                                        @lang('language.goingAndComingbackOtherDay')
                                                        <br>
                                                    </label>
                                                </p> --}}

                                                {{-- <div class="row return-trip mb-4" style="display: none">
                                                    <!-- start return datetime -->
                                                    <div class="col-6 mt-3 private-date date return_date">
                                                        <p style="text-align: right">
                                                            @lang('titles.returnDate')
                                                            <sup style="color: red">*</sup>
                                                        </p>
                                                        <div class="row  date" style="position: relative">
                                                            <input name="return_private_date" class="form-control" type="date" min="2013-12-25" placeholder="@lang('titles.returnDate')">
                                                        </div>
                                                    </div>
                                                    <div class="col-6 mt-3 private-date return_time">
                                                        <p style="text-align: right">
                                                            @lang('titles.returnTime')
                                                            <sup style="color: red">*</sup>
                                                        </p>
                                                        <div class="row time">
                                                            <input type="time" name="return_private_time" class="form-control" value="" placeholder="@lang('titles.returnTime')">
                                                        </div>
                                                    </div>
                                                    <!-- end return datetime-->

                                                </div> --}}

                                            </div>
                                        </div>

                                        <div class="row" style="width: 100%; margin: 0;">
                                            {{-- old location reserve for other  --}}
                                            <div class="col-md-3"></div>

                                            <div class="col-md-4 col-8">
                                                @auth
                                                    @if (Auth::user()->account_type !== 'partner')
                                                        <div style="position: relative; {{ Auth::user()->affiliate_balance + Auth::user()->own_balance >= 10 ? '' : 'margin-top: 20px;'}}">
                                                            <input class="form-control" style="width: 100%" type="text" name="promo_code" placeholder="@lang('language.promoCode')" value="">
                                                            <button type="button" class="btn btn-primary private-check-promo">@lang('language.check')</button>
                                                        </div>
                                                        @if (Auth::user()->affiliate_balance + Auth::user()->own_balance >= 10)
                                                            <div style="position: relative; margin: 10px 0; text-align: right">
                                                                <label class="switch">
                                                                    <input class="eco-use10egpfromwallet" name="use_10EGPfROMWallet" type="checkbox">
                                                                    <span class="slider round"></span>
                                                                </label>
                                                                <div style="margin-top: -30px; text-align: left" class="use10egpfromwallet-label">
                                                                    @lang('language.use10egpfromwallet')
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <input type="hidden" name="promo_code" value="">
                                                    @endif
                                                @else
                                                    <div style="position: relative; margin-top: 20px;">
                                                        <input class="form-control" style="width: 100%" type="text" name="promo_code" placeholder="@lang('language.promoCode')" value="">
                                                        <button type="button" class="btn btn-primary private-check-promo">@lang('language.check')</button>
                                                    </div>
                                                @endauth
                                            </div>
                                            <div class="col-md-3 col-4 pull-left request-cost" style="font-size: 20px; margin: 10px 0 30px 0;">
                                                0 @lang('language.egp')
                                            </div>
                                        </div>
                                        <div class="row" style="width: 100%; margin: 0">
                                            <div class="col-sm-12">
                                                <p style="font-size: 14px; width: 100%;">
                                                    يتم اضافة 30 جنيه لكل ساعة انتظار او تأخير <br>
                                                    وتكلفة اى مشوار اضافى داخل المدينة او الى القرى بالتنسيق مع الكابتن قبل بدء الرحلة
                                                </p>
                                            </div>
                                            <div class="col-sm-4"></div>
                                            @auth
                                                @if (Auth::user()->account_type !== 'driver')
                                                    <div class="col-sm-4 mt-3 private-submit-container">
                                                        <input style="width: 100%" class="btn btn-primary float-right" type="submit" name="request_private" value="@lang('language.requestPrivate')">
                                                    </div>
                                                @endif
                                            @else
                                                <div class="col-sm-4 mt-3 private-submit-container">
                                                    <input style="width: 100%" class="btn btn-primary float-right" type="submit" name="request_private" value="@lang('language.requestPrivate')">
                                                </div>
                                            @endauth
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('components.footer')

@endsection

@section('page-scripts')

    <script type="text/javascript" src="{!! asset('js/picker.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.date.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/picker.time.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/ar.js') !!}"></script>


    <!-- Modal -->
    <div class="modal fade" dir="rtl" id="confirmPrivate" tabindex="-1" role="dialog" aria-labelledby="confirmPrivateLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmPrivateLabel">@lang('language.confirm')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="    margin: -1rem  auto -1rem -1rem; ">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @lang('language.areYouSureYouWantToRequestPrivate')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('language.cancelOrder')</button>
                    <button type="button" class="btn btn-primary confirm-private">@lang('language.confirm')</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript"> // select change scripts
        // start generic fromto

        $.ajaxSetup({
            // async: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function () {
            getPrivateRegions();
        })

        $('select[name=private_date]').on('change', function() {
            var optionValue = $(this).val();
            
            var current = "{{ date("Y-m-d") }}";
            var currentHour = "{{ date('h') }}";
            var currentType = "{{ date('A') }}";
            
            start = 10;
            if (currentType == 'PM') {
                start = parseInt(currentHour) + 12 + 2;
            }

            $('select[name=private_time]').empty();
            var time = '';

            if (optionValue == current) {
                for (var i = start; i <= 22; i++){
                    if (i >= 12) {
                        if (i > 12) {
                            time += "<option value='" + (i - 12) + ":00 PM'>"+ (i - 12) +":00 PM</option>";
                        } else {
                            time += "<option value='" + i + ":00 PM'>"+ i +":00 PM</option>";
                        }
                    } else {
                        time += "<option value='" + i + ":00 AM'>"+ i +":00 AM</option>";
                    }
                }
            } else {
                for (var i = 10; i <= 22; i++){
                    if (i >= 12) {
                        if (i > 12) {
                            time += "<option value='" + (i - 12) + ":00 PM'>"+ (i - 12) +":00 PM</option>";
                        } else {
                            time += "<option value='" + i + ":00 PM'>"+ i +":00 PM</option>";
                        }
                    } else {
                        time += "<option value='" + i + ":00 AM'>"+ i +":00 AM</option>";
                    }
                }
            }

            $('select[name=private_time]').append(time);
        });

        var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
        var oneWeekAfter = new Date((new Date()).valueOf()+(1000*60*60*24)*7);
        var oneWeekAfter2 = new Date((new Date()).valueOf()+(1000*60*60*24)*100);

        console.log(oneWeekAfter);
        var privateDatePicker = $('input[name=private_date]').pickadate({
            disable: [
                { from: [0,0,0], to: yesterday },
                { from: oneWeekAfter, to: oneWeekAfter2 },
            ]
        });
        var privateDatePicker2 = $('input[name=return_private_date]').pickadate({
            disable: [
                { from: [0,0,0], to: yesterday },
                { from: oneWeekAfter, to: oneWeekAfter2 },
            ]
        });

        var today = new Date();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var minutes = (today.getMinutes());

        if (today.getMinutes() > 29) {
            minutes = 60
        } else {
            minutes = 30;
        }
        var privateTimePicker = $('input[name=private_time]').pickatime({
            interval: 60
        });
        var privateTimePicker2 = $('input[name=return_private_time]').pickatime({
            interval: 60
        });


        $('.from-region').change(function () {
            getInCityToRegions();
            updateInCityPrice();
        });
        $('.to-region').change(function () {
            updateInCityPrice();
        });
        // end  generic fromto

        function getInCityToRegions() {
            // to
            $.ajax({
                method: 'POST',
                url: '/api/incity/getAllTos',
                data: {
                    from_region: $('.from-region').val()
                },
                success: function (data) {
                    $('.to-region').html('');
                    console.log('incity/getAllTos', data);
                    for (city of data.response) {
                        let selected = city.main_region == '1' ? 'selected' : '';
                        if (city.region !== "مدينة المنيا" || city.region !== "المنيا الجديده")
                        $('.to-region').append(`
                            <option ${selected} value="${city.id}">${city.region}</option>
                        `);
                    }
                    removeDuplicate('.to-region option');
                    updateInCityPrice();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus, errorThrown);
                }
            })
        }

        // start economy script
        var availableTripsByDate;
        // end economy script

        // start private scripts
        $('input[name=use_10EGPfROMWallet]').change(function () {
            applyInCityPromo();
            console.log($('input[name=use_10EGPfROMWallet]:checked').length);
        })

        $('input[name=eco_use_10EGPfROMWallet]').change(function () {
            applyPromo();
        })

        $('input[name=goingandcoming]').change(function () {
            updateInCityPrice();
            console.log($('input[name=goingandcoming]:checked').val());
            if ($('input[name=goingandcoming]:checked').val() == 'going_and_comingback_otherday')
            $('.return-trip').slideDown('fast');
            else
            $('.return-trip').slideUp('fast');
        })

        function getPrivateDate(dateElement) {
            var privateDateArray = dateElement.val().split(' ');
            var months = [ 'يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر' ];
            privateDateArray[1] = months.indexOf(privateDateArray[1]) + 1;

            if (privateDateArray[1] < 10)
            privateDateArray[1] = '0' + privateDateArray[1];
            finalPrivateDate = '';
            for (var i = 2; i >= 0; i--) {
                if (i == 0) {
                    finalPrivateDate += privateDateArray[i];
                    continue ;
                }
                finalPrivateDate += privateDateArray[i] + '-';
            }
            if (finalPrivateDate == 'undefined-00-')
                return null;
            return finalPrivateDate;
        }

        $('form[action="/add-private-trip"]').submit(function (e) {
            e.preventDefault();
            $('#confirmPrivate').modal();
        })

        $('.confirm-private').click(function (e) {
            confirmPrivate();
        })

        function confirmPrivate() {
            $('.private-submit-container input').attr("disabled","disable");
            $('.private-submit-container input').val('@lang('language.pleaseWait')');
            $('.error').remove();

            $('#loader').fadeIn();

            // var finalPrivateDate = getPrivateDate($('input[name=private_date]'));
            // var privateReturnDate = getPrivateDate($('input[name=return_private_date]'));
            var finalPrivateDate = $('select[name=private_date').val();
            var privateTime = $('select[name=private_time').val();

            // alert($('input[name=goingandcoming]:checked').val())

            $.ajax({
                url: '/incity/add',
                method: 'POST',
                data: {
                    from_region: $('.from-region').val(),
                    to_region: $('.to-region').val(),
                    from_address: $('input[name=from_address]').val(),
                    to_address: $('input[name=to_address]').val(),
                    request_type: $('input[name=reservation_type]').val(),
                    setfor_name: $('.private-setfor_name').val(),
                    setfor_phone: $('.private-setfor_phone').val(),
                    date: finalPrivateDate,
                    //return_date: privateReturnDate,
                    time: privateTime,
                    //return_time: privateTimePicker2.val(),
                    trip_type: $('input[name=goingandcoming]:checked').val(),
                    price: totalPrice,
                    use_10EGP_credit: $('input[name=use_10EGPfROMWallet]:checked').length,
                    promo_code: $('input[name=promo_code]').val(),
                    passenger_notes: $('textarea[name=passenger_notes]').val()
                },
                success: function (data) {
                    console.log(data);
                    $('.private-submit-container input').removeAttr('disabled');
                    $('.private-submit-container input').val('@lang('language.requestPrivate')');
                    if (data.code == '422') {
                        $('.error').remove();
                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                if (field == 'price')
                                alert('@lang('language.cantMakePrivateTripWithoutPrice')');
                                $('.' + field).append(`
                                <small class="error">
                                    <br>
                                    ${data.response[field]}
                                </small>
                                `);
                                // $(window).scrollTop($('.' + field).offset().top - 60);

                            }
                        }
                        $('#confirmPrivate').modal('hide');
                    }

                    if (data.code == '401')
                    window.location.href = '/login';

                    if (data.code == '403')
                    window.location.href = '/phone/verify';

                    if (data.code == '200')
                    window.location.href = '/incity/success';

                    $('#loader').fadeOut();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus);
                    alert(errorThrown.toString());
                    $('#loader').fadeOut();
                    $('input[name="book_trip"]').removeAttr('disabled');
                    $('input[name="book_trip"]').val('@lang('language.book')');
                }
            })
        }


        function getPrivateRegions() {
            // from
            $.ajax({
                method: 'POST',
                url: '/api/private/regions',
                data: {
                    city_name: 'Minya'
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('.from-region').html('');
                        for (city of data.response) {
                            let selected = city.main_region == '1' ? 'selected' : '';
                            if (city.region == "مدينة المنيا" || city.region == "المنيا الجديده")
                            $('.from-region').append(`
                            <option ${selected} value="${city.id}">${city.region}</option>
                            `);
                        }
                    } else if (data.code == '422') {
                        $('.error').remove();
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                alert(data.response[error]);
                            }
                        }
                    }

                    // to
                    $.ajax({
                        method: 'POST',
                        url: '/api/incity/getAllTos',
                        data: {
                            from_region: $('.from-region').val()
                        },
                        success: function (data) {
                            $('.to-region').html('');
                            console.log('incity/getAllTos', data);
                            for (city of data.response) {
                                let selected = city.main_region == '1' ? 'selected' : '';
                                if (city.region !== "مدينة المنيا" || city.region !== "المنيا الجديده")
                                $('.to-region').append(`
                                    <option ${selected} value="${city.id}">${city.region}</option>
                                `);
                            }
                            removeDuplicate('.to-region option');
                            updateInCityPrice();
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.error(textStatus, errorThrown);
                        }
                    })
                }
            })
        }

        var totalPrice;

        function updateInCityPrice() {
            $.ajax({
                method: 'POST',
                url: '/api/incity/price',
                data: {
                    from_region: $('.from-region').val(),
                    to_region: $('.to-region').val(),
                    going_and_comingback: $('input[name=goingandcoming]:checked').val()
                },
                success: function (data) {
                    console.log('incity/price', data);
                    if (data.code == '200') {
                        totalPrice = data.response.price;
                        $('.request-cost').html(`${data.response.price} @lang('language.egp')`);
                        // if (data.response.price !== 0) {
                        // } else {
                        //     updateInCityPrice()
                        // }
                    } else {
                        $('.request-cost').html(`<small>@lang('language.contactAdminAboutThePrice') </small>`);
                    }
                }
            })
        }
        // end   private scripts



        $('.private-check-promo').click(function () {
            applyInCityPromo();
        })

        function applyInCityPromo() {
            updateInCityPrice();
            setTimeout(function () {
                $.ajax({
                    method: 'POST',
                    url: '/api/incity/promo',
                    data: {
                        from_city: $('select[name=from_region]').val(),
                        to_city: $('select[name=to_region]').val(),
                        promo_code: $('input[name=promo_code]').val()
                    },
                    success: function (data) {
                        console.log('incity/promo', data);
                        if (data.code == '200') {
                            if (data.response.type == 'percent')
                            totalPrice = Math.floor(
                                totalPrice - (totalPrice * (data.response.amount / 100) )
                            );
                            else if (data.response.type == 'flat_rate')
                            totalPrice = totalPrice - data.response.amount;

                            $('.request-cost').html(`${totalPrice} @lang('language.egp')`);

                            $('input[name=promo_code]').attr('disabled', true);
                            $('.private-check-promo').attr('disabled', true);

                            $('input[name=promo_code]').addClass('is-valid').removeClass('is-invalid');
                            $('.private-check-promo').addClass('btn-success').removeClass('btn-primary').removeClass('btn-danger');
                        } else {
                            $('input[name=promo_code]').removeAttr('disabled');
                            $('.private-check-promo').removeAttr('disabled');

                            $('input[name=promo_code]').addClass('is-invalid').removeClass('is-valid');
                            $('.private-check-promo').addClass('btn-danger').removeClass('btn-primary').removeClass('btn-success');
                        }
                        if ($('input[name=use_10EGPfROMWallet]:checked').length) {
                            totalPrice -= 10;
                            $('.request-cost').html(`${totalPrice} @lang('language.egp')`);
                        }

                    }
                });

            }, 500);
        }

        function formatDate(date) {
            var monthNames = [
                "@lang('language.Jan')", "@lang('language.Feb')", "@lang('language.Mar')",
                "@lang('language.Apr')", "@lang('language.May')", "@lang('language.Jun')", "@lang('language.Jul')",
                "@lang('language.Aug')", "@lang('language.Sep')", "@lang('language.Oct')",
                "@lang('language.Nov')", "@lang('language.Dec')"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' '
            /*+ year*/;
        }

        function removeDuplicate(selector) {
            var liText = '', liList = $(selector), listForRemove = [];
            $(liList).each(function () {
                var text = $(this).text();
                if (liText.indexOf('|'+ text + '|') == -1) {
                    liText += '|'+ text + '|';
                }
                else {
                    listForRemove.push($(this));
                }
            });
            $(listForRemove).each(function () { $(this).remove(); });

        }

        function getAlltos() {
            $.ajax({
                url: '/getAlltos',
                method: 'POST',
                data: {
                    from_city: $('select[name=from_city]').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        $('select[name=to_city]').html('');
                        for (city of data.response) {
                            $('select[name=to_city]').append(`
                                <option value="${city.city_name}">
                                    ${city.city_name_ar}
                                </option>
                            `);
                        }
                        removeDuplicate('select[name=to_city] option');
                        $('select[name=to_city]').change();
                    } else if (data.code == '422') {
                        $('.error').remove();
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                alert(data.response[error]);
                            }
                        }
                    }
                }
            })
        }



        var today = new Date().toISOString().split('T')[0];
        document.getElementsByName("private_date")[0].setAttribute('min', today);
        $('input[name=privaeDateViewer]').click(function () {
            $('input[name=private_date]').click();
        })
        $('.reserve-for-others button').click(function () {
            $('.reserve-for-others button').removeClass('active');
            $(this).addClass('active');
            if ($(this).attr('reserve-for-other') == 'true') {
                $('.other-person-info').slideDown('fast')
                // $('.other-person-info').css({'display': 'block'});
                $('input[name=reservation_type]').val('other')
            } else {
                $('.other-person-info').slideUp('fast')
                // $('.other-person-info').css({'display': 'none'});
                $('input[name=reservation_type]').val('self')
            }
        })

        $('.setfor-private-toggler').change(function () {
            if ($('.setfor-private-toggler:checked').length) {
                $('.other-person-info').slideDown('fast')
                $('input[name=reservation_type]').val('other')
            } else {
                $('.other-person-info').slideUp('fast')

                $('input[name=reservation_type]').val('self')
            }
        })
    </script>
@endsection
