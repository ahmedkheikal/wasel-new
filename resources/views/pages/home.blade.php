@extends('layouts.skeleton')

@section('styles')
    <link rel="stylesheet" href="{!! asset('css/home.css?v=1') !!}">
    <style media="screen">
        footer > div , html, body{
            background: #fba829;
            color: white;
        }
    </style>
@endsection

@section('content')
    <style media="screen">
        @media (min-width: 1100px) {
            .navbar-light .navbar-nav .nav-link {
                text-align: right;
                color: white;
            }
            .account-info-drop * {
                color: #666 !important;
            }
        }
        footer > div , footer > div i , footer > div * , nav, nav .navbar-toggler span , .revealed-invite-link * , .bg-light{
            background: #fba829 !important;
            color: white !important;
            border: none !important;
            box-shadow: none !important
        }
        .navbar-toggler-icon {
            background: white;
            color: white;
        }
    </style>
    @include('components.header')
    <style media="screen">
        .navbar-light .navbar-toggler-icon {
            background-image: url('{!! asset('images/menu-icon.png') !!}') !important
        }
    </style>
    <div class="main-showcase">
        {{-- <img src="{!! asset('images/cover.png') !!}" alt=""> --}}
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-sm-8">
                    <p class="main-title" style="height: 100px;">
                        <img src="{!! asset('images/main-title.png') !!}" class="main-logo" alt="">
                    </p>
                    <p class="description">
                        أسهل طريقة للتنقل بين محافظات مصر
                    </p>
                </div>

                {{-- Start Service --}}
                <div class="col-md-12 mt-5" dir="rtl">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card mb-3 card-text-color">
                                <a href="{{ url('/private-trip') }}">
                                  <div class="card-body clearfix">
                                    <h5 class="c-text">احجز رحلة خاصة</h5>
                                    <p class="mb-0 c-text">
                                        اطلب رحلتك الخاصة حسب الموعد والعنوان اللذان تفضل  وسنقوم بتوفير سيارة خاصة لك بأسرع وقت
                                    </p>
                                    <i class="fa fa-dollar-sign c-icon"></i>
                                  </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card mb-3 card-text-color">
                                <a href="{{ url('incity') }}">
                                  <div class="card-body clearfix">
                                    <h5 class="c-text">احجز رحلة داخل المحافظة</h5>
                                    <p class="mb-0 c-text">
                                         اطلب عربية مكيفة من مدينة المنيا الى باقي مراكز المحافظة بأسعار ثابتة
                                    </p>
                                    <i class="fa fa-city c-icon"></i>
                                  </div>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row justify-content-center">
                        <!-- <div class="col-md-6">
                            <div class="card mb-3 card-text-color">
                                <a href="{{ url('/express') }}">
                                  <div class="card-body clearfix">
                                    <h5 class="c-text">اطلب خدمة الشحن</h5>
                                    <p class="mb-0 c-text">
                                        اطلب خدمة الشحن من واصل وارسل واستلم حاجتك من اي مكان الى القاهرة أو العكس
                                    </p>
                                    <i class="fa fa-home c-icon"></i>
                                  </div>
                                </a>
                            </div>
                        </div> -->
                        <!-- <div class="col-md-6">
                            <div class="card mb -3 card-text-color">
                                <a href="{{ url('/economy-trip') }}">
                                  <div class="card-body clearfix">
                                    <h5 class="c-text">احجز رحلة اقتصادية</h5>
                                    <p class="mb-0 c-text">
                                        احجز كرسي في احد الرحلات الاقتصادية واستمتع برحلة امنة ومريحة بأقل تكلفة
                                    </p>
                                    <i class="fas fa-money-bill-wave c-icon"></i>
                                  </div>
                                </a>
                            </div>
                        </div> -->
                    </div>
                </div>
                {{-- End Service --}}
            </div>
        </div>
    </div>
    @include('components.footer')
@endsection

@section('scripts')
    <script type="text/javascript">
        $('footer').removeClass('mt-5')
        $('.logo').attr('src', '/images/logo.png')

        AOS.init({
            easing: 'ease-out-back',
            duration: 1000
        });

        $('.card-body').hover(function() {
            $(this).children('i').css('background', '#fba829');
            $(this).children('i').css('color', '#fff');
        }, function() {
          $(this).children('i').css('background', '#fff');
          $(this).children('i').css('color', '#fba829');
        });
    </script>
@endsection
