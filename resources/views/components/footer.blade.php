<footer class="mt-5">
    <div class="card pt-4">
        <div class="container">

            <?php  $curr = new DateTime(); ?>
            <div class="row contact-us">

                <div class="offset-sm-3 col-sm-6 text-center">

                    <ul class="contact-info text-center">
                        <li>
                            <a href="mailto:support@waselegypt.com" target="_blank">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/waselegypt/" target="_blank">
                                <i class="fab fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/waselegypt/" target="_blank">
                                <i class="fab fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/wasel.egy/" target="_blank">
                                <i class="fab fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/waselegypt1" target="_blank">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>

                </div>


                <div class="offset-sm-3 col-sm-6 text-center">
                    <ul class="contact-info pull-left call-telephone text-center">
                        <li>
                            <a href="tel:01000633110" target="_blank" >
                                <i class="fa fa-phone" aria-hidden="true" style="transform: rotateX(180deg); transform: rotateY(180deg)"></i>
                                <span style="font-size: 18px;"> 01000 633 110</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="links-copy-rights text-center">
                <p class="text-center" style="width: 100%;">&copy; <?php echo $curr->format('Y') ?> WASEL, llc.</p>
            </div>
        </div>
    </div>

</footer>
