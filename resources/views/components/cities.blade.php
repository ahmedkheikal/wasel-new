{{-- <option {{ $selected == '' ? 'selected' : '' }} disabled value="">المحافظة</option> --}}
{{-- value="" --}}
<option selected disabled>المحافظة</option>
<option {{ $selected == 'Alexandria' ? 'selected' : '' }}  value="Alexandria">@lang("db.Alexandria")</option>
<option {{ $selected == 'Cairo' ? 'selected' : '' }}  value="Cairo">@lang("db.Cairo")</option>
<option {{ $selected == 'Giza' ? 'selected' : '' }}  value="Giza">@lang("db.Giza")</option>
<option {{ $selected == 'Faiyum' ? 'selected' : '' }}  value="Faiyum">@lang("db.Faiyum")</option>
<option {{ $selected == 'Hurghada' ? 'selected' : '' }}  value="Hurghada">@lang("db.Hurghada")</option>
<option {{ $selected == 'Bani Suef' ? 'selected' : '' }} value="Bani Suef">@lang("db.bani_suef")</option>
<option {{ $selected == 'Minya' ? 'selected' : '' }} value="Minya">@lang("db.Minya")</option>
<option {{ $selected == 'Assiut' ? 'selected' : '' }} value="Assiut">@lang("db.Asyut")</option>
<option {{ $selected == 'Sohag' ? 'selected' : '' }} value="Sohag">@lang("db.Sohag")</option>
<option {{ $selected == 'Qena' ? 'selected' : '' }} value="Qena">@lang("db.Qena")</option>
<option {{ $selected == 'New_Valley' ? 'selected' : '' }} value="New_Valley">@lang("db.New_Valley")</option>
<option {{ $selected == 'Matruh' ? 'selected' : '' }} value="Matruh">@lang("db.Matruh")</option>
<option {{ $selected == 'Red_Sea' ? 'selected' : '' }} value="Red_Sea">@lang("db.Red_Sea")</option>
<option {{ $selected == 'South_Sinai' ? 'selected' : '' }} value="South_Sinai">@lang("db.South_Sinai")</option>
<option {{ $selected == 'North_Sinai' ? 'selected' : '' }} value="North_Sinai">@lang("db.North_Sinai")</option>
<option {{ $selected == 'Suez' ? 'selected' : '' }} value="Suez">@lang("db.Suez")</option>
<option {{ $selected == 'Beheira' ? 'selected' : '' }} value="Beheira">@lang("db.Beheira")</option>
<option {{ $selected == 'Helwan' ? 'selected' : '' }} value="Helwan">@lang("db.Helwan")</option>
<option {{ $selected == 'Sharqia' ? 'selected' : '' }} value="Sharqia">@lang("db.Sharqia")</option>
<option {{ $selected == 'Dakahlia' ? 'selected' : '' }} value="Dakahlia">@lang("db.Dakahlia")</option>
<option {{ $selected == 'Kafr_el_Sheikh' ? 'selected' : '' }} value="Kafr_el_Sheikh">@lang("db.Kafr_el_Sheikh")</option>
<option {{ $selected == 'Monufia' ? 'selected' : '' }} value="Monufia">@lang("db.Monufia")</option>
<option {{ $selected == 'Gharbia' ? 'selected' : '' }} value="Gharbia">@lang("db.Gharbia")</option>
<option {{ $selected == 'Ismailia' ? 'selected' : '' }} value="Ismailia">@lang("db.Ismailia")</option>
<option {{ $selected == 'Qalyubia' ? 'selected' : '' }} value="Qalyubia">@lang("db.Qalyubia")</option>
<option {{ $selected == 'Luxor' ? 'selected' : '' }} value="Luxor">@lang("db.Luxor")</option>
<option {{ $selected == 'Aswan' ? 'selected' : '' }} value="Aswan">@lang("db.Aswan")</option>
<option {{ $selected == 'Damietta' ? 'selected' : '' }} value="Damietta">@lang("db.Damietta")</option>
<option {{ $selected == 'Port_Said' ? 'selected' : '' }} value="Port_Said">@lang("db.Port_Said")</option>
