<div style="text-align:center; width: 100%;">
    <img class="profile-picture" style="width: 100px; height: 100px" src="{{ $user->profile_picture }}" alt="{{ $user->username }}">
    <h3>{{ $user->username }}</h3>

    @if (isset($phone))
        @if ($phone == true)
            <small>
                @lang('language.phone'):
                <a href="tel:{{ $user->phone }}">
                    {{ $user->phone }}
                </a>
            </small>
        @endif
    @endif
</div>
