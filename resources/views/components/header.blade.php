{{-- <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"> --}}
<nav class="navbar navbar-expand-lg bg-light navbar-light" style="z-index: 100">
    <a class="navbar-brand" href="/">
        <img class="logo" style="height: 20px" src="{!! asset('images/logo-orange.png') !!}" alt="">
        <img src="{!! asset('images/logo-orange.png') !!}" style="display: none" alt="">
    </a>
    {{-- <li class="revealed-invite-link">
        <a class="nav-link" href="/invite-friends">@lang('titles.getOffer')</a>
    </li> --}}
    {{-- <button onclick="openNav()" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="color: white'">
        <span class="navbar-toggler-icon"></span>
    </button> --}}
    <button class="navbar-toggler" onclick="openNav()" class="" type="button">
        <span class="navbar-toggler-icon"></span>
    </button>

    {{-- mobile view --}}
    <div id="mySidenav" class="sidenav">

        @auth
            @php
            $balance = Auth::user()->affiliate_balance + Auth::user()->own_balance
            @endphp
            <ul dir="rtl" class="navbar-nav ml-auto mobile-header" style="padding-right: 5px; background: white !important; color: #222 !important">
                <span class="closebtn" onclick="closeNav()">
                    <i class="fa fa-chevron-right"></i>
                </span>
                <div class="row">
                    <div class="col-5">
                        <img class="profile-picture header-pic" src="{{ Auth::user()->profile_picture }}" alt="">
                    </div>
                    <div class="col-7">
                        <h5 class="mt-2">{{ Auth::user()->username }}</h5>
                        <p style="color: #999">
                            {{ Auth::user()->rating }}
                            <i class="fa fa-star"></i>،
                            <span style="color: #222">
                                @lang("language.". Auth::user()->account_type)
                            </span>
                        </p>
                    </div>
                </div>
                <div class="float-left">
                </div>
                @if (Auth::user()->account_type == 'partner')
                    <li class="nav-item">
                        <a class="nav-link" href="/my-trips">@lang('titles.economyTrips')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/my-incity-trips">@lang('titles.myInCityTrips')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/private-trips">@lang('titles.privateTrips')</a>
                    </li>
                @else
                    @if (Auth::user()->account_type == 'driver')
                        <style media="screen">
                            a[href="/invite-friends"] {
                                display: none;
                            }
                        </style>
                        <li class="nav-item">
                            <a class="nav-link" href="/add-economy-trip">@lang('titles.travellingAlone')</a>
                        </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="/my-trips">@lang('titles.myTrips')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/my-incity-trips">@lang('titles.myInCityTrips')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/private-trips">@lang('titles.myPrivateTrips')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/shipping-orders">@lang('titles.myShippingOrders')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/invite-friends">@lang('language.affiliate.getOffer')</a>
                    </li>
                @endif
                <li class="nav-item wallet-nav">
                    <a class="nav-link" href="/wallet">@lang('titles.wallet'): <small class="balance-nav"> {{ $balance }} @lang('language.egp')</small></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/account-settings">@lang('titles.accountSettings')</a>
                </li>
                <div class="dropdown-divider"></div>
                <li class="nav-item">
                    <a class="nav-link logout-btn" href="{!! route('logout') !!}">@lang('titles.logOut')</a>
                </li>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        @else
            <span class="closebtn" onclick="closeNav()">
                <i class="fa fa-chevron-right"></i>
            </span>
            <ul class="navbar-nav ml-auto mobile-header">
                <li class="nav-item">
                    <a style="margin-top: 5px;" class="nav-link" href="/invite-friends">
                        @lang('titles.getOffer')
                    </a>
                </li>
                <li class="nav-item">
                    <a style="margin-top: 5px;" class="nav-link" href="{{ route('login') }}">
                        @lang('language.login')
                        {{-- <button type="button" class="header-login-btn btn btn-info">@lang('language.login')</button> --}}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">
                        <button type="button" class="mobile-header-register btn btn-primary">@lang('language.register')</button>
                    </a>
                </li>
                <li class="nav-item">
                    <a style="font-weight: bold" class="nav-link" href="/become-a-captain">
                        @lang('language.becomeACaptain')
                        {{-- <button type="button" class="header-login-btn btn btn-info">@lang('language.becomeACaptain')</button> --}}
                    </a>
                </li>
            </ul>
        @endauth
    </div>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        {{-- <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
                <a class="nav-link" href="#">@lang('titles.home') <span class="sr-only">(current)</span></a>
            </li>

        </ul> --}}


        {{-- desktop view --}}
        <div class="form-inline my-2 my-lg-0 ml-auto desktop-header">
            {{-- <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
            @auth
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item dropdown" style="list-style: none;">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img style="height: 25px; width: 25px; object-fit: cover; border-radius: 50%;" src="{{ Auth::user()->profile_picture }}" alt="">
                            &nbsp;
                            {{ Auth::user()->username }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right text-right account-info-drop" aria-labelledby="navbarDropdown">
                            @if (Auth::user()->account_type == 'partner')
                                <a class="dropdown-item" href="/my-trips">@lang('titles.economyTrips')</a>
                                <a class="dropdown-item" href="/my-incity-trips">@lang('titles.myInCityTrips')</a>
                                <a class="dropdown-item" href="/private-trips">@lang('titles.privateTrips')</a>
                            @else
                                @if (Auth::user()->account_type == 'driver')
                                    <a class="dropdown-item" href="/add-economy-trip">@lang('titles.travellingAlone')</a>
                                @endif
                                <a class="dropdown-item" href="/my-trips">@lang('titles.myTrips')</a>
                                <a class="dropdown-item" href="/my-incity-trips">@lang('titles.myInCityTrips')</a>
                                <a class="dropdown-item" href="/private-trips">@lang('titles.myPrivateTrips')</a>
                                <a class="dropdown-item" href="/shipping-orders">@lang('titles.myShippingOrders')</a>
                                <a class="dropdown-item" href="/invite-friends">@lang('language.affiliate.getOffer')</a>
                            @endif
                            <a class="dropdown-item wallet-nav" href="/wallet">@lang('titles.wallet'): <small class="balance-nav"> {{ $balance }} @lang('language.egp')</small></a>
                            <a class="dropdown-item" href="/account-settings">@lang('titles.accountSettings')</a>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item logout-btn" href="{!! route('logout') !!}">@lang('titles.logOut')</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            @else

                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/become-a-captain" style="margin-top: 5px; font-weight: bold">
                            @lang('language.becomeACaptain')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/invite-friends" style="margin-top: 5px">
                            @lang('titles.getOffer')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">
                            <button type="button" class="header-register btn btn-success">@lang('language.register')</button>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}" style="margin-top: 5px">
                            @lang('language.login')
                        </a>
                    </li>
                </ul>
            @endauth
        </div>
    </div>
</nav>
