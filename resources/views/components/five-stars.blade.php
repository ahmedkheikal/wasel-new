{{-- requires font awesome --}}
@php
    $randClass = 'starClass' . rand(99, 999)
@endphp

<style media="screen">
.five-stars.{{ $randClass }} {
    padding-left: 15%
}
.five-stars.{{ $randClass }} > span {
    position: relative;
    display: block;
    float: left;
    font-size: 25px;
    transition: 200ms linear all;
    vertical-align: center;
    overflow: hidden;
}
.five-stars.{{ $randClass }} > span:hover {
    font-size: 30px;
    transition: 200ms linear all;
}

.five-stars.{{ $randClass }} > span > input {
    position: absolute !important;
    top: -10px !important;
    left: -10px !important;
    width: 50px !important;
    height: 50px !important;
    z-index: 20 !important;
    opacity: 0;
}
.five-stars.{{ $randClass }} > span.active, .five-stars.{{ $randClass }} > span {
    color: lightgrey;
}
.five-stars.{{ $randClass }} > span.active ~ span, .five-stars.{{ $randClass }} > span.active {
    color: gold;
}

</style>
@if ($inForm == true)
    <div class="five-stars {{ $randClass }} clearfix">
        <span class="star1">
            <input type="radio" name="{{ $name }}" value="5" class="star-radio-{{ $name }}">
            <i class="fa fa-star"></i>
        </span>
        <span class="star2">
            <input type="radio" name="{{ $name }}" value="4" class="star-radio-{{ $name }}">
            <i class="fa fa-star"></i>
        </span>
        <span class="star3">
            <input type="radio" name="{{ $name }}" value="3" class="star-radio-{{ $name }}">
            <i class="fa fa-star"></i>
        </span>
        <span class="star4">
            <input type="radio" name="{{ $name }}" value="2" class="star-radio-{{ $name }}">
            <i class="fa fa-star"></i>
        </span>
        <span class="star5">
            <input type="radio" name="{{ $name }}" value="1" class="star-radio-{{ $name }}">
            <i class="fa fa-star"></i>
        </span>
    </div>
    <script type="text/javascript">
    $('.{{ $randClass }} .star-radio-{{ $name }}').change(function () {
        $('.{{ $randClass }} > span').removeClass('active')
        $(this).parent().addClass('active');
    })
    </script>
@else
    <div class="five-stars {{ $randClass }} clearfix">
        <span class="star1 {{ $rating == 5 ? 'active' : '' }}">
            <i class="fa fa-star"></i>
        </span>
        <span class="star2 {{ $rating == 4 ? 'active' : '' }}">
            <i class="fa fa-star"></i>
        </span>
        <span class="star3 {{ $rating == 3 ? 'active' : '' }}">
            <i class="fa fa-star"></i>
        </span>
        <span class="star4 {{ $rating == 2 ? 'active' : '' }}">
            <i class="fa fa-star"></i>
        </span>
        <span class="star5 {{ $rating == 1 ? 'active' : '' }}">
            <i class="fa fa-star"></i>
        </span>
    </div>
@endif
