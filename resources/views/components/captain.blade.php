@if ($captain)    
    <div style="text-align:center; width: 100%;">
        <img class="profile-picture" src="{{ $captain->profile_picture }}" alt="{{ $captain->username }}">
        <h3>{{ $captain->username }}</h3>
        @if (isset($phoneVisible) && $phoneVisible == true)
            <small>
                @lang('language.phone'):
                <a href="tel:{{ $captain->phone }}">
                    {{ $captain->phone }}
                </a>
            </small> <br>
        @endif
        <small>@lang('language.carModel'): {{ $captain->car_model }}</small> <br>
        <small>@lang('language.carColor'): {{ $captain->car_color }}</small> <br>
        <small>@lang('language.plateNo'): {{ $captain->chassis_no }}</small> <br>
    </div>
@endif
