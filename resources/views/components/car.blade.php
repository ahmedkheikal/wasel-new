<style media="screen">
    .car-model-inner, .car-model {
        width: 100%;
    }
    .car-model {
        position: relative;
    }
    .frontseat, .backseat-right, .backseat-left {
        width: 20%;
        position: absolute;
    }
    .frontseat {
        top: 35%;
        left: 55%;
    }
    .backseat-right {
        top: 60%;
        left: 55%;
    }
    .backseat-left {
        top: 60%;
        left: 25%;
    }
    .frontseat > .seat-img, .backseat-right > .seat-img, .backseat-left > .seat-img {
        width: 100%;
    }
    .seats {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
    }
    .seat {
        position: absolute;
    }
    .tick-en, .tick-dis {
        position: absolute;
        width: 50%;
        top: -5%;
        left: -5%;
    }
    .checked .tick-en {
        z-index: 20
    }
</style>
<div class="" dir="ltr">
    <div class="car-model clearfix">
        <img src="{!! asset('images/car.png') !!}" class="car-model-inner" alt="car model">
        <div class="input-group seats">

            <div class="frontseat seat {{ $seat_1 == '1' ? 'checked' : '' }}">
                <img class="seat-img" src="{!! asset('images/seat.png') !!}" alt="">
                <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
            </div>

            <div class="backseat-right seat {{ $seat_2 == '1' ? 'checked' : '' }}">
                <img class="seat-img" src="{!! asset('images/seat.png') !!}" alt="">
                <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
            </div>

            <div class="backseat-left seat {{ $seat_3 == '1' ? 'checked' : '' }}">
                <img class="seat-img" src="{!! asset('images/seat.png') !!}" alt="">
                <img class="tick-en" src="{!! asset('images/tick-en.png') !!}" alt="">
                <img class="tick-dis" src="{!! asset('images/tick-dis.png') !!}" alt="">
            </div>

        </div>
    </div>
</div>
