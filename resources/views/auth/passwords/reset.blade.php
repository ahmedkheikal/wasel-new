@extends('layouts.page')

@section('content')
    @include('components.header')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="text-align: right">@lang('language.resetPassword')</div>

                    <div class="card-body" dir="rtl">
                        {{-- <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}"> --}}
                        <form method="POST" action="/reset-password" aria-label="@lang('language.resetPassword')">
                            {{-- @csrf --}}

                            {{-- <input type="hidden" name="token" value="{{ $token }}"> --}}

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('language.newPassword')</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('language.confirmPassword')</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-4 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('language.changePassword')
                                    </button>
                                </div>
                            </div>
                            <div class="alert alert-success confirmation-message" style="display: none;">
                                @lang('language.passwordUpdatedSuccessfully')
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('form[action="/reset-password"]').submit(function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: '/password/new',
                processData: false,
                cache: false,
                contentType: false,
                data: new FormData(this),
                success: function (data) {
                    if (data.code == '200') {
                        $('.confirmation-message').slideDown('fast');
                        location.href = '/login';
                    }
                    if (data.code == '422') {
                        $('form').append(`
                            <div class="alert alert-danger text-right">
                                <ul class="errors-list"></ul>
                            </div>
                        `);
                        for (var error in data.response) {
                            if (data.response.hasOwnProperty(error)) {
                                for (var i = 0; i < data.response[error].length; i++) {
                                    $('.errors-list').append(`
                                        <li>${data.response[error][i]}</li>
                                    `);
                                }
                            }
                        }
                    }
                }
            })
        })
    </script>
@endsection
