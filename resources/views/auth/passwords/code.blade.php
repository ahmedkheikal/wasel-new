@extends('layouts.page')

@section('content')
    @include('components.header')
    <div class="container mt-5">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="text-align: right">@lang('language.resetPassword')</div>

                    <div class="card-body" dir="rtl">
                        {{-- <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}"> --}}
                        <form method="POST" action="/send-reset-password-code" aria-label="@lang('language.resetPassword')">
                            {{-- @csrf --}}

                            <div class="alert alert-success text-center">
                                @lang('language.resetCodeSent')
                            </div>

                            <div class="form-group row">
                                <label for="resetCode" class="col-md-4 col-form-label text-md-right">@lang('language.resetCode')</label>

                                <div class="col-md-6">
                                    <input id="resetCode" type="text" class="form-control {{ $errors->has('resetCode') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('resetCode') }}" required autofocus>

                                    @if ($errors->has('resetCode'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('resetCode') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-4 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('language.verifyCode')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('form[action="/send-reset-password-code"]').submit(function (e) {
            e.preventDefault();
            $('#loader').fadeIn()
            $.ajax({
                method: 'POST',
                url: '/password/code',
                data: {
                    reset_code: $('#resetCode').val()
                },
                success: function (data) {
                    if (data.code == '200') {
                        location.href = '/password/new';
                    } else {
                        $().after(`
                            <div class="alert alert-danger">
                                @lang('language.resetCodeIncorrect')
                            </div>
                        `);
                    }
                    $('#loader').fadeOut()
                }
            })
        })
    </script>
@endsection
