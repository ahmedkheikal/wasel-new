@extends('layouts.page')

@section('content')
    @include('components.header')
    <div class="container mt-5">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="text-align: right;">@lang('language.resetPassword')</div>

                    <div class="card-body">

                        <form class="resetPassword-form" method="POST" action="" aria-label="@lang('language.resetPassword')">
                            @csrf

                            <div class="form-group row" dir="rtl">
                                <label for="emailOrPhone" class="col-md-4 col-form-label text-md-right">@lang('language.emailOrPhone')</label>

                                <div class="col-md-6">
                                    <input id="emailOrPhone" type="text" class="form-control{{ $errors->has('emailOrPhone') ? ' is-invalid' : '' }}" name="emailOrPhone" value="{{ old('emailOrPhone') }}" required>

                                    @if ($errors->has('emailOrPhone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('emailOrPhone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('language.sendPasswordResetCode')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('components.footer')
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('form.resetPassword-form').submit(function (e) {
            e.preventDefault();
            $('.alert').remove();
            $('#loader').fadeIn()
            $('button[type="submit"]').attr('disabled', 'disabled')
            $.ajax({
                url: '/password/send-reset-code',
                method: 'POST',
                data: {
                    emailOrPhone: $('input[name=emailOrPhone]').val()
                },
                success: function (data) {
                    if (data.code == '401')
                    $('form').prepend(`
                        <div class="alert alert-danger text-center">
                            @lang('language.noAccount')
                        </div>
                    `);
                    if (data.code == '200')
                    location.href = '/password/code';

                    if (data.code == '422') {
                        for (var field in data.response) {
                            if (data.response.hasOwnProperty(field)) {
                                $('input[name='+ field +']').after(data.response[field]);
                            }
                        }
                    }
                    $('#loader').fadeOut()
                    $('button[type="submit"]').removeAttr('disabled')
                }
            })
        })
    </script>
@endsection
