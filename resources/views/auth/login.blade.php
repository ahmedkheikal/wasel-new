@extends('layouts.page')

@section('styles')
    <style media="screen">
    .justify-content-center * {
        text-align: right;
    }
    </style>
@endsection

@section('content')
    @include('components.header')
    <div class="container" style="margin-top: 80px">
        <div id="loader">
            <img src="{!! asset('images/loader.svg') !!}" alt="">
        </div>
        <div class="row justify-content-center" dir="rtl">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-right">@lang('language.login')</div>
                    <div class="card-body">
                        <form class="login-form" method="POST" action="" aria-label="{{ __('Login') }}">
                            {{-- <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}"> --}}
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('language.emailOrPhone')</label>

                                <div class="col-md-6">
                                    <input id="emailphone" type="text" class="form-control
                                    {{ $errors->has('emailphone') ? ' is-invalid' : '' }}" name="emailphone" value="{{ old('emailphone') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('language.password')</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check text-right">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            @lang('language.rememberMe')
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4 submit-container" style="text-align: right; ">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('language.login')
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        @lang('language.forgotPassword')
                                    </a>

                                </div>
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 offset-md-4 mt-4">
                                    <a href="{!! route('register') !!}">
                                        <button class="btn" type="button" style="width: 100%; text-align: center; background: transparent; color: #fba228; font-weight: bold;     border: 2px solid #fba228; " >
                                            @lang('language.register')
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')

@endsection

@section('page-scripts')
    <script type="text/javascript">
        $('.login-form').submit(function (e) {
            e.preventDefault();
            $('#loader').fadeIn();
            $('.alert').remove();
            $.ajax({
                method: 'POST',
                url: '/ajax-login',
                data: {
                    emailphone: $('#emailphone').val(),
                    password: $('#password').val(),
                    remember: $('#remember:checked').length
                },
                success: function (data) {
                    console.log(data);
                    if (data.code == '200') {
                        if (data.response.ajax == false) {
                            window.location.href = data.response.url;
                        } else if (data.response.ajax == true) {
                            $.ajax({
                                method: data.response.method,
                                url: data.response.url,
                                data: data.response.params,
                                success: function (data) {
                                    console.log(data);
                                    if (data.code == '403')
                                    window.location.href = '/phone/verify';
                                    window.location.href = data.response.url;
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.error(textStatus);
                                    console.error(errorThrown);
                                    alert(errorThrown);
                                    $('#loader').fadeOut();
                                }
                            })
                        } else {
                            window.location.href = '/';
                        }
                    } else {
                        if (typeof data.response != 'string') {
                            for (var error in data.response) {
                                if (data.response.hasOwnProperty(error)) {
                                    alert(data.response[error]);
                                }
                            }
                        } else {
                            $('.submit-container').after(`
                                <div class="alert alert-danger mt-3 text-center" style="width: 100%">
                                @lang('validation.loginInfoNotCorrect')
                                </div>
                            `)
                        }
                        $('#loader').fadeOut();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus);
                    console.error(errorThrown);
                    alert(errorThrown);
                    $('#loader').fadeOut();
                }
            })
        })

    </script>
@endsection
