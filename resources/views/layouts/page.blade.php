@extends('layouts.skeleton')

@section('styles')
    <style media="screen">
    .bg-light {
        background: white !important;
    }
    </style>
    @yield('page-styles')
@endsection

@section('scripts')
    <script type="text/javascript">
    $('.navbar-expand-lg').removeClass('navbar-dark');
    $('.navbar-expand-lg').removeClass('bg-dark');

    $('.navbar-expand-lg').addClass('bg-light');
    $('.navbar-expand-lg').addClass('navbar-light');

    $('.navbar-expand-lg img.logo').attr('src', '{!! asset('images/logo-orange.png') !!}');
    </script>
    @yield('page-scripts')
@endsection
