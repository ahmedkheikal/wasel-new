<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="manifest" href="{!! asset('manifest.json') !!}">
        <meta name="theme-color" content="#fba92b">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="أسهل طريقة للتنقل بين محافظات مصر">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- contains bootstrap and some general styles  --}}
        <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
        <link rel="icon" href="{!! asset('images/favicon.jpg') !!}" type="image/gif" sizes="16x16">
        <link rel="stylesheet" href="{{ asset('css/app.css?v=1') }}">
        <link rel="stylesheet" href="{!! asset('css/aos.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/side-nav.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/standalone.css') !!}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        {{-- @if (Config::get('app.locale') == 'ar')
            <link rel="stylesheet" href="{!! asset('css/bootstrap-rtl.min.css') !!}">
        @endif --}}
        <?php // TODO: metatags musto go here ?>

        @yield('styles')
        <title>
            @lang('titles.wasel')
            @auth
                | {{ Auth::user()->username }}
            @endauth
        </title>

        @if (env('APP_ENV') == 'godaddyProduction')
            <!-- Hotjar Tracking Code for https://waselegypt.com -->
            <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1109503,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            </script>
        @endif
    </head>
    <body style="height: 100vh;">
        {{-- facebook app --}}
        <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '207282569826125',
                xfbml      : true,
                version    : 'v3.2'
            });
            FB.AppEvents.logPageView();
        };

        (
            function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk')
        );
        </script>


        @yield('content')

        {{-- contains bootstrap js and jquery --}}
        <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/aos.js') !!}"></script>
        <script type="text/javascript">

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('.logout-btn').click(function (e) {
                e.preventDefault();
                $('#logout-form').submit();
            })



        </script>
        {{-- start google analytics --}}
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114009601-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-114009601-1');
        </script>
        {{-- end google analytics --}}

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '873885666123049');
            fbq('track', 'PageView');
            </script>
            <noscript>
                <img height="1" width="1"
                src="https://www.facebook.com/tr?id=873885666123049&ev=PageView
                &noscript=1"/>
            </noscript>
            <!-- End Facebook Pixel Code -->

        @yield('scripts')
    </body>
    <script>
    /* Set the width of the side navigation to 250px */
    function openNav() {
        console.log('asdf');
        document.getElementById("mySidenav").style.right = "0";
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
        document.getElementById("mySidenav").style.right = "-260px";
    }

    $( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
        if (thrownError == 'Too Many Requests') {
            alert('Too Many Requests, Reloading page')
            setTimeout(function () {
                location.reload();

            }, 10000)
        }
    });


      if ('serviceWorker' in navigator) {
        console.log("Will the service worker register?");
        navigator.serviceWorker.register('{!! asset('service-worker.js') !!}', {
            scope: '/'
        }) // need to write sw.js code
          .then(function(reg){
            console.log("Yes, it did.");
            console.log(reg);
          }).catch(function(err) {
            console.log("No it didn't. This happened: ", err)
          });
      }

      if (window.matchMedia('(display-mode: standalone)').matches) {
          $('.navbar').addClass('fixed-top');
      }
    </script>
</html>
