<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title></title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo">
    <style type="text/css">
        @media screen {
            * {
                font-family: "Cairo"
            }
        }
    </style>
</head>
<body style="margin:0; padding:0; background-color: #F2F2F2;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2">
        <tr>
            <td align="center" valign="top">
                <div style="border-radius: 5px; max-width: 500px; background: #fba829; margin: 0; box-shadow: 0 3px 1px rgba(0,0,0,0.13);">
                    <img src="http://betalaravel.waselegypt.com/images/logo.png" style="min-height: 40px; max-height: 40px; text-align: center; margin-top: 20px; object-fit: cover;">
                    <div style="border-radius: 5px; margin: 2%; padding: 0; width: 90%; background: transparent; padding-top: 20px">

                        {{-- start email  --}}


                        <div style="font-family: sans-serif; color: white; max-width: 500px; width: 100%; margin:auto; padding: 0; box-sizing: border-box;">
                            <div style="margin-top: 20px;margin-bottom: 50px; min-height: 180px">
                                <div style="width: 50%; float: left; ">
                                    <div style="text-align:center; border-right: 4px solid white;">
                                        {{-- <img style="width: 50%; display: block; margin: auto" src="'. $trip->captain->profile_picture .'" alt="'. $trip->captain->username .'"> --}}
                                        <img style="width: 50%; display: block; margin: auto; border-radius: 50%; object-fit: cover;" src="https://waselegypt.com/assets/images/profile.png" alt="'. $trip->captain->username .'">
                                        <h3 style="margin: 5px 0">أحمد علي</h3>
                                        <p style="margin-top: 0">
                                            4.2
                                            <img style="filter: invert(100%);height: 15px;" src="https://www.waselegypt.com/images/star-rating.png" alt="star">
                                        </p>
                                    </div>

                                </div>
                                <div style="width: 50%; float: right; text-align: left; padding-left: 30px; box-sizing: border-box; font-size: 20px">
                                    <p style="margin: 0; ">0102 524 8181</p>
                                    <p style="margin: 0;">Nissan Sunny</p>
                                    <p style="margin: 0;">Black</p>
                                    <p style="margin: 0;">ن ر ا 526 </p>

                                </div>
                                {{-- <div style="width: 50%; float: right ">
                                    <h4 style="margin-top: 0">Details</h4>
                                    <div class="ticket-info">
                                        <p>From: '. $trip->from_city .'</p>
                                        <p>To: '. $trip->to_city. ' </p>
                                        <p>Date: '. (new \DateTime($trip->date))->format('d M Y') .'</p>
                                        <p>Time: '. (new \DateTime($trip->departure_time))->format('h:i A') .'</p>
                                        <p>Seats to reserve: '. $seats_to_reserve_sms .'
                                        </p>
                                        <h3>Ticket total '. $reservation->price .'</h3>
                                    </div>
                                </div> --}}

                            </div>
                            <div style="min-height: 25vh; text-align: right; font-weight: bolder">

                                <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px">
                                            @lang('titles.from'): @lang('db.Cairo')
                                        </h4>
                                    </div>
                                </div>
                                <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px">
                                            @lang('titles.to'): @lang('db.Minya')
                                        </h4>
                                    </div>
                                </div>

                                <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px">
                                            @lang('titles.date'):
                                        </h4>
                                    </div>
                                </div>
                                <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px">
                                            @lang('titles.time'):
                                        </h4>
                                    </div>
                                </div>

                                <div style="padding: 7px 7px; width: 100%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px">
                                            @lang('titles.fromAddress'):
                                        </h4>
                                    </div>
                                </div>
                                <div style="padding: 7px 7px; width: 100%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px">
                                            @lang('titles.toAddress'):
                                        </h4>
                                    </div>
                                </div>

                                <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px">
                                            @lang('titles.reservedSeats'):
                                        </h4>
                                    </div>
                                </div>

                                <div style="padding: 7px 7px; width: 50%; float: right; background: transparent; color: #222; box-sizing: border-box;  height: 54px">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                    </div>
                                </div>

                                <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px; font-size: 9px;">
                                            @lang('language.frontSeat'):
                                        </h4>
                                    </div>
                                </div>
                                <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px; font-size: 9px;">
                                            @lang('language.backSeatLeft'):
                                        </h4>
                                    </div>
                                </div>
                                <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box;">
                                    <div style="background: white; border-radius: 20px; color: #fba829; font-weight: bold;">
                                        <h4 style="margin: 0; padding: 5px 10px; font-size: 9px;">
                                            @lang('language.backSeatRight'):
                                        </h4>
                                    </div>
                                </div>

                                <div style="padding: 2px 2px; width: 33%; float: right; background: transparent; color: #222; box-sizing: border-box; color: white; margin-top: 20px; margin-bottom: -50px">
                                    @lang('titles.price'):
                                    <div dir="rtl" style="background: #38c172; border-radius: 20px; color: white; font-weight: bold; text-align: center;">
                                        <h4 style="margin: 0; padding: 5px 10px; font-size: 15px;">
                                            50 @lang('language.egp')
                                        </h4>
                                    </div>
                                </div>

                            </div>
                        </div>


                        {{-- end email  --}}


                    </div>

                    <small style="font-size: 10px"><br></small>
                    <img src="https://www.waselegypt.com/images/email-background.png" style="width: 100vw; max-width: 500px; ">
                    <footer style="text-align: right; color: white; text-align: center; background: #026dc0; font-weight: bold; margin-bottom: -60px;">
                        <p style="padding-top: 0; margin: 0; padding-top: 5px">
                            <a style="color: white; text-decoration: none;" href="mailto:support@waselegypt.com">
                                support@waselegypt.com
                            </a>
                        </p>
                        <p style="margin-top: 0; padding-bottom: 10px">
                            <a style="color: white; text-decoration: none;"  href="tel:+201000633110">
                                +2 01000 633 110
                            </a>
                        </p>
                    </footer>
                    <small style="font-size: 10px"><br><br></small>

                </div>
            </td>
        </tr>
    </table>

</body>
</html>
